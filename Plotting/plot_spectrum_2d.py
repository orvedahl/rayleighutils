"""
Plot 2D power spectrum of given quantity

Usage:
    plot_spectrum_2d.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: power_spectrum]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --list          List the possible radial indices [default: False]
    --no-mean       Remove the mean [default: False]
    --rad-ind=<i>   Give radial index, or "max", "min", "2nd-small", "2nd-large" [default: 0]
    --quantity=<q>  Power spectrum of <q>, 'velocity', 's', 'Bfield' [default: velocity]
    --Pm=<p>        Magnetic Prandtl number for converting time [default: 1]
    --log           Plot log10(power) instead of power [default: False]
    --log-axes      Plot symlog on x & y axes [default: False]
    --lut=<l>       Use a different lut.py file
    --ml-max=<m>    Specify m_max and l_max for axes as comma separated list
"""

from __future__ import print_function

import env
from ReadRayleigh.diagnostic_reading import ShellSpectra, build_file_list
import Utilities.time_utils as time_utils
from Utilities.look_up_quantity import general_lookup
from Utilities.indices import find_index
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import os

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots,
         list_contents, rad_index, quantity, Pm, log_scale, log_axes, new_lut, no_mean,
         ml_max, alt=False, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    # build file list
    data_dir = os.path.join(data_dir, "Shell_Spectra")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # read first file to get meta data
    a = ShellSpectra(files[0], path='')
    lmax = a.lmax; mmax = a.mmax

    if (list_contents):
        print("\nPossible radial indices: {}".format(a.inds))
        print("\tcorresponding radii: {}".format(a.radius))
        print("\nPossible Quantities:")
        print("\tquantity code\tname")
        print("\t-------------------------")
        for i in range(len(a.lut)):
            if (a.lut[i] != 5000):
                print("\t{}\t\t{}".format(i, general_lookup(index=i, alt=alt)))
        print()
        return

    if (rad_index in ["max", "min", "2nd-small", "2nd-large"]):
        rad_index = find_index(a.radius, index=rad_index)
    else:
        rad_index = int(rad_index)

    # build data cube
    print("\n...doing average...")
    data, avgtimes = time_utils.TimeAvg(files, ShellSpectra, axis=-1, data_attr="vals")

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(avgtimes["tstart"]*Pm))
    print("Last  time: {} sec".format(avgtimes["tend"]*Pm))
    print("\nAveraged over {} sec".format(avgtimes["dt"]*Pm))
    print("Number of records {}".format(avgtimes["nrec"]))

    print("\nChosen radial slice:")
    print("\tplotting rad_index = {}".format(a.inds[rad_index]))
    print("\tcorresponds to radius = {}".format(a.radius[rad_index]))
    data = data[:,:,rad_index,:] # size [l,m,q]

    nl, nm, nq = np.shape(data)
    power = np.zeros((nl,nm), dtype=np.float64)

    if (quantity.lower() == 'velocity'):
        print("...computing velocity spectra")
        qr = general_lookup(q='vr', alt=alt)
        qt = general_lookup(q='vt', alt=alt)
        qp = general_lookup(q='vp', alt=alt)

        qr = a.lut[qr]
        qt = a.lut[qt]
        qp = a.lut[qp]

        power[:,0]   = np.abs(data[:,0,qr])    # m=0
        power[:,0]  += np.abs(data[:,0,qt])
        power[:,0]  += np.abs(data[:,0,qp])
        power[:,1:]  = 2*np.abs(data[:,1:,qr]) # m>0
        power[:,1:] += 2*np.abs(data[:,1:,qt])
        power[:,1:] += 2*np.abs(data[:,1:,qp])

    elif (quantity.lower() == 'bfield'):
        print("...computing B-field spectra")
        qr = general_lookup(q='br', alt=alt)
        qt = general_lookup(q='bt', alt=alt)
        qp = general_lookup(q='bp', alt=alt)

        qr = a.lut[qr]
        qt = a.lut[qt]
        qp = a.lut[qp]

        power[:,0]   = np.abs(data[:,0,qr])    # m=0
        power[:,0]  += np.abs(data[:,0,qt])
        power[:,0]  += np.abs(data[:,0,qp])
        power[:,1:]  = 2*np.abs(data[:,1:,qr]) # m>0
        power[:,1:] += 2*np.abs(data[:,1:,qt])
        power[:,1:] += 2*np.abs(data[:,1:,qp])

    else:
        print("...computing {} spectra".format(quantity))
        qcode = general_lookup(q=quantity, alt=alt)
        qcode = a.lut[qcode]

        power[:,0]  = np.abs(data[:,0,qcode])    # m=0
        power[:,1:] = 2*np.abs(data[:,1:,qcode]) # m>0

    if (no_mean):
        power[0,0] = 0. # remove the mean in spectral space, i.e., ell=m=0

    vmin = np.amin(power); vmax = np.amax(power)

    # calculate characteristic \ell values
    lvals = np.arange(0, lmax+1)
    mvals = np.arange(0, mmax+1)

    if (no_plots):
        return

    cmap = 'plasma'
    X, Y = np.meshgrid(lvals, mvals, indexing='ij')

    plt.clf()
    if (log_scale):
        linthresh = 1e-2*np.amax(np.abs(power))
        norm = colors.SymLogNorm(linthresh=linthresh, linscale=0.5, vmin=vmin, vmax=vmax)
    else:
        norm = None
    img = plt.pcolormesh(X, Y, power, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm)
    colorbar = plt.colorbar(img)

    plt.ylabel("$m$ values")
    plt.xlabel("$\ell$ values")

    title = "2D Power of {}".format(quantity)
    plt.title(title)

    if (ml_max is None):
        plt.xlim(0, lmax)
        plt.ylim(0, mmax)
    else:
        m_axis_max = ml_max[0]
        l_axis_max = ml_max[1]
        plt.xlim(0, l_axis_max)
        plt.ylim(0, m_axis_max)

    if (log_axes):
        plt.xscale('symlog')
        plt.yscale('symlog')

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    list_contents = args['--list']
    rad_index = args['--rad-ind']
    quantity = args['--quantity']
    Pm = float(args['--Pm'])
    log_scale = args['--log']
    log_axes = args['--log-axes']
    new_lut = args['--lut']
    no_mean = args['--no-mean']
    ml_max = args['--ml-max']
    if (ml_max is not None):
        ml_max = [float(s) for s in ml_max.split(",")]

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots,
         list_contents, rad_index, quantity, Pm, log_scale, log_axes, new_lut, no_mean, ml_max)

