"""
Plot volume average of induction terms vs time calculated from AZ_Avgs data

Usage:
    plot_energy_KE.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: vol_avg_induction.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --log                Plot on a log scale [default: False]
    --debug              Debug mode [default: False]
    --store              Write results to file for future reading [default: False]
    --read=<f>           Read stored results from file [default: ]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import os
from NumericalToolsLocal.public import LegendreGrid, save_data, read_data, outputname
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import lut_lookup
from Utilities.data_structures import Empty
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, log_scale, debug, store_results, readfile, **kwargs):

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    path = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file,end_file,path=path)

    if (readfile == ''):
        quants = [523, 528,  64, #533, # -<u>.grad(<B>)
                  521, 526, 531, # <B>.grad(<u>)
                  566, 570, 574, # bar{ curl(u' x B') }
                   64,  64,  64] # laplacian(<B>)
        labels = [r'$-(u\cdot\nabla B)_r$',
                  r'$-(u\cdot\nabla B)_\theta$',
                  r'$-(u\cdot\nabla B)_\phi$',
                  r'$(B\cdot\nabla u)_r$',
                  r'$(B\cdot\nabla u)_\theta$',
                  r'$(B\cdot\nabla u)_\phi$',
                 r'$(\nabla \times ( u^\prime \times B^\prime ))_r$',
                 r'$(\nabla \times ( u^\prime \times B^\prime ))_\theta$',
                 r'$(\nabla \times ( u^\prime \times B^\prime ))_\phi$',
                 r'$(\nabla^2 B)_r$', r'$(\nabla^2 B)_\theta$', r'$(\nabla^2 B)_\phi$']

        quant_list = list(quants)
        a = AzAverage(filename=files[0],path='',quantity=quant_list)

        # compile a time series of all the files
        print("\nReading data files...")
        data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                  AzAverage, axis=-1, data_attr="vals", quantity=quant_list)
        alltime = Pm*np.array(time)

        print("\nFirst file: {}".format(files[-last_number_files:exclude_last][0]))
        print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
        print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

        nt = len(alltime)
        nr = a.nr
        nth = a.ntheta
        radius = a.radius
        rad2 = radius*radius
        theta = np.arccos(a.costheta)
        sinth = a.sintheta

        # volume integrating:
        #     int f * r**2*sinth*dth*dr*dphi / Vol
        #     int [int f dphi ] * r**2*sinth*dth*dr / Vol
        #     int [int f dphi/2pi] * r**2*sinth*dth*dr / (Vol/2pi)
        #     int [az_avg(f)] * r**2*sinth*dth*dr / (Vol/2pi)
        print("\nIntegrating over theta...")
        Leg = LegendreGrid(nth, a=-1, b=1)
        weights = Leg.weights; C = Leg.integ_coeff

        nq = np.shape(data)[2]
        tmp = np.zeros((nr,nt,nq))
        for q in quants:
            ind = a.lut[q]
            for t in range(nt):
                for r in range(nr):
                    # I think the sin(th) is taken care of in some u-substitution
                    tmp[r,t,ind] = C*np.sum(data[:,r,ind,t]*weights)

        print("\nIntegrating over radius...")
        vol_avg_terms = np.zeros((nt,nq))
        for q in quants:
            ind = a.lut[q]
            for t in range(nt):
                vol_avg_terms[t,ind] = my_integrator(rad2*tmp[:,t,ind], radius,
                                               data_reversed=1, method='cheb-end')

        # normalize by volume, making it an average
        # don't include a factor of 2*pi, because these are Az_Avgs, where
        # a 2*pi has already been applied
        volume = 2./3.*(radius[0]**3 - radius[-1]**3)
        vol_avg_terms /= volume

    else: # read data from file
        print("\nReading data from stored file {}".format(readfile))
        try:
            readdata, keys = read_data(readfile)
            alltime       = readdata['time']
            vol_avg_terms = readdata['data']
            labels        = readdata['labels']
            quants        = readdata['quants']
            lut           = readdata['lut']
            print("\t...success")
        except:
            print("\t...FAILED")
            sys.exit()

        # put the lut in an empty class container
        a = Empty(lut=lut)

    if (store_results):
        output = outputname(savefile, ext=".stored")
        print("\nStoring results to file {}".format(output))
        try:
            save_data(output, compress=True,
                     data=vol_avg_terms,
                     time=alltime,
                     labels=np.array(labels),
                     quants=np.array(quants),
                     lut=a.lut)
            print("\t...success")
        except:
            print("\t...FAILED")

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))
    print()

    if (no_plots):
        return

    print("Plotting...")

    if (saveplot):
        plt.figure(1,figsize=(7.5, 8.5), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7.5,8.5),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    plt.subplot(411)
    i = 0
    for q in quants[0:3]:
        if (q == 64):
            i += 1
            continue
        ind = a.lut[q]
        label = labels[i]
        plt.plot(alltime, vol_avg_terms[:,ind], label=label)
        i += 1
    if (log_scale):
        plt.yscale('symlog')
    plt.ylabel('Induction Terms')
    plt.legend(loc='best', shadow=True, ncol=3, fontsize='x-small') 

    plt.subplot(412)
    for q in quants[3:6]:
        if (q == 64):
            i += 1
            continue
        ind = a.lut[q]
        label = labels[i]
        plt.plot(alltime, vol_avg_terms[:,ind], label=label)
        i += 1
    if (log_scale):
        plt.yscale('symlog')
    plt.ylabel('Induction Terms')
    plt.legend(loc='best', shadow=True, ncol=3, fontsize='x-small') 

    plt.subplot(413)
    for q in quants[6:9]:
        if (q == 64):
            i += 1
            continue
        ind = a.lut[q]
        label = labels[i]
        plt.plot(alltime, vol_avg_terms[:,ind], label=label)
        i += 1
    if (log_scale):
        plt.yscale('symlog')
    plt.ylabel('Induction Terms')
    plt.legend(loc='best', shadow=True, ncol=3, fontsize='x-small') 

    plt.subplot(414)
    for q in quants[9:12]:
        if (q == 64):
            i += 1
            continue
        ind = a.lut[q]
        label = labels[i]
        plt.plot(alltime, vol_avg_terms[:,ind], label=label)
        i += 1
    if (log_scale):
        plt.yscale('symlog')
    plt.ylabel('Induction Terms')
    plt.legend(loc='best', shadow=True, ncol=3, fontsize='x-small') 

    plt.xlabel('Magnetic Diffusion Time')

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    debug = args['--debug']
    store = args['--store']
    readfile = args['--read']
    log_scale = args['--log']

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, log_scale, debug, store, readfile)

