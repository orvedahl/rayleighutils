"""
Plot time averaged Shell_Slice file

Usage:
    plot_slices.py [options]

Options:
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: shell_slice]
    --no-plots        Suppress the plots [default: False]
    --data-dir=<d>    Location of Shell_Slices directory [default: ./]
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (files[-l:] is used) [default: 0]
    --radial-ind=<r>  Specify what radial index to use [default: 0]
    --q-code=<q>      Plot quantity code q [default: 64]
    --lookup          Interpret q-codes as quantity names, e.g. entropy [default: False]
    --no-mean         Subtract off the mean for each quantity [default: False]
    --saturate        Saturate the colorbar at extreme values [default: False]
    --res=<r>         Interpolation resolution [default: 512]
    --dpi=<d>         Set dpi for plot [default: 200]
    --store           Save data to file [default: False]
    --read=<r>        Read from data file [default: ]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSlice,build_file_list
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from Utilities.look_up_quantity import general_lookup
from NumericalToolsLocal.filesystem.file_io import outputname, save_data, read_data
from Utilities.loop_progress import print_progress
import sys
try:
    from mpl_toolkits.basemap import Basemap
    basemap_support = True
except:
    basemap_support = False

def main(data_dir, start_file, end_file, last_number_files,
         saveplot, savefile, no_plots, rad_ind, resolution, store_data, readfile,
         qcode, lookup, no_mean, saturate=False, dpi=200, **kwargs):

    projection = 'ortho' # 'moll' & 'hammer' both dont look right under python2

    print("\n\treading data...")
    if (data_dir[-1] != "/"):
        data_dir = data_dir + "/"
    if (readfile == ''):
        files = build_file_list(start_file, end_file, path=data_dir+'Shell_Slices')
        icount = 0.0
        ntotal = len(files[-last_number_files:])
        for i,f in enumerate(files[-last_number_files:]):
            print_progress("\tprogress = {:.2f} %".format(100.*i/ntotal))
            a = ShellSlice(filename=f, path='./')
            if (i == 0):
                data = np.zeros(np.shape(a.vals)[:-3])
                if (lookup):
                    q = general_lookup(q=str(qcode))
                else:
                    q = int(qcode)
                qcode = a.lut[q]
                tstart = a.time[0]
            for t in range(a.niter):
                data = data + a.vals[:,:,rad_ind,qcode,t]
                icount += 1.0
            tend = a.time[-1]
        data = data/icount

        # ensure it has proper dimensions
        data = data.reshape(a.nphi, a.ntheta)
        data = np.transpose(data) # now it is (theta, phi)

        # store single time snapshot too
        snapshot = a.vals[:,:,rad_ind,qcode,-1]
        snapshot = snapshot.reshape(a.npi, a.ntheta)
        snapshot = np.transpose(snapshot)

        radius = a.radius
        nphi = a.nphi
        ntheta = a.ntheta

    else:
        print("\nReading data from stored file {}".format(readfile))
        try:
            # read data from stored file
            readdata = read_data(readfile, all_keys=True)
            data     = readdata['data']
            radius   = readdata['radius']
            nphi     = readdata['nphi']
            ntheta   = readdata['ntheta']
            q        = readdata['qindex']
            snapshot = readdata['snapshot']
            print("\t...success\n")
        except:
            print("\t...FAILED\n")
            sys.exit()

    if (not basemap_support):
        print("\nBasemap is not supported/installed")
        store_data = True

    if (store_data):
        output = outputname(savefile, ext=".shell_slice_stored")
        if (not basemap_support):
            output = outputname(output, insert="_NoBasemap")
        print("\nStoring results to file {}".format(output))

        try:
            save_data(output, compress=True,
                      data=data, radius=radius, nphi=nphi, ntheta=ntheta, qindex=q,
                      snapshot=snapshot)
            print("\t...success\n")
        except:
            print("\t...FAILED\n")

    if (not basemap_support):
        return

    if (no_mean): # subtract off the mean
        data = data - np.mean(data)

    if (not saturate): # no saturation
        vmin = np.amin(data)
        vmax = np.amax(data)
    else:
        # saturate colorbar at +/- 2 sigma
        maxabs = 2.*np.std(data)
        data[np.where(data >  maxabs)] =  maxabs
        data[np.where(data < -maxabs)] = -maxabs
        data /= maxabs
        vmin = -1.; vmax = 1.

    # setup grid
    dlon = 360./nphi
    dlat = 180./ntheta

    # make 1D grids of latitude/longitude
    lons = dlon*np.arange(nphi  ) - 180.
    lats = dlat*np.arange(ntheta) - 90.

    # generate 2D grid of latitude/longitude
    llons, llats = np.meshgrid(lons, lats)

    # set plot characteristics
    plot_colorbar = True

    r = radius[rad_ind]/radius[0]
    xlabel = r'$r/r_o = {:.2f}$'.format(r)

    suptitle = '' #r'Shell Slice'

    meta = {}
    meta['title'] = general_lookup(index=q)
    meta['suptitle'] = suptitle
    meta['xlabel'] = xlabel

    meta['suptitlefont'] = 13
    meta['tfont'] = 12
    meta['xfont'] = 10
    meta['cfont'] = 10

    if (no_plots):
        return

    ######################################
    # shell slice plots
    ######################################
    plt.figure(1, dpi=dpi, figsize=(8,4))

    print("\tsetting up plot/interpolating...")
    # initialize the projection
    # lon_0/lat_0 are the center point of projection
    # resolution 'l' is low resolution coastlines
    m = Basemap(projection=projection, lon_0=-20, lat_0=35, resolution='l')

    # get x-y projection points on the plane
    x, y = m(llons, llats)

    # interpolate, necessary for moderately sized shell slices (ell >= 255)
    # data is assumed to be (lat, lon)=(theta,phi) with lats/lons in degrees
    nx = ny = resolution
    topodat,x,y = m.transform_scalar(data, lons, lats, nx, ny, returnxy=True,
                                        masked=True, order=1)

    print("\tdrawing data for plot...")
    # plot the data
    cmap = plt.cm.RdYlBu_r
    m.pcolormesh(x, y, topodat, cmap=cmap, vmin=vmin, vmax=vmax)
    if (plot_colorbar):
        cb = m.colorbar()
        cb.set_ticks(np.round(np.linspace(vmin,vmax,num=7,endpoint=True),decimals=2))
        cb.ax.tick_params(labelsize=meta['cfont']) 

    # draw grid lines
    m.drawparallels(np.arange(-90., 120., 30.))
    m.drawmeridians(np.arange(0.,   420., 60.))
    m.drawmapboundary(fill_color='aqua')

    # draw labels
    plt.title(meta['title'], fontsize=meta['tfont'])
    plt.xlabel(meta['xlabel'], fontsize=meta['xfont'])
    plt.suptitle(meta['suptitle'], fontsize=meta['suptitlefont'])

    if (saveplot):
        print("\tsaving image...")
        output = outputname(savefile, ext=".png")
        plt.savefig(output, dpi=dpi)
        print("saved image: {}".format(output))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

   
    start_file = int(args['--start'])
    end_file = int(args['--end'])
    last_number_files = int(args['--last'])

    data_dir = args['--data-dir']

    saveplot = args['--save']
    savefile = args['--output']
    no_plots = args['--no-plots']

    store_data = args['--store']
    readfile  = args['--read']

    rad_ind = int(args['--radial-ind'])
    qcode = args['--q-code']
    lookup = args['--lookup']
    no_mean = args['--no-mean']
    saturate = args['--saturate']
    dpi = int(args['--dpi'])
    resolution = int(args['--res'])

    main(data_dir, start_file, end_file, last_number_files,
         saveplot, savefile, no_plots, rad_ind, resolution, store_data, readfile,
         qcode, lookup, no_mean, saturate=saturate, dpi=dpi)

