"""
Plot 2 Shell_Slice files side by side

Usage:
    plot_2_slices.py [options] <shell_slice_1> <shell_slice_2>

Options:
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: shell_slice.eps]
    --no-plots        Suppress the plots [default: False]
    --radial-ind=<r>  Specify what radial index to use [default: 0]
    --time-ind=<t>    What time index to use [default: -1]
    --q-code-1=<q1>   Plot quantity code q1 in first plot [default: 64]
    --q-code-2=<q2>   Plot quantity code q2 in second plot [default: 64]
    --lookup          Interpret q-codes as quantity names, e.g. entropy [default: False]
    --no-mean         Subtract off the mean for each quantity [default: False]
    --saturate        Saturate the colorbar at extreme values [default: False]
    --dpi=<d>         Set dpi for plot [default: 200]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSlice
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from matplotlib import ticker
from Utilities.look_up_quantity import general_lookup

def main(shell_1, shell_2, saveplot, savefile, no_plots, rad_ind,
         time_ind, qcode1, qcode2, lookup, no_mean, saturate=False, dpi=200, **kwargs):

    projection = 'ortho' # 'moll' & 'hammer' both dont look right under python2

    print("\n\treading data...")
    #Read in our shell slice
    a1 = ShellSlice(filename=shell_1, path='./')
    a2 = ShellSlice(filename=shell_2, path='./')

    if (lookup):
        q1 = general_lookup(q=str(qcode1))
        q2 = general_lookup(q=str(qcode2))
    else:
        q1 = int(qcode1)
        q2 = int(qcode2)
    qcode = [a1.lut[q1], a2.lut[q2]]

    Rsun = 6.96e10
    r1 = a1.radius[rad_ind]/Rsun
    r2 = a2.radius[rad_ind]/Rsun
    xlabel1 = r'$r/R_\odot = {:.2f}$'.format(r1)
    xlabel2 = r'$r/R_\odot = {:.2f}$'.format(r1)

    title1 = r'$\mathrm{Pr} = 0.5$, $\mathrm{Ra}_F = 2.1\times 10^5$'
    #title1 = r'$\mathrm{Pr} = 0.25$, $\mathrm{Ra}_F = 5.4\times 10^4$'
    title2 = r'$\mathrm{Pr} = 4$, $\mathrm{Ra}_F = 2.1\times 10^5$'

    meta = {}
    meta['suptitle'] = 'Radial Velocity'
    meta['title'] = [title1, title2]
    meta['xlabel'] = [xlabel1, xlabel2]

    meta['suptitlefont'] = 13
    meta['tfont'] = 12
    meta['xfont'] = 10
    meta['cfont'] = 10

    if (no_plots):
        return

    ######################################
    # shell slice plots
    ######################################
    plt.figure(1, dpi=dpi, figsize=(8,4))

    for ind, a in enumerate([a1, a2]):

        plt.subplot(121+ind) # set the proper subplot

        print("\tpreparing data for plot {}...".format(ind))
        # make sure data has proper dimensions
        data = a.vals[:,:,rad_ind,qcode[ind],time_ind].reshape(a.nphi,a.ntheta)
        data = np.transpose(data)

        if (no_mean): # subtract off the mean
            data = data - np.mean(data)

        dlon = 360./a.nphi
        dlat = 180./a.ntheta

        if (not saturate):
            # scale data by mean
            data /= np.mean(data)

            # saturate the colorbar at 2*mean & 0.5*mean
            vmin = 2.0
            vmax = 0.5
        else:
            # saturate colorbar at +/- 3 sigma
            maxabs = 2.*np.std(data)
            data[np.where(data >  maxabs)] =  maxabs
            data[np.where(data < -maxabs)] = -maxabs
            data /= maxabs
            vmin = -1.; vmax = 1.

        # make 1D grids of latitude/longitude
        lons = dlon*np.arange(a.nphi  ) - 180.
        lats = dlat*np.arange(a.ntheta) - 90.

        # generate 2D grid of latitude/longitude
        llons, llats = np.meshgrid(lons, lats)

        print("\tsetting up plot/interpolating for plot {}...".format(ind))
        # initialize the projection
        # lon_0/lat_0 are the center point of projection
        # resolution 'l' is low resolution coastlines
        m = Basemap(projection=projection, lon_0=-20, lat_0=35, resolution='l')

        # get x-y projection points on the plane
        x, y = m(llons, llats)

        # interpolate, necessary for moderately sized shell slices (ell >= 255)
        nx = 512; ny = 512
        topodat,x,y = m.transform_scalar(data, lons, lats, nx, ny, returnxy=True,
                                         masked=True, order=1)

        print("\tdrawing data for plot {}...".format(ind))
        # plot the data
        cmap = plt.cm.RdYlBu_r
        m.pcolormesh(x, y, topodat, cmap=cmap, vmin=vmin, vmax=vmax)
        cb = m.colorbar()
        cb.set_ticks(np.round(np.linspace(vmin,vmax,num=7,endpoint=True),decimals=2))
        cb.ax.tick_params(labelsize=meta['cfont']) 

        # draw grid lines
        m.drawparallels(np.arange(-90., 120., 30.))
        m.drawmeridians(np.arange(0.,   420., 60.))
        m.drawmapboundary(fill_color='aqua')

        plt.title(meta['title'][ind], fontsize=meta['tfont'])
        plt.xlabel(meta['xlabel'][ind], fontsize=meta['xfont'])

    plt.suptitle(meta['suptitle'], fontsize=meta['suptitlefont'])

    if (saveplot):
        print("\tsaving image...")
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    shell_1 = args['<shell_slice_1>']
    shell_2 = args['<shell_slice_2>']
    saveplot = args['--save']
    savefile = args['--output']
    no_plots = args['--no-plots']

    radial_ind = int(args['--radial-ind'])
    time_ind = int(args['--time-ind'])
    qcode1 = args['--q-code-1']
    qcode2 = args['--q-code-2']
    lookup = args['--lookup']
    no_mean = args['--no-mean']
    saturate = args['--saturate']
    dpi = int(args['--dpi'])

    main(shell_1, shell_2, saveplot, savefile, no_plots, radial_ind, time_ind,
         qcode1, qcode2, lookup, no_mean, saturate=saturate, dpi=dpi)

