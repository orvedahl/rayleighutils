"""
Plot characteristic \ell values for single run

Usage:
    plot_characteristic_ell_single.py [options] <data-file>

Options:
    --save           Save the image [default: False]
    --output=<o>     Save image as <o> [default: nondim_plots]
    --debug          Run in debug mode [default: False]
    --Ra-crit=<r>    Set the critical Ra_F below which we dont fit [default: 1e4]
    --plot-power     Make the x-axis Ra**b*Pr**c [default: False]
    --data=<o>       What data to use 'upper-avg', 'middle-rms' [default: upper-avg]
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
from NumericalToolsLocal import fit_RaPr

def main(which_data, save, output, input_file, debug, cutoff, plot_powers):

    # read data file for output values
    data = read_datafile(input_file, separator=None)

    # extract the data
    # Pr = 2
    Pr2_kappa     = data['2']['kappa'][:]
    Pr2_Ra_F      = data['2']['Ra_F'][:]
    Pr2_Ra_BL     = data['2']['Ra_BL'][:]
    Pr2_nu        = 2.*Pr2_kappa
    Pr2_lavg_upper  = data['2']['lavg_upper'][:]
    Pr2_lavg_middle = data['2']['lavg_middle'][:]
    Pr2_lavg_lower  = data['2']['lavg_lower'][:]
    Pr2_lrms_upper  = data['2']['lrms_upper'][:]
    Pr2_lrms_middle = data['2']['lrms_middle'][:]
    Pr2_lrms_lower  = data['2']['lrms_lower'][:]
    Pr2_Pr          = 2.0*np.ones_like(Pr2_Ra_F)

    # Pr = 1
    myPr1_kappa     = data['-1']['kappa'][:]
    myPr1_Ra_F      = data['-1']['Ra_F'][:]
    myPr1_Ra_BL     = data['-1']['Ra_BL'][:]
    myPr1_nu        = 1.*myPr1_kappa
    myPr1_lavg_upper  = data['-1']['lavg_upper'][:]
    myPr1_lavg_middle = data['-1']['lavg_middle'][:]
    myPr1_lavg_lower  = data['-1']['lavg_lower'][:]
    myPr1_lrms_upper  = data['-1']['lrms_upper'][:]
    myPr1_lrms_middle = data['-1']['lrms_middle'][:]
    myPr1_lrms_lower  = data['-1']['lrms_lower'][:]
    myPr1_Pr          = 1.0*np.ones_like(myPr1_Ra_F)

    # Pr = 4
    Pr4_kappa     = data['4']['kappa'][:]
    Pr4_Ra_F      = data['4']['Ra_F'][:]
    Pr4_Ra_BL     = data['4']['Ra_BL'][:]
    Pr4_nu        = 4.*Pr4_kappa
    Pr4_lavg_upper  = data['4']['lavg_upper'][:]
    Pr4_lavg_middle = data['4']['lavg_middle'][:]
    Pr4_lavg_lower  = data['4']['lavg_lower'][:]
    Pr4_lrms_upper  = data['4']['lrms_upper'][:]
    Pr4_lrms_middle = data['4']['lrms_middle'][:]
    Pr4_lrms_lower  = data['4']['lrms_lower'][:]
    Pr4_Pr          = 4.0*np.ones_like(Pr4_Ra_F)

    # Pr = 0.25
    Pr0_25_kappa     = data['0.25']['kappa'][:]
    Pr0_25_Ra_F      = data['0.25']['Ra_F'][:]
    Pr0_25_Ra_BL     = data['0.25']['Ra_BL'][:]
    Pr0_25_nu        = 0.25*Pr0_25_kappa
    Pr0_25_lavg_upper  = data['0.25']['lavg_upper'][:]
    Pr0_25_lavg_middle = data['0.25']['lavg_middle'][:]
    Pr0_25_lavg_lower  = data['0.25']['lavg_lower'][:]
    Pr0_25_lrms_upper  = data['0.25']['lrms_upper'][:]
    Pr0_25_lrms_middle = data['0.25']['lrms_middle'][:]
    Pr0_25_lrms_lower  = data['0.25']['lrms_lower'][:]
    Pr0_25_Pr          = 0.25*np.ones_like(Pr0_25_Ra_F)

    # Pr = 0.5
    Pr0_5_kappa     = data['0.5']['kappa'][:]
    Pr0_5_Ra_F      = data['0.5']['Ra_F'][:]
    Pr0_5_Ra_BL     = data['0.5']['Ra_BL'][:]
    Pr0_5_nu        = 0.5*Pr0_5_kappa
    Pr0_5_lavg_upper  = data['0.5']['lavg_upper'][:]
    Pr0_5_lavg_middle = data['0.5']['lavg_middle'][:]
    Pr0_5_lavg_lower  = data['0.5']['lavg_lower'][:]
    Pr0_5_lrms_upper  = data['0.5']['lrms_upper'][:]
    Pr0_5_lrms_middle = data['0.5']['lrms_middle'][:]
    Pr0_5_lrms_lower  = data['0.5']['lrms_lower'][:]
    Pr0_5_Pr          = 0.5*np.ones_like(Pr0_5_Ra_F)

    # Paper results: Table 6, Ra_F is too high by pi so divide to make it correct
    Pr1_kappa     = data['1']['kappa'][:]
    Pr1_Ra_F      = data['1']['Ra_F'][:]
    Pr1_Ra_BL     = data['1']['Ra_BL'][:]
    Pr1_nu        = 1.*Pr1_kappa
    Pr1_lavg_upper  = data['1']['lavg_upper'][:]
    Pr1_lavg_middle = data['1']['lavg_middle'][:]
    Pr1_lavg_lower  = data['1']['lavg_lower'][:]
    Pr1_lrms_upper  = data['1']['lrms_upper'][:]
    Pr1_lrms_middle = data['1']['lrms_middle'][:]
    Pr1_lrms_lower  = data['1']['lrms_lower'][:]
    Pr1_Pr          = 1.0*np.ones_like(Pr1_Ra_F)

    # generic Ra number array, smallest is at the end, biggest is at the front
    all_Ra_F = np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F))
    min_Ra = np.amin(all_Ra_F)
    max_Ra = np.amax(all_Ra_F)
    Ra_F = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)
    all_Ra_BL = np.concatenate((Pr2_Ra_BL, Pr0_25_Ra_BL, Pr0_5_Ra_BL, Pr1_Ra_BL, Pr4_Ra_BL))
    min_Ra = np.amin(all_Ra_BL)
    max_Ra = np.amax(all_Ra_BL)
    Ra_BL = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)

    # cutoff for low vs. high Rayleigh number
    Ra_F_crit = cutoff

    lmin = 1; lmax = 30
    min_Ra_F = 0.25*Ra_F[0]; max_Ra_F = 4*Ra_F[-1]

    plt.figure(1, figsize=(5, 5), dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    aspect = 1.0*np.log10(max_Ra_F/min_Ra_F) / np.log10(lmax/lmin)

    #######################################################
    # fits to \ell ~ Ra^b Pr^c
    #######################################################
    _RaF = np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F))
    _Pr  = np.concatenate((Pr2_Pr, Pr0_25_Pr, Pr0_5_Pr, Pr1_Pr, Pr4_Pr))
    uRaF, uind = np.unique(_RaF, return_index=True)

    if (which_data == "upper-avg"):
        data = np.concatenate((Pr2_lavg_upper, Pr0_25_lavg_upper,
                           Pr0_5_lavg_upper, Pr1_lavg_upper, Pr4_lavg_upper))
        Pr2_data    = Pr2_lavg_upper
        Pr0_25_data = Pr0_25_lavg_upper; Pr0_5_data  = Pr0_5_lavg_upper
        Pr1_data    = Pr1_lavg_upper; Pr4_data    = Pr4_lavg_upper
        ylabel = 'average l'; title = 'Upper Conv'
    elif (which_data == "middle-avg"):
        data = np.concatenate((Pr2_lavg_middle, Pr0_25_lavg_middle,
                           Pr0_5_lavg_middle, Pr1_lavg_middle, Pr4_lavg_middle))
        Pr2_data    = Pr2_lavg_middle
        Pr0_25_data = Pr0_25_lavg_middle; Pr0_5_data  = Pr0_5_lavg_middle
        Pr1_data    = Pr1_lavg_middle; Pr4_data    = Pr4_lavg_middle
        ylabel = 'average l'; title = 'Middle Conv'
    elif (which_data == "lower-avg"):
        data = np.concatenate((Pr2_lavg_lower, Pr0_25_lavg_lower,
                           Pr0_5_lavg_lower, Pr1_lavg_lower, Pr4_lavg_lower))
        Pr2_data    = Pr2_lavg_lower
        Pr0_25_data = Pr0_25_lavg_lower; Pr0_5_data  = Pr0_5_lavg_lower
        Pr1_data    = Pr1_lavg_lower; Pr4_data    = Pr4_lavg_lower
        ylabel = 'average l'; title = 'Lower Conv'
    elif (which_data == "upper-rms"):
        data = np.concatenate((Pr2_lrms_upper, Pr0_25_lrms_upper,
                           Pr0_5_lrms_upper, Pr1_lrms_upper, Pr4_lrms_upper))
        Pr2_data    = Pr2_lrms_upper
        Pr0_25_data = Pr0_25_lrms_upper; Pr0_5_data  = Pr0_5_lrms_upper
        Pr1_data    = Pr1_lrms_upper; Pr4_data    = Pr4_lrms_upper
        ylabel = 'rms l'; title = 'Upper Conv'
    elif (which_data == "middle-rms"):
        data = np.concatenate((Pr2_lrms_middle, Pr0_25_lrms_middle,
                           Pr0_5_lrms_middle, Pr1_lrms_middle, Pr4_lrms_middle))
        Pr2_data    = Pr2_lrms_middle
        Pr0_25_data = Pr0_25_lrms_middle; Pr0_5_data  = Pr0_5_lrms_middle
        Pr1_data    = Pr1_lrms_middle; Pr4_data    = Pr4_lrms_middle
        ylabel = 'rms l'; title = 'Middle Conv'
    elif (which_data == "lower-rms"):
        data = np.concatenate((Pr2_lrms_lower, Pr0_25_lrms_lower,
                           Pr0_5_lrms_lower, Pr1_lrms_lower, Pr4_lrms_lower))
        Pr2_data    = Pr2_lrms_lower
        Pr0_25_data = Pr0_25_lrms_lower; Pr0_5_data  = Pr0_5_lrms_lower
        Pr1_data    = Pr1_lrms_lower; Pr4_data    = Pr4_lrms_lower
        ylabel = 'rms l'; title = 'Lower Conv'

    # restrict data to be > Ra_F_crit
    ind = np.where(_RaF > Ra_F_crit)
    data = data[ind]; fit_RaF = _RaF[ind]; fit_Pr = _Pr[ind]

    #######################################################
    # do the fit
    #######################################################
    print("...fitting {}...".format(which_data))
    a0 = (0.6, 0.24, 0.)
    params, errors, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(fit_RaF, data, fit_Pr, a0,
                                                                method='MC')
    a = params[0]; b = params[1]; c = params[2]
    da = errors[0]; db = errors[1]; dc = errors[2]
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(b, db, 100*db/abs(b)))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(c, dc, 100*dc/abs(c)))
    print("\ta = {}".format(a))

    #######################################################
    # plots
    #######################################################
    plt.subplot(111) #, aspect=aspect)
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    plt.text(1.2*min_Ra_F, 1.8*lmin, str1, fontsize='small')
    plt.text(1.2*min_Ra_F, 1.2*lmin, str2, fontsize='small')

    if (plot_powers):
        bb = b; cc = c
    else:
        bb = 1; cc = 0
    plt.plot(Pr0_25_Ra_F**bb*0.25**cc, Pr0_25_data, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F**bb*0.5**cc, Pr0_5_data, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F**bb*1.**cc, Pr1_data, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F**bb*2.**cc, Pr2_data, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F**bb*4.**cc, Pr4_data, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='upper left', frameon=True, fontsize='xx-small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    #plt.ylim(lmin, lmax)
    #plt.xlim(min_Ra_F, max_Ra_F)
    if (plot_powers):
        plt.plot(_RaF[uind]**bb*_Pr[uind]**cc, a*_RaF[uind]**bb*_Pr[uind]**cc, color='k',
                 linestyle='--')
        label = r"$\mathrm{{Ra}}_\mathrm{{F}}^{{{:.3f}}}\mathrm{{Pr}}^{{{:.3f}}}$".format(b,c)
        plt.xlabel(label)
    else:
        plt.axvline(x=Ra_F_crit, color='k', linestyle='-.')
        plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    plt.ylabel(ylabel)
    plt.title(title)

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_l_single.png")
        print("saved image: {}".format(output+"_l_single.png"))
    else:
        plt.show()

    plt.close()

def read_datafile(filename, separator=None):
    """
    parse data file to extract calculated quantities
    """
    from collections import OrderedDict

    # allocate space
    data_values = OrderedDict()

    with open(filename, "r") as f:

        # only parse non-empty lines and lines that don't start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                fields = line.split(separator) # separate the fields

                # remove all whitespace from each field
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract specific values
                Pr       = fields[0]
                _kappa   = float(fields[1])
                _Ra_F    = float(fields[2])
                _Ra_BL   = float(fields[3])
                _lavg_u  = float(fields[4])
                _lrms_u  = float(fields[5])
                _lavg_m  = float(fields[6])
                _lrms_m  = float(fields[7])
                _lavg_l  = float(fields[8])
                _lrms_l  = float(fields[9])

                # this is a new entry so initialize the data structure
                if (Pr not in data_values.keys()):
                    data_values[Pr] = OrderedDict({'kappa':[], 'Ra_F':[], 'Ra_BL':[],
                                       'lavg_upper':[], 'lavg_middle':[], 'lavg_lower':[],
                                       'lrms_upper':[], 'lrms_middle':[], 'lrms_lower':[]})

                # add the data
                data_values[Pr]['kappa'].append(  _kappa)
                data_values[Pr]['Ra_F'].append(   _Ra_F)
                data_values[Pr]['Ra_BL'].append(  _Ra_BL)
                data_values[Pr]['lavg_upper'].append( _lavg_u)
                data_values[Pr]['lavg_middle'].append(_lavg_m)
                data_values[Pr]['lavg_lower'].append( _lavg_l)
                data_values[Pr]['lrms_upper'].append( _lrms_u)
                data_values[Pr]['lrms_middle'].append(_lrms_m)
                data_values[Pr]['lrms_lower'].append( _lrms_l)

    # convert the lists into np arrays
    for pr in data_values.keys():
        for field in data_values[pr].keys():
            data_values[pr][field] = np.array(data_values[pr][field])

    return data_values

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(args['--data'], args['--save'], args['--output'], args['<data-file>'],
         args['--debug'], float(args['--Ra-crit']), args['--plot-power'])

