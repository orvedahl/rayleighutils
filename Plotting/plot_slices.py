"""
Plot single Shell_Slice file using Basemap

Usage:
    plot_slices.py [options] <shell_slice>

Options:
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: shell_slice.png]
    --no-plots        Suppress the plots [default: False]
    --radial-ind=<r>  Specify what radial index to use [default: 0]
    --time-ind=<t>    What time index to use [default: 0]
    --q-code=<q>      Plot quantity code q [default: 64]
    --lookup          Interpret q-codes as quantity names, e.g. entropy [default: False]
    --no-mean         Subtract off the mean for each quantity [default: False]
    --saturate        Saturate the colorbar at extreme values [default: False]
    --dpi=<d>         Set dpi for plot [default: 200]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSlice
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from matplotlib import ticker
from Utilities.look_up_quantity import general_lookup
from NumericalToolsLocal import plotting

def main(shellfile, saveplot, savefile, no_plots, rad_ind,
         time_ind, qcode, lookup, no_mean, saturate=False, dpi=200, **kwargs):

    projection = 'ortho' # 'moll' & 'hammer' both dont look right under python2

    print("\n\treading data...")
    #Read in our shell slice
    a = ShellSlice(filename=shellfile, path='./')

    if (lookup):
        q = general_lookup(q=str(qcode))
    else:
        q = int(qcode)
    qcode = a.lut[q]

    Rstar = 1
    r = a.radius[rad_ind]/Rstar
    #xlabel = r'$r/r_o = {:.2f}$'.format(r)
    xlabel = r'$r = {:.2f}$'.format(r)

    title = ''

    meta = {}
    meta['suptitle'] = general_lookup(index=q) #'Radial Velocity'
    meta['title'] = title
    meta['xlabel'] = xlabel

    meta['suptitlefont'] = 13
    meta['tfont'] = 12
    meta['xfont'] = 10
    meta['cfont'] = 10

    if (no_plots):
        return

    ######################################
    # shell slice plots
    ######################################
    plt.figure(1, dpi=dpi, figsize=(4,4))

    print("\tpreparing data...")
    # make sure data has proper dimensions
    data = a.vals[:,:,rad_ind,qcode,time_ind].reshape(a.nphi,a.ntheta)
    data = np.transpose(data)

    if (no_mean): # subtract off the mean
        data = data - np.mean(data)

    dlon = 360./a.nphi
    dlat = 180./a.ntheta

    if (not saturate):
        vmin = np.amin(data); vmax = np.amax(data)
    else:
        # saturate colorbar at +/- 3 sigma
        maxabs = 2.*np.std(data)
        data[np.where(data >  maxabs)] =  maxabs
        data[np.where(data < -maxabs)] = -maxabs
        data /= maxabs
        vmin = -1.; vmax = 1.

    print("\t...vmin/vmax = {}/{}".format(vmin,vmax))

    # make 1D grids of latitude/longitude
    lons = dlon*np.arange(a.nphi  ) - 180.
    lats = dlat*np.arange(a.ntheta) - 90.

    # generate 2D grid of latitude/longitude
    llons, llats = np.meshgrid(lons, lats)

    print("\tsetting up plot/interpolating...")
    # initialize the projection
    # lon_0/lat_0 are the center point of projection
    # resolution 'l' is low resolution coastlines
    if (projection == 'stere'):
        lllon, lllat = -60., -50.
        uclon, uclat = 120., 20.
        m = Basemap(projection=projection, lon_0=-20, lat_0=35, resolution='l',
                    llcrnrlon=lllon, llcrnrlat=lllat, urcrnrlon=uclon, urcrnrlat=uclat)
    elif (projection == 'merc'):
        lllon, lllat = np.amin(lons), -85.
        uclon, uclat = np.amax(lons), 85.
        m = Basemap(projection=projection, lon_0=-20, lat_0=35, resolution='l',
                    llcrnrlon=lllon, llcrnrlat=lllat, urcrnrlon=uclon, urcrnrlat=uclat)
    else:
        m = Basemap(projection=projection, lon_0=-20, lat_0=35, resolution='l')

    # get x-y projection points on the plane
    x, y = m(llons, llats)

    # interpolate, necessary for moderately sized shell slices (ell >= 255)
    nx = 512; ny = 512
    topodat,x,y = m.transform_scalar(data, lons, lats, nx, ny, returnxy=True,
                                        masked=True, order=1)

    print("\tdrawing data for plot...")

    # plot the data
    cmap = plt.cm.RdBu_r
    norm = plotting.MidPointNorm(midpoint=0)
    m.pcolormesh(x, y, topodat, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm)
    cb = m.colorbar()
    cb.set_ticks(np.round(np.linspace(vmin,vmax,num=7,endpoint=True),decimals=2))
    cb.ax.tick_params(labelsize=meta['cfont']) 

    # draw grid lines
    m.drawparallels(np.arange(-90., 120., 30.))
    m.drawmeridians(np.arange(0.,   420., 60.))
    m.drawmapboundary(fill_color='aqua')

    plt.title(meta['title'], fontsize=meta['tfont'])
    plt.xlabel(meta['xlabel'], fontsize=meta['xfont'])

    plt.suptitle(meta['suptitle'], fontsize=meta['suptitlefont'])

    if (saveplot):
        print("\tsaving image...")
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    shellfile = args['<shell_slice>']
    saveplot = args['--save']
    savefile = args['--output']
    no_plots = args['--no-plots']

    radial_ind = int(args['--radial-ind'])
    time_ind = int(args['--time-ind'])
    qcode = args['--q-code']
    lookup = args['--lookup']
    no_mean = args['--no-mean']
    saturate = args['--saturate']
    dpi = int(args['--dpi'])

    main(shellfile, saveplot, savefile, no_plots, radial_ind, time_ind,
         qcode, lookup, no_mean, saturate=saturate, dpi=dpi)

