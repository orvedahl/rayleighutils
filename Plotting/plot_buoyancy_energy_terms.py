"""
Plot custom outputs and a few global averages.

Usage:
    plot_energy_KE.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: KE_differential_rotation.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Ra=<r>             Set Rayleigh number [default: 1e6]
    --Pr=<r>             Thermal Prandlt number [default: 1]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --debug              Debug mode [default: False]
    --lut=<l>            Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
from ReadRayleigh.diagnostic_reading import GlobalAverage, ReferenceState
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import os
from NumericalToolsLocal.public import LegendreGrid, interp, derivative, smooth
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup, lut_lookup
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, Pr, Rayleigh, debug, new_lut, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    path = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file,end_file,path=path)

    # only need radial velocity
    quant_list = [1]
    azavg = AzAverage(filename=files[0],path='',quantity=quant_list)

    # compile a time series of all the files
    data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                  AzAverage, axis=-1, data_attr="vals", quantity=quant_list)
    az_alltime = Pm*np.array(time)

    # read reference state
    ref = ReferenceState(filename="reference", path=data_dir)
    temperature = ref.temperature

    print("\nAzimuthal Averages...")
    print("First file: {}".format(files[-last_number_files:exclude_last][0]))
    print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

    print("\nFirst time: {} simulation-secs".format(az_alltime[0]))
    print("Last  time: {} simulation-secs".format(az_alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(az_alltime[-1]-az_alltime[0]))

    nt = len(az_alltime)
    nr = azavg.nr
    nth = azavg.ntheta
    radius = azavg.radius
    rad2 = radius*radius
    theta = np.arccos(azavg.costheta)
    sinth = azavg.sintheta

    # extract data
    vr_index = azavg.lut[shortcut_lookup('vr')]
    vr = data[:,:,vr_index,:]                   # size [nth,nr,nt]

    # volume integrating:
    #     int f * r**2*sinth*dth*dr*dphi / Vol
    #     int [int f dphi ] * r**2*sinth*dth*dr / Vol
    #     int [int f dphi/2pi] * r**2*sinth*dth*dr / (Vol/2pi)
    #     int [az_avg(f)] * r**2*sinth*dth*dr / (Vol/2pi)
    #
    #     we want f = ur*r**3*T_0/r_o
    print("\nIntegrating over theta...")
    Leg = LegendreGrid(nth, a=-1, b=1)
    weights = Leg.weights; C = Leg.integ_coeff

    tmp = np.zeros((nr,nt))
    r_coeff = radius**3 / radius[0] * temperature
    if (debug):
        tmp2 = np.zeros((nr,nt))
        for t in range(nt):
            for r in range(nr):
                # I think the sin(th) is taken care of in some u-substitution
                tmp[r,t] = C*np.sum(vr[:,r,t]*weights)
                tmp2[r,t] = my_integrator(sinth*vr[:,r,t], theta,
                                          method='trap', data_reversed=1)
    else:
        for t in range(nt):
            for r in range(nr):
                # I think the sin(th) is taken care of in some u-substitution
                tmp[r,t] = C*np.sum(vr[:,r,t]*weights)

    print("\nIntegrating over radius...")
    buoyancy_work = np.zeros((nt))
    if (debug):
        buoyancy_debug = np.zeros((nt))
        for t in range(nt):
            buoyancy_work[t] = my_integrator(r_coeff[:]*tmp[:,t], radius,
                                           data_reversed=1, method='cheb-end')
            buoyancy_debug[t] = my_integrator(r_coeff[:]*tmp2[:,t], radius,
                                        data_reversed=1, method='cheb-end')
    else:
        for t in range(nt):
            buoyancy_work[t] = my_integrator(r_coeff[:]*tmp[:,t], radius,
                                           data_reversed=1, method='cheb-end')

    # normalize by volume, making it an average
    # don't include a factor of 2*pi, because these are Az_Avgs, where
    # a 2*pi has already been applied
    volume = 2./3.*(radius[0]**3 - radius[-1]**3)
    buoyancy_work /= volume
    if (debug):
        buoyancy_debug /= volume

    # add in the Rayleigh and Prandtl numbers
    buoyancy_work *= Rayleigh/Pr
    if (debug):
        buoyancy_debug *= Rayleigh/Pr

    # get the Global Averages
    path = os.path.join(data_dir, "G_Avgs")
    files = build_file_list(start_file, end_file, path=path)

    #             KE   ME   J^2, visc,  buoyancy
    quant_list = [125, 475, 703, 301,   302]
    gavg = GlobalAverage(filename=files[0],path='', quantity=quant_list)
    data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                       GlobalAverage, axis=0, quantity=quant_list)
    alltime = Pm*np.array(time)
    print("\nGlobal Averages...")
    print("First file: {}".format(files[-last_number_files:exclude_last][0]))
    print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    KE = data[:, gavg.lut[125]]
    ME = data[:, gavg.lut[475]]
    viscous_heat = data[:, gavg.lut[301]]
    ohmic_heat = data[:, gavg.lut[703]]

    # get time deriv of KE & ME
    KEdot = derivative(data[:,gavg.lut[125]], alltime)
    MEdot = derivative(data[:,gavg.lut[475]], alltime)

    LHS = KEdot + MEdot + viscous_heat + ohmic_heat

    # fix the buoyancy work term and interpolate to the G_Avgs time grid
    f_interp = interp(buoyancy_work, az_alltime)
    corrected_buoyancy_work = data[:,gavg.lut[302]]/viscous_heat - f_interp(alltime)

    if (no_plots):
        return

    print("\nPlotting...")

    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7.5,4),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (False):
        plt.plot(alltime, KE, label='KE')
        plt.plot(alltime, ME, label='ME')
    plt.plot(alltime, viscous_heat, label='Viscous Heat')
    plt.plot(alltime, ohmic_heat, label='Ohmic Heat')
    if (False):
        if (True):
            #plt.plot(alltime, np.abs(corrected_buoyancy_work), label='|Buoyancy Work|')
            plt.plot(alltime, smooth(np.abs(corrected_buoyancy_work)), label='smooth(|Buoyancy|)')
        else:
            plt.plot(alltime, corrected_buoyancy_work, label='Buoyancy Work')
    if (False):
        plt.plot(alltime, KEdot, label='dKE/dt')
        plt.plot(alltime, MEdot, label='dME/dt')
    plt.plot(alltime, LHS, label='dKE/dt+dME/dt+Visc+J^2')
    plt.yscale('symlog')
    plt.xlabel('Magnetic Diffusion Time')
    plt.ylabel('Various G_Avgs Quantities')
    if (saveplot):
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 

    if (False):
        plt.twinx()
        plt.plot(alltime, np.abs(LHS - corrected_buoyancy_work), color='k', linestyle='--')
        plt.yscale('log')

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    Pr = float(args['--Pr'])
    Ra = float(args['--Ra'])
    debug = args['--debug']
    new_lut = args['--lut']


    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, Pr, Ra, debug, new_lut)

