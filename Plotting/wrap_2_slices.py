"""
Plot multiple 2 Shell_Slice files side by side

Usage:
    wrap_2_slices.py [options] <data-dir-1> <data-dir-2>

Options:
    --eps             Save eps images [default: False]
    --output=<o>      Set basename of images [default: two_shell_slices]

    --radial-ind=<r>  Specify what radial index to use [default: 0]
    --time-ind=<t>    What time index to use [default: -1]
    --q-code-1=<q1>   Plot quantity code q1 in first plot [default: 64]
    --q-code-2=<q2>   Plot quantity code q2 in second plot [default: 64]
    --lookup          Interpret q-codes as quantity names, e.g. entropy [default: False]
    --no-mean         Subtract off the mean for each quantity [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import build_file_list
import plot_2_slices

def main(data_dir1, data_dir2, eps, savefile, rad_ind, time_ind,
         qcode1, qcode2, lookup, no_mean, **kwargs):

    saveplot = True; no_plots = False

    if (eps):
        ext = ".eps"
    else:
        ext = ".png"

    if (data_dir1[-1] != "/"):
        data_dir1 = data_dir1 + "/"
    if (data_dir2[-1] != "/"):
        data_dir2 = data_dir2 + "/"
    files1 = build_file_list(0, 10000000, path=data_dir1+"Shell_Slices/")
    files2 = build_file_list(0, 10000000, path=data_dir2+"Shell_Slices/")

    nfiles1 = len(files1)
    nfiles2 = len(files2)

    nfiles = min(nfiles1, nfiles2)

    print("\nLooping over {} shell slice files\n".format(nfiles))
    i_image = nfiles
    for i in range(nfiles):

        print("working on file {} ({:.2f} %)".format(i, (i+1)*100./nfiles))
        file1 = files1[nfiles1-1-i]
        file2 = files2[nfiles2-1-i]

        output = savefile+"_"+str(i_image)+ext
        plot_2_slices.main(file1, file2, saveplot, output, no_plots,
                           rad_ind, time_ind, qcode1, qcode2, lookup, no_mean,
                           saturate=False)
        i_image -= 1

    print("\n---Complete---\n")

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    datadir_1 = args['<data-dir-1>']
    datadir_2 = args['<data-dir-2>']
    savefile = args['--output']
    eps = args['--eps']

    radial_ind = int(args['--radial-ind'])
    time_ind = int(args['--time-ind'])
    qcode1 = args['--q-code-1']
    qcode2 = args['--q-code-2']
    lookup = args['--lookup']
    no_mean = args['--no-mean']

    main(datadir_1, datadir_2, eps, savefile, radial_ind, time_ind,
         qcode1, qcode2, lookup, no_mean)

