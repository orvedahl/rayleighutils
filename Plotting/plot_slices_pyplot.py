"""
Plot single Shell_Slice file using Matplotlib

Usage:
    plot_slices.py [options] <shell_slice>

Options:
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: shell_slice.png]
    --no-plots        Suppress the plots [default: False]
    --radial-ind=<r>  Specify what radial index to use [default: 0]
    --time-ind=<t>    What time index to use [default: 0]
    --q-code=<q>      Plot quantity code q [default: 64]
    --lookup          Interpret q-codes as quantity names, e.g. entropy [default: False]
    --no-mean         Subtract off the mean for each quantity [default: False]
    --saturate        Saturate the colorbar at extreme values [default: False]
    --dpi=<d>         Set dpi for plot [default: 200]
    --colorbar        Show a colorbar [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSlice
import numpy as np
import matplotlib.pyplot as plt
from Utilities.look_up_quantity import general_lookup
import NumericalToolsLocal.public as NT

def main(shellfile, saveplot, savefile, no_plots, rad_ind,
         time_ind, qcode, lookup, no_mean, saturate, dpi, colorbar, **kwargs):

    projection = 'mollweide' # 'aitoff'

    print("\n\treading data...")
    a = ShellSlice(filename=shellfile, path='./')

    if (lookup):
        q = general_lookup(q=str(qcode))
    else:
        q = int(qcode)
    qcode = a.lut[q]

    r = a.radius[rad_ind]
    xlabel = r'$r = {:.2f}$'.format(r)

    meta = {}
    meta['title'] = general_lookup(index=q) #'Radial Velocity'
    meta['xlabel'] = xlabel

    meta['suptitlefont'] = 13
    meta['tfont'] = 12
    meta['xfont'] = 10
    meta['cfont'] = 10

    if (no_plots):
        return

    ######################################
    # shell slice plots
    ######################################
    fig = plt.figure(1, dpi=dpi, figsize=(4,2))

    print("\tpreparing data...")
    # make sure data has proper dimensions
    data = a.vals[:,:,rad_ind,qcode,time_ind].reshape(a.nphi,a.ntheta)
    data = np.transpose(data)

    if (no_mean): # subtract off the mean
        data = data - np.mean(data)

    dlon = 360./a.nphi
    dlat = 180./a.ntheta

    if (not saturate):
        vmin = np.amin(data); vmax = np.amax(data)
    else:
        # saturate colorbar at +/- 3 sigma
        maxabs = 2.*np.std(data)
        data[np.where(data >  maxabs)] =  maxabs
        data[np.where(data < -maxabs)] = -maxabs
        data /= maxabs
        vmin = -1.; vmax = 1.

    print("\t...vmin/vmax = {}/{}".format(vmin,vmax))

    # make 1D grids of latitude/longitude, pcolormesh requires *radians*
    lons = (dlon*np.arange(a.nphi  ) - 180.)*np.pi/180.
    lats = (dlat*np.arange(a.ntheta) - 90. )*np.pi/180.

    # generate 2D grid of latitude/longitude
    llons, llats = np.meshgrid(lons, lats)

    print("\tdrawing data for plot...")
    ax = fig.add_subplot(111, projection=projection)
    cmap = plt.cm.RdBu_r
    norm = NT.MidPointNorm(midpoint=0.5*(vmax+vmin))
    im = ax.pcolormesh(llons, llats, data, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm)

    # set tick positions/labels
    xticks = np.array([-135, -90, -45, 0, 45, 90, 135])*np.pi/180.
    ax.set_xticks(xticks)
    xticklabels = ['']*len(xticks)
    ax.set_xticklabels(xticklabels)

    yticks = np.array([-60, -30, 0, 30, 60])*np.pi/180.
    ax.set_yticks(yticks)
    yticklabels = ['']*len(yticks)
    ax.set_yticklabels(yticklabels)

    ax.grid(True)

    if (colorbar):
        cb = fig.colorbar(im, orientation='horizontal')
        cb.set_ticks(np.round(np.linspace(vmin,vmax,num=7,endpoint=True),decimals=2))
        cb.ax.tick_params(labelsize=meta['cfont']) 

    ax.set_title(meta['title'], fontsize=meta['tfont'])
    ax.set_xlabel(meta['xlabel'], fontsize=meta['xfont'])

    if (saveplot):
        print("\tsaving image...")
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    shellfile = args['<shell_slice>']
    saveplot = args['--save']
    savefile = args['--output']
    no_plots = args['--no-plots']

    radial_ind = int(args['--radial-ind'])
    time_ind = int(args['--time-ind'])
    qcode = args['--q-code']
    lookup = args['--lookup']
    no_mean = args['--no-mean']
    saturate = args['--saturate']
    dpi = int(args['--dpi'])
    colorbar = args['--colorbar']

    main(shellfile, saveplot, savefile, no_plots, radial_ind, time_ind,
         qcode, lookup, no_mean, saturate, dpi, colorbar)

