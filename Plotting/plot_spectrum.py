"""
Plot power spectrum of given quantity

Usage:
    plot_spectrum.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: power_spectrum]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --list          List the possible radial indices [default: False]
    --rad-ind=<i>   Give radial index, or "max", "min", "2nd-small", "2nd-large" [default: 0]
    --quantity=<q>  Take power spectrum of <q>, 'velocity', 's' [default: velocity]
    --alt           Use alternate lookup table [default: False]
    --lut=<l>       Use a different lut.py file
"""

from __future__ import print_function

import env
from ReadRayleigh.diagnostic_reading import ShellSpectra, build_file_list
import Utilities.time_utils as time_utils
from Utilities.look_up_quantity import general_lookup
from Utilities.indices import find_index
import matplotlib.pyplot as plt
import numpy as np
import os

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots,
         list_contents, rad_index, new_lut, quantity='velocity', alt=False, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    # build file list
    data_dir = os.path.join(data_dir, "Shell_Spectra")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # read first file to get meta data
    a = ShellSpectra(files[0], path='')
    lmax = a.lmax; mmax = a.mmax

    if (list_contents):
        print("\nPossible radial indices: {}".format(a.inds))
        print("\tcorresponding radii: {}".format(a.radius))
        print("\nPossible Quantities:")
        print("\tquantity code\tname")
        print("\t-------------------------")
        for i in range(len(a.lut)):
            if (a.lut[i] != 5000):
                print("\t{}\t\t{}".format(i, general_lookup(index=i, alt=alt)))
        print()
        return

    if (rad_index in ["max", "min", "2nd-small", "2nd-large"]):
        rad_index = find_index(a.radius, index=rad_index)
    else:
        rad_index = int(rad_index)

    # build data cube
    print("\n...doing average...")
    if (True):
        data, avgtimes = time_utils.TimeAvg(files, ShellSpectra, axis=3, data_attr="lpower")
    else:
        tind = -1
        data = a.lpower[:,:,:,tind,:]+a.lpower[:,:,:,tind,:]+a.lpower[:,:,:,tind,:]
        avgtimes = {"tstart":a.time[tind], "tend":a.time[tind],
                    "dt":a.time[tind]-a.time[tind], "nrec":len(a.time)}

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(avgtimes["tstart"]))
    print("Last  time: {} sec".format(avgtimes["tend"]))
    print("\nAveraged over {} sec".format(avgtimes["dt"]))
    print("Number of records {}".format(avgtimes["nrec"]))

    print("\nChosen radial slice:")
    print("\tplotting rad_index = {}".format(a.inds[rad_index]))
    print("\tcorresponds to radius = {}".format(a.radius[rad_index]))
    data = data[:,rad_index,:,:]

    if (quantity.lower() != 'velocity'):
        print("...computing {} spectra".format(quantity))
        qcode = general_lookup(q=quantity, alt=alt)
        qcode = a.lut[qcode]

        tot_power  = data[:,qcode,0]
        m0_power   = data[:,qcode,1]
        conv_power = data[:,qcode,2]
    else:
        print("...computing velocity spectra")
        qr = general_lookup(q='vr', alt=alt)
        qt = general_lookup(q='vt', alt=alt)
        qp = general_lookup(q='vp', alt=alt)

        qr = a.lut[qr]
        qt = a.lut[qt]
        qp = a.lut[qp]

        tot_power  = data[:,qr,0] + data[:,qt,0] + data[:,qp,0]
        m0_power   = data[:,qr,1] + data[:,qt,1] + data[:,qp,1]
        conv_power = data[:,qr,2] + data[:,qt,2] + data[:,qp,2]

    # calculate characteristic \ell values
    ell = np.arange(0, lmax+1)
    l_avg = np.sum(ell*tot_power)/np.sum(tot_power)
    l_rms = np.sqrt(np.sum(ell*ell*tot_power)/np.sum(tot_power))
    print("\nCharacteristic \\ell values:")
    print("\taverage l = {}".format(l_avg))
    print("\trms l val = {}".format(l_rms))

    print("version = {}".format(a.version))

    plt.figure(1)
    lw = 1.5
    plt.plot(ell[:], tot_power[:],  label ='Total Power',linewidth=lw)
    plt.plot(ell[:], m0_power[:],   label ='m=0 Power',linewidth=lw)
    plt.plot(ell[:], conv_power[:], label ='Convective Power ( Total - {m=0} )',linewidth=lw)
    plt.axvline(x=l_avg, color='k', linestyle='-.', label=r'$\ell$ avg')
    plt.axvline(x=l_rms, color='k', linestyle=':', label=r'$\ell$ rms')
    legend = plt.legend(loc='lower left', shadow=True, ncol = 2) 
    plt.yscale('log')
    plt.xscale('symlog')
    #plt.xlim(xmin=0,xmax=lmax)
    plt.xlabel('Spherical Harmonic Degree '+r'$\ell$')
    plt.ylabel('Power')
    plt.title("{} Power Spectrum".format(quantity))
    plt.tight_layout()

    if (no_plots):
        return

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    list_contents = args['--list']
    rad_index = args['--rad-ind']
    alt = args['--alt']
    q = args['--quantity']
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots,
         list_contents, rad_index, new_lut, alt=alt, quantity=q)

