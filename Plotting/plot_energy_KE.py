"""
Plots average KE vs time

Usage:
    plot_energy_KE.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: energy_trace.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --magnetic           Plot the magnetic energy [default: False]
    --derivs             Plot the derivatives [default: False]
    --fft                FFT the time series [default: False]
    --reynolds           Calculate Re = sqrt(2*KE), assumes rho=1 [default: False]
    --quants=<q>         Comma separated list of quantities to use
    --lut=<l>            Use a different lut.py file
    --store              Write results to file for future reading [default: False]
    --read=<f>           Read stored results from file [default: ]
    --python             Use pure python diagnostic_reading file [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.import_diagnostics import diagnostics
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import numpy as np
import NumericalToolsLocal.public as NT
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, magnetic, derivs, do_fft, reynolds, Pm, quants, new_lut,
         store_results, readfile, python_diag, **kwargs):

    # import reading routines
    imports = ["GlobalAverage", "build_file_list", "ReferenceState"]
    diags = diagnostics(imports, python=python_diag, package="ReadRayleigh")
    build_file_list = diags["build_file_list"]
    GlobalAverage   = diags["GlobalAverage"]
    ReferenceState  = diags["ReferenceState"]

    # integration method
    method = 'cheb-end'

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    if (readfile == ''):
        if (exclude_last == 0):
            exclude_last = None
        else:
            exclude_last *= -1

        path = os.path.join(data_dir, "G_Avgs")
        files = build_file_list(start_file, end_file, path=path)
        files = files[-last_number_files:exclude_last]

        if (quants is None):
            quant_list = None
        else:
            quant_list = list(quants)

        a = GlobalAverage(filename=files[0],path='',
                          quantity=quant_list)
        ke_index  = a.lut[shortcut_lookup('ke')]   # Kinetic Energy (KE)
        if (magnetic):
            me_index  = a.lut[shortcut_lookup('me')]   # Magnetic Energy (ME)

        # compile a time series of all the files
        data, time = time_utils.TimeSeries(files, GlobalAverage, axis=0, quantity=quant_list)
        alltime = Pm*np.array(time)

        ke = data[:,ke_index]
        if (magnetic):
            me = data[:,me_index]

        print("\nFirst file: {}".format(files[0]))
        print("Last  file: {}".format(files[-1]))
        print("\nTotal number of files: {}".format(len(files)))

        # need to get the grid data so read the reference state
        ref = ReferenceState(filename="reference", path=data_dir)
        radius = ref.radius
        density = ref.density

    else:
        print("\nReading data from stored file {}".format(readfile))
        T = time_utils.Timer(); T.start()
        try:
            readdata, keys = NT.read_data(readfile)
            ke = readdata['ke']
            if (magnetic):
                me = readdata['me']
            alltime = readdata['time']
            radius  = readdata['radius']
            density = readdata['density']
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()
        T.stop(); dt = T.print_interval()

    if (store_results):
        output = NT.outputname(savefile, ext=".stored")
        print("\nStoring results to file {}".format(output))

        T = time_utils.Timer(); T.start()
        try:
            kwargs = {'ke':ke, 'time':alltime, 'radius':radius, 'density':density}
            if (magnetic):
                kwargs['me'] = me
            NT.save_data(output, compress=True, **kwargs)
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()
        T.stop(); dt = T.print_interval()

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    r_outer = np.max(radius)
    r_inner = np.min(radius)
    H = r_outer - r_inner
    volume = 4*np.pi*my_integrator(radius**2, radius, method=method, data_reversed=1)
    # get mass by integrating the density of the shell
    mass = 4*np.pi*my_integrator(density*radius**2, radius, method=method, data_reversed=1)
    mean_KE_density = np.mean(ke)
    mean_KE_total = mean_KE_density*volume     # dimensional KE, [KE] = ergs

    print("\nAvg total KE density [simulation 'erg cm^-3'] : {}".format(mean_KE_density))
    print("Avg total KE [simulation 'erg']               : {}".format(mean_KE_total))
    if (magnetic):
        mean_ME_density = np.mean(me)
        mean_ME_total = mean_ME_density*volume     # dimensional ME, [ME] = ergs
        print("\nAvg total ME density [simulation 'erg cm^-3'] : {}".format(mean_ME_density))
        print("Avg total ME [simulation 'erg']               : {}".format(mean_ME_total))

    if (reynolds):
        # assuming rho=1
        # Re = sqrt( 2*KE )
        Re = np.sqrt(2*mean_KE_density)
        print("\nReynolds estimate = {}\n".format(Re))

    #Create two plots.
    # Plot 1:  Total KE and its breakdown.
    # Plot 2:  Breakdown of the mean KE
    if (no_plots):
        return

    #Changing the plotting parameter somewhat depending on saveplot
    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 16})
    else:
        plt.figure(1,figsize=(7.5,4),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alltime,ke,'red',label=r'KE$_{total}$')
    if (magnetic):
        plt.plot(alltime,me,'blue',label=r'ME$_{total}$')
    plt.yscale('log')
    plt.xlabel('Magnetic Diffusion Time')
    plt.ylabel('Energy Density')

    if (saveplot):
        legend = plt.legend(loc='best', shadow=True, ncol = 2, fontsize = 'large') 
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 
    if (derivs):
        plt.twinx()
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        from NumericalToolsLocal.calculus.differentiate import derivative
        dkedt = derivative(ke, alltime, data_reversed=False)
        plt.plot(alltime,dkedt,'black', linestyle='--', label=r'$|$dKE$_{total}$/dt$|$')
        #plt.plot(alltime,np.abs(dkedt),'black', linestyle='--', label=r'$|$dKE$_{total}$/dt$|$')
        if (magnetic):
            dmedt = derivative(me, alltime, data_reversed=False)
            plt.plot(alltime,dmedt,'black', linestyle=':', label=r'$|$dME$_{total}$/dt$|$')
        plt.yscale('symlog',linthreshy=1e-2)
        plt.ylabel('dKE/dt')
        if (saveplot):
            legend = plt.legend(loc='upper right', shadow=True, ncol = 2, fontsize = 'x-small') 
        else:
            legend = plt.legend(loc='best', shadow=True, ncol = 2) 
    plt.tight_layout()

    # remove the scientific notation offset
    plt.gca().get_xaxis().get_major_formatter().set_scientific(False)

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

    if (do_fft):
        import NumericalToolsLocal.fft as my_fft

        # interp to uniform grid
        print("\ninterpolating to uniform grid...")
        t_uniform, ke_uniform = my_fft.interp_uniform(ke, alltime)
        if (magnetic):
            t_uniform, me_uniform = my_fft.interp_uniform(me, alltime)

        # FFT the data
        print("\nFFT...")
        fk, kfreq, ke_power, xwindow = my_fft.FFT(ke_uniform, t_uniform)
        if (magnetic):
            fk, kfreq, me_power, xwindow = my_fft.FFT(me_uniform, t_uniform)

        # plot it
        print("\nplotting...")
        plt.clf()
        plt.plot(kfreq, ke_power, label='FFT(KE)', color='r', linestyle='-')
        if (magnetic):
            plt.plot(kfreq, me_power, label='FFT(ME)', color='b', linestyle='-')

        plt.legend(loc='best', shadow=True, ncol=2, fontsize='x-small')
        plt.ylabel("power")
        plt.xlabel("angular freq")
        plt.xscale('log')
        plt.yscale('log')

        plt.show()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    magnetic = args['--magnetic']
    derivs = args['--derivs']
    do_fft = args['--fft']
    Re = args['--reynolds']
    Pm = float(args['--Pm'])
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]
    new_lut = args['--lut']
    store_results = args['--store']
    readfile = args['--read']
    python_diag = args['--python']

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, magnetic, derivs, do_fft, Re, Pm, quants, new_lut, store_results, readfile,
         python_diag)

