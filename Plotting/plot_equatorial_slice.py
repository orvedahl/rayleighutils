"""
Plot equatorial slice data

Usage:
    plot_equatorial_slice.py [options]

Options:
    --timestep=<t>    Specify iteration number to plot [default: 00005000]
    --quantity=<q>    Give quantity code [default: 64]
    --no-mean         Remove the m=0 mean [default: False]
    --Pm=<p>          Convert to magnetic diffusion times [default: 1]
    --save            Save an image [default: False]
    --output=<o>      Save image as <o> [default: equatorial_slice.png]
    --data-dir=<d>    Specify location of data [default: ./]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import Equatorial_Slice
import numpy as np
import matplotlib.pyplot as plt

def main(timestep, quantity_code, remove_mean, save, output, data_dir, Prm):

    tindex = 0          # Display the first timestep from the file

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    a = Equatorial_Slice(filename=timestep, path=data_dir+'Equatorial_Slices/')

    #Set up the grid
    nr = a.nr
    nphi = a.nphi
    r = a.radius/np.max(a.radius)
    phi = np.zeros(nphi+1,dtype='float64')
    phi[0:nphi] = a.phi
    phi[nphi] = np.pi*2  # For display purposes, it is best to have a redunant data point at 0,2pi

    radius_matrix, phi_matrix = np.meshgrid(r,phi)
    X = radius_matrix * np.cos(phi_matrix)
    Y = radius_matrix * np.sin(phi_matrix)

    time = a.time[tindex]*Prm

    qindex = a.lut[quantity_code]

    field = np.zeros((nphi+1,nr),dtype='float64')
    field[0:nphi,:] =a.vals[:,:,qindex,tindex]
    field[nphi,:] = field[0,:]  #replicate phi=0 values at phi=2pi

    #remove the mean if desired
    if (remove_mean):
        for i in range(nr):
            the_mean = np.mean(field[:,i])
            field[:,i] = field[:,i]-the_mean

    #Plot
    plt.figure(1)
    cmap = 'RdBu_r' #'jet'
    img = plt.pcolormesh(X,Y,field,cmap=cmap)

    #x/y range
    plt.xlim(-1,1)
    plt.ylim(-1,1)

    #x/y labels
    plt.xlabel(r"$r/r_o$")
    plt.ylabel(r"$r/r_o$")

    time = "{:.3f}".format(time)
    plt.title("$t/t_\eta = {{{}}}$".format(time))

    #equal aspect ratio
    plt.axis('equal')

    if (save):
        plt.savefig(output)
    else:
        plt.show()

if __name__ == "__main__":
    import docopt
    args = docopt.docopt(__doc__)

    timestep = args['--timestep']
    quantity_code = int(args['--quantity'])
    remove_mean = args['--no-mean']
    save = args['--save']
    output = args['--output']
    data_dir = args['--data-dir']
    Prm = float(args['--Pm'])

    main(timestep, quantity_code, remove_mean, save, output, data_dir, Prm)

