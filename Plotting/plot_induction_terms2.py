"""
Plot grid of azimuthal averages

Usage:
    plot_azavg.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: az_average.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-mean         Remove the mean [default: False]
    --Pm=<p>          Set Magnetic Prandtl for displaying time [default: 1]
    --single-time     Do not do an average over time [default: False]
    --list            List the available quanitites [default: False]
"""
from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
import matplotlib.pyplot as plt
import os
import sys
import numpy as np
from Utilities import azavg_util, time_utils, azavg_calculus
from Utilities.look_up_quantity import general_lookup, find_available
import NumericalToolsLocal.public as NT

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_mean,
         list_contents, Pm, single_time):

    # build file list
    data_dir = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    quants = [523, 528,      #533, # -<u>.grad(<B>)
              521, 526, 531, # <B>.grad(<u>)
              566, 570, 574, # bar{ curl(u' x B') }
                1,   2,   3, # velocity
              401, 402, 403] # B field

    # read first file to get meta data
    az = AzAverage(filename=files[0], path='', quantity=quants)
    nr = az.nr
    nth = az.ntheta
    radius = az.radius
    sintheta = az.sintheta
    costheta = az.costheta
    #costheta = np.cos(np.arcsin(sintheta))
    #costheta[0:nth/2] *= -1.0

    if (list_contents):
        vals = find_available(az.lut)
        print("\nFound the following values:")
        for i in vals:
            print("\t{:3d} --- {}".format(i, general_lookup(index=i)))
        print()
        sys.exit()

    # do time average, data is shape (nth, nr, nq)
    if (not single_time):
        print("\n...doing average...")
        #data, avgtimes = time_utils.TimeAvg2(files, AzAverage, axis=-1, data_attr="vals",
        #                                    quantity=quants, method="int")
        #data, avgtimes = time_utils.TimeAvg(files, AzAverage, axis=-1, data_attr="vals",
        #                                    quantity=quants)
        data, avgtimes, timeseries, time = time_utils.TimeAvgSeries(files, AzAverage,
                                               axis=-1, data_attr="vals",
                                               quantity=quants, method="int")
    else:
        print("\n...choosing specific time...")
        tind = 0
        data = az.vals[:,:,:,tind]
        avgtimes = {"dt":0, "tstart":az.time[tind], "tend":az.time[tind], "nrec":az.niter}

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(Pm*avgtimes["tstart"]))
    print("Last  time: {} sec".format(Pm*avgtimes["tend"]))
    print("\nAveraged over {} sec".format(Pm*avgtimes["dt"]))
    print("Number of records {}".format(avgtimes["nrec"]))

    quantity = data[:,:,:]; nq = np.shape(quantity)[2]

    # remove the \ell=0 bit from all quantities
    if (no_mean):
        print("\n...removing \ell=0 from all quanitites...")
        for j in range(nq):
            for i in range(nr):
                quantity[:,i,j] -= np.mean(quantity[:,i,j])

    print("\n...setting up figure...")
    T = time_utils.Timer(); T.start()
    if (saveplot):
        fig = plt.figure(figsize=(8.5, 11.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        fig = plt.figure(figsize=(8.5, 11.0), dpi=80)
        plt.rcParams.update({'font.size': 14})

    main_ax = fig.add_subplot(1,1,1)
    main_ax.axis('off')
    T.stop(); dt = T.print_interval()

    tsize = 20
    lsize = 15

    qcodes = [523, 528,-533, # -<u>.grad(<B>)
              521, 526, 531, # <B>.grad(<u>)
              566, 570, 574, # bar{ curl(u' x B') }
             -525,-530,-535] # laplacian

    Ufield = (quantity[:,:,az.lut[1]], quantity[:,:,az.lut[2]], quantity[:,:,az.lut[3]])
    Bfield = (quantity[:,:,az.lut[401]], quantity[:,:,az.lut[402]], quantity[:,:,az.lut[403]])

    print("\n...calculating missing data...")
    calculated = {}

    print("\t-U.grad(B)...")
    T = time_utils.Timer(); T.start()
    UdotDelB = azavg_calculus.AdotGradB(Ufield, Bfield, radius, np.arccos(costheta))
    T.stop(); dt = T.print_interval()
    calculated[-533] = -UdotDelB[2]

    print("\tLap(B)...")
    T = time_utils.Timer(); T.start()
    L = azavg_calculus.laplacian(Bfield, radius, np.arccos(costheta))
    T.stop(); dt = T.print_interval()
    calculated[-525] = L[0]/Pm
    calculated[-530] = L[1]/Pm
    calculated[-535] = L[2]/Pm

    print("\n...plotting...")
    units = ''
    for i, qcode in enumerate(qcodes):
        A = fig.add_subplot(4,3,i+1)

        if (qcode > 0):
            q = az.lut[qcode]
            plt_data = quantity[:,:,q]
        else:
            plt_data = calculated[qcode]

        azavg_util.plot_azav(fig, A, plt_data, radius, costheta, sintheta, r_bcz=0,
                         mycmap='RdBu_r', cbar=True, orientation='vertical',
                         boundsfactor=1.5, boundstype='rms',
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)

        if (i == 0):
            plt.title("Radial", fontsize=tsize)
        elif (i == 1):
            plt.title("Theta", fontsize=tsize)
        elif (i == 2):
            plt.title("Phi", fontsize=tsize)

    plt.suptitle("Induction Terms", fontsize=tsize)

    # add Y-labels manually --- both axis.set_ylabel(...) & plt.ylabel aren't working?
    xoffset = 0.00; yoffset = 0.11
    label = r"$-u\cdot\nabla B$"
    main_ax.text(xoffset, yoffset+0.78, label, ha='center', va='center',
                   transform=main_ax.transAxes, rotation='vertical', fontsize=tsize)
    label = r"$B\cdot\nabla u$"
    main_ax.text(xoffset, yoffset+0.53, label, ha='center', va='center',
                   transform=main_ax.transAxes, rotation='vertical', fontsize=tsize)
    label = r"$\nabla\times\left(u^\prime\times B^\prime\right)$"
    main_ax.text(xoffset, yoffset+0.26, label, ha='center', va='center',
                   transform=main_ax.transAxes, rotation='vertical', fontsize=tsize)
    label = r"$\nabla^2 B / \mathrm{Pm}$"
    main_ax.text(xoffset, yoffset+0.00, label, ha='center', va='center',
                   transform=main_ax.transAxes, rotation='vertical', fontsize=tsize)

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

    ############################
    if (False):
        plt.clf()
        fig = plt.figure(figsize=(8.5, 11.0), dpi=80)
        plt.rcParams.update({'font.size': 14})
        A = fig.add_subplot(1,1,1)
        if (False):
            Bphi = timeseries[:,:,az.lut[403],:]
            Bdot = NT.derivative(Bphi, time, method='fd', order='2')
            plt_data = time_utils.Average(Bdot, time=time, method="int", axis=-1)
            plt.title(r"TimeAvg = $\bar{\partial_tB_\phi}$")
        else:
            plt.title(r"sum of RHS")
            plt_data = calculated[-535] + calculated[-533] + \
                       quantity[:,:,az.lut[531]] + quantity[:,:,az.lut[574]]
        azavg_util.plot_azav(fig, A, plt_data, radius, costheta, sintheta, r_bcz=0,
                             mycmap='RdBu_r', cbar=True, orientation='vertical',
                             boundsfactor=1.5, boundstype='rms',
                             units=units, fontsize=lsize,
                             underlay=[0], nlevs=6, show_contours=True)
        plt.show()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_mean = args['--no-mean']
    list_contents = args['--list']
    Pm = float(args['--Pm'])
    single_time = args['--single-time']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_mean,
         list_contents, Pm, single_time)

