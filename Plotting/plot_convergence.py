
from collections import OrderedDict
import matplotlib.pyplot as plt
import numpy as np

integrate = False

if (integrate):
    data = OrderedDict()

    datafile = 'convergence_integ_chebyshev.dat'
    _data = np.loadtxt(datafile)
    data['cheb zeros'] = {'N':_data[:,0], 'def int':_data[:,1], 'indef':_data[:,2]}

    datafile = 'convergence_integ_mid.dat'
    _data = np.loadtxt(datafile)
    data['midpoint'] = {'N':_data[:,0], 'def int':_data[:,1], 'indef':_data[:,2]}

    datafile = 'convergence_integ_trap.dat'
    _data = np.loadtxt(datafile)
    data['trapezoid'] = {'N':_data[:,0], 'def int':_data[:,1], 'indef':_data[:,2]}

    Nmin = []; Nmax = []
    for k in data.keys():
        Nmi = np.amin(data[k]['N']); Nma = np.amax(data[k]['N'])
        Nmin.append(Nmi); Nmax.append(Nma)
    Nmin = np.amin(Nmin); Nmax = np.amax(Nmax)
    # plot
    for key in data.keys():
        N = data[key]['N']; def_int = data[key]['def int']; indef = data[key]['indef']
        plt.plot(N, def_int, linestyle=':', marker='x', label=key+' def int')
        plt.plot(N, indef,   linestyle=':', marker='x', label=key+' indef')

    N = data['cheb zeros']['N']; def_int = data['cheb zeros']['def int']
    plt.plot(N, def_int[0]*(N/N[0])**-4 , linestyle='--', color='k', label='$O(N^{-4})$')
    plt.plot(N, def_int[0]*(N/N[0])**-2 , linestyle='--', color='k', label='$O(N^{-2})$')
    plt.xlim(0.5*Nmin, 1e2*Nmax)
    plt.legend(loc='upper right')
    plt.xlabel('N');plt.ylabel('Error'); plt.title('Integration Convergence')
    plt.xscale('log'); plt.yscale('log')
    plt.legend(loc='best')
    plt.savefig('integrate_convergence.png')
    print("\nsaved: integrate_convergence.png\n")

else:
    data = OrderedDict()

    datafile = 'convergence_deriv_chebyshev.dat'
    _data = np.loadtxt(datafile)
    data['cheb DCT'] = {'N':_data[:,0], 'err':_data[:,1]}

    datafile = 'convergence_deriv_chebyshev_DM.dat'
    _data = np.loadtxt(datafile)
    data['cheb DM'] = {'N':_data[:,0], 'err':_data[:,1]}

    datafile = 'convergence_deriv_chebyshev_DM_extrema.dat'
    _data = np.loadtxt(datafile)
    data['extrema DM'] = {'N':_data[:,0], 'err':_data[:,1]}

    datafile = 'convergence_deriv_fd_2.dat'
    _data = np.loadtxt(datafile)
    data['FD 2'] = {'N':_data[:,0], 'err':_data[:,1]}

    datafile = 'convergence_deriv_fd_4.dat'
    _data = np.loadtxt(datafile)
    data['FD 4'] = {'N':_data[:,0], 'err':_data[:,1]}

    datafile = 'convergence_deriv_fd_6.dat'
    _data = np.loadtxt(datafile)
    data['FD 6'] = {'N':_data[:,0], 'err':_data[:,1]}

    Nmin = []; Nmax = []
    for k in data.keys():
        Nmi = np.amin(data[k]['N']); Nma = np.amax(data[k]['N'])
        Nmin.append(Nmi); Nmax.append(Nma)
    Nmin = np.amin(Nmin); Nmax = np.amax(Nmax)
    # plot
    for key in data.keys():
        N = data[key]['N']; error = data[key]['err']
        plt.plot(N, error, linestyle=':', marker='x', label=key)

    N = data['cheb DCT']['N']; error = data['cheb DCT']['err']
    plt.plot(N, error[0]*(N/N[0])**-6 , linestyle='--', color='k', label='$O(N^{-6})$')
    plt.plot(N, error[0]*(N/N[0])**-4 , linestyle='--', color='k', label='$O(N^{-4})$')
    plt.plot(N, error[0]*(N/N[0])**-2 , linestyle='--', color='k', label='$O(N^{-2})$')
    plt.xlim(0.5*Nmin, 1e2*Nmax)
    plt.legend(loc='upper right')
    plt.xlabel('N');plt.ylabel('Error'); plt.title('Differentiation Convergence')
    plt.xscale('log'); plt.yscale('log')
    plt.legend(loc='best')
    plt.savefig('derivative_convergence.png')
    print("\nsaved: derivative_convergence.png\n")

