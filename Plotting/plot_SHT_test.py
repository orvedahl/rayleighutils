"""
Explore SHT package and compare to Shell_Slice & Shell_Spectra

Usage:
    plot_SHT_test.py [options] <data-dir>

Options:
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: SHT_test.png]
    --no-plots        Suppress the plots [default: False]
    --radial-ind=<r>  Specify what radial index to use [default: 0]
    --time-ind=<t>    What time index to use [default: 0]
    --iter=<i>        What iteration to plot [default: 5000]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSlice, ShellSpectra
import os
import matplotlib.pyplot as plt
import numpy as np

import pyshtools

def LegendreZeros(N, domain=[-1,1]):
    coef = np.zeros((N+1)); coef[-1] = 1.0
    Pm = np.polynomial.legendre.Legendre(coef, domain=domain)
    zeros = Pm.roots()
    return zeros

def main(data_dir, iteration, radial_ind, time_ind, saveplot, savefile, no_plots):

    qcode = 64 # entropy

    print("\n\treading data...")
    iteration = '{:08d}'.format(iteration) # zero pad if necessary

    spectrafile = os.path.join(data_dir, "Shell_Spectra", iteration)
    spectra = ShellSpectra(filename=spectrafile, path='') # size [l, m, r, q, t]

    # extract shell spectra data
    sp_qind = spectra.lut[qcode]
    sp_nell = spectra.nell
    sp_nm   = spectra.nm
    sp_lmax = spectra.lmax
    sp_mmax = spectra.mmax
    sp_rad  = spectra.radius[radial_ind]
    sp_time = spectra.time[time_ind]
    sp_data = spectra.vals[:,:,radial_ind, sp_qind, time_ind] # [l,m]
    sp_power= spectra.lpower[:,radial_ind, sp_qind, time_ind,:] # [l,3], sum over m,
    ell = np.arange(0,sp_lmax+1) # [0:lmax] inclusive             i=0 is total, i=1 is m=0,
    m   = np.arange(0,sp_mmax+1) # [0:mmax] inclusive             i=2 is total-(m=0)

    shellfile = os.path.join(data_dir, "Shell_Slices", iteration)
    shell = ShellSlice(filename=shellfile, path='') # data is size [phi, th, r, q, t]

    # extract shell slice data
    sh_qind = shell.lut[qcode]
    sh_nth  = shell.ntheta
    sh_nphi = shell.nphi
    sh_rad  = shell.radius[radial_ind]
    sh_time = shell.time[time_ind]
    sh_data = shell.vals[:,:,radial_ind, sh_qind, time_ind] # [nphi, ntheta]
    sh_sinth= shell.sintheta
    sh_costh= shell.costheta
    sh_theta= np.arccos(sh_costh)
    sh_phi  = np.arange(0., 2*np.pi, 2.*np.pi/sh_nphi)

    # print some diagnostic info
    print("\nShell Slice:")
    print("\tnphi = {}".format(sh_nphi))
    print("\tntheta = {}".format(sh_nth))
    print("\tshape of data = {}".format(np.shape(sh_data)))
    print("\tradius = {}".format(sh_rad))
    print("\ttime = {}".format(sh_time))
    print("\nShell Spectra:")
    print("\tlmax = {}".format(sp_lmax))
    print("\tmmax = {}".format(sp_mmax))
    print("\tnell = {}".format(sp_nell))
    print("\tnm = {}".format(sp_nm))
    print("\tshape of data = {}".format(np.shape(sp_data)))
    print("\tradius = {}".format(sp_rad))
    print("\ttime = {}".format(sp_time))

    # now do some SHTools stuff
    print("\nStarting the SHTools stuff...\n")
    SHT_data = np.transpose(sh_data) # needs to be [nth, nphi]

    if (True):
        # sh_data = [nlat, nlon] i.e. data = [ntheta, nphi]
        grid = pyshtools.SHGrid.from_array(SHT_data, grid='DH') # SHGrid class
        SHTlats = grid.lats(degrees=False) # 1d array
        SHTlons = grid.lons(degrees=False) # 1d array
        SHTphi = 1.*SHTlons
        SHTtheta = 0.5*np.pi - SHTlats[::-1] # Rayleigh theta grid runs from [pi, 0]

        lmax_calc = int(2.*sh_nth/3. - 1)
        coeff = grid.expand(normalization='ortho', csphase=-1) #, lmax_calc=lmax_calc)
        SHTell = coeff.degrees() # 1d array of spherical harmonic degrees
        SHTlmax = coeff.lmax # single number
        SHTspectrum = coeff.spectrum(convention='power', unit='per_l', base=10.0) # 1d array
        #coeff.plot_spectrum(convention='power', unit='per_l', base=10.0, xscale='lin', # or 'log'
        #                    yscale='lin', show=False, fname=output_image_filename)
        #coeff.plot_spectrum2d(convention='power', xscale='log', yscale='log', vscale='log',
        #                      vrange=(1e-5, 1e5), show=False, fname=output_image_filename)
        #evaluate = coeff.expand(lat, lon, degrees=True) # returns float/array based on lat/lon

    print("\t\tRayleigh\tSHT")
    print("---------------------------------")
    print("len(ell)\t{}\t\t{}".format(len(ell), len(SHTell)))
    print("ell[0]\t{}\t\t{}".format(ell[0], SHTell[0]))
    print("ell[-1]\t{}\t\t{}".format(ell[-1], SHTell[-1]))
    print("len(power)\t{}\t\t{}".format(len(sp_power[:,0]), len(SHTspectrum)))
    if (not no_plots):
        plt.clf()
        plt.plot(ell, sp_power[:,0], label='Rayleigh')
        plt.plot(SHTell, SHTspectrum, label='SHTools')
        try:
            plt.plot(np.abs(SHTspectrum - sp_power[:,0]), label='abs err')
        except:
            print("\tarrays are different length")
        plt.xscale('symlog', linthreshy=1e-12); plt.yscale('log')
        plt.xlabel('ell'); plt.ylabel('power')
        plt.legend(loc='best')
        plt.ylim(1e-39, 1e-31)
        plt.show()

        plt.clf()
        print("nphi\t{}\t\t{}".format(len(sh_phi), len(SHTlons)))
        plt.plot(sh_phi, label='Rayleigh')
        plt.plot(SHTphi, label='SHT')
        try:
            plt.plot(np.abs(SHTlons-sh_phi), label='abs err')
        except:
            print("\tarrays are different length")
        plt.yscale('symlog', linthreshy=1e-16); plt.legend(loc='best')
        plt.xlabel('index'); plt.ylabel('phi')
        plt.show()

        plt.clf()
        print("th[0]\t{}\t\t{}".format(sh_theta[0], SHTtheta[0]))
        print("th[-1]\t{}\t\t{}".format(sh_theta[-1], SHTtheta[-1]))
        print("nth\t{}\t\t{}".format(len(sh_theta), len(SHTtheta)))
        plt.plot(sh_theta, marker='x', label='Rayleigh')
        plt.plot(SHTtheta, marker='x', label='SHT')
        _theta = LegendreZeros(len(sh_theta), domain=[np.pi, 0])
        plt.plot(_theta, marker='x', label='numpy')
        _thetaL, dth = np.linspace(0., np.pi, len(sh_theta), retstep=True)
        _thetaL += dth; _thetaL = _thetaL[::-1]
        plt.plot(_thetaL, marker='x', label='linear')
        try:
            plt.plot(np.abs(SHTtheta-sh_theta), label='SHT-Rayleigh')
            plt.plot(np.abs(SHTtheta-_theta), label='SHT-numpy')
            plt.plot(np.abs(SHTtheta-_thetaL), label='SHT-linear')
            plt.plot(np.abs(sh_theta-_theta), label='Rayleigh-numpy')
            plt.plot(np.abs(sh_theta-_thetaL), label='Rayleigh-linear')
        except:
            print("\tarrays are different length")
        plt.yscale('symlog', linthreshy=5e-3); plt.legend(loc='best')
        plt.xlabel('index'); plt.ylabel('theta')
        plt.show()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    data_dir = args['<data-dir>']

    saveplot = args['--save']
    savefile = args['--output']
    no_plots = args['--no-plots']

    radial_ind = int(args['--radial-ind'])
    time_ind = int(args['--time-ind'])
    iteration = int(args['--iter'])

    main(data_dir, iteration, radial_ind, time_ind, saveplot, savefile, no_plots)

