"""
Plot single Shell_Slice file using Mayavi

Usage:
    plot_slices_mayavi.py [options]

Options:
    --start=<s>        Starting file [default: 0]
    --end=<e>          Ending file [default: 100000000]
    --last=<l>         Use last <l> number of files (so files[-l:] is used) [default: 0]
    --data-dir=<d>     Specify location of data [default: ./]
    --save             Save an image [default: False]
    --output=<o>       Save images as <o>.png [default: shell_slice]
    --output-dir=<d>   Put output images in directory <d> [default: ./]
    --qcode=<q>        Plot quantity code q, 1=vr, 66=S' [default: 1]
    --hillsize=<h>     Set the hillsize for the topology [default: 0.01]
    --scale=<s>        Set scale for saturating the plot [default: 1.5]
    --rmean=<r>        Choose mean radius in Mayavi's units [default: 0.3]
    --rind=<i>         What radial index to use [default: 0]
    --no-mean          Subtract off the mean for each quantity [default: False]
    --start-count=<c>  Starting iteration of output files [default: 0]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSlice, build_file_list
from Utilities import time_utils
from Utilities.time_utils import Timer
import os
import numpy as np
from mayavi import mlab

def scale_data(indata, scale_factor=1.5, no_mean=False, data_lim=None):
    """
    rescale data so that
       1) indata has zero spherical mean
       2) indata is rescaled to have min at -1, max at +1
       3) indata "saturates at absolute values > scale_factor*stdev(indata)

    indata is assumed to be dimensioned as [theta, phi]
    """
    nth, nphi = np.shape(indata)

    # remove spherical mean
    if (no_mean):
        indata -= np.mean(indata)

    # saturate data at data_lim
    if (data_lim is None):
        sigma = np.std(indata)
        data_lim = scale_factor*sigma
    ind_above = np.where(indata >  data_lim)
    ind_below = np.where(indata < -data_lim)
    indata[ind_above] =  data_lim
    indata[ind_below] = -data_lim

    # rescale to be in [-1,1]
    indata /= data_lim

    return indata

def main(save, savefile, start_file, end_file, last_number_files, data_dir, output_dir,
         rind, qcode, hillsize, scale, rmean, no_mean, start_count):

    time_it = Timer()
    time_it.start()

    # set output parameters
    if (save):
        if (not os.path.exists(output_dir)):
            os.makedirs(output_dir)
        # store inputs
        with open(os.path.join(output_dir, "input.txt"), 'w') as mf:
            n = "\n"
            mf.write(n)
            mf.write("-----------------------------------------------------------"+n)
            mf.write("Input parameters for plot_slices_mayavi.py image generation"+n)
            mf.write("-----------------------------------------------------------"+n)
            mf.write(n)
            mf.write("start       = {}".format(start_file)+n)
            mf.write("end         = {}".format(end_file)+n)
            mf.write("last        = {}".format(last_number_files)+n)
            mf.write("data-dir    = {}".format(data_dir)+n)
            mf.write("output-dir  = {}".format(output_dir)+n)
            mf.write("output      = {}".format(savefile)+n)
            mf.write("qcode       = {}".format(qcode)+n)
            mf.write("rind        = {}".format(rind)+n)
            mf.write("hillsize    = {}".format(hillsize)+n)
            mf.write("scale       = {}".format(scale)+n)
            mf.write("rmean       = {}".format(rmean)+n)
            mf.write("no-mean     = {}".format(no_mean)+n)
            mf.write("start-count = {}".format(start_count)+n)
            mf.write(n)

        # give output proper pathing
        savefile = os.path.join(output_dir, savefile)

    # build list of files
    path = os.path.join(data_dir, 'Shell_Slices')
    files = build_file_list(start_file, end_file, path=path)
    files = files[-last_number_files:]
    ntotal = len(files)

    # read in time series of data
    a = ShellSlice(files[0], path='')
    data, time = time_utils.TimeSeries(files, ShellSlice, axis=-1)
    time_it.stop(); dt = time_it.interval()
    print("\nElapsed time to read files = {} sec, {} min\n".format(dt, dt/60.))

    print("First file = {}".format(files[0]))
    print("Last file = {}".format(files[-1]))
    print("Number of files = {}".format(ntotal))
    print("\nFirst time = {}".format(time[0]))
    print("Last time = {}".format(time[-1]))
    print("Number of records = {}".format(len(time)))

    # extract quantities & radial slice
    qcode = a.lut[qcode]; radius = a.radius[rind]
    data = data[:,:,rind,qcode,:] # size of [nphi, nth, nt]
    ntheta = a.ntheta; nphi = a.nphi
    ntime = len(time)

    print("\nUsing radial index = {}, radius = {}".format(rind, radius))
    print("Possible radial levels")
    print("\tindex\tradius")
    for i in range(len(a.radius)):
        print("\t{}\t{}".format(i, a.radius[i]))

    # set contour levels, scale factors, colormaps, etc
    winsize = 512
    winx = winy = winsize

    # set the scene parameters
    azimuth_angle = 180
    elevation_angle = 70
    view_distance = 1.3

    # set spherical grid
    pi = np.pi; cos = np.cos; sin = np.sin
    phi, theta = np.mgrid[0:pi:ntheta*1j, 0:2*pi:nphi*1j]
    rmean = 0.3 # mean radius in Mayavi's units

    # get scaling factor from first time slice
    data_lim = scale*np.std(np.transpose(data[:,:,0]))

    # setup window & figure
    mlab.figure(1, bgcolor=(1,1,1), fgcolor=(0,0,0), size=(winx,winy))
    print("\nLooping over time...")
    for i in range(ntime):
        print("...starting i = {} (out of {}), time = {}".format(i, ntime, time[i]))

        # scale the data
        quantity = scale_data(np.transpose(data[:,:,i]), scale_factor=scale,
                              no_mean=no_mean, data_lim=data_lim)

        # use quantity to add elevation to the globe and get x,y,z coords
        r = hillsize*quantity + rmean
        x = r*sin(phi)*cos(theta)
        y = r*sin(phi)*sin(theta)
        z = r*cos(phi)

        # clear figure
        mlab.clf()

        # render topology using elevation_data, color using quantity
        mlab.mesh(x, y, z, scalars=-quantity, colormap='RdYlBu')

        # render the view
        mlab.view(azimuth=azimuth_angle, elevation=elevation_angle, distance=view_distance)

        # save/show
        if (save):
            output = savefile + "_{}.png".format(start_count)
            mlab.savefig(output)
            print("\tsaved image {}".format(output))
        else:
            mlab.show()
        start_count += 1

    time_it.stop(); dt = time_it.interval()
    print("\nTotal time to make images = {} sec, {} min".format(dt, dt/60.))
    print("\n---Complete---\n")

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    save = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    output_dir = args['--output-dir']
    rind = int(args['--rind'])
    qcode = int(args['--qcode'])
    hillsize = float(args['--hillsize'])
    scale = float(args['--scale'])
    rmean = float(args['--rmean'])
    no_mean = args['--no-mean']
    start_count = int(args['--start-count'])

    main(save, savefile, start_file, end_file, last_number_files, data_dir, output_dir,
         rind, qcode, hillsize, scale, rmean, no_mean, start_count)

