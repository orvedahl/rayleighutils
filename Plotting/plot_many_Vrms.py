"""
plot the rms velocity for many different runs and read in the
data from a datafile.

Usage:
    plot_many_Vrms.py [options] <data-file>...

    <data-file> is a space separated list of files from which to read
    the data. Minimum one file is required.

Options:
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: Vrms.png]
    --debug         Debuging [default: False]
    --dimensioned   Plot physical units [default: False]
"""

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import sys

def main(input_files, saveplot, savefile, dimensioned, debug=False, **kwargs):

    # read data from file(s)
    data = {}
    for file in input_files:
        datatmp = read_input(file, debug=debug)
        data.update(datatmp)

    if (debug):
        sys.exit()

    # gather an array of all possible radii
    radius = np.array([])
    for pr in data.keys():
        for i in range(len(data[pr]['radius'])):
            radius = np.concatenate((radius, data[pr]['radius'][i][:]))
    radius = np.sort(np.array(list(set(radius)))) # make radius unique (set) and sort it
    radius /= np.amax(radius)
    min_radius = np.amin(radius)
    max_radius = np.amax(radius)

    # gather an array of all possible Rayleigh numbers
    Ra = np.array([])
    for pr in data.keys():
        Ra = np.concatenate((Ra, data[pr]['Ra_F'][:]))
    Ra = np.sort(np.array(list(set(Ra)))) # make Ra unique (set) and sort it
    minRa = np.amin(Ra)
    maxRa = np.amax(Ra)
    color_ind = np.linspace(0.05,0.95,len(Ra))

    colormap = 'jet'

    # return color based on Rayleigh number
    def get_color(r, cmap='jet'):
        colormap = plt.get_cmap(cmap)
        icol = -1
        for i, ra in enumerate(Ra):  # for whatever reason np.where is not working robustly
            if (r == ra):
                icol = i
                break
        if (icol >= 0):
            col = colormap(color_ind[i]) # turn that into a color
        else:
            col = 'k'

        return col

    #####################################
    # setup the figure
    #####################################
    Prandtl = ['0.25', '0.5', '1.0', '2.0', '4.0']; nrows=3; ncols=2
    #Prandtl = ['0.25', '0.5', '2.0', '4.0']; nrows=2; ncols=2
    xlim = (min_radius, max_radius)
    if (dimensioned):
        ylim = (0, 110.)
    else:
        ylim = (0, 1.2)
    fig, axarr = plt.subplots(nrows, ncols, dpi=200) #, figsize=(10,7.5))
    iPr = 0
    for row in range(nrows):
        for col in range(ncols):
            if (iPr >= len(Prandtl)):
                axarr[row,col].spines['left'].set_visible(False)          # the black box outline
                axarr[row,col].spines['right'].set_visible(False)
                axarr[row,col].spines['top'].set_visible(False)
                axarr[row,col].spines['bottom'].set_visible(False)
                plt.setp(axarr[row,col].get_xticklabels(), visible=False) # tick labels
                plt.setp(axarr[row,col].get_yticklabels(), visible=False)
                axarr[row,col].get_xaxis().set_visible(False)             # ticks
                axarr[row,col].get_yaxis().set_visible(False)
                continue
            Pr = Prandtl[iPr]

            # set title & labels that depend on parameters
            axarr[row,col].set_title("Pr = {}".format(Pr), y=1.04, fontsize=12)

            if (col > 0):
                plt.setp(axarr[row,col].get_yticklabels(), visible=False)
            else:
                plt.setp(axarr[row,col].get_yticklabels(), fontsize=8)
            #if (row != nrows-1):
            #    plt.setp(axarr[row,col].get_xticklabels(), visible=False)
            #else:
            #    plt.setp(axarr[row,col].get_xticklabels(), fontsize=8)
            plt.setp(axarr[row,col].get_xticklabels(), fontsize=8)

            if (col == 0):
                if (dimensioned):
                    axarr[row,col].set_ylabel("RMS Velocity (m/s)", fontsize=10)
                else:
                    axarr[row,col].set_ylabel("RMS Velocity", fontsize=10)
            if (row == nrows-1):
                axarr[row,col].set_xlabel(r"Radius", fontsize=10)

            # plot data
            for i,r in enumerate(data[Pr]['Ra_F']):
                radius = data[Pr]['radius'][i][:]
                Vrms   = data[Pr]['Vrms'][i][:]
                if (dimensioned):
                    axarr[row,col].plot(radius/radius[0], Vrms/100.,
                                     color=get_color(r, cmap=colormap))
                else:
                    axarr[row,col].plot(radius/radius[0], Vrms/np.amax(Vrms),
                                     color=get_color(r, cmap=colormap))

            # set limits
            axarr[row,col].set_xlim(xlim)
            axarr[row,col].set_ylim(ylim)
            #axarr[row,col].set_xscale('log')
            #axarr[row,col].set_yscale('log')

            iPr += 1

    l=0.07; r=0.97; b=0.12; t=0.91; w=0.1; h=0.5
    plt.subplots_adjust(left=l, bottom=b, right=r, top=t, wspace=w, hspace=h)

    # add colorbar
    #  need to get a "scalar mappable" going first
    #  also need "fake" data i.e. sm._A = []
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=plt.Normalize(vmin=minRa, vmax=maxRa))
    sm._A = []
    if (len(Prandtl)%2 != 0):
        cb_x = 0.55; cb_y = 0.2
    else:
        cb_x = 0.57; cb_y = 0.3
    cb_w = 0.02;  cb_h = 0.1
    cbaxes = fig.add_axes([cb_x, cb_y, cb_w, cb_h]) # position = (x,y,width,height)
    cm = fig.colorbar(sm, cax=cbaxes, ticks=[])     # turn off tick marks
    cm.outline.set_color('white')                   # outline the thing in white
    cm.outline.set_linewidth(1)                     # manually label low/high Ra
    cm.ax.text(-.55,1.04,r'High Ra$_F$', fontsize=6)
    cm.ax.text(-.55,-.15,r'Low Ra$_F$', fontsize=6)

    #plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

def read_input(filename, separator=None, debug=False):
    """
    parse data file to extract spectra data
    """
    from collections import OrderedDict

    # allocate space
    data = OrderedDict()

    with open(filename, 'r') as f:

        # only parse non-empty lines and lines that dont start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                # split line and remove whitespace
                fields = line.split(separator)
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract the data fields
                Pr     = fields[0]
                _Ra_F  = fields[1]
                _sep   = fields[2]

                _data  = fields[3:] # list of ['1', '2', ..., '---', '6', '7', ...]
                ind_sep = _data.index("---")
                _Vrms   = _data[:ind_sep]
                _radius = _data[ind_sep+1:]

                # this is a new entry so initialize the data structure
                if (Pr not in data.keys()):
                    data[Pr] = {'Ra_F':[], 'radius':[], 'Vrms':[]}

                # add the data
                data[Pr]['Ra_F'].append(float(_Ra_F))   # list of floats
                data[Pr]['radius'].append(    _radius)  # list of (list of strings)
                data[Pr]['Vrms'].append(      _Vrms)    # list of (list of strings)

    # convert data into np arrays
    for Pr in data.keys():
        data[Pr]['Ra_F'] = np.array(data[Pr]['Ra_F'])

        for k in range(len(data[Pr]['Vrms'])):
            data[Pr]['Vrms'][k] = np.array(data[Pr]['Vrms'][k][:], dtype=np.float64)

        for k in range(len(data[Pr]['radius'])):
            data[Pr]['radius'][k] = np.array(data[Pr]['radius'][k][:], dtype=np.float64)

    if (debug):
        import sys
        for Pr in data.keys():
            print("Pr = {}".format(Pr))
            print("number of Ra_F = {}".format(len(data[Pr]['Ra_F'])))
            for Ra in data[Pr]['Ra_F']:
                print("\t{}".format(Ra))

    return data

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    saveplot = args['--save']
    savefile = args['--output']
    input_files = args['<data-file>']
    dimensioned = args['--dimensioned']

    main(input_files, saveplot, savefile, dimensioned, debug=args['--debug'])

