"""
    Plot the reference state
    This routine reads in a ReferenceState file
    It plots the density, temperature, and density scaleheight.

    The Attributes of the ReferenceState Structure are:
    ----------------------------------
    self.n_r         : number of radial points
    self.radius      : radial coordinates
    self.density     : density
    self.dlnrho      : logarithmic derivative of density
    self.d2lnrho     : d_by_dr of dlnrho
    self.pressure    : pressure
    self.temperature : temperature
    self.dlnt        : logarithmic derivative of temperature
    self.dsdr        : entropy gradient (radial)
    self.entropy     : entropy
    self.gravity     : gravity
    --------------------------------------

Usage:
    plot_reference.py [options]

Options:
    --data-dir=<d>    Specify location of data [default: ./]
    --save            Save an image [default: Fals]
    --output=<o>      Save image as <o> [default: reference.png]
"""
from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ReferenceState
import matplotlib.pyplot as plt

def main(data_dir, saveplot, savefile):

    ref = ReferenceState(filename="reference", path=data_dir)
    plt.figure(1)

    plt.subplot(221)
    plt.plot(ref.radius/ref.radius[1],ref.density)
    plt.xlabel(r'Radius (r/r$_{outer}$)')
    plt.ylabel('Density'+r' g cm$^{-3}$')

    plt.subplot(222)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(ref.radius/ref.radius[1],ref.temperature)
    plt.xlabel(r'Radius (r/r$_{outer}$)')
    plt.ylabel('Temperature (K)')
    plt.twinx()
    plt.plot(ref.radius/ref.radius[1],ref.pressure, color='r')
    plt.ylabel(r'Pressure (dyn cm$^{-2}$)')

    plt.subplot(223)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(ref.radius/ref.radius[1],ref.gravity)
    plt.xlabel(r'Radius (r/r$_{outer}$)')
    plt.ylabel('Gravity '+r'(cm s$^{-2}$)')

    plt.subplot(224)
    plt.plot(ref.radius/ref.radius[1],-1.0/ref.dlnrho*1e-8) # the 1e-8 goes from cm --> Mm
    plt.xlabel(r'Radius (r/r$_{outer}$)')
    plt.ylabel('Density Scale Height (Mm)')
    plt.tight_layout()
    if (saveplot):
        plt.savefig(savefile)
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    save = args['--save']
    output = args['--output']
    data_dir = args['--data-dir']

    main(data_dir, save, output)

