"""
Plots moments as a function of radius from the Shell_Avgs/ datafiles

Usage:
    plot_shell_avgs.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: energy_trace.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --no-plots           Suppress the plots [default: False]
    --first-moment       Also plot the first moment [default: False]
    --quants=<q>         Comma separated list of quantities to use
    --Pm=<p>             Magnetic Prandtl for converting time [default: 1]
    --lut=<l>            Use a different lut.py file
    --list               List the available quantities in the data [default: False]
    --store              Write results to file for future reading [default: False]
    --read=<f>           Read stored results from file [default: ]
    --python             Use pure python diagnostic_reading file [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.import_diagnostics import diagnostics
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import numpy as np
import NumericalToolsLocal.public as NT
from Utilities.look_up_quantity import lut_lookup, nonzero_lut, print_lut
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, first_moment, quants, new_lut, list_lust, Pm,
         store_results, readfile, python_diag, **kwargs):

    # import reading routines
    imports = ["ShellAverage", "build_file_list"]
    diags = diagnostics(imports, python=python_diag, package="ReadRayleigh")
    build_file_list = diags["build_file_list"]
    ShellAverage    = diags["ShellAverage"]

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    if (readfile == ''):
        if (exclude_last == 0):
            exclude_last = None
        else:
            exclude_last *= -1

        path  = os.path.join(data_dir, "Shell_Avgs")
        files = build_file_list(start_file, end_file, path=path)
        files = files[-last_number_files:exclude_last]

        if (quants is None):
            quant_list = None
        else:
            quant_list = list(quants)

        a = ShellAverage(filename=files[0], path='', quantity=quant_list)
        lut = a.lut
        radius = a.radius

        if (list_lut):
            print_lut(lut)
            return

        # compile a time average of all the files
        # data = size(nr, 0:3, nq)
        data, times = time_utils.TimeAvg2(files, ShellAverage, axis=3, quantity=quant_list)

        print("\nFirst file: {}".format(files[0]))
        print("Last  file: {}".format(files[-1]))
        print("\nTotal number of files: {}".format(len(files)))

        # convert to magnetic diffusion time
        times['tstart'] *= Pm
        times['tend']   *= Pm
        times['dt']     *= Pm

    else:
        print("\nReading data from stored file {}".format(readfile))
        T = time_utils.Timer(); T.start()
        try:
            readdata, keys = NT.read_data(readfile)
            data   = readdata['data']
            tstart = readdata['tstart']
            tend   = readdata['tend']
            nrec   = readdata['nrec']
            dt     = readdata['dt']
            lut    = readdata['lut']
            radius = readdata['radius']
            times  = {'tstart':tstart, 'tend':tend, 'dt':dt, 'nrec':nrec}
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()
        T.stop(); dt = T.print_interval()

        if (list_lut):
            print_lut(lut)
            return

    if (store_results):
        output = NT.outputname(savefile, ext=".stored")
        print("\nStoring results to file {}".format(output))

        T = time_utils.Timer(); T.start()
        try:
            kwargs = {'data':data, 'tstart':tstart, 'tend':tend, 'dt':dt, 'nrec':nrec,
                      'radius':radius, 'lut':lut}
            NT.save_data(output, compress=True, **kwargs)
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()
        T.stop(); dt = T.print_interval()

    print("\nStarting time: {}".format(times['tstart']))
    print(  "Ending time  : {}".format(times['tend']))
    print("\nAveraging interval : {}".format(times['dt']))
    print(  "Number of records  : {}".format(times['nrec']))

    if (no_plots):
        return

    print("\nPlotting...\n")
    r_outer = np.max(radius)
    r_inner = np.min(radius)

    nr, _n, nq = np.shape(data)

    if (saveplot):
        fontsize = 16
        dpi = 300
    else:
        fontsize = 14
        dpi = 100
    plt.figure(1,figsize=(7.5, 4.0), dpi=dpi)
    plt.rcParams.update({'font.size': fontsize})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.yscale('log')
    plt.xlabel(r'Radius $(r/r_o)$')
    plt.ylabel('Shell Average')

    nonzero_quantities = nonzero_lut(lut)

    # add mean data
    for q in nonzero_quantities.keys():
        i = lut[q]
        label = nonzero_quantities[q]
        plt.plot(radius/r_outer, data[:,0, i], label=label)
        if (first_moment):
            label += " 1st moment"
            plt.plot(radius/r_outer, data[:,1, i], label=label, linestyle='--')

    if (saveplot):
        legend = plt.legend(loc='best', shadow=True, fontsize = 'large') 
    else:
        legend = plt.legend(loc='best', shadow=True)
    plt.tight_layout()

    # remove the scientific notation offset
    plt.gca().get_xaxis().get_major_formatter().set_scientific(False)

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}\n".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    first_moment = args['--first-moment']
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]
    new_lut = args['--lut']
    list_lut = args['--list']
    Pm = float(args['--Pm'])
    store_results = args['--store']
    readfile = args['--read']
    python_diag = args['--python']

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, first_moment, quants, new_lut, list_lut, Pm, store_results, readfile,
         python_diag)

