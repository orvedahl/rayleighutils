"""
Plot a cut through a meridional slice. This is a radial profile at
a given (theta,phi) coordinate.

Note: theta grid goes from South Pole to North Pole (i.e., it's reversed)

Usage:
    plot_radial_profile.py [options]

Options:
    --timestep=<t>    Specify iteration number to plot [default: 00005000]
    --quantity=<q>    Give quantity code [default: 64]
    --phi-index=<p>   Set the phi index [default: 1]
    --th-index=<t>    Comma separated list of theta indices [default: 10,50,100]
    --no-mean         Remove the m=0 mean [default: False]
    --save            Save an image [default: False]
    --output=<o>      Save image as <o> [default: meridional_slice.png]
    --data-dir=<d>    Specify location of data [default: ./]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import Meridional_Slice
import os
import numpy as np
import matplotlib.pyplot as plt

def main(timestep, quantity_code, pindex, thindex, remove_mean, save, output, data_dir):

    radtodeg = 180.0/np.pi

    # Display the first timestep from the file
    tindex = 0

    path = os.path.join(data_dir, "Meridional_Slices")
    a = Meridional_Slice(filename=timestep, path=path)

    # Set up the grid
    nr = a.nr
    ntheta = a.ntheta
    r = a.radius/np.max(a.radius)

    # shift theta such that theta=0 corresponds to equator and poles are where we expect
    theta = np.arccos(a.costheta)-np.pi/2  

    radius_matrix, theta_matrix = np.meshgrid(r,theta)

    X = radius_matrix * np.cos(theta_matrix)
    Y = radius_matrix * np.sin(theta_matrix)

    qindex = a.lut[quantity_code]
    field = np.zeros((ntheta,nr),dtype='float64')
    field[:,:] =a.vals[pindex,:,:,qindex,tindex]

    #remove the mean if desired
    if (remove_mean):
        for i in range(nr):
            the_mean = np.mean(field[:,i])
            field[:,i] = field[:,i]-the_mean

    print('\nPossible indices:')
    print('\tPhi:')
    for i in range(len(a.phi)):
        print("\t\t{} = {} ({} degrees)".format(i, a.phi[i], a.phi[i]*radtodeg))
    print('\tTheta:')
    print("\t\t0-{} inclusive".format(a.ntheta-1))

    print('\n    Displaying profile at phi   (degrees) = {}'.format(\
           a.phi[pindex]*radtodeg))
    for i, th in enumerate(thindex):
        print('{:2d}) Displaying profile at theta (degrees) = {}'.format(\
           i+1, np.arccos(a.costheta[th])*radtodeg))
    print('Time = ',a.time[tindex])

    # Plot
    plt.clf()
    for th in thindex:
        plt.plot(r,field[th,:],linestyle='-',marker='x',label='theta index = {}'.format(th))
    plt.legend(loc='best')
    plt.xlabel('r/r_o'); plt.ylabel('Quantity')

    if (save):
        plt.savefig(output)
    else:
        plt.show()

if __name__ == "__main__":
    import docopt
    args = docopt.docopt(__doc__)

    timestep = args['--timestep']
    quantity_code = int(args['--quantity'])
    pindex = int(args['--phi-index'])
    thindex = args['--th-index'].split(",")
    for i,th in enumerate(thindex):
        thindex[i] = int(th)
    remove_mean = args['--no-mean']
    save = args['--save']
    output = args['--output']
    data_dir = args['--data-dir']

    main(timestep, quantity_code, pindex, thindex, remove_mean, save, output, data_dir)
