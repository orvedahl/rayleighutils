"""
Plot the Nusselt as a function of time

Usage:
    plot_Nu_time.py [options]

Options:
    --start=<s>      Starting file [default: 0]
    --end=<e>        Ending file [default: 100000000]
    --last=<l>       Use last <l> number of files (so files[-l:] is used) [default: 0]
    --Pm=<p>         Magnetic Prandtl number [default: 1]
    --aspect=<a>     Set aspect ratio [default: 0.4]
    --smooth         Plot the smoothed data [default: False]
    --store          Write results to file for future reading [default: False]
    --read=<f>       Read stored results from file [default: ]
    --save           Save the image [default: False]
    --output=<o>     Save image as <o> [default: Nusselt_time]
    --data-dir=<d>   Specify location of data [default: ./]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ReferenceState, build_file_list, ShellAverage
import matplotlib.pyplot as plt
import Utilities.calculate_Nu
import numpy as np
import sys
from NumericalToolsLocal.fft.signals import smooth as signal_smooth
from NumericalToolsLocal.filesystem.file_io import read_data, save_data
import Utilities.data_structures as data_structures

def main(start_file, end_file, last_number_files, save, output, data_dir, Prm,
         store_data, readfile, verbose=False, smooth=False, aspect_ratio=0.4):

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'

    if (readfile == ''):

        # read reference
        ref = ReferenceState(filename="reference", path=data_dir)
        radius = ref.radius

        # read files
        files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
        print("\nreading data files...")
        for i,f in enumerate(files[-last_number_files:]):
            a = ShellAverage(f,path='')
            if (i != 0):
                data = np.concatenate((data, a.vals), axis=3)
                time = np.concatenate((time, a.time), axis=0)
            else:
                data = np.zeros((a.nr,4,a.nq,a.niter),dtype='float64')
                time = a.time

        time *= Prm # convert to magnetic diffusion times

        if (verbose):
            print("\nFirst file: {}".format(files[-last_number_files:][0]))
            print("Last  file: {}".format(files[-last_number_files:][-1]))
            print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
            print("\nFirst time: {} secs".format(time[0]))
            print("Last  time: {} secs".format(time[-1]))

    else:
        print("\nReading data from stored file {}".format(readfile))
        try:
            readdata = read_data(readfile, all_keys=True)
            data = readdata['data']
            time = readdata['time']
            radius = readdata['radius']
            lut = list(readdata['lut'])
            print("\t...success")
        except:
            print("\t...FAILED")
            sys.exit()

        ref = data_structures.Empty(radius=radius)
        a = data_structures.Empty(lut=lut)

        if (verbose):
            print("\nFirst time: {} secs".format(time[0]))
            print("Last  time: {} secs".format(time[-1]))

    if (store_data):
        savefile = output + ".Nu_time_stored" 
        print("\nStoring results to file {}".format(savefile))

        try:
            save_data(savefile, compress=True,
                      data=data, time=time, radius=radius, lut=np.array(a.lut))
            print("\t...success")
        except:
            print("\t...FAILED")

    # get nusselt number as a function of time
    Nu = np.zeros_like(time)
    print("\ncalculating Nusselt...")
    for j in range(len(time)):
        Nu[j] = calculate_Nu.main(start_file, end_file, last_number_files, False,
                                  "", data_dir, return_Nu=True, verbose=False,
                                  data=data[:,:,:,j], ref_state=ref, lut=a.lut,
                                  aspect_ratio=aspect_ratio)

    # make plot
    print("\nmaking plot...")
    plt.clf()
    plt.plot(time, Nu, linestyle='-', color='b', label='Nu')
    if (smooth):
        wlength = int(0.05*len(time)); win = 'blackman'
        time_s = signal_smooth(time, window_length=wlength, window=win)
        Nu_s   = signal_smooth(Nu,   window_length=wlength, window=win)
        plt.plot(time_s, Nu_s, linestyle='-', color='r', label='smoothed Nu')
        plt.legend(loc='upper right')
    plt.xlabel("Magnetic Diffusion Time")
    plt.ylabel(r"$\mathrm{Nu}$")

    if (save):
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    store_data = args['--store']
    readfile = args['--read']
    main(int(args['--start']), int(args['--end']), int(args['--last']),
             args['--save'], args['--output'], args['--data-dir'],
             float(args['--Pm']), store_data, readfile, smooth=args['--smooth'],
             aspect_ratio=float(args['--aspect']))

