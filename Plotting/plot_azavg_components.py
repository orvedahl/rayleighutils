"""
Plot grid of azimuthal averages

Usage:
    plot_azavg_components.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: az_average.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-mean         Remove the mean [default: False]
    --Pm=<p>          Set Magnetic Prandtl for displaying time [default: 1]
    --single-time     Do not do an average over time [default: False]
    --magnetic        Plot the Magnetic field instead of the velocity [default: False]
    --list            List the available quanitites [default: False]
"""
from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
import matplotlib.pyplot as plt
import os
import sys
import numpy as np
from Utilities import azavg_util, time_utils
from Utilities.look_up_quantity import general_lookup, find_available

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_mean,
         list_contents, Pm, single_time, magnetic):

    # build file list
    data_dir = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # read first file to get meta data
    az = AzAverage(filename=files[0], path='')
    nr = az.nr
    nth = az.ntheta
    radius = az.radius
    sintheta = az.sintheta
    costheta = az.costheta
    #costheta = np.cos(np.arcsin(sintheta))
    #costheta[0:nth/2] *= -1.0

    if (list_contents):
        vals = find_available(az.lut)
        print("\nFound the following values:")
        for i in vals:
            print("\t{:3d} --- {}".format(i, general_lookup(index=i)))
        print()
        sys.exit()

    # do time average, data is shape (nth, nr, nq)
    if (not single_time):
        print("\n...doing average...")
        data, avgtimes = time_utils.TimeAvg(files, AzAverage, axis=-1, data_attr="vals")
    else:
        print("\n...choosing specific time...")
        tind = 0
        data = az.vals[:,:,:,tind]
        avgtimes = {"dt":0, "tstart":az.time[tind], "tend":az.time[tind], "nrec":az.niter}

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(Pm*avgtimes["tstart"]))
    print("Last  time: {} sec".format(Pm*avgtimes["tend"]))
    print("\nAveraged over {} sec".format(Pm*avgtimes["dt"]))
    print("Number of records {}".format(avgtimes["nrec"]))

    time = 0.5*Pm*(avgtimes["tstart"] + avgtimes["tend"])

    quantity = data[:,:,:]; nq = np.shape(quantity)[2]

    # remove the \ell=0 bit from all quantities
    if (no_mean):
        print("\n...removing \ell=0 from all quanitites...")
        for j in range(nq):
            for i in range(nr):
                quantity[:,i,j] -= np.mean(quantity[:,i,j])

    print("\n...setting up figure...")
    if (saveplot):
        fig = plt.figure(figsize=(8.5, 11.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        fig = plt.figure(figsize=(8.5, 11.0), dpi=80)
        plt.rcParams.update({'font.size': 14})

    main_ax = fig.add_subplot(1,1,1)
    main_ax.axis('off')

    tsize = 20
    lsize = 15

    if (magnetic):
        print("\n...plotting the B field...")
        qcodes = [401, 402, 403, -1]
        titles = [r'$B_r$', r'$B_\theta$', r'$B_\phi$', r'$|B|$']
    else:
        print("\n...plotting the velocity field...")
        qcodes = [1, 2, 3, -1]
        titles = [r'$v_r$', r'$v_\theta$', r'$v_\phi$', r'$|v|$']

    suptitle = r"$t/t_\eta \sim {{{:.2f}}}$".format(time)

    units = ''
    btype = ['rms', 'rms', 'rms', 'minmax']
    cmap = ['RdBu_r', 'RdBu_r', 'RdBu_r', 'plasma']
    for i, qcode in enumerate(qcodes):
        A = fig.add_subplot(2,2,i+1)

        if (i != 3):
            # specific components
            q = az.lut[qcode]
            plot_data = quantity[:,:,q]
        else:
            # calculate magnitude
            q1 = az.lut[qcodes[0]] # radial
            q2 = az.lut[qcodes[1]] # theta
            q3 = az.lut[qcodes[2]] # phi

            plot_data = quantity[:,:,q1]**2 + quantity[:,:,q2]**2 + quantity[:,:,q3]**2
            plot_data = np.sqrt(plot_data)

        azavg_util.plot_azav(fig, A, plot_data, radius, costheta, sintheta, r_bcz=0,
                         mycmap=cmap[i], cbar=True, orientation='vertical',
                         boundsfactor=1.5, boundstype=btype[i],
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)

        plt.title(titles[i], fontsize=tsize)

    plt.suptitle(suptitle, fontsize=tsize)

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_mean = args['--no-mean']
    list_contents = args['--list']
    Pm = float(args['--Pm'])
    single_time = args['--single-time']
    magnetic = args['--magnetic']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_mean,
         list_contents, Pm, single_time, magnetic)

