"""
Read Br, Bth, Bphi from Shell Spectra to determine various quantities.

    1) dipolarity, how much l=1 is there?
    2) magnetic energy of specific l values

Usage:
    plot_shell_slice.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o>.png [default: B_field_spectra]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-plots        Suppress the plots [default: False]
    --rad-ind=<r>     Radial index, or "max", "min", "2nd-small", "2nd-large" [default: 0]
    --power-ind=<p>   Plot the total(0), m=0(1), or the m/=0(2) power [default: 0]
    --Pm=<p>          Magnetic Prandtl number for time axis [default: 1]
    --Pr=<p>          Prandtl number for converting time axis [default: 1]
    --no-fix-time     Don't adjust time for saw-tooth effect [default: False]
    --store           Write results to a file for future reading [default: False]
    --read=<f>        Read stored results from file [default: ]
    --convert-time    After reading, convert time to magnetic diff times [default: False]
    --start-plot=<s>  Start plotting data starting with <s> %, e.g. 0.05 [default: 0]
    --end-plot=<e>    Stop plotting data ending with <e> %, e.g. 0.95 [default: 1]
    --debug           Debug mode [default: False]
    --lut=<l>         Use a different lut.py file
"""

from __future__ import print_function
import numpy as np
import env
from ReadRayleigh.diagnostic_reading import build_file_list, ShellSpectra
import matplotlib.colors as colors
import matplotlib.pyplot as plt
from Utilities.look_up_quantity import shortcut_lookup
from Utilities.loop_progress import print_progress
from Utilities.spectral_utils import mag_sq, calc_axisymmetric_energy, calc_total_energy
from Utilities.spectral_utils import calc_dipole_energy, calc_quadrupole_energy
from Utilities.spectral_utils import calc_ell_energy
from Utilities.indices import find_index
from NumericalToolsLocal.fft.signals import smooth
from NumericalToolsLocal.filesystem.file_io import outputname, save_data, read_data
import NumericalToolsLocal.public as NT
from plot_magnetic_northpole import get_Poloidal, dipole_components
import time as timer
import Utilities.time_utils as time_utils
import sys

def main(start_file, end_file, last_number_files, saveplot, savefile,
         data_dir, no_plots, radial_ind, Pr, Prm, no_fix_time,
         store_results, readfile, debug, convert_to_mag_diff_time,
         start_plot, end_plot, new_lut, p_ind, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    # set some indices
    l1_ind = 1; l2_ind = 2; l3_ind = 3; l4_ind = 4; l5_ind = 5
    m0_ind = 0; m1_ind = 1; m2_ind = 2

    # read all the binary files and compile the data
    if (readfile == ''):
        if (data_dir[-1] != '/'):
            data_dir = data_dir + '/'

        stime = timer.time()

        # build file list
        files = build_file_list(start_file, end_file, path=data_dir+'Shell_Spectra')

        ntotal_recs=0
        files = files[-last_number_files:]
        ntotal = len(files)
        print("\nProcessing {} files...".format(ntotal))

        # only need B-field, so only ask for B-field
        #quant_list = [801,802,803]
        quant_list = [shortcut_lookup('br'), shortcut_lookup('bt'), shortcut_lookup('bp')]

        # read first file for lut, grid and other meta data
        data = ShellSpectra(files[0], path='', quantity=quant_list) # size [l, m, r, q, t]
        br_ind = data.lut[shortcut_lookup('br')]
        bt_ind = data.lut[shortcut_lookup('bt')]
        bp_ind = data.lut[shortcut_lookup('bp')]

        # construct time series of all files
        a, time = time_utils.TimeSeries(files, ShellSpectra, axis=-1, quantity=quant_list)

        if (radial_ind in ["max", "min", "2nd-small", "2nd-large"]):
            radial_ind = find_index(data.radius, index=radial_ind)
        else:
            radial_ind = int(radial_ind)

        # extract data of interest
        br   = a[:,:,radial_ind, br_ind, :] # size [l,m,t]
        bth  = a[:,:,radial_ind, bt_ind, :]
        bphi = a[:,:,radial_ind, bp_ind, :]

        print() # this resets the print_progress stuff
        etime = timer.time(); dt = etime - stime
        print("Elapsed time = {} sec, {} min".format(dt, dt/60.))

        time = np.array(time)
        lmax = data.lmax; nell = data.nell
        mmax = data.mmax; nm = data.nm
        radius = data.radius[radial_ind]  # specific radius

        # adjust the time so that it is monotonically increasing
        if (not no_fix_time):
            print("\nadjusting time to fix \"saw-tooth\" effect")

            ntime = len(time)
            indices = []; indices.append(0) # use index=0 as starting point
            for i in range(1,len(time)):    # find the breaks in time and
                if (time[i] < time[i-1]):   # the corresponding indices
                    indices.append(i)       # index: | 0 | 1 | 2 | X | 4 |
                                            # slice: 0   1   2   X   4   5
                                            # if X==i, X-1==i-1, then slice
                                            # is X, but adjustment time is X-1
            nindices = len(indices)
            told = np.copy(time)
            for i in range(1,nindices):     # skip index=0 element
                s = indices[i]              # slice is X
                toffset = time[s-1]         # adjustment time is X-1
                if (i != nindices-1):       # manage the last slice separately
                    e = indices[i+1]
                else:
                    e = ntime               # this is ntime, not n+1 or n-1
                                            # which corresponds to the
                time[s:e] += toffset        # absence of "s:e+1" in favor of "s:e"
                                            # to avoid double counting time[e]

            if (nindices != 1):
                print("\tfound {} breaks in time".format(nindices-1)) # don't count 0 index
            else:
                print("\tfound no breaks in time")
            if (debug):
                plt.clf()
                plt.plot(told, linestyle='-', color='r', label='Old Time', marker='x')
                plt.plot(time, linestyle='-', color='b', label='Fixed Time', marker='x')
                plt.ylabel("time")
                plt.xlabel("index")
                plt.title("time")
                plt.legend()
                plt.show()

        # time comes out in viscous time, convert to magnetic time
        time *= Prm

        print("\nTotal number of files: {}".format(ntotal))

    # read results from file
    else:
        print("\nReading data from stored file {}".format(readfile))
        T = time_utils.Timer(); T.start()
        try:
            readdata, keys = read_data(readfile) # read data/extract pieces
            radius = readdata['radius']
            lmax   = readdata['lmax']
            nell   = readdata['nell']
            mmax   = readdata['mmax']
            nm     = readdata['nm']
            time   = readdata['time']
            br     = readdata['Br']
            bth    = readdata['Bth']
            bphi   = readdata['Bphi']
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()
        T.stop(); dt = T.print_interval()

        if (convert_to_mag_diff_time):
            print("\tconverting to magnetic diffusion time\n")
            time *= Prm

    if (store_results):
        output = savefile+".mag_spectra_stored"
        print("\nStoring results to file {}".format(output))

        # save multiple arrays to single file
        try:
            save_data(output, compress=True,
                      time=time, Br=br, Bth=bth, Bphi=bphi,
                      radius=radius, lmax=lmax, nell=nell, mmax=mmax, nm=nm)
            print("\t...success")
        except:
            print("\t...FAILED\n")

    print("\nFirst time: {} simulation-secs".format(time[0]))
    print("Last  time: {} simulation-secs".format(time[-1]))
    print("\nTotal time: {} simulation-secs".format(time[-1]-time[0]))

    print("\nRadius: {}".format(radius))

    P = get_Poloidal(br[l1_ind,:,:], radius, ell=1) # size [m, t]
    method = 1
    dipoles = dipole_components(P, m0_ind, m1_ind, method=method)
    x_dipole = dipoles[0]; y_dipole = dipoles[1]; z_dipole = dipoles[2]
    total_dipole = dipoles[3]; horiz_dipole = dipoles[4]

    # get spherical coordinates of dipole as func of time
    theta = np.arccos(z_dipole/total_dipole)
    phi   = np.arccos(x_dipole/horiz_dipole)
    latitude = 0.5*np.pi - theta

    # calculate the dipolarity
    #   f_dipole**2 = sum_m B(l=1)**2  /  sum_l sum_m B(l,m)**2
    dipole_energy = calc_dipole_energy(br[l1_ind,:,:], bth[l1_ind,:,:], bphi[l1_ind,:,:],
                                       m0_ind, m1_ind) # size [t]

    lmax = None
    lmax = 13
    total_energy = calc_total_energy(br[:,:,:], bth[:,:,:], bphi[:,:,:], lmax=lmax) # size [t]

    f_dipole = np.sqrt(dipole_energy/total_energy)  # size [t]

    # other energies
    quad_E = calc_quadrupole_energy(br[l2_ind,:,:], bth[l2_ind,:,:], bphi[l2_ind,:,:],
                                    m0_ind, m1_ind, m2_ind)
    other_energy = total_energy - dipole_energy - quad_E

    axisymmetric_energy = calc_axisymmetric_energy(br[:,m0_ind,:], bth[:,m0_ind,:],
                                                   bphi[:,m0_ind,:])
    non_axisymmetric_energy = total_energy - axisymmetric_energy

    s = int(start_plot*len(time))
    e = int(  end_plot*len(time))
    print("\nAverage Values")
    print("\n\tFirst time: {} simulation-secs".format(time[s]))
    print("\tLast  time: {} simulation-secs".format(time[e-1])) # e-1 because of slicing
    print("\nAverage f-dipole = {}".format(np.mean(f_dipole[s:e])))
    print("\tused l_max = {}".format(lmax))
    Axisym = np.mean(axisymmetric_energy[s:e])
    NonAxi = np.mean(non_axisymmetric_energy[s:e])
    print("\nAverage axisymmetric energy = {}".format(Axisym))
    print("Average non-axisymmetric energy = {}".format(NonAxi))
    print("\nAverage dipole energy = {}".format(np.mean(dipole_energy[s:e])))
    print("Average quadrupole energy = {}".format(np.mean(quad_E[s:e])))
    print("Average other pole energy = {}".format(np.mean(other_energy[s:e])))
    print("Average total energy = {}".format(np.mean(total_energy[s:e])))

    if (no_plots):
        return

    # f-dipole
    plt.clf()
    plt.plot(time[s:e], f_dipole[s:e], linestyle='-', color='k', label='Dipolarity')
    plt.ylim(-0.1, 1.1)
    plt.xlabel("Magnetic Diffusion Time")
    plt.ylabel("Dipolarity")

    if (saveplot):
        output = outputname(savefile, insert="_dipolarity")
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    # energies
    norm = np.amax(total_energy[s:e])
    plt.clf()
    #plt.plot(time[s:e], other_energy[s:e]/norm, linestyle='-', color='g', label='Other Poles')
    #plt.plot(time[s:e], quad_E[s:e]/norm, linestyle='-', color='b', label='Quadrupole')
    #plt.plot(time[s:e], dipole_energy[s:e]/norm, linestyle='-', color='r', label='Dipole')
    plt.plot(time[s:e], total_energy[s:e]/norm, linestyle='-', color='k', label='Total')
    indices = [l1_ind, l2_ind, l3_ind, l4_ind, l5_ind]
    other_energy = total_energy
    for ell in range(len(indices)):
        l_ind = indices[ell]
        E = calc_ell_energy(br[l_ind,:,:], bth[l_ind,:,:], bphi[l_ind,:,:],
                            m0_ind, *indices[:ell+1])
        plt.plot(time[s:e], E[s:e]/norm, linestyle='-', label=r'$\ell = {{{}}}$'.format(ell+1))
        other_energy -= E
    plt.plot(time[s:e], other_energy[s:e]/norm, linestyle='-', label='Other Poles')
    plt.legend(loc='lower right', fontsize='x-large')
    plt.xlabel("Magnetic Diffusion Time", fontsize=14)
    plt.ylabel("Magnetic Energy", fontsize=14)
    plt.yscale('log')

    if (saveplot):
        output = outputname(savefile, insert="_energy")
        plt.savefig(output, dpi=300)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    # make a power spectrum as a function of time for Br
    plt.clf()

    # power should be size [l,t]
    print("\n...calculating the power")
    T = time_utils.Timer(); T.start()
    nl, nm, nt = np.shape(br)
    power = np.zeros((nl, nt, 3))
    for t in range(nt):
        # m=0
        power[:,t,1] += np.real(br[:,0,t])**2 + np.imag(br[:,0,t])**2

        # m!=0
        for m in range(1,nm):
            power[:,t,2] += np.real(br[:,m,t])**2 + np.imag(br[:,m,t])**2

        power[:,t,0] = power[:,t,2] + power[:,t,1] # total power
    T.stop(); dt = T.print_interval()

    ell = np.arange(nl)
    l_ind = 1
    X, Y = np.meshgrid(time[s:e], ell[l_ind:])
    print("\n...plotting")

    #cmap = 'RdBu_r'  # this is a diverging colormap
    cmap = 'plasma'  # this one is sequential
    #cmap = 'inferno' # this one is sequential
    #cmap = 'magma'   # this one is sequential

    #p_ind = 0 # choose a particular power to plot, 0=total, 1=(m=0), 2=(m/=0)
    Power = power[l_ind:,:,p_ind]

    vmin = np.amin(Power); vmax = np.amax(Power)
    print("\tmin/max Power(ell,t) = {}/{}".format(vmin,vmax))
    linthresh=1e-2*np.amax(np.abs(Power))
    norm = colors.SymLogNorm(linthresh=linthresh, linscale=0.5, vmin=vmin, vmax=vmax)
    img = plt.pcolormesh(X, Y, Power, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm)
    colorbar = plt.colorbar(img)

    plt.yscale('log')

    plt.xlabel("Magnetic Diffusion Time", fontsize=14)
    plt.ylabel(r"Spherical Harmonic Degree, $\ell$", fontsize=14)

    plt.title("Power in Br at r = {}".format(radius))

    plt.xlim(np.amin(time), np.amax(time))
    plt.ylim(np.amin(ell[l_ind:]), np.amax(ell[l_ind:]))

    if (saveplot):
        output = outputname(savefile, insert="_Br_power")
        plt.savefig(output, dpi=300)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    plt.rcParams.update({'font.size':16})
    plt.clf()
    last_ind = -1
    #cmap = 'gnuplot2'
    fracs = [0.15, 0.25, 0.33, 0.5, 0.66, 0.75, 0.85]
    for i,f in enumerate(fracs):
        ind = int(nt*f)
        cind = (i+1) / len(fracs)
        plt.plot(ell[l_ind:last_ind], Power[:last_ind,ind],
                 #color=NT.get_color(cind, cmap=cmap),
                 label='t = {:.3f}'.format(time[ind]))
    #plt.legend(loc='best')
    #plt.xlabel(r"Spherical Harmonic Degree, $\ell$", fontsize=14)
    #plt.ylabel("Power in Br at r = {}".format(radius), fontsize=14)
    plt.legend(loc='lower left', shadow=True, ncol=1, fontsize='large')
    plt.xlabel(r"Spherical Harmonic Degree, $\ell$")
    plt.ylabel(r"Power in $B_r$ at Outer Boundary")
    plt.xscale('log'); plt.yscale('log')
    plt.xlim(np.amin(ell[l_ind:]), np.amax(2*ell[l_ind:]))

    if (saveplot):
        output = outputname(savefile, insert="_Br_power_cuts")
        plt.savefig(output, dpi=300)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    plt.rcParams.update({'font.size':16})
    plt.clf()
    for l in [0,1,2,3]:
        plt.plot(time, Power[l,:], label='$\ell = {{{}}}$'.format(ell[l]+1))
    plt.legend(loc='lower left', shadow=True, ncol=1, fontsize='large')
    plt.xlabel(r"Magnetic Diffusion Time")
    plt.ylabel(r"Power in $B_r$ at Outer Boundary")
    plt.yscale('log')
    plt.xlim(np.amin(time), np.amax(time))

    if (saveplot):
        output = outputname(savefile, insert="_Br_power_ell_cuts")
        plt.savefig(output, dpi=300)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file  = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    radial_ind = args['--rad-ind']
    p_ind = int(args['--power-ind'])
    Pr  = float(args['--Pr'])
    Prm = float(args['--Pm'])
    no_fix_time = args['--no-fix-time']
    store_results = args['--store']
    readfile = args['--read']
    debug = args['--debug']
    convert_time = args['--convert-time']
    start_plot = float(args['--start-plot'])
    end_plot   = float(args['--end-plot'])
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, saveplot, savefile,
         data_dir, no_plots, radial_ind, Pr, Prm, no_fix_time,
         store_results, readfile, debug, convert_time, start_plot, end_plot, new_lut, p_ind)

