"""

  Global-Averages (G_Avgs) plotting example
  Reads in time steps 2.7 million through 4 million
  Plots average KE vs time

  This example routine makes use of the GlobalAverage
  data structure associated with the G_Avg output.
  Upon initializing a GlobalAverage object, the 
  object will contain the following attributes:

    ----------------------------------
    self.niter                  : number of time steps
    self.nq                     : number of diagnostic quantities output
    self.qv[0:nq-1]             : quantity codes for the diagnostics output
    self.vals[0:niter-1,0:nq-1] : Globally averaged diagnostics as function of time and quantity index
    self.iters[0:niter-1]       : The time step numbers stored in this output file
    self.time[0:niter-1]        : The simulation time corresponding to each time step
    self.lut                    : Lookup table for the different diagnostics output

Usage:
    plot_energy_trace.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --kappa=<k>     Thermal diffusivity in cm**2/sec [default: 2e12]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_trace.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import GlobalAverage, build_file_list, ReferenceState
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, kappa, no_plots, **kwargs):
    # Set saveplot to True to save to a .png file. 
    # Set to False to view plots interactively on-screen.
    #saveplot = False
    #savefile = 'energy_trace.pdf'  #Change .pdf to .png if pdf conversion gives issues

    # integration method
    method = 'cheb-end'

    # Build a list of all files ranging from iteration 0 million to 10 million
    #files = build_file_list(0,10000000,path='G_Avgs')
    if (data_dir[-1] != "/"):
        data_dir = data_dir + '/'
    files = build_file_list(start_file,end_file,path=data_dir+'G_Avgs')
    #The indices associated with our various outputs are stored in a lookup table
    #as part of the GlobalAverage data structure.  We define several variables to
    #hold those indices here:
    a = GlobalAverage(filename=files[0],path='')  # read a file to get the lookup table
    ke_index  = a.lut[shortcut_lookup('ke')]   #a.lut[125]  # Kinetic Energy (KE)
    rke_index = a.lut[shortcut_lookup('rke')]  #a.lut[126]  # KE associated with radial motion
    tke_index = a.lut[shortcut_lookup('tke')]  #a.lut[127]  # KE associated with theta motion
    pke_index = a.lut[shortcut_lookup('pke')]  #a.lut[128]  # KE associated with azimuthal motion

    #We also grab some energies associated with the mean (m=0) motions
    mrke_index = a.lut[shortcut_lookup('mrke')]  #a.lut[130]  # KE associated with mean radial motion
    mtke_index = a.lut[shortcut_lookup('mtke')]  #a.lut[131]  # KE associated with mean theta motion
    mpke_index = a.lut[shortcut_lookup('mpke')]  #a.lut[132]  # KE associated with mean azimuthal motion

    #Next, we intialize some empy lists to hold those variables
    ke = []
    rke = []
    tke = []
    pke = []

    mrke = []
    mtke = []
    mpke = []

    # We also define an empty list to hold the time.  This time
    # will be absolute time since the beginning of the run, 
    # not since the beginning of the first output file.  
    # In this case, the run was dimensional, so the time is recorded 
    # in seconds.  We will change that to days as we go.
    alldays = []

    # Next, we loop over all files and grab the desired data
    # from the file, storing it in appropriate lists as we go.
    for f in files[-last_number_files:]:
        a = GlobalAverage(filename=f,path='')


        days = a.time/(3600.0*24.0)
        for i,d in enumerate(days):
            alldays.append(d)
            ke.append(a.vals[i,ke_index])
            rke.append(a.vals[i,rke_index])
            tke.append(a.vals[i,tke_index])
            pke.append(a.vals[i,pke_index])

            mrke.append(a.vals[i,mrke_index])
            mtke.append(a.vals[i,mtke_index])
            mpke.append(a.vals[i,mpke_index])

    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))

    print("\nFirst time: {} days".format(alldays[0]))
    print("Last  time: {} days".format(alldays[-1]))
    print("\nTotal time: {} days".format(alldays[-1]-alldays[0]))

    # need to get the grid data so read the reference state
    reference = ReferenceState(filename="reference", path=data_dir)
    r_outer = np.max(reference.radius)
    r_inner = np.min(reference.radius)
    H = r_outer - r_inner
    volume = 4*np.pi*my_integrator(reference.radius**2, reference.radius, method=method, data_reversed=True)
    # get mass by integrating the density of the shell
    mass = 4*np.pi*my_integrator(reference.density*reference.radius**2, reference.radius, method=method, data_reversed=True)
    KE_factor = H*H/kappa/kappa/mass
    mean_KE_density = np.mean(ke)
    mean_KE_total = mean_KE_density*volume     # dimensional KE, [KE] = ergs
    mean_KE_total_ND = mean_KE_total*KE_factor # nondimensional KE

    print("\nAvg total KE density [erg cm^-3] : {}".format(mean_KE_density))
    print("Avg total KE [erg]               : {}".format(mean_KE_total))
    print("Avg total KE nondimensional      : {}\n".format(mean_KE_total_ND))

    #Create two plots.
    # Plot 1:  Total KE and its breakdown.
    # Plot 2:  Breakdown of the mean KE
    if (no_plots):
        return

    #Changing the plotting parameter somewhat depending on saveplot
    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(15,5),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(121)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alldays,ke,'black', label = 'KE'+r'$_{total}$')
    plt.plot(alldays,rke,'r', label = 'KE'+r'$_{rad}$')
    plt.plot(alldays,tke,'g', label = 'KE'+r'$_\theta$')
    plt.plot(alldays,pke,'b', label = 'KE'+r'$_\phi$')
    plt.yscale('log')
    plt.xlabel('Time (days)')
    plt.ylabel('Energy Density '+r'( erg cm$^{-3}$)')
    if (saveplot):
        legend = plt.legend(loc='upper right', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='upper right', shadow=True, ncol = 2) 

    t1 = r'$\frac{1}{2}\rho$'
    plt.subplot(122)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alldays,mrke,'r', label = t1+r'$<v^2_r>$')
    plt.plot(alldays,mtke,'g', label = t1+r'$<v^2_\theta>$')
    plt.plot(alldays,mpke,'b', label = t1+r'$<v^2_\phi>$')
    plt.yscale('log')
    plt.ylim([1,1e7])
    plt.xlabel('Time (days)')
    plt.ylabel('Energy Density '+r'( erg cm$^{-3}$)')
    if (saveplot):
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2) 
    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    kappa = float(args['--kappa'])
    no_plots = args['--no-plots']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, kappa, no_plots)

