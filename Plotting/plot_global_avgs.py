"""
Plot global average vs time

Usage:
    plot_global_avgs.py [options] <quants>...

    <quants>   space separated list of quantity codes to plot

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: energy_trace.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --logx               Log scale the x-axis [default: False]
    --logy               Log scale the y-axis [default: False]
    --list               List available quantities [default: False]
    --lut=<l>            Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import GlobalAverage, build_file_list, ReferenceState
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup, general_lookup, find_available, lut_lookup
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, quants, logx, logy, list_contents, new_lut, **kwargs):

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    path = os.path.join(data_dir, "G_Avgs")
    files = build_file_list(start_file, end_file, path=path)

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    if (list_contents):
        a = GlobalAverage(filename=files[0], path='') # no quantity=list, want all of them
        vals = find_available(a.lut)
        print("\nAvailable quantities are:\n")
        for i in vals:
            print("\t{} --> {:<15s}".format(i, general_lookup(index=i)))
        print()
        return

    quant_list = list(quants)
    a = GlobalAverage(filename=files[0],path='',quantity=quant_list)

    # compile a time series of all the files
    data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                       GlobalAverage, axis=0, quantity=quant_list)
    alltime = Pm*np.array(time)

    print("\nFirst file: {}".format(files[-last_number_files:exclude_last][0]))
    print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    if (no_plots):
        return

    #Changing the plotting parameter somewhat depending on saveplot
    if (saveplot):
        plt.figure(1,figsize=(8.5, 4), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(8.5, 4), dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    for q in quant_list:
        data_values = data[:,a.lut[q]]
        l = lut_lookup(q)
        plt.plot(alltime, data_values, label=l)
    if (logx):
        plt.xscale('log')
    if (logy):
        plt.yscale('log')
    plt.xlabel('Time (simulation secs)')
    plt.ylabel('Quantity Value')
    if (saveplot):
        legend = plt.legend(loc='best', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    logx = args['--logx']
    logy = args['--logy']
    list_contents = args['--list']
    new_lut = args['--lut']

    quants = args['<quants>']
    quants = [int(a) for a in quants]

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, quants, logx, logy, list_contents, new_lut)

