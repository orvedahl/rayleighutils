"""
Plot grid of azimuthal averages

Usage:
    plot_azavg.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: az_average.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-mean         Remove the mean [default: False]
    --comp=<r>        What component to plot, 'r', 't', 'p' [default: r]
"""
from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
import matplotlib.pyplot as plt
import os
import numpy as np
from Utilities import azavg_util, time_utils
from Utilities.look_up_quantity import general_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         comp, no_mean):

    # build file list
    data_dir = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # read first file to get meta data
    az = AzAverage(filename=files[0], path='')
    nr = az.nr
    nth = az.ntheta
    radius = az.radius
    sintheta = az.sintheta
    costheta = az.costheta
    #costheta = np.cos(np.arcsin(sintheta))
    #costheta[0:nth/2] *= -1.0

    # do time average, data is shape (nth, nr, nq)
    if (True):
        print("\n...doing average...")
        data, avgtimes = time_utils.TimeAvg(files, AzAverage, axis=-1, data_attr="vals")
    else:
        print("\n...choosing specific time...")
        tind = 0
        data = az.vals[:,:,:,tind]
        avgtimes = {"dt":0, "tstart":az.time[tind], "tend":az.time[tind], "nrec":az.niter}

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(avgtimes["tstart"]))
    print("Last  time: {} sec".format(avgtimes["tend"]))
    print("\nAveraged over {} sec".format(avgtimes["dt"]))
    print("Number of records {}".format(avgtimes["nrec"]))

    quantity = data[:,:,:]; nq = np.shape(quantity)[2]

    # remove the \ell=0 bit from all quantities
    if (no_mean):
        for q in range(nq):
            for i in range(nr):
                quantity[:,i,j] -= np.mean(quantity[:,i,j])

    # setup the figure
    if (saveplot):
        fig = plt.figure(figsize=(8, 8.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        fig = plt.figure(figsize=(8, 8.0), dpi=80)
        plt.rcParams.update({'font.size': 14})

    tsize = 20
    lsize = 15

    if (comp.lower()[0] == 'r'):
        qcodes = [523, 521, 566, 64] # laplacian bits are wrong, just plot entropy
        titles = [r"$-u \cdot\nabla B$",
                  r"$B \cdot\nabla u$",
                  r"$\nabla\times\left(u^\prime\times B^\prime\right)$",
                  r"$T$"]
        bigtitle = "Radial Components"

    elif (comp.lower()[0] == 't'):
        qcodes = [528, 526, 570, 64]
        titles = [r"$-u \cdot\nabla B$",
                  r"$B \cdot\nabla u$",
                  r"$\nabla\times\left(u^\prime\times B^\prime\right)$",
                  r"$T$"]
        bigtitle = "Theta Components"

    elif (comp.lower()[0] == 'p'):
        qcodes = [533, 531, 574, 64]
        titles = [r"$-u \cdot\nabla B$",
                  r"$B \cdot\nabla u$",
                  r"$\nabla\times\left(u^\prime\times B^\prime\right)$",
                  r"$T$"]
        bigtitle = "Phi Components"

    else:
        raise IndexError("unknown component, comp = {}".format(comp))

    units = ''
    for i, title in enumerate(titles):
        axis = fig.add_subplot(2,2,i+1)

        q = az.lut[qcodes[i]]
        azavg_util.plot_azav(fig, axis, quantity[:,:,q], radius, costheta, sintheta, r_bcz=0,
                         mycmap='RdBu_r', cbar=True, orientation='vertical',
                         boundsfactor=1.5, boundstype='rms',
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)

        plt.title(title, fontsize=tsize)

    plt.suptitle(bigtitle, fontsize=tsize)

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    comp = args['--comp']
    no_mean = args['--no-mean']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         comp, no_mean)

