"""
Usage:
    plot_fluxes.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save           Save an image [default: False]
    --output=<o>     Save images as <o> [default: fluxes.png]
    --data-dir=<d>   Specify location of data [default: ./]
    --no-plots       Suppress the plots [default: False]
    --alt            Use alternate look up table [default: False]
    --quants=<q>     Comma separated list of indices to plot [default: 1,2,3]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list
import matplotlib.pyplot as plt
import numpy as np
import os
from Utilities.look_up_quantity import general_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, quantities, alt, **kwargs):

    #Time-average the data generated between timesteps 0 million and 10 million
    # Note that this averaging scheme assumes taht the radial grid didn't
    # change during the course of the run. 
    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
    icount = 0.0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellAverage(f,path='')
        if (i == 0):
            if (not alt):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
            else:
                data = np.zeros((a.nr,a.nq),dtype='float64')
            t_start = a.time[0]
        for j in range(a.niter):
            if (not alt):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
            else:
                data[:,:] = data[:,:]+a.vals[:,:,j]
            icount = icount+1.0
        t_end = a.time[-1]

    delta_time = (t_end - t_start)
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} secs".format(t_start/3600./24.))
    print("Last  time: {} secs".format(t_end/3600./24.))
    print("\nAveraged over {} secs".format(delta_time))

    data = data/icount  # The average is now stored in data
    radius = a.radius
    fpr = 4.0*np.pi*radius*radius  # four pi r^2
    rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1

    #Create the plot

    lw = 1.5

    if (saveplot):
        plt.figure(1,figsize=(7.0, 5.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7,5),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    for q in quantities:
        index = a.lut[q] # giving it the index, so don't need to lookup table
        if (not alt):
            quantity = data[:,0,index]
        else:
            quantity = data[:,index]

        plt.plot(rnorm,quantity,label = 'index={:d}'.format(q),
                 linewidth=lw)

    #plt.xlim([0.75,1])
    plt.xlabel('Radius '+r'(r/r$_{o}$)')
    legend = plt.legend(loc='upper left', shadow=True, ncol = 3) 
    plt.tight_layout()

    if (no_plots):
        return

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    alt = args['--alt']

    quant = (args['--quants']).split(",")
    quantities = []
    for i in quant:
        quantities.append(int(i))

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, quantities, alt)

