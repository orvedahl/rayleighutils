"""
Calculate various things surrounding the non-dimensional Boussinesq
runs as a function of time

Usage:
    plot_nondimensional_numbers.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: boussinesq_timeseries.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --Pr=<p>        Prandtl number, Pr=nu/kappa [default: 1]
    --Pm=<p>        Magnetic Prandtl number, Pr=nu/eta [default: 1]
    --Ek=<e>        Ekman number, Ek=nu/Omega/L**2 [default: 1e-5]
    --aspect=<a>    Set aspect ratio [default: 0.35]
    --logx          Log scale the x-axis [default: False]
    --logy          Log scale the y-axis [default: False]
    --symlogy       Sym-Log scale the y-axis [default: False]
    --lut=<l>       Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list, ReferenceState, GlobalAverage
#import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import volume_avg
import os,sys
from collections import OrderedDict
from Utilities.look_up_quantity import lut_lookup, print_lut
from Utilities import time_utils
from Utilities.loop_progress import progress_bar

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Pr, Prm, Ek, no_plots, aspect_ratio, logx, logy, symlogy, new_lut, **kwargs):

    print("\nUsing:")
    print("           Prandtl = {}".format(Pr))
    print("  Magnetic Prandtl = {}".format(Prm))
    print("             Ekman = {}".format(Ek))
    print("      Aspect Ratio = {}".format(aspect_ratio))

    Numbers = OrderedDict() # hold all output arrays

    # integration method
    method='cheb-end'

    # read the Global Averages
    quants = [401, 1101, 413] # KE, ME, v**2
    path = os.path.join(data_dir, "G_Avgs")
    files = build_file_list(start_file, end_file, path=path)[-last_number_files:]
    G = GlobalAverage(files[0], path='', quantity=quants)
    Gdata, Gtime = time_utils.TimeSeries(files, GlobalAverage, quantity=quants, axis=0)
    Gtime *= Prm
    t_start = Gtime[0]; t_end = Gtime[-1]

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(t_start))
    print("Last  time: {} sec".format(t_end))

    # read the Global Averages
    quants = [2203, 2204, 2224] # visc_heat, ohmic_heat, nusselt
    path = os.path.join(data_dir, "Shell_Avgs")
    files = build_file_list(start_file, end_file, path=path)[-last_number_files:]
    S = ShellAverage(files[0], path='', quantity=quants)
    Sdata, Stime = time_utils.TimeSeries(files, ShellAverage, quantity=quants, axis=-1)
    Stime *= Prm
    t_start = Stime[0]; t_end = Stime[-1]

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(t_start))
    print("Last  time: {} sec".format(t_end))

    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    radius = S.radius
    fpr = 4.0*np.pi*radius*radius # four pi r^2
    rnorm = radius/radius[0]      # normalize radius so that upper boundary is r=1

    print("\n-----------------------------------------------")

    # extract the G_Avgs data
    ke_index = G.lut[lut_lookup('ke')] # total KE
    ke = Gdata[:,ke_index]

    me_index = G.lut[lut_lookup('me')] # total ME
    me = Gdata[:,me_index]

    # calculate Reynolds number
    u2_index = lut_lookup('v2')
    if (u2_index is not None):
        u2_index = G.lut[u2_index]
        Re = np.sqrt(Gdata[:,u2_index])
    else:
        Re = None

    nu_index = S.lut[lut_lookup('my_nu')] # custom nusselt number

    # calculate Elsasser number & Lorentz number
    El = 2.*Prm*Ek*me
    Lo = np.sqrt(El*Ek/Prm)

    # calculate Nusselt and Reynolds (if needed)
    Nu = np.zeros_like(Stime)

    if (Re is None):
        # read reference state
        ref = ReferenceState(filename="reference", path=data_dir)
        rho = ref.density

        # this assumes a constant density
        U_rms_squared = 2*Sdata[:,0,ke_index,:]/np.reshape(rho, (len(rho), 1)) # size [r, t]
        U2_tilde = np.zeros_like(Stime)

        n_time = len(Stime)
        print("\n...looping over time, len(time) = {}".format(n_time))
        progress_bar(0, n_time)
        for i in range(n_time):
            # volume average of U_rms**2
            U2_tilde[i] = volume_avg(U_rms_squared[:,i], radius, method=method,
                                     data_reversed=True)
            progress_bar(i+1, n_time)
        Re = np.sqrt(U2_tilde)
        Retime = Stime
    else:
        Retime = Gtime

    # get Nu at the top of the domain and use 0th moment
    Nu[:] = Sdata[0,0,nu_index,:]

    # store the numbers
    Numbers['Nu'] = [Nu, Stime]
    Numbers['Re'] = [Re, Retime]
    Numbers['El'] = [El, Gtime]
    Numbers['Lo'] = [Lo, Gtime]

    Nu_mean = np.mean(Nu)
    Re_mean = np.mean(Re)
    El_mean = np.mean(El)
    Lo_mean = np.mean(Lo)
    print("\nAverage Nusselt Number, Nu = {}".format(Nu_mean))
    print("                      Nu-1 = {}".format(Nu_mean-1.))
    print("\nAvg Reynolds number, Re = {}".format(Re_mean))
    print("\nAvg Magnetic Reynolds number, Rem = Re*Pm = {}".format(Re_mean*Prm))
    print("\nAvg Elsasser number, El = {}".format(El_mean))
    print("\nAvg Lorentz number, Lo = {}".format(Lo_mean))

    print("\n...plotting")
    plt.clf()
    linthreshy = 1e100
    for key in Numbers.keys():
        plt.plot(Numbers[key][1], Numbers[key][0], label=key)
        linthreshy = min(linthreshy, 0.5*np.mean(Numbers[key][0]))
    plt.legend(loc='best')
    plt.xlabel('Time', fontsize=14)
    plt.ylabel('Nondimensional Numbers', fontsize=14)

    if (logy):
        plt.yscale('log')
    if (symlogy):
        print("...using symlog with linthreshy = {}".format(linthreshy))
        plt.yscale('symlog', linthreshy=linthreshy)
    if (logx):
        plt.xscale('log')

    if (saveplot):
        plt.savefig(savefile, dpi=300)
        print("saved image: {}".format(savefile))
    else:
        plt.show()
    plt.close()
    print()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    Pr = float(args['--Pr'])
    Prm = float(args['--Pm'])
    Ek = float(args['--Ek'])
    aspect_ratio = float(args['--aspect'])
    no_plots = args['--no-plots']
    logx = args['--logx']
    logy = args['--logy']
    symlogy = args['--symlogy']
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Pr, Prm, Ek, no_plots, aspect_ratio, logx, logy, symlogy, new_lut)

