"""
Plot panel of three flux profiles. All similiar Ra_F, but different Pr.

Usage:
    plot_flux_panel.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_flux.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --Lnorm=<L>     Normalize 4*pi*r**2*Flux by <L> [default: 3.846e33]
    --no-plots      Suppress the plots [default: False]

"""

from __future__ import print_function
from python.diagnostic_reading import ShellAverage, build_file_list, ReferenceState
import matplotlib.pyplot as plt
import numpy as np
import os
from look_up_quantity import shortcut_lookup, general_lookup

def average_files(start_file, end_file, path='./Shell_Avgs',
                  alt=False, last_number_files=0):

    files = build_file_list(start_file, end_file, path=path)
    icount = 0.0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellAverage(f,path='')
        if (i == 0):
            if (not alt):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
            else:
                data = np.zeros((a.nr,a.nq),dtype='float64')
            t_start = a.time[0]
        for j in range(a.niter):
            if (not alt):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
            else:
                data[:,:] = data[:,:]+a.vals[:,:,j]
            icount = icount+1.0
        t_end = a.time[-1]

    delta_time = (t_end - t_start)/3600./24.
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} days".format(t_start/3600./24.))
    print("Last  time: {} days".format(t_end/3600./24.))
    print("\nAveraged over {} days".format(delta_time))

    data = data/icount  # The average is now stored in data

    return data, a

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Lnorm, no_plots, **kwargs):

    if (saveplot):
        plt.figure(1,figsize=(8.0, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(8,4),dpi=100)
        plt.rcParams.update({'font.size': 14})

    data_dirs = ['Prandtl-0.25/kappa_8_nu_2',
                 'Prandtl-0.5/kappa_6_nu_3',
                 'Prandtl-2/kappa_4_nu_8']
    alt_dir = [False, False, False]
    Pr = [r'$a$       Pr$ = 0.25$',
          r'$b$       Pr$ = 0.5$',
          r'$c$       Pr$ = 2$']

    for ind, data_dir in enumerate(data_dirs):
        alt = alt_dir[ind]

        if (data_dir[-1] != '/'):
            data_dir = data_dir + '/'
        data, a = average_files(start_file, end_file, path=data_dir+'Shell_Avgs',
                                alt=alt, last_number_files=last_number_files)
        radius = a.radius
        fpr = 4.0*np.pi*radius*radius  # four pi r^2
        rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1

        #Next, we identify and grab the various fluxes we would like to plot
        cf_index  = a.lut[shortcut_lookup('cond_flux',alt=alt)]     # Conductive energy flux
        vhf_index = a.lut[shortcut_lookup('vol_heat_flux',alt=alt)] # Flux of volumetric heating
        enf_index = a.lut[shortcut_lookup('enth_flux',alt=alt)]     # Enthalpy flux
        vsf_index = a.lut[shortcut_lookup('visc_flux',alt=alt)]     # Viscous energy flux
        kef_index = a.lut[shortcut_lookup('ke_flux',alt=alt)]       # Kinetic energy flux

        if (not alt):
            cflux = data[:,0,cf_index] # Grab appropriate slices of the data array
            hflux = data[:,0,vhf_index]
            eflux = data[:,0,enf_index]
            vflux =-data[:,0,vsf_index]
            kflux = data[:,0,kef_index]
        else:
            cflux = data[:,cf_index] # Grab appropriate slices of the data array
            hflux = data[:,vhf_index]
            eflux = data[:,enf_index]
            vflux =-data[:,vsf_index]
            kflux = data[:,kef_index]

        tflux = cflux+eflux+vflux+kflux+hflux # total flux

        # radius starts at R=1 and goes inwards to R=0.7
        L2 = np.sqrt(np.sum((tflux*fpr/Lnorm - 1)**2))
        if (Lnorm != 1):
            print("\tL2     : {}".format(L2))

        plot_ind = 131 + ind
        plt.subplot(plot_ind, aspect=0.1)
        lw = 1.5
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.plot(rnorm,cflux*fpr/Lnorm,label=r'F$_{c}$',linewidth=lw, color='k', linestyle='-')
        plt.plot(rnorm,vflux*fpr/Lnorm,label=r'F$_{\nu}$',linewidth=lw, color='k', linestyle=':')
        plt.plot(rnorm,hflux*fpr/Lnorm,label=r'F$_{rad}$',linewidth=lw, color='k', linestyle='-.')
        plt.plot(rnorm,eflux*fpr/Lnorm,label=r'F$_{e}$',linewidth=lw, color='r', linestyle='-')
        plt.plot(rnorm,tflux*fpr/Lnorm,label=r'F$_{tot}$',linewidth=lw, color='k', linestyle='--')
        plt.plot(rnorm,kflux*fpr/Lnorm,label=r'F$_{KE}$',linewidth=lw, color='b', linestyle='-')

        plt.xlim(rnorm[-1], rnorm[0])
        plt.ylim(-1.0, 1.75)

        plt.xticks([0.8, 0.85, 0.9, 0.95])

        if (ind == 0):
            plt.ylabel(r'Dimensionless Flux')
        else:
            plt.tick_params(axis='y', which='major', labelleft='off')

        plt.xlabel(r'radius/R$_{star}$')

        if (ind == 2):
            plt.legend(loc='lower center', ncol=3, mode='expand', fontsize=10.5)

        plt.text(0.78, 1.5, Pr[ind], fontsize=14)

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    Lnorm = float(args['--Lnorm'])
    no_plots = args['--no-plots']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Lnorm, no_plots)

