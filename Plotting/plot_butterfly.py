"""
Plot butterfly diagram from Azimuthal Average data.

    -start with B_r(r,theta,phi,t)
    -get B_r(r,theta,t) from AZ_Avgs output files
    -select outer boundary to get B_r(theta,t)
    -plot B_r(theta,t) with t on x-axis

Usage:
    plot_butterfly.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: butterfly.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --no-plots           Suppress the plots [default: False]
    --fft                Plot the FFT of the time axis [default: False]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --start-plot=<s>     Start plotting at <s> % of data, e.g. 0.05 [default: 0]
    --end-plot=<e>       Stop plotting at <e> % of data, e.g. 0.95 [default: 1]
    --quantity=<q>       Specify quantity name [default: br]
    --store              Write results to file for future reading [default: False]
    --read=<f>           Read stored results from file [default: ]
    --log                Plot log10(data) [default: False]
    --equal-limits       Make the vmin/vmax values symmetric [default: False]
    --normalize          Make vmin/vmax = -1/+1 [default: False]
    --lut=<l>            Use a different lut.py file
"""
from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
import matplotlib.pyplot as plt
import os
import sys
import numpy as np
import matplotlib.colors as colors

from NumericalToolsLocal import fft as myfft
from NumericalToolsLocal import file_io
from NumericalToolsLocal import signals
from NumericalToolsLocal import plotting

from Utilities.look_up_quantity import shortcut_lookup
from Utilities.time_utils import Timer, TimeSeries

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile,
         data_dir, no_plots, do_fft, Prm, start_plot, end_plot, log_scale,
         store_results, readfile, quantity, equal_limits, normalize, new_lut, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    rad_ind = 0 # pick radial index closest to the upper boundary

    if (readfile == ''):
        if (exclude_last == 0):
            exclude_last = None
        else:
            exclude_last *= -1

        # Read in the data
        if (data_dir[-1] != '/'):
            data_dir = data_dir + '/'
        files = build_file_list(start_file, end_file, path=data_dir+'AZ_Avgs')
        files = files[-last_number_files:exclude_last]

        index = [shortcut_lookup(quantity)]

        print("\nreading data...")
        a = AzAverage(filename=files[0], path='', quantity=index)
        data, time = TimeSeries(files, AzAverage, axis=-1, data_attr="vals", quantity=index)

        time *= Prm # convert to magnetic diffusion times

        # extract Br
        br_index = a.lut[index]
        #br_index = a.lut[shortcut_lookup('br')]
        data = np.squeeze(data[:,rad_ind,br_index,:])

        s = int(start_plot*len(time))
        e = int(end_plot  *len(time))
        time = time[s:e]
        data = data[:,s:e]

        print("\nFirst file: {}".format(files[-last_number_files:][0]))
        print("Last  file: {}".format(files[-last_number_files:][-1]))
        print("\nTotal number of files: {}".format(len(files[-last_number_files:])))

        # extract meta data for plot
        nr = a.nr; nth = a.ntheta; nt = len(time)
        costheta = a.costheta

        radius = a.radius[rad_ind]

    else:
        print("\nReading data from stored file {}".format(readfile))
        T = Timer(); T.start()
        try:
            readdata, keys = file_io.read_data(readfile)
            data     = readdata['data']
            time     = readdata['time']
            nr       = readdata['nr']
            nth      = readdata['nth']
            nt       = readdata['nt']
            radius   = readdata['radius']
            costheta = readdata['costheta']
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()
        T.stop(); dt = T.print_interval()

        s = int(start_plot*len(time))
        e = int(end_plot  *len(time))
        time = time[s:e]
        data = data[:,s:e]

    if (store_results):
        output = file_io.outputname(savefile, ext=".butterfly_stored")
        print("\nStoring results to file {}".format(output))

        T = Timer(); T.start()
        try:
            file_io.save_data(output, compress=True,
                      data=data, time=time,
                      nr=nr, nth=nth, nt=nt,
                      costheta=costheta, radius=radius)
            print("\t...success")
        except:
            print("\t...FAILED")
        T.stop(); dt = T.print_interval()

    print("\nFirst time: {} simulation-secs".format(time[0]))
    print("Last  time: {} simulation-secs".format(time[-1]))
    print("\nTotal time: {} simulation-secs".format(time[-1]-time[0]))

    latitude = 90. - np.arccos(costheta)*180./np.pi

    print("\nUsing radius = {}".format(radius))

    if (normalize):
        max_val = np.amax(np.abs(data))
        data /= max_val

    vmin = np.amin(data)
    vmax = np.amax(data)
    if (equal_limits):
        if (vmin*vmax < 0): # this only works if they are different signs
            vmax = max(abs(vmin), vmax)
            vmin = -vmax
    print("\nScaling values, vmin/vmax = {}/{}".format(vmin, vmax))

    if (no_plots):
        return

    # make the grid
    X, Y = np.meshgrid(time, latitude)

    # setup the figure
    if (saveplot):
        plt.rcParams.update({'font.size': 12})
        fig = plt.figure(figsize=(7.5,5.5))
    else:
        plt.rcParams.update({'font.size': 12})
        fig = plt.figure(figsize=(7.5,5.5))

    cmap = 'RdBu_r' # 'bwr', 'jet', 'coolwarm'
    if (log_scale):
        linthresh=1e-2*np.amax(np.abs(data))
        norm = colors.SymLogNorm(linthresh=linthresh, linscale=0.5, vmin=vmin, vmax=vmax)
    else:
        norm = plotting.MidPointNorm(midpoint=0)
    img = plt.pcolormesh(X, Y, data, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm)
    if (normalize):
        if (log_scale):
            ticks = [-1, -0.5, -0.25, -0.05, 0, 0.05, 0.25, 0.5, 1]
        else:
            ticks = [-1,-.75,-.5,-.25,0,.25,.5,.75]
        colorbar = plt.colorbar(img, ticks=ticks)
    else:
        colorbar = plt.colorbar(img)

    colorbar.ax.tick_params(direction='out')

    #plt.title(r"Butterfly Diagram $B_r(r_o,\theta,t)$")
    #plt.title( "Butterfly Diagram")
    plt.xlabel("Magnetic Diffusion Time", fontsize=14)
    plt.ylabel("Latitude (deg)", fontsize=14)

    plt.xlim(np.amin(time), np.amax(time))
    plt.ylim(np.amin(latitude), np.amax(latitude))

    if (False):
        plt.xlim(0, 6)

    if (saveplot):
        plt.savefig(savefile, dpi=300)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

    if (do_fft):
        # data is arranged as data(theta, time)
        fft_axis = 1
        window = 'blackman-harris'

        # Fk will be Fk(theta, omega)
        Fk, omega, P, xwin = myfft.FFT(data, time, window=window, axis=fft_axis, angular=1)

        P = np.real(P) # power is real, but stored as complex number, change that
        P = np.log10(P)

        # plot it
        X, Y = np.meshgrid(omega, latitude)
        cmap = 'jet' #'inferno' # 'gist_heat', 'plasma', 'magma'
        img = plt.pcolormesh(X, Y, P, cmap=cmap)
        colorbar = plt.colorbar(img)

        plt.title(r"$\log_{10}$(Power) in Butterfly Diagram $B_r(r_o,\theta,\omega)$")
        plt.xlabel(r"Magnetic Diffusion Frequency, $\omega$")
        plt.ylabel("Latitude (deg)")

        plt.xlim(np.min(omega), np.max(omega))
        plt.ylim(np.min(latitude), np.max(latitude))

        if (saveplot):
            output = file_io.outputname(savefile, insert="_power")
            plt.savefig(output)
            print("saved image: {}".format(output))
        else:
            plt.show()

        plt.close()

        # collapse 2D Power spectrum, and plot max(P) vs omega
        P_1d = np.sum(P, axis=0)/len(P[:,0])  # P is size[nlatitude, nomega]

        plt.plot(omega, P_1d, linestyle='-', color='r', label='power')
        wlength = int(0.02*len(omega))
        P_1d_s  = signals.smooth(P_1d,  window_length=wlength, window='flat')
        plt.plot(omega, P_1d_s, linestyle='-', color='b', label='smoothed power')
        plt.legend(loc='upper right')
        plt.xlabel(r"Magnetic Diffusion Frequency, $\omega$")
        plt.ylabel(r"$\log_{10}$(Marginalized Power)")
        plt.title(r"$\log_{10}$(Power) in Butterfly Diagram $B_r(r_o,\theta,\omega)$")
        plt.xlim(np.min(omega), np.max(omega))

        # find Nth max of power and plot corresponding omega as vertical line
        N = 3
        ind = np.argsort(P_1d_s)[-N:]
        for i in range(N):
            plt.axvline(x=omega[ind[i]], linestyle=':', color='k')

        if (saveplot):
            output = file_io.outputname(savefile, insert="_power1d")
            plt.savefig(output)
            print("saved image: {}".format(output))
        else:
            plt.show()

        plt.close()

    plt.clf()
    for i in [0.333, 0.1, 0.05, 0.01, 0, -0.01, -0.05, -0.1, -0.333]:
        j = int(nth*0.5*(1 + i))
        angle = latitude[j]
        if (angle >= 0):
            lstyle = '-'
        else:
            lstyle = '--'
        plt.plot(time, data[j, :], linestyle=lstyle, label='latitude = {:.2f}'.format(angle))
    plt.plot(time, 0.*time, linestyle=':', color='k')

    plt.xlabel('Magnetic Diffusion Time')
    plt.ylabel('Br at surface')
    plt.legend(loc='best')
    if (saveplot):
        output = file_io.outputname(savefile, insert="_1d_cuts")
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    do_fft   = args['--fft']
    Prm      = float(args['--Pm'])
    start_plot = float(args['--start-plot'])
    end_plot   = float(args['--end-plot'])
    log_scale  = args['--log']
    store_results = args['--store']
    readfile = args['--read']
    quantity = args['--quantity']
    equal_limits = args['--equal-limits']
    normalize = args['--normalize']
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile,
         data_dir, no_plots, do_fft, Prm, start_plot, end_plot, log_scale,
         store_results, readfile, quantity, equal_limits, normalize, new_lut)

