"""
Plot azimuthal averages

Usage:
    plot_azavg.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: az_average.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --quantity=<q>       What quantity code to plot [default: 64]
    --no-mean            Remove the mean [default: False]
"""
from __future__ import print_function
import env
import NumericalToolsLocal.public as NT
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
import matplotlib.pyplot as plt
import os
import numpy as np
from Utilities import azavg_util, time_utils, azavg_calculus
from Utilities.look_up_quantity import general_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         qcode, no_mean):

    # build file list
    data_dir = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # read first file to get meta data
    az = AzAverage(filename=files[0], path='')
    nr = az.nr
    nth = az.ntheta
    radius = az.radius
    sintheta = az.sintheta
    costheta = az.costheta
    #costheta = np.cos(np.arcsin(sintheta))
    #costheta[0:nth/2] *= -1.0

    # do time average, data is shape (nth, nr, nq)
    if (True):
        print("\n...doing average...")
        data, avgtimes = time_utils.TimeAvg(files, AzAverage, axis=-1, data_attr="vals")
    else:
        print("\n...choosing specific time...")
        tind = 0
        data = az.vals[:,:,:,tind]
        avgtimes = {"dt":0, "tstart":az.time[tind], "tend":az.time[tind], "nrec":az.niter}

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(avgtimes["tstart"]))
    print("Last  time: {} sec".format(avgtimes["tend"]))
    print("\nAveraged over {} sec".format(avgtimes["dt"]))
    print("Number of records {}".format(avgtimes["nrec"]))

    # get quantity index and extract the data
    q_ind = az.lut[qcode]
    quantity = data[:,:,q_ind]

    # remove the \ell=0 bit
    if (no_mean):
        for i in range(nr):
            quantity[:,i] -= np.mean(quantity[:,i])

    #############################
    vr_index = az.lut[1]
    vt_index = az.lut[2]
    vp_index = az.lut[3]
    dvrdr_index = az.lut[10]
    dvrdt_index = az.lut[19]
    dvrdp_index = az.lut[28]
    dvtdr_index = az.lut[11]
    dvtdt_index = az.lut[20]
    dvtdp_index = az.lut[29]
    dvpdr_index = az.lut[12]
    dvpdt_index = az.lut[21]
    dvpdp_index = az.lut[30]
    s_index  = az.lut[64]
    dsdr_index = az.lut[70]
    dsdt_index = az.lut[76]
    dsdp_index = az.lut[82]

    _q = 'vp'
    if (_q == 's'):
        S = data[:,:,s_index]
        dSdr = data[:,:,dsdr_index]
        dSdt = data[:,:,dsdt_index]
    elif (_q == 'vr'):
        S = data[:,:,vr_index]
        dSdr = data[:,:,dvrdr_index]
        dSdt = data[:,:,dvrdt_index]
    elif (_q == 'vt'):
        S = data[:,:,vt_index]
        dSdr = data[:,:,dvtdr_index]
        dSdt = data[:,:,dvtdt_index]
    elif (_q == 'vp'):
        S = data[:,:,vp_index]
        dSdr = data[:,:,dvpdr_index]
        dSdt = data[:,:,dvpdt_index]
    Gr, Gth, Gphi = azavg_calculus.gradient(S, radius, np.arccos(costheta))

    # theta gradient
    for i in range(nr):
        Gth[:,i] *= radius[i] # turn this into d_dtheta

    #plt.clf()
    #colors=['b', 'g', 'r'] #, 'c', 'm', 'y', 'k']
    #rnorm = radius/radius[0]
    #for i,c in enumerate(colors):
    #    j = i+2
    #    if (True):
    #        plt.plot(np.arccos(costheta), 100*np.abs((Gth[:,nr/j]-dSdt[:,nr/j])/dSdt[:,nr/j]),
    #                 label='diff nr/{}'.format(j), linestyle=':', color=c)
    #    else:
    #        plt.plot(rnorm, 100*np.abs((Gr[nth/j,:]-dSdr[nth/j,:])/dSdr[nth/j,:]),
    #                 label='diff nth/{}'.format(j), linestyle=':', color=c)
    #plt.yscale('symlog', linthreshy=1e-8)
    #plt.yscale('log')
    #plt.title("Percent Error")
    #plt.legend(loc='best')
    #plt.show()

    # radial gradient
    max_err = np.amax(Gr - dSdr); min_err = np.amin(Gr - dSdr)
    print("Gradient Errors")
    print("\tmax err in radial = {}".format(max_err))
    print("\tmin err in radial = {}".format(min_err))
    print("\tmin/max of dSdr = {}/{}".format(np.amin(dSdr), np.amax(dSdr)))
    print("\tmin/max of Gr   = {}/{}".format(np.amin(Gr), np.amax(Gr)))
    print()

    max_err = np.amax(Gth - dSdt); min_err = np.amin(Gth - dSdt)
    print("\tmax err in theta = {}".format(max_err))
    print("\tmin err in theta = {}".format(min_err))
    print("\tmin/max of dSdt = {}/{}".format(np.amin(dSdt), np.amax(dSdt)))
    print("\tmin/max of Gth  = {}/{}".format(np.amin(Gth), np.amax(Gth)))

    #tit = 'radial'
    #ray = dSdr; num = Gr
    tit = 'theta'
    ray = dSdt; num = Gth

    diff = np.log10(np.abs(num - ray))
    clim  = max(abs(np.amin(ray)), np.amax(ray))
    climd = max(abs(np.amin(diff)), np.amax(diff))

    clim  = [-clim, clim]
    climd = [np.amax(diff)-6, np.amax(diff)] # only do 6 orders of mag lower than max

    print()
    max_err = np.amax(diff); min_err = np.amin(diff)
    print("\t{}".format(tit))
    print("\tmax log10(err) in diff plot = {}".format(max_err))
    print("\tmin log10(err) in diff plot = {}".format(min_err))
    print("\tmax err in diff plot = {}".format(10**max_err))
    print("\tmin err in diff plot = {}".format(10**min_err))
    #############################

    #res = NT.is_grid_legendre(costheta, (-1., 1.))
    #print("costheta grid is legendre = {}".format(res['legendre']))
    #print("                    error = {}".format(res['error']))
    #res = NT.is_grid_legendre(np.arccos(costheta)[::-1], (0., np.pi))
    #print("theta grid is legendre = {}".format(res['legendre']))
    #print("                 error = {}".format(res['error']))

    # setup the figure
    if (saveplot):
        fig = plt.figure(figsize=(12, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        fig = plt.figure(figsize=(12, 4.0), dpi=80)
        plt.rcParams.update({'font.size': 14})

    tsize = 20
    lsize = 15
    axis = fig.add_subplot(1,3,1)
    units = ''
    azavg_util.plot_azav(fig, axis, ray, radius, costheta, sintheta, r_bcz=0,
                         mycmap='RdBu_r', cbar=True, orientation='vertical',
                         mini=clim[0], maxi=clim[1],
                         boundsfactor=1.5, boundstype='rms',
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)
    title = 'Rayleigh ' + tit
    plt.title(title, fontsize=tsize)

    axis = fig.add_subplot(1,3,2)
    units = ''
    azavg_util.plot_azav(fig, axis, num, radius, costheta, sintheta, r_bcz=0,
                         mycmap='RdBu_r', cbar=True, orientation='vertical',
                         mini=clim[0], maxi=clim[1],
                         boundsfactor=1.5, boundstype='rms',
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)

    title = 'Num Grad(S) ' + tit
    plt.title(title, fontsize=tsize)

    axis = fig.add_subplot(1,3,3)
    units = ''
    azavg_util.plot_azav(fig, axis, diff, radius, costheta, sintheta, r_bcz=0,
                         mycmap='RdBu_r', cbar=True, orientation='vertical',
                         mini=climd[0], maxi=climd[1],
                         boundsfactor=1.5, boundstype='rms',
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)

    title = 'log10|difference|'
    plt.title(title, fontsize=tsize)

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    quantity = int(args['--quantity'])
    no_mean = args['--no-mean']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         quantity, no_mean)

