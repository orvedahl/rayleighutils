"""
Plot meridional & equatorial slices side-by-side

Usage:
    plot_merid_equat_slice.py [options] <meridional_slice> <equatorial_slice>

    <meridional_slice> should be something like:  Meridional_Slice/00050000

    <equatorial_slice> should be something like:  Equatorial_Slice/00050000

Options:
    --quantity=<q>    Give quantity code [default: 64]
    --phi-index=<p>   Set the phi index [default: 1]
    --time-index=<t>  Set time index [default: 0]
    --no-mean         Remove the m=0 mean [default: False]
    --Pm=<p>          Convert to magnetic diffusion times [default: 1]
    --save            Save an image [default: False]
    --dpi=<d>         Set dpi for png image [default: 200]
    --line            Draw line indicating Meridional location [default: False]
    --title=<t>       Set the title [default: ]
    --output=<o>      Save image as <o> [default: merid_equat_slice.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --list            List available quantities [default: False]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import Meridional_Slice, Equatorial_Slice
from Utilities.look_up_quantity import print_lut, lut_lookup, quantity_in_lut
import numpy as np
import matplotlib.pyplot as plt

def main(merid_slice, equat_slice, tindex, quantity_code, pindex,
         remove_mean, save, output, data_dir, Prm, draw_line, suptitle, list_lut):

    if (list_lut):
        print("\nMeridional Slice:")
        a = Meridional_Slice(filename=merid_slice, path=data_dir)
        print_lut(a.lut)
        print("\nEquatorial Slice:")
        a = Equatorial_Slice(filename=equat_slice, path=data_dir)
        print_lut(a.lut)
        return

    cmap = 'RdBu_r'

    aspect = 1.5
    width = 6
    height = width/aspect
    fig = plt.figure(1, figsize=(width, height), dpi=dpi)

    ##############################################################################
    # Meridional Slice
    ##############################################################################
    a = Meridional_Slice(filename=merid_slice, path=data_dir)

    # set up the grid
    nr = a.nr
    ntheta = a.ntheta
    r = a.radius/np.max(a.radius)
    # apply shift in theta so theta=0 corresponds to equator and poles appear as expected
    theta = np.arccos(a.costheta)-np.pi/2  

    radius_matrix, theta_matrix = np.meshgrid(r,theta)

    X = radius_matrix * np.cos(theta_matrix)
    Y = radius_matrix * np.sin(theta_matrix)

    time = a.time[tindex]*Prm

    qindex = a.lut[quantity_code]
    field = np.zeros((ntheta,nr),dtype='float64')
    field[:,:] =a.vals[pindex,:,:,qindex,tindex]

    # remove the mean if desired
    if (remove_mean):
        for i in range(nr):
            the_mean = np.mean(field[:,i])
            field[:,i] = field[:,i]-the_mean
    radtodeg = 180.0/np.pi
    phi_angle = a.phi[pindex]*radtodeg
    print('\nMeridional slice at phi (degrees) = ', phi_angle)
    print('Time = ',time)

    # plot meridional slice
    # (left, bottom, width, height) in fractions of figure width/height
    eps = 0.2
    l = 2./3. - 0.02
    b = 0.05
    w = 1./3.*(1. - eps)
    h = 1. - eps
    axMerid = fig.add_axes((l,b,w,h))
    img = axMerid.pcolormesh(X,Y,field,cmap=cmap)
    axMerid.set_aspect(1, adjustable='box')

    ####### DEBUG
#    ith = int(ntheta/4); ir = 0# int(nr/4)
#    print("x,y = {}, {}".format(X[ith,ir], Y[ith,ir]))
#    print("r,th = {}, {}".format(radius_matrix[ith,ir],np.arccos(theta_matrix[ith,ir])*radtodeg))
#    print("data = {}".format(field[ith,ir]))
#    axMerid.scatter(X[ith,ir], Y[ith,ir], marker='x', color='k')
    ####### DEBUG

    # x/y range
    axMerid.set_xlim( 0,1)
    axMerid.set_ylim(-1,1)

    # x/y labels
    axMerid.set_axis_off()
    #axMerid.set_xlabel(r"$r/r_o$")
    #axMerid.set_ylabel(r"$r/r_o$")

    time = "{:.3f}".format(time)
    angle = "{:.1f}".format(phi_angle)
    axMerid.set_title("$t/t_\eta = {{{}}}$, $\phi = {{{}}}^o$".format(time, angle))

    ##############################################################################
    # Equatorial Slice
    ##############################################################################
    a = Equatorial_Slice(filename=equat_slice, path=data_dir)

    # set up the grid
    nr = a.nr
    nphi = a.nphi
    r = a.radius/np.max(a.radius)
    phi = np.zeros(nphi+1,dtype='float64')
    phi[0:nphi] = a.phi
    phi[nphi] = np.pi*2  # for display purposes, best to have a redunant point at 0,2pi

    radius_matrix, phi_matrix = np.meshgrid(r,phi)
    X = radius_matrix * np.cos(phi_matrix)
    Y = radius_matrix * np.sin(phi_matrix)

    time = a.time[tindex]*Prm

    qindex = a.lut[quantity_code]

    field = np.zeros((nphi+1,nr),dtype='float64')

    t_index = lut_lookup('t')
    if (t_index == quantity_code and not(quantity_in_lut(t_index, a.lut))):
        q1 = lut_lookup('tm'); q2 = lut_lookup('tp')
        if (quantity_in_lut(q1, a.lut) and quantity_in_lut(q2, a.lut)):
            print("\n...Calculating full temperature from mean & pert...\n")
            q1 = a.lut[q1]; q2 = a.lut[q2]
            field[0:nphi,:] = a.vals[:,:,q1,tindex] + a.vals[:,:,q2,tindex]
        else:
            raise ValueError("Cannot extract Full Temperture info from Equatorial Slice")
    else:
        field[0:nphi,:] = a.vals[:,:,qindex,tindex]
    field[nphi,:] = field[0,:]  # replicate phi=0 values at phi=2pi

    # remove the mean if desired
    if (remove_mean):
        for i in range(nr):
            the_mean = np.mean(field[:,i])
            field[:,i] = field[:,i]-the_mean

    print('\nEquatorial slice')
    print('Time = ',time)

    # plot equatorial slice
    eps = 0.2
    l = 0.05
    b = 0.05
    w = 2./3.*(1. - eps)
    h = 1. - eps
    axEquat = fig.add_axes((l,b,w,h))
    img = axEquat.pcolormesh(X,Y,field,cmap=cmap)
    axEquat.set_aspect(1, adjustable='box')

    if (draw_line):
        phi_angle /= radtodeg # back in radians
        ind = np.where(abs(phi - phi_angle) < 5e-3)
        print("\ndrawing line at phi (degrees) = ", phi[ind]*radtodeg)
        print()
        rinner = r[-1]; router = r[0]
        xpts = [rinner*np.cos(phi[ind]), router*np.cos(phi[ind])]
        ypts = [rinner*np.sin(phi[ind]), router*np.sin(phi[ind])]
        axEquat.plot(xpts, ypts, linestyle=':', color='k')

    # x/y range
    axEquat.set_xlim(-1,1)
    axEquat.set_ylim(-1,1)

    # x/y labels
    axEquat.set_axis_off()
    #axEquat.set_xlabel(r"$r/r_o$")
    #axEquat.set_ylabel(r"$r/r_o$")

    time = "{:.3f}".format(time)
    axEquat.set_title("$t/t_\eta = {{{}}}$".format(time))

    fig.suptitle(suptitle)

    if (save):
        plt.savefig(output, dpi=dpi)
        print("\nsaved image = {}".format(output))
    else:
        plt.show()

if __name__ == "__main__":
    import docopt
    args = docopt.docopt(__doc__)

    quantity_code = int(args['--quantity'])
    pindex = int(args['--phi-index'])
    tindex = int(args['--time-index'])
    remove_mean = args['--no-mean']
    Prm = float(args['--Pm'])
    save = args['--save']
    dpi = int(args['--dpi'])
    output = args['--output']
    data_dir = args['--data-dir']
    draw_line = args['--line']
    suptitle = args['--title']
    list_lut = args['--list']

    merid_slice = args['<meridional_slice>']
    equat_slice = args['<equatorial_slice>']

    main(merid_slice, equat_slice, tindex, quantity_code, pindex,
         remove_mean, save, output, data_dir, Prm, draw_line, suptitle, list_lut)

