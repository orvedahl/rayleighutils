"""
Plot characteristic \ell values for various runs

Usage:
    plot_characteristic_ell.py [options] <data-file>

Options:
    --save           Save the image [default: False]
    --output=<o>     Save image as <o> [default: nondim_plots]
    --debug          Run in debug mode [default: False]
    --quant=<q>      What are you plotting? vrs, vr, vhoriz [default: vel]
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
from NumericalToolsLocal import fit_RaPr
from collections import OrderedDict
import sys

# plotting arguments for various runs
myP = OrderedDict()
myP['vel'] = {'Rtextx1':0.10, 'Rtexty1':0.90, 'Ptextx1':0.10, 'Ptexty1':0.80, # coords for
              'Rtextx2':0.10, 'Rtexty2':0.15, 'Ptextx2':0.10, 'Ptexty2':0.05, # Ra/Pr string
              'Rtextx3':0.10, 'Rtexty3':0.15, 'Ptextx3':0.10, 'Ptexty3':0.05, # fro each panel
              'Rtextx4':0.10, 'Rtexty4':0.15, 'Ptextx4':0.10, 'Ptexty4':0.05,
              'Rtextx5':0.10, 'Rtexty5':0.90, 'Ptextx5':0.10, 'Ptexty5':0.80,
              'Rtextx6':0.10, 'Rtexty6':0.90, 'Ptextx6':0.10, 'Ptexty6':0.80,
              'lmin':2, 'lmax':30, 'lmin_rms':1.5, 'lmax_rms':40,
              'lmin_var':2, 'lmax_var':1e3,
              'title':'Velocity', 'showtext':True, # do we show the Ra,Pr text
              'legend':1, # which plot has legend
              'legfont':'xx-small', 'legloc':'lower right',
              'Ra_crit':5e3, 'Ra_crit_rms':3e3,
              'plot_var':True
              }
myP['vr']  = {'Rtextx1':0.10, 'Rtexty1':0.15, 'Ptextx1':0.10, 'Ptexty1':0.05, # coords for
              'Rtextx2':0.10, 'Rtexty2':0.15, 'Ptextx2':0.10, 'Ptexty2':0.05, # Ra/Pr string
              'Rtextx3':0.10, 'Rtexty3':0.15, 'Ptextx3':0.10, 'Ptexty3':0.05, # fro each panel
              'Rtextx4':0.10, 'Rtexty4':0.15, 'Ptextx4':0.10, 'Ptexty4':0.05,
              'Rtextx5':0.10, 'Rtexty5':0.90, 'Ptextx5':0.10, 'Ptexty5':0.80,
              'Rtextx6':0.10, 'Rtexty6':0.90, 'Ptextx6':0.10, 'Ptexty6':0.80,
              'lmin':3, 'lmax':85, 'lmin_rms':4, 'lmax_rms':110,
              'lmin_var':3, 'lmax_var':4e3,
              'title':'Radial Velocity', 'showtext':True, # do we show the Ra,Pr text
              'legend':4, # which plot has legend
              'legfont':'xx-small', 'legloc':'upper left',
              'Ra_crit':3e3, 'Ra_crit_rms':3e3,
              'plot_var':True
              }
myP['vhoriz'] = {'Rtextx1':0.10, 'Rtexty1':0.15, 'Ptextx1':0.10, 'Ptexty1':0.05, # coords for
              'Rtextx2':0.10, 'Rtexty2':0.15, 'Ptextx2':0.10, 'Ptexty2':0.05, # Ra/Pr string
              'Rtextx3':0.10, 'Rtexty3':0.15, 'Ptextx3':0.10, 'Ptexty3':0.05, # fro each panel
              'Rtextx4':0.10, 'Rtexty4':0.15, 'Ptextx4':0.10, 'Ptexty4':0.05,
              'Rtextx5':0.10, 'Rtexty5':0.90, 'Ptextx5':0.10, 'Ptexty5':0.80,
              'Rtextx6':0.10, 'Rtexty6':0.90, 'Ptextx6':0.10, 'Ptexty6':0.80,
              'lmin':1, 'lmax':30, 'lmin_rms':1, 'lmax_rms':40,
              'lmin_var':1, 'lmax_var':7e2,
              'title':'Horizontal Velocity', 'showtext':True, # do we show the Ra,Pr text
              'legend':6, # which plot has legend
              'legfont':'xx-small', 'legloc':'lower right',
              'Ra_crit':5e3, 'Ra_crit_rms':5e3,
              'plot_var':True
              }
myP['s']   = {'Rtextx1':0.10, 'Rtexty1':0.15, 'Ptextx1':0.10, 'Ptexty1':0.05, # coords for
              'Rtextx2':0.10, 'Rtexty2':0.15, 'Ptextx2':0.10, 'Ptexty2':0.05, # Ra/Pr string
              'Rtextx3':0.10, 'Rtexty3':0.90, 'Ptextx3':0.10, 'Ptexty3':0.80, # fro each panel
              'Rtextx4':0.10, 'Rtexty4':0.15, 'Ptextx4':0.10, 'Ptexty4':0.05,
              'Rtextx5':0.10, 'Rtexty5':0.90, 'Ptextx5':0.10, 'Ptexty5':0.80,
              'Rtextx6':0.10, 'Rtexty6':0.90, 'Ptextx6':0.10, 'Ptexty6':0.80,
              'lmin':0.001, 'lmax':4, 'lmin_rms':0.05, 'lmax_rms':30,
              'lmin_var':1, 'lmax_var':100,
              'title':'Entropy', 'showtext':True, # do we show the Ra,Pr text
              'legend':4, # which plot has legend
              'legfont':'xx-small', 'legloc':'upper left',
              'Ra_crit':5e3, 'Ra_crit_rms':5e3,
              'plot_var':True
              }
myP['vrs'] = {'Rtextx1':0.10, 'Rtexty1':0.15, 'Ptextx1':0.10, 'Ptexty1':0.05, # coords for
              'Rtextx2':0.10, 'Rtexty2':0.15, 'Ptextx2':0.10, 'Ptexty2':0.05, # Ra/Pr string
              'Rtextx3':0.10, 'Rtexty3':0.15, 'Ptextx3':0.10, 'Ptexty3':0.05, # fro each panel
              'Rtextx4':0.10, 'Rtexty4':0.15, 'Ptextx4':0.10, 'Ptexty4':0.05,
              'Rtextx5':0.10, 'Rtexty5':0.90, 'Ptextx5':0.10, 'Ptexty5':0.80,
              'Rtextx6':0.10, 'Rtexty6':0.90, 'Ptextx6':0.10, 'Ptexty6':0.80,
              'lmin':1, 'lmax':85, 'lmin_rms':1, 'lmax_rms':110,
              'lmin_var':1, 'lmax_var':100,
              'title':r'$v_rS^*$', 'showtext':True, # do we show the Ra,Pr text
              'legend':2, # which plot has legend
              'legfont':'xx-small', 'legloc':'center right',
              'Ra_crit':3e3, 'Ra_crit_rms':3e3,
              'plot_var':True
              }

# fitting routine optional arguments
fit_args = OrderedDict()
fit_args['method'] = 'MC' # 'MC', 'Boot', 'None'
fit_args['Ntrials'] = 1000
#fit_args['Ntrials'] = 7500
fit_args['error'] = 'uniform' # 'uniform' or 'normal'
fit_args['confidence'] = 0.68
fit_args['upper_lower_errors'] = False
fit_args['debug_plots'] = False

def main(save, output, input_file, debug, quant):

    quant = quant.lower()
    if (quant not in myP.keys()):
        print("\nERROR: unrecognized plot quantity = {}\n".format(quant))
        sys.exit()

    # read data file for output values
    data = read_datafile(input_file, separator=None)

    # extract the data
    # Pr = 2
    Pr2_kappa     = data['2']['kappa'][:]
    Pr2_Ra_F      = data['2']['Ra_F'][:]
    Pr2_Ra_BL     = data['2']['Ra_BL'][:]
    Pr2_nu        = 2.*Pr2_kappa
    Pr2_lavg_upper  = data['2']['lavg_upper'][:]
    Pr2_lavg_middle = data['2']['lavg_middle'][:]
    Pr2_lavg_lower  = data['2']['lavg_lower'][:]
    Pr2_lrms_upper  = data['2']['lrms_upper'][:]
    Pr2_lrms_middle = data['2']['lrms_middle'][:]
    Pr2_lrms_lower  = data['2']['lrms_lower'][:]
    Pr2_Pr          = 2.0*np.ones_like(Pr2_Ra_F)

    # Pr = 1
    myPr1_kappa     = data['-1']['kappa'][:]
    myPr1_Ra_F      = data['-1']['Ra_F'][:]
    myPr1_Ra_BL     = data['-1']['Ra_BL'][:]
    myPr1_nu        = 1.*myPr1_kappa
    myPr1_lavg_upper  = data['-1']['lavg_upper'][:]
    myPr1_lavg_middle = data['-1']['lavg_middle'][:]
    myPr1_lavg_lower  = data['-1']['lavg_lower'][:]
    myPr1_lrms_upper  = data['-1']['lrms_upper'][:]
    myPr1_lrms_middle = data['-1']['lrms_middle'][:]
    myPr1_lrms_lower  = data['-1']['lrms_lower'][:]
    myPr1_Pr          = 1.0*np.ones_like(myPr1_Ra_F)

    # Pr = 4
    Pr4_kappa     = data['4']['kappa'][:]
    Pr4_Ra_F      = data['4']['Ra_F'][:]
    Pr4_Ra_BL     = data['4']['Ra_BL'][:]
    Pr4_nu        = 4.*Pr4_kappa
    Pr4_lavg_upper  = data['4']['lavg_upper'][:]
    Pr4_lavg_middle = data['4']['lavg_middle'][:]
    Pr4_lavg_lower  = data['4']['lavg_lower'][:]
    Pr4_lrms_upper  = data['4']['lrms_upper'][:]
    Pr4_lrms_middle = data['4']['lrms_middle'][:]
    Pr4_lrms_lower  = data['4']['lrms_lower'][:]
    Pr4_Pr          = 4.0*np.ones_like(Pr4_Ra_F)

    # Pr = 0.25
    Pr0_25_kappa     = data['0.25']['kappa'][:]
    Pr0_25_Ra_F      = data['0.25']['Ra_F'][:]
    Pr0_25_Ra_BL     = data['0.25']['Ra_BL'][:]
    Pr0_25_nu        = 0.25*Pr0_25_kappa
    Pr0_25_lavg_upper  = data['0.25']['lavg_upper'][:]
    Pr0_25_lavg_middle = data['0.25']['lavg_middle'][:]
    Pr0_25_lavg_lower  = data['0.25']['lavg_lower'][:]
    Pr0_25_lrms_upper  = data['0.25']['lrms_upper'][:]
    Pr0_25_lrms_middle = data['0.25']['lrms_middle'][:]
    Pr0_25_lrms_lower  = data['0.25']['lrms_lower'][:]
    Pr0_25_Pr          = 0.25*np.ones_like(Pr0_25_Ra_F)

    # Pr = 0.5
    Pr0_5_kappa     = data['0.5']['kappa'][:]
    Pr0_5_Ra_F      = data['0.5']['Ra_F'][:]
    Pr0_5_Ra_BL     = data['0.5']['Ra_BL'][:]
    Pr0_5_nu        = 0.5*Pr0_5_kappa
    Pr0_5_lavg_upper  = data['0.5']['lavg_upper'][:]
    Pr0_5_lavg_middle = data['0.5']['lavg_middle'][:]
    Pr0_5_lavg_lower  = data['0.5']['lavg_lower'][:]
    Pr0_5_lrms_upper  = data['0.5']['lrms_upper'][:]
    Pr0_5_lrms_middle = data['0.5']['lrms_middle'][:]
    Pr0_5_lrms_lower  = data['0.5']['lrms_lower'][:]
    Pr0_5_Pr          = 0.5*np.ones_like(Pr0_5_Ra_F)

    # Paper results: Table 6, Ra_F is too high by pi so divide to make it correct
    Pr1_kappa     = data['1']['kappa'][:]
    Pr1_Ra_F      = data['1']['Ra_F'][:]
    Pr1_Ra_BL     = data['1']['Ra_BL'][:]
    Pr1_nu        = 1.*Pr1_kappa
    Pr1_lavg_upper  = data['1']['lavg_upper'][:]
    Pr1_lavg_middle = data['1']['lavg_middle'][:]
    Pr1_lavg_lower  = data['1']['lavg_lower'][:]
    Pr1_lrms_upper  = data['1']['lrms_upper'][:]
    Pr1_lrms_middle = data['1']['lrms_middle'][:]
    Pr1_lrms_lower  = data['1']['lrms_lower'][:]
    Pr1_Pr          = 1.0*np.ones_like(Pr1_Ra_F)

    ######################################################
    # OVERWRITE arrrays
    ######################################################
    if (myP[quant]['plot_var']):
        Pr1_lrms_upper  = Pr1_lrms_upper**2  - Pr1_lavg_upper**2
        Pr1_lrms_middle = Pr1_lrms_middle**2 - Pr1_lavg_middle**2
        Pr1_lrms_lower  = Pr1_lrms_lower**2  - Pr1_lavg_lower**2

        myPr1_lrms_upper  = myPr1_lrms_upper**2  - myPr1_lavg_upper**2
        myPr1_lrms_middle = myPr1_lrms_middle**2 - myPr1_lavg_middle**2
        myPr1_lrms_lower  = myPr1_lrms_lower**2  - myPr1_lavg_lower**2

        Pr2_lrms_upper  = Pr2_lrms_upper**2  - Pr2_lavg_upper**2
        Pr2_lrms_middle = Pr2_lrms_middle**2 - Pr2_lavg_middle**2
        Pr2_lrms_lower  = Pr2_lrms_lower**2  - Pr2_lavg_lower**2

        Pr4_lrms_upper  = Pr4_lrms_upper**2  - Pr4_lavg_upper**2
        Pr4_lrms_middle = Pr4_lrms_middle**2 - Pr4_lavg_middle**2
        Pr4_lrms_lower  = Pr4_lrms_lower**2  - Pr4_lavg_lower**2

        Pr0_25_lrms_upper  = Pr0_25_lrms_upper**2  - Pr0_25_lavg_upper**2
        Pr0_25_lrms_middle = Pr0_25_lrms_middle**2 - Pr0_25_lavg_middle**2
        Pr0_25_lrms_lower  = Pr0_25_lrms_lower**2  - Pr0_25_lavg_lower**2

        Pr0_5_lrms_upper  = Pr0_5_lrms_upper**2  - Pr0_5_lavg_upper**2
        Pr0_5_lrms_middle = Pr0_5_lrms_middle**2 - Pr0_5_lavg_middle**2
        Pr0_5_lrms_lower  = Pr0_5_lrms_lower**2  - Pr0_5_lavg_lower**2

    # generic Ra number array, smallest is at the end, biggest is at the front
    all_Ra_F = np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F))
    min_Ra = np.amin(all_Ra_F)
    max_Ra = np.amax(all_Ra_F)
    Ra_F = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)
    all_Ra_BL = np.concatenate((Pr2_Ra_BL, Pr0_25_Ra_BL, Pr0_5_Ra_BL, Pr1_Ra_BL, Pr4_Ra_BL))
    min_Ra = np.amin(all_Ra_BL)
    max_Ra = np.amax(all_Ra_BL)
    Ra_BL = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)

    # generic nu/kappa arrays, smallest is at the front
    all_kappa = np.concatenate((Pr2_kappa, Pr0_25_kappa, Pr0_5_kappa, Pr1_kappa, Pr4_kappa))
    min_k = np.amin(all_kappa)
    max_k = np.amax(all_kappa)
    kappa = np.logspace(np.log10(min_k), np.log10(max_k), 16)
    all_nu = np.concatenate((Pr2_nu, Pr0_25_nu, Pr0_5_nu, Pr1_nu, Pr4_nu))
    min_n = np.amin(all_nu)
    max_n = np.amax(all_nu)
    nu = np.logspace(np.log10(min_n), np.log10(max_n), 16)

    # cutoff for low vs. high Rayleigh number
    aRa_F_crit = myP[quant]['Ra_crit']     #5e3
    rRa_F_crit = myP[quant]['Ra_crit_rms'] #3e3

    lmin = myP[quant]['lmin']; lmax = myP[quant]['lmax']
    if (myP[quant]['plot_var']):
        lmin_rms = myP[quant]['lmin_var']; lmax_rms = myP[quant]['lmax_var']
    else:
        lmin_rms = myP[quant]['lmin_rms']; lmax_rms = myP[quant]['lmax_rms']

    min_Ra_F = 0.25*Ra_F[0]; max_Ra_F = 4*Ra_F[-1]
    min_kappa = 0.5*min_k; max_kappa = 2*max_k
    min_nu = 0.5*min_n; max_nu = 2*max_n

    plt.figure(1, figsize=(5, 8), dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    aspect     = 1.0*np.log10(max_Ra_F/min_Ra_F) / np.log10(lmax/lmin)
    aspect_rms = 1.0*np.log10(max_Ra_F/min_Ra_F) / np.log10(lmax_rms/lmin_rms)

    #######################################################
    # fits to \ell ~ Ra^b Pr^c
    #######################################################
    _RaF = np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F))
    _Pr  = np.concatenate((Pr2_Pr, Pr0_25_Pr, Pr0_5_Pr, Pr1_Pr, Pr4_Pr))
    uRaF, uind = np.unique(_RaF, return_index=True)
    _avgell_u = np.concatenate((Pr2_lavg_upper, Pr0_25_lavg_upper,
                           Pr0_5_lavg_upper, Pr1_lavg_upper, Pr4_lavg_upper))
    _avgell_m = np.concatenate((Pr2_lavg_middle, Pr0_25_lavg_middle,
                           Pr0_5_lavg_middle, Pr1_lavg_middle, Pr4_lavg_middle))
    _avgell_l = np.concatenate((Pr2_lavg_lower, Pr0_25_lavg_lower,
                           Pr0_5_lavg_lower, Pr1_lavg_lower, Pr4_lavg_lower))
    _rmsell_u = np.concatenate((Pr2_lrms_upper, Pr0_25_lrms_upper,
                           Pr0_5_lrms_upper, Pr1_lrms_upper, Pr4_lrms_upper))
    _rmsell_m = np.concatenate((Pr2_lrms_middle, Pr0_25_lrms_middle,
                           Pr0_5_lrms_middle, Pr1_lrms_middle, Pr4_lrms_middle))
    _rmsell_l = np.concatenate((Pr2_lrms_lower, Pr0_25_lrms_lower,
                           Pr0_5_lrms_lower, Pr1_lrms_lower, Pr4_lrms_lower))
    aind = np.where(_RaF > aRa_F_crit); rind = np.where(_RaF > rRa_F_crit)
    #_RaF = _RaF[ind]; _Pr = _Pr[ind]
    _avgell_u = _avgell_u[aind]; _avgell_m = _avgell_m[aind]; _avgell_l = _avgell_l[aind]
    _rmsell_u = _rmsell_u[rind]; _rmsell_m = _rmsell_m[rind]; _rmsell_l = _rmsell_l[rind]
    all_RaF = np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F))
    all_Pr  = np.concatenate((Pr2_Pr, Pr0_25_Pr, Pr0_5_Pr, Pr1_Pr, Pr4_Pr))
    uRaF, uind = np.unique(all_RaF, return_index=True)
    a_RaF = _RaF[aind]; a_Pr = _Pr[aind]
    r_RaF = _RaF[rind]; r_Pr = _Pr[rind]
    # avg ell upper
    print("...fitting average ell upper...")
    a0 = (4e3, 0.6, 0.)
    _par, _err, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(a_RaF, _avgell_u, a_Pr, a0, **fit_args)
    avgell_u_a = _par[0]; avgell_u_b = _par[1]; avgell_u_c = _par[2]; 
    avgell_u_da = _err[0]; avgell_u_db = _err[1]; avgell_u_dc = _err[2]; 
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[1], _err[1], 100*_err[1]/abs(_par[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[2], _err[2], 100*_err[2]/abs(_par[2])))
    print("\ta = {}".format(_par[0]))
    # avg ell middle
    print("...fitting average ell middle...")
    a0 = (4e3, 0.6, 0.1)
    _par, _err, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(a_RaF, _avgell_m, a_Pr, a0, **fit_args)
    avgell_m_a = _par[0]; avgell_m_b = _par[1]; avgell_m_c = _par[2]; 
    avgell_m_da = _err[0]; avgell_m_db = _err[1]; avgell_m_dc = _err[2]; 
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[1], _err[1], 100*_err[1]/abs(_par[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[2], _err[2], 100*_err[2]/abs(_par[2])))
    print("\ta = {}".format(_par[0]))
    # avg ell lower
    print("...fitting average ell lower...")
    a0 = (4e3, 0.6, 0.1)
    _par, _err, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(a_RaF, _avgell_l, a_Pr, a0, **fit_args)
    avgell_l_a = _par[0]; avgell_l_b = _par[1]; avgell_l_c = _par[2]; 
    avgell_l_da = _err[0]; avgell_l_db = _err[1]; avgell_l_dc = _err[2]; 
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[1], _err[1], 100*_err[1]/abs(_par[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[2], _err[2], 100*_err[2]/abs(_par[2])))
    print("\ta = {}".format(_par[0]))
    # rms ell upper
    print("...fitting rms ell upper...")
    a0 = (1e3, -0.5, 0.1)
    _par, _err, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(r_RaF, _rmsell_u, r_Pr, a0, **fit_args)
    rmsell_u_a = _par[0]; rmsell_u_b = _par[1]; rmsell_u_c = _par[2]; 
    rmsell_u_da = _err[0]; rmsell_u_db = _err[1]; rmsell_u_dc = _err[2]; 
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[1], _err[1], 100*_err[1]/abs(_par[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[2], _err[2], 100*_err[2]/abs(_par[2])))
    print("\ta = {}".format(_par[0]))
    # rms ell middle
    print("...fitting rms ell middle...")
    a0 = (1e3, -0.5, -0.1)
    _par, _err, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(r_RaF, _rmsell_m, r_Pr, a0, **fit_args)
    rmsell_m_a = _par[0]; rmsell_m_b = _par[1]; rmsell_m_c = _par[2]; 
    rmsell_m_da = _err[0]; rmsell_m_db = _err[1]; rmsell_m_dc = _err[2]; 
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[1], _err[1], 100*_err[1]/abs(_par[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[2], _err[2], 100*_err[2]/abs(_par[2])))
    print("\ta = {}".format(_par[0]))
    # rms ell lower
    print("...fitting rms ell lower...")
    a0 = (1e3, -0.5, -0.1)
    _par, _err, _chi2, _chi2r, _pte = fit_RaPr.fitting_RaPr(r_RaF, _rmsell_l, r_Pr, a0, **fit_args)
    rmsell_l_a = _par[0]; rmsell_l_b = _par[1]; rmsell_l_c = _par[2]; 
    rmsell_l_da = _err[0]; rmsell_l_db = _err[1]; rmsell_l_dc = _err[2]; 
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[1], _err[1], 100*_err[1]/abs(_par[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_par[2], _err[2], 100*_err[2]/abs(_par[2])))
    print("\ta = {}".format(_par[0]))
    #######################################################
    # average plots
    #######################################################
    a = avgell_u_a; b = avgell_u_b; c = avgell_u_c
    da = avgell_u_da; db = avgell_u_db; dc = avgell_u_dc
    #xmin = np.min(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmax = np.max(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmin = 0.7*xmin; xmax = 1.3*xmax
    #aspect = 1.0*np.log10(xmax/xmin) / np.log10(lmax/lmin)
    plt.subplot(321, aspect=aspect)
    plt.axvline(x=aRa_F_crit, color='k', linestyle='-.')
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    ax = plt.gca()
    plt.text(myP[quant]['Rtextx1'], myP[quant]['Rtexty1'], str1, fontsize='small', transform=ax.transAxes)
    plt.text(myP[quant]['Ptextx1'], myP[quant]['Ptexty1'], str2, fontsize='small', transform=ax.transAxes)
    #plt.plot(all_RaF[uind], a*all_RaF[uind]**b*all_Pr[uind]**c,
    #         color='k', linestyle='--', label=None)
    plt.plot(Pr0_25_Ra_F, Pr0_25_lavg_upper, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_lavg_upper, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_lavg_upper, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_lavg_upper, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_lavg_upper, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (myP[quant]['legend'] == 1):
        plt.legend(loc=myP[quant]['legloc'], frameon=True, fontsize=myP[quant]['legfont'], numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(lmin, lmax)
    plt.xlim(min_Ra_F, max_Ra_F)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    plt.ylabel(r"Upper Convection Zone")
    plt.title(r"$\left<\ell\right>$")

    a = avgell_m_a; b = avgell_m_b; c = avgell_m_c
    da = avgell_m_da; db = avgell_m_db; dc = avgell_m_dc
    #xmin = np.min(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmax = np.max(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmin = 0.7*xmin; xmax = 1.3*xmax
    #aspect = 1.0*np.log10(xmax/xmin) / np.log10(lmax/lmin)
    plt.subplot(323, aspect=aspect)
    plt.axvline(x=aRa_F_crit, color='k', linestyle='-.')
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    ax = plt.gca()
    plt.text(myP[quant]['Rtextx3'], myP[quant]['Rtexty3'], str1, fontsize='small', transform=ax.transAxes)
    plt.text(myP[quant]['Ptextx3'], myP[quant]['Ptexty3'], str2, fontsize='small', transform=ax.transAxes)
    #plt.plot(all_RaF[uind], a*all_RaF[uind]**b*all_Pr[uind]**c,
    #         color='k', linestyle='--', label=None)
    plt.plot(Pr0_25_Ra_F, Pr0_25_lavg_middle, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_lavg_middle, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_lavg_middle, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_lavg_middle, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_lavg_middle, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (myP[quant]['legend'] == 3):
        plt.legend(loc=myP[quant]['legloc'], frameon=True, fontsize=myP[quant]['legfont'], numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(lmin, lmax)
    plt.xlim(min_Ra_F, max_Ra_F)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    plt.ylabel(r"Middle Convection Zone")
    #title = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%" "\n" \
    #    r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b), c, 100*dc/abs(c))
    #plt.title(title)
    #plt.title(r"Average $\ell$")

    a = avgell_l_a; b = avgell_l_b; c = avgell_l_c
    da = avgell_l_da; db = avgell_l_db; dc = avgell_l_dc
    #xmin = np.min(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmax = np.max(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmin = 0.7*xmin; xmax = 1.3*xmax
    #aspect = 1.0*np.log10(xmax/xmin) / np.log10(lmax/lmin)
    plt.subplot(325, aspect=aspect)
    plt.axvline(x=aRa_F_crit, color='k', linestyle='-.')
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    ax = plt.gca()
    plt.text(myP[quant]['Rtextx5'], myP[quant]['Rtexty5'], str1, fontsize='small', transform=ax.transAxes)
    plt.text(myP[quant]['Ptextx5'], myP[quant]['Ptexty5'], str2, fontsize='small', transform=ax.transAxes)
    #plt.plot(all_RaF[uind], a*all_RaF[uind]**b*all_Pr[uind]**c,
    #         color='k', linestyle='--', label=None)
    plt.plot(Pr0_25_Ra_F, Pr0_25_lavg_lower, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_lavg_lower, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_lavg_lower, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_lavg_lower, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_lavg_lower, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (myP[quant]['legend'] == 5):
        plt.legend(loc=myP[quant]['legloc'], frameon=True, fontsize=myP[quant]['legfont'], numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(lmin, lmax)
    plt.xlim(min_Ra_F, max_Ra_F)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    plt.ylabel(r"Lower Convection Zone")
    #title = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%" "\n" \
    #    r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b), c, 100*dc/abs(c))
    #plt.title(title)
    #plt.title(r"Average $\ell$")

    ############################
    # RMS plots
    ############################
    a = rmsell_u_a; b = rmsell_u_b; c = rmsell_u_c
    da = rmsell_u_da; db = rmsell_u_db; dc = rmsell_u_dc
    #xmin = np.min(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmax = np.max(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmin = 0.7*xmin; xmax = 1.3*xmax
    #aspect_rms = 1.0*np.log10(xmax/xmin) / np.log10(lmax_rms/lmin_rms)
    plt.subplot(322, aspect=aspect_rms)
    plt.axvline(x=rRa_F_crit, color='k', linestyle='-.')
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    ax = plt.gca()
    plt.text(myP[quant]['Rtextx2'], myP[quant]['Rtexty2'], str1, fontsize='small', transform=ax.transAxes)
    plt.text(myP[quant]['Ptextx2'], myP[quant]['Ptexty2'], str2, fontsize='small', transform=ax.transAxes)
    #plt.plot(all_RaF[uind], a*all_RaF[uind]**b*all_Pr[uind]**c,
    #         color='k', linestyle='--', label=None)
    plt.plot(Pr0_25_Ra_F, Pr0_25_lrms_upper, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_lrms_upper, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_lrms_upper, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_lrms_upper, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_lrms_upper, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (myP[quant]['legend'] == 2):
        plt.legend(loc=myP[quant]['legloc'], frameon=True, fontsize=myP[quant]['legfont'], numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(lmin_rms, lmax_rms)
    plt.xlim(min_Ra_F, max_Ra_F)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    #plt.ylabel(r"Upper Convection Zone")
    if (myP[quant]['plot_var']):
        plt.title(r"$\left<\ell^2\right> - \left<\ell\right>^2$")
    else:
        plt.title(r"$\sqrt{\left<\ell^2\right>}$")

    a = rmsell_m_a; b = rmsell_m_b; c = rmsell_m_c
    da = rmsell_m_da; db = rmsell_m_db; dc = rmsell_m_dc
    #xmin = np.min(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmax = np.max(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmin = 0.7*xmin; xmax = 1.3*xmax
    #aspect_rms = 1.0*np.log10(xmax/xmin) / np.log10(lmax_rms/lmin_rms)
    plt.subplot(324, aspect=aspect_rms)
    plt.axvline(x=rRa_F_crit, color='k', linestyle='-.')
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    ax = plt.gca()
    plt.text(myP[quant]['Rtextx4'], myP[quant]['Rtexty4'], str1, fontsize='small', transform=ax.transAxes)
    plt.text(myP[quant]['Ptextx4'], myP[quant]['Ptexty4'], str2, fontsize='small', transform=ax.transAxes)
    #plt.plot(all_RaF[uind], a*all_RaF[uind]**b*all_Pr[uind]**c,
    #         color='k', linestyle='--', label=None)
    plt.plot(Pr0_25_Ra_F, Pr0_25_lrms_middle, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_lrms_middle, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_lrms_middle, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_lrms_middle, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_lrms_middle, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (myP[quant]['legend'] == 4):
        plt.legend(loc=myP[quant]['legloc'], frameon=True, fontsize=myP[quant]['legfont'], numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(lmin_rms, lmax_rms)
    plt.xlim(min_Ra_F, max_Ra_F)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    #plt.ylabel(r"Middle Convection Zone")
    #title = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%" "\n" \
    #    r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b), c, 100*dc/abs(c))
    #plt.title(title)
    #plt.title(r"Average $\ell$")

    a = rmsell_l_a; b = rmsell_l_b; c = rmsell_l_c
    da = rmsell_l_da; db = rmsell_l_db; dc = rmsell_l_dc
    #xmin = np.min(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmax = np.max(all_RaF[uind]**b*all_Pr[uind]**c)
    #xmin = 0.7*xmin; xmax = 1.3*xmax
    #aspect_rms = 1.0*np.log10(xmax/xmin) / np.log10(lmax_rms/lmin_rms)
    plt.subplot(326, aspect=aspect_rms)
    plt.axvline(x=rRa_F_crit, color='k', linestyle='-.')
    str1 = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b))
    str2 = r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(c, 100*dc/abs(c))
    ax = plt.gca()
    plt.text(myP[quant]['Rtextx6'], myP[quant]['Rtexty6'], str1, fontsize='small', transform=ax.transAxes)
    plt.text(myP[quant]['Ptextx6'], myP[quant]['Ptexty6'], str2, fontsize='small', transform=ax.transAxes)
    #plt.plot(all_RaF[uind], a*all_RaF[uind]**b*all_Pr[uind]**c,
    #         color='k', linestyle='--', label=None)
    plt.plot(Pr0_25_Ra_F, Pr0_25_lrms_lower, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_lrms_lower, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_lrms_lower, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_lrms_lower, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_lrms_lower, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    if (myP[quant]['legend'] == 6):
        plt.legend(loc=myP[quant]['legloc'], frameon=True, fontsize=myP[quant]['legfont'], numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(lmin_rms, lmax_rms)
    plt.xlim(min_Ra_F, max_Ra_F)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    #plt.ylabel(r"Lower Convection Zone")
    #title = r"$\mathrm{{Ra}}_\mathrm{{F}}^a$ = {:.2f} +/- {:.0f}%" "\n" \
    #   r"$\mathrm{{Pr}}^b$ = {:.2f} +/- {:.0f}%".format(b, 100*db/abs(b), c, 100*dc/abs(c))
    #plt.title(title)
    #plt.title(r"Average $\ell$")
    plt.tight_layout()

    plt.suptitle(myP[quant]['title'], fontsize='large')

    if (save):
        plt.savefig(output+"_l.png")
        print("saved image: {}".format(output+"_l.png"))
    else:
        plt.show()

    plt.close()

def read_datafile(filename, separator=None):
    """
    parse data file to extract calculated quantities
    """
    from collections import OrderedDict

    # allocate space
    data_values = OrderedDict()

    with open(filename, "r") as f:

        # only parse non-empty lines and lines that don't start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                fields = line.split(separator) # separate the fields

                # remove all whitespace from each field
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract specific values
                Pr       = fields[0]
                _kappa   = float(fields[1])
                _Ra_F    = float(fields[2])
                _Ra_BL   = float(fields[3])
                _lavg_u  = float(fields[4])
                _lrms_u  = float(fields[5])
                _lavg_m  = float(fields[6])
                _lrms_m  = float(fields[7])
                _lavg_l  = float(fields[8])
                _lrms_l  = float(fields[9])

                # this is a new entry so initialize the data structure
                if (Pr not in data_values.keys()):
                    data_values[Pr] = OrderedDict({'kappa':[], 'Ra_F':[], 'Ra_BL':[],
                                       'lavg_upper':[], 'lavg_middle':[], 'lavg_lower':[],
                                       'lrms_upper':[], 'lrms_middle':[], 'lrms_lower':[]})

                # add the data
                data_values[Pr]['kappa'].append(  _kappa)
                data_values[Pr]['Ra_F'].append(   _Ra_F)
                data_values[Pr]['Ra_BL'].append(  _Ra_BL)
                data_values[Pr]['lavg_upper'].append( _lavg_u)
                data_values[Pr]['lavg_middle'].append(_lavg_m)
                data_values[Pr]['lavg_lower'].append( _lavg_l)
                data_values[Pr]['lrms_upper'].append( _lrms_u)
                data_values[Pr]['lrms_middle'].append(_lrms_m)
                data_values[Pr]['lrms_lower'].append( _lrms_l)

    # convert the lists into np arrays
    for pr in data_values.keys():
        for field in data_values[pr].keys():
            data_values[pr][field] = np.array(data_values[pr][field])

    return data_values

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(args['--save'], args['--output'], args['<data-file>'], args['--debug'],
         args['--quant'])

