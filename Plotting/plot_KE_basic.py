"""
Plots average KE vs time

Usage:
    plot_energy_KE.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_trace.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --magnetic      Plot the magnetic energy [default: False]

"""

from __future__ import print_function # this is for python 3 compatability
import env
from ReadRayleigh.diagnostic_reading import GlobalAverage, build_file_list, ReferenceState
import matplotlib.pyplot as plt

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, magnetic):

    # make sure the parent directory ends with a "/"
    if (data_dir[-1] != "/"):
        data_dir = data_dir + '/'

    # Build a list of all files ranging from start_file iteration to end_file iteration
    files = build_file_list(start_file,end_file,path=data_dir+'G_Avgs')

    # read in the first file to get the lookup table, assumed to be the same for all files
    a = GlobalAverage(filename=files[0],path='')
    ke_index = a.lut[125]      # 125 = Kinetic Energy (KE)
    if (magnetic):
        me_index = a.lut[475]  # 475 = Magnetic Energy (ME)

    # intialize some empty lists to hold the energy
    ke = []
    if (magnetic):
        me = []

    # also define an empty list to hold the time
    alldays = []
    alltime = []

    # loop over all files and grab the desired data
    for f in files[-last_number_files:]:
        a = GlobalAverage(filename=f,path='')  # open the file

        days = a.time/(3600.0*24.0)            # extract the time
        time = a.time

        for i,d in enumerate(days):            # loop over all time records of the file
            alldays.append(d)                  # and store the time and energy
            alltime.append(time[i])
            ke.append(a.vals[i,ke_index])
            if (magnetic):
                me.append(a.vals[i,me_index])

    # report what files were read
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    # if you don't want to plot stuff, then return
    if (no_plots):
        return

    # change the plotting parameters somewhat depending on saveplot
    if (saveplot):
        dpi = 300
        plt.figure(1,figsize=(7.5, 4.0), dpi=dpi)
        plt.rcParams.update({'font.size': 12})
    else:
        dpi = 100
        plt.figure(1,figsize=(7.5,4),dpi=dpi)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0)) # scientific notation
    plt.plot(alltime,ke,'red',label=r'KE$_{total}$')             # plot KE
    if (magnetic):
        plt.plot(alltime,me,'blue',label=r'ME$_{total}$')        # plot ME
    plt.yscale('log')
    plt.xlabel('Time (simulation secs)')
    plt.ylabel('Energy Density '+r'(simulation erg cm$^{-3}$)')
    plt.tight_layout()
    if (saveplot):
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2, fontsize = 'x-small') 
        plt.savefig(savefile, dpi=dpi)
        print("saved image: {}".format(savefile))
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 
        plt.show()

    plt.close()

if __name__ == "__main__":

    #from docopt import docopt
    #args = docopt(__doc__)

    start_file = 0                #int(args['--start'])
    end_file   = 100000000        #int(args['--end'])
    last_number_files = 0         #int(args['--last'])
    saveplot = False              #args['--save']
    savefile = 'energy_trace.png' #args['--output']
    data_dir = './'               #args['--data-dir']
    no_plots = False              #args['--no-plots']
    magnetic = False              #args['--magnetic']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, magnetic)

