"""
Plot meridional slices

Usage:
    plot_meridional_slice.py [options]

Options:
    --timestep=<t>    Specify iteration number to plot [default: 00005000]
    --quantity=<q>    Give quantity code [default: 64]
    --phi-index=<p>   Set the phi index [default: 1]
    --no-mean         Remove the m=0 mean [default: False]
    --Pm=<p>          Convert to magnetic diffusion times [default: 1]
    --save            Save an image [default: False]
    --output=<o>      Save image as <o> [default: meridional_slice.png]
    --data-dir=<d>    Specify location of data [default: ./]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import Meridional_Slice
import numpy as np
import matplotlib.pyplot as plt

def main(timestep, quantity_code, pindex, remove_mean, save, output, data_dir, Prm):

    tindex = 0          # Display the first timestep from the file

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    a = Meridional_Slice(filename=timestep, path=data_dir+'Meridional_Slices/')


    #Set up the grid
    nr = a.nr
    ntheta = a.ntheta
    r = a.radius/np.max(a.radius)
    # We apply a shift in theta so that theta=0 corresponds to equator and the poles appear where we expect them to...
    theta = np.arccos(a.costheta)-np.pi/2  

    radius_matrix, theta_matrix = np.meshgrid(r,theta)

    X = radius_matrix * np.cos(theta_matrix)
    Y = radius_matrix * np.sin(theta_matrix)

    time = a.time[tindex]*Prm

    qindex = a.lut[quantity_code]
    field = np.zeros((ntheta,nr),dtype='float64')
    field[:,:] =a.vals[pindex,:,:,qindex,tindex]


    #remove the mean if desired
    if (remove_mean):
        for i in range(nr):
            the_mean = np.mean(field[:,i])
            field[:,i] = field[:,i]-the_mean
    radtodeg = 180.0/np.pi
    print('Displaying meridional slice at phi (radians, degrees) = ', a.phi[pindex], a.phi[pindex]*radtodeg)

    print('Time = ',time)
    #Plot
    plt.figure(1, figsize=(4,8))
    cmap = 'RdBu_r' # 'jet'
    img = plt.pcolormesh(X,Y,field,cmap=cmap)
    plt.gca().set_aspect(1)

    time = "{:.3f}".format(time)
    angle = "{:.3f}".format(a.phi[pindex]*radtodeg)
    plt.title("$t/t_\eta = {{{}}}$, $\phi = {{{}}}^o$".format(time, angle))

    if (save):
        plt.savefig(output)
    else:
        plt.show()

if __name__ == "__main__":
    import docopt
    args = docopt.docopt(__doc__)

    timestep = args['--timestep']
    quantity_code = int(args['--quantity'])
    pindex = int(args['--phi-index'])
    remove_mean = args['--no-mean']
    save = args['--save']
    output = args['--output']
    data_dir = args['--data-dir']
    Prm = float(args['--Pm'])

    main(timestep, quantity_code, pindex, remove_mean, save, output, data_dir, Prm)
