"""

  Shell-Averages (Shell_Avgs) plotting example
  Reads in time steps 3 million through 3.3 million
  Plots average Energy flux (normalized by 4pi r^2)

  This example routine makes use of the ShellAverage
  data structure associated with the Shell_Avg output.
  Upon initializing a ShellAverage object, the 
  object will contain the following attributes:

    ----------------------------------
    self.niter                         : number of time steps
    self.nq                            : number of diagnostic quantities output
    self.nr                            : number of radial points
    self.qv[0:nq-1]                    : quantity codes for the diagnostics output
    self.radius[0:nr-1]                : radial grid

    self.vals[0:nr-1,0:3,0:nq-1,0:niter-1] : The spherically averaged diagnostics
                                             0-3 refers to moments (index 0 is mean, index 3 is kurtosis)    
    self.iters[0:niter-1]              : The time step numbers stored in this output file
    self.time[0:niter-1]               : The simulation time corresponding to each time step
    self.version                       : The version code for this particular output (internal use)
    self.lut                           : Lookup table for the different diagnostics output
   -------------------------------------

Usage:
    plot_energy_flux.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_flux.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --Lnorm=<L>     Normalize 4*pi*r**2*Flux by <L> [default: 3.846e33]
    --no-plots      Suppress the plots [default: False]
    --kappa=<k>     Set thermal diffusivity cm**2/sec [default: 2e12]
    --Pr=<p>        Prandtl number, Pr=nu/kappa [default: 1]
    --aspect=<a>    Set aspect ratio [default: 0.4]
    --write-to=<f>  Store rms velocity data to file <f> [default: ]
    --plot-Vrms     Plot the rms velocity [default: False]
    --paper-data    Run using old data format [default: False]
    --rms-only      Only process the rms stuff, not the fluxes [default: False]
    --Ra=<r>        Set Rayleigh number, if r==-1, calculate it from reference file [default: -1]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list, ReferenceState
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from NumericalToolsLocal.calculus.integrate import volume_avg
import os
from Utilities.look_up_quantity import shortcut_lookup, general_lookup
import Utilities.calculate_Nu as calculate_Nu

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Lnorm, kappa, Pr, no_plots, write_to='', plot_Vrms=False,
         paper_data=False, rms_only=False, Ra=-1, aspect_ratio=0.4, **kwargs):
    # Set saveplot to True to save to a file. 
    # Set to False to view plots interactively on-screen.
    #saveplot = False 
    #savefile = 'energy_flux.pdf'  #If pdf gives issues, try .png instead.

    # make sure we use the correct lookup table
    if (paper_data):
        alt = True
    else:
        alt = False

    # integration method
    method='cheb-end'

    #Time-average the data generated between timesteps 0 million and 10 million
    # Note that this averaging scheme assumes taht the radial grid didn't
    # change during the course of the run. 
    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
    icount = 0.0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellAverage(f,path='')
        if (i == 0):
            if (not paper_data):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
            else:
                data = np.zeros((a.nr,a.nq),dtype='float64')
            t_start = a.time[0]
        for j in range(a.niter):
            if (not paper_data):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
            else:
                data[:,:] = data[:,:]+a.vals[:,:,j]
            icount = icount+1.0
        t_end = a.time[-1]

    delta_time = (t_end - t_start)/3600./24.
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} days".format(t_start/3600./24.))
    print("Last  time: {} days".format(t_end/3600./24.))
    print("\nAveraged over {} days".format(delta_time))

    data = data/icount  # The average is now stored in data
    radius = a.radius
    fpr = 4.0*np.pi*radius*radius  # four pi r^2
    rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1

    # read reference state
    ref = ReferenceState(filename="reference", path=data_dir)

    #Next, we identify and grab the various fluxes we would like to plot
    ###for k in ['cond_flux', 'vol_heat_flux', 'enth_flux', 'visc_flux', 'ke_flux']:
    ###    print(k, ":", shortcut_lookup(k, alt=alt))
    for k in range(len(a.lut)):
        if (a.lut[k] != 5000):
            print(k, general_lookup(index=k, alt=alt))
    if (not rms_only):
        cf_index  = a.lut[shortcut_lookup('cond_flux',alt=alt)]  #a.lut[122]  # Conductive energy flux
        vhf_index = a.lut[shortcut_lookup('vol_heat_flux',alt=alt)]  #a.lut[121]  # The effective flux associated with volumetric heating
        enf_index = a.lut[shortcut_lookup('enth_flux',alt=alt)]  #a.lut[119]  # Enthalpy flux
        vsf_index = a.lut[shortcut_lookup('visc_flux',alt=alt)]  #a.lut[120]  # Viscous energy flux
        kef_index = a.lut[shortcut_lookup('ke_flux',alt=alt)]    #a.lut[117]  # Kinetic energy flux

        if (not paper_data):
            cflux = data[:,0,cf_index] # Grab appropriate slices of the data array
            hflux = data[:,0,vhf_index]
            eflux = data[:,0,enf_index]
            vflux =-data[:,0,vsf_index]
            kflux = data[:,0,kef_index]
        else:
            cflux = data[:,cf_index] # Grab appropriate slices of the data array
            try:
                hflux = data[:,vhf_index]
            except:
                hflux = np.zeros_like(cflux)
            try:
                eflux = data[:,enf_index]
            except:
                eflux = np.zeros_like(cflux)
            try:
                vflux =-data[:,vsf_index]
            except:
                vflux = np.zeros_like(cflux)
            try:
                kflux = data[:,kef_index]
            except:
                kflux = np.zeros_like(cflux)

        tflux = cflux+eflux+vflux+kflux+hflux # total flux

    # calculate the Reynolds numbers
    #  get the rms velocity from the KE:
    #    KE = 0.5*rho*V_rms**2
    #    V_rms**2 = 2*KE/rho
    #    KE_rad = 0.5*rho*Vr_rms**2
    #    Vr_rms**2 = 2*KE_rad/rho
    ke_index  = a.lut[shortcut_lookup('ke',alt=alt)]   #a.lut[125]  # total KE
    rke_index = a.lut[shortcut_lookup('rke',alt=alt)]  #a.lut[126] # radial component of KE
    if (not paper_data):
        rke = data[:,0,rke_index]
        ke  = data[:,0,ke_index]
    else:
        rke = data[:,rke_index]
        ke  = data[:,ke_index]
    rho = ref.density

    U_rms_squared  = 2*ke/rho
    Ur_rms_squared = 2*rke/rho

    U_rms  = np.sqrt(U_rms_squared)
    Ur_rms = np.sqrt(Ur_rms_squared)
    Urms_peak = np.amax(U_rms)

    H = np.amax(radius) - np.amin(radius)
    nu = kappa*Pr

    # volume integrate v_squared
    U2_tilde = volume_avg(U_rms_squared, radius, method=method, data_reversed=True)

    Reynolds = np.sqrt(U2_tilde)*H/nu
    Reynolds_peak = Urms_peak*H/nu
    print("\nReynolds number, Re = {}".format(Reynolds))
    print("Peak Re number, Re_peak = {}".format(Reynolds_peak))

    # calculate \delta V
    # see Eqn 20 of Featherstone & Hindman 2016 ApJ 818
    nr = len(Ur_rms)
    nr_mid = int(nr/2.)
    delta_V = (np.amax(Ur_rms) - Ur_rms[nr_mid]) / Ur_rms[nr_mid]
    print("\nVelocity difference, delta V = {}".format(delta_V))

    if (write_to != ''):
        print("\nsaving data to: {}".format(write_to))
        if (not os.path.exists(write_to)):
            mf = open(write_to, 'w')
            mf.write("#\n")
            mf.write("# store velocity rms data from various runs\n")
            mf.write("# each data entry will take up 1 line and have the form:\n")
            mf.write("#    Pr  Ra_F  ---  <space separated V_rms data> --- <radius data>\n")
            mf.write("#\n")
            mf.write("#For example here would be 3 distinct entries:\n")
            mf.write("#    2    4.3031e5  ---  1e8 3e8 ... 3e4 --- 5e10 5.1e10 ... 6.5e10\n")
            mf.write("#    0.5  4.3031e5  ---  1e8 2e8 ... 2e4 --- 5e10 5.1e10 ... 6.5e10\n")
            mf.write("#    0.25 5.3789e4  ---  3e8 4e8 ... 1e4 --- 5e10 5.1e10 ... 6.5e10\n")
            mf.write("#\n")
        else:
            mf = open(write_to, 'a')

        if (Ra == -1):
            import calculate_Ra
            c_p = 3.5e8
            Ra, Ra_BL = calculate_Ra.main(False, '', Lnorm, data_dir, kappa, Pr, c_p, return_Ra=True)
        mf.write("{}\t{}".format(Pr, Ra))
        mf.write("\t---")
        for i in range(len(Ur_rms)):
            mf.write("\t{}".format(Ur_rms[i]))
        mf.write("\t---")
        for i in range(len(radius)):
            mf.write("\t{}".format(radius[i]))
        mf.write("\n")
        mf.close()

    if (plot_Vrms):
        plt.clf()
        Vr = Ur_rms / np.amax(Ur_rms)
        #plt.plot(radius/radius[0], Vr, label='V_r rms', color='r')
        plt.plot(radius, Vr, label='V_r rms', color='r')
        plt.ylim(-0.1, 1.1)
        plt.xlabel('radius')
        plt.ylabel('rms Vr')
        plt.legend(loc='upper left')
        if (not no_plots):
            plt.show()

    # calculate the boundary layer thickness,
    # see Eqn 27 of Featherstone & Hindman 2016 ApJ 818
    s_index = a.lut[shortcut_lookup('s',alt=alt)]  #a.lut[64]
    if (not paper_data):
        entropy = data[:,0,s_index]
    else:
        entropy = data[:,s_index]
    max_S = np.amax(entropy)
    w_BL = my_integrator(max_S - entropy, radius, method=method, data_reversed=True)/max_S
    w_BL *= 1e-8 # convert from cm to Mm
    print("\nBoundary Layer thickness, w_BL = {} Mm".format(w_BL))

    #####
    import calculate_BL
    calculate_BL.main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
                             1, verbose=False, alt=alt)
    if (not alt):
        calculate_BL.main(start_file, end_file, last_number_files, saveplot, savefile,
                          data_dir, 2, verbose=False, alt=alt)
    else:
        try:
            calculate_BL.main(start_file, end_file, last_number_files, saveplot, savefile,                                   data_dir, 2, verbose=False, alt=alt)
        except:
            print("\nWARNING: Could not use method=2 to calculate Bdry Layer\n")
    #####

    delta_S = max_S - np.amin(entropy)
    print("\nDelta S = {} erg/g/K".format(delta_S))

    if (rms_only):
        return

    # calculate fractional convective flux f_conv
    #     f_conv = 0 --> lack of convective heat transport
    #     f_conv = 1 --> convective is dominant over thermal conduction
    # see Eqn 26 of Featherstone & Hindman 2016 ApJ 818
    numer = 4*np.pi*my_integrator((eflux+kflux+vflux)*radius*radius, radius,
                                  method=method, data_reversed=True)
    denom = 4*np.pi*my_integrator((eflux+kflux+vflux+cflux)*radius*radius, radius,
                                  method=method, data_reversed=True)
    f_conv = numer/denom
    if (not alt):
        Nusselt = calculate_Nu.main(start_file, end_file, last_number_files,
                           saveplot, savefile, data_dir, verbose=False,
                           return_Nu=True, aspect_ratio=aspect_ratio)
    else:
        # 1/(1-f_conv) = F_conv/F_cond + 1 + (F_KE + F_visc)/F_cond
        #              = Nusselt + (F_KE + F_visc)/F_cond
        #             ~= Nusselt + F_KE/F_cond
        i_ke   = a.lut[shortcut_lookup('ke_flux',alt=alt)]
        i_cond = a.lut[shortcut_lookup('cond_flux',alt=alt)]
        ke_flux   = data[:,i_ke]
        cond_flux = data[:,i_cond]
        vol_ke   = volume_avg(ke_flux,   radius, method=method, data_reversed=True)
        vol_cond = volume_avg(cond_flux, radius, method=method, data_reversed=True)
        Nusselt_correction = vol_ke / vol_cond
        print("\nNusselt number, Nu ~= 1/(1-f_conv) - {}".format(Nusselt_correction))

    print("\nFractional Convective Flux, f_conv = {}".format(f_conv))
    print("\nNusselt number, Nu = {}".format(Nusselt))

    # radius starts at R=1 and goes inwards to R=0.7
    top_flux_total = tflux[0]*fpr[0]/Lnorm
    bottom_flux_total = tflux[-1]*fpr[-1]/Lnorm
    print("\n--Total Flux--")
    print("\tbottom : {}".format(bottom_flux_total))
    print("\ttop    : {}".format(top_flux_total))
    L2 = np.sqrt(np.sum((tflux*fpr/Lnorm - 1)**2))
    if (Lnorm != 1):
        print("\tL2     : {}".format(L2))

    # another test to see if equilibration has taken place
    # the entropy gradient should be:
    #    dSdr(rtop) = - Lstar / (4*pi*rtop**2 kappa * rho(rtop) * T(rtop))
    dsdr_index = a.lut[shortcut_lookup('dsdr',alt=alt)]  #a.lut[70]
    if (not paper_data):
        entropy_dr = data[:,0,dsdr_index]
    else:
        entropy_dr = data[:,dsdr_index]
    rtop = ref.radius[0]
    rho_top = ref.density[0]
    T_top = ref.temperature[0]
    dSdr = -Lnorm / (4*np.pi*rtop**2*kappa*rho_top*T_top)
    abs_err = dSdr - entropy_dr[0]
    rel_err = abs_err / dSdr
    if (Lnorm != 1):
        print("\n--Entropy Gradient--")
        print("\tdSdr(top) background  = {}".format(dSdr))
        print("\tdSdr(top) from output = {}".format(entropy_dr[0]))
        print("\tabs error = {:.4e}".format(abs_err))
        print("\trel error = {:.4e} ({:.2f} %)\n".format(rel_err, 100*rel_err))

    #Create the plot

    lw = 1.5

    if (saveplot):
        plt.figure(1,figsize=(7.0, 5.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7,5),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(rnorm,tflux*fpr/Lnorm,label = 'F'+r'$_{total}$',linewidth=lw,color='black')
    plt.plot(rnorm,eflux*fpr/Lnorm,label = 'F'+r'$_{enth}$',linewidth=lw)
    plt.plot(rnorm,cflux*fpr/Lnorm,label = 'F'+r'$_{cond}$',linewidth=lw)
    plt.plot(rnorm,vflux*fpr/Lnorm,label = 'F'+r'$_{visc}$',linewidth=lw)
    plt.plot(rnorm,hflux*fpr/Lnorm,label = 'F'+r'$_{heat}$',linewidth=lw)
    plt.plot(rnorm,kflux*fpr/Lnorm,label = 'F'+r'$_{KE}$',linewidth=lw)
    if (Lnorm != 1):
        plt.plot(rnorm,np.ones_like(rnorm), color='k', linestyle=':', linewidth=lw)

    plt.xlim([0.75,1])
    if (Lnorm == 1):
        plt.ylim([-2e33,5e33])
        plt.ylabel(r'4$\pi r^2$'+'x Energy Flux '+r'( erg s$^{-1}$)')
    else:
        plt.ylim([-0.5, 1.75])
        plt.ylabel(r'4$\pi r^2$'+'x Energy Flux / L'+r'$_{star}$')
    plt.xlabel('Radius '+r'(r/r$_{o}$)')
    legend = plt.legend(loc='upper left', shadow=True, ncol = 3) 
    plt.tight_layout()

    if (no_plots):
        return

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    Lnorm = float(args['--Lnorm'])
    kappa = float(args['--kappa'])
    no_plots = args['--no-plots']
    Pr = float(args['--Pr'])
    Ra = float(args['--Ra'])

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Lnorm, kappa, Pr, no_plots, write_to=args['--write-to'],
         plot_Vrms=args['--plot-Vrms'], paper_data=args['--paper-data'],
         rms_only=args['--rms-only'], Ra=Ra, aspect_ratio=float(args['--aspect']))

