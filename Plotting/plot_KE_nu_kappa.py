"""
Plot the kinetic energy versus nu & kappa as a colormap

Usage:
    plot_KE_nu_kappa.py [options] <data-file>

Options:
    --save           Save the image [default: False]
    --eps            Save as EPS figure [default: False]
    --output=<o>     Save image as <o> [default: KE_nu_kappa]
    --log            Plot log scaled x and y axis [default: False]
    --pcolormesh     Plot using pcolormesh instead of a scatter plot [default: False]
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from NumericalToolsLocal.plotting.plotting import get_color

def main(save, output, input_file, logscale, use_pcolormesh, eps):

    viscous_timescale = True

    # read data file for output values
    data = read_datafile(input_file, separator=None)

    # extract only what you need
    # Pr = 2
    Pr2_kappa     = data['2']['kappa'][:]
    Pr2_nu        = 2.*Pr2_kappa
    Pr2_KE_dim    = data['2']['KE'][:]
    Pr2_KE_nondim = data['2']['KE_ND'][:]
    Pr2_Ra_F      = data['2']['Ra_F'][:]
    Pr2_Ra_BL     = data['2']['Ra_BL'][:]
    Pr2_Pr        = 2.*np.ones_like(Pr2_Ra_F)

    # Pr = 1
    myPr1_kappa     = data['1']['kappa'][:]
    myPr1_nu        = 1.*myPr1_kappa
    myPr1_KE_dim    = data['1']['KE'][:]
    myPr1_KE_nondim = data['1']['KE_ND'][:]
    myPr1_Ra_F      = data['1']['Ra_F'][:]
    myPr1_Ra_BL     = data['1']['Ra_BL'][:]
    myPr1_Pr        = 1.*np.ones_like(myPr1_Ra_F)

    # Pr = 4
    Pr4_kappa     = data['4']['kappa'][:]
    Pr4_nu        = 4.*Pr4_kappa
    Pr4_KE_dim    = data['4']['KE'][:]
    Pr4_KE_nondim = data['4']['KE_ND'][:]
    Pr4_Ra_F      = data['4']['Ra_F'][:]
    Pr4_Ra_BL     = data['4']['Ra_BL'][:]
    Pr4_Pr        = 4.*np.ones_like(Pr4_Ra_F)

    # Pr = 0.25
    Pr0_25_kappa     = data['0.25']['kappa'][:]
    Pr0_25_nu        = 0.25*Pr0_25_kappa
    Pr0_25_KE_dim    = data['0.25']['KE'][:]
    Pr0_25_KE_nondim = data['0.25']['KE_ND'][:]
    Pr0_25_Ra_F      = data['0.25']['Ra_F'][:]
    Pr0_25_Ra_BL     = data['0.25']['Ra_BL'][:]
    Pr0_25_Pr        = 0.25*np.ones_like(Pr0_25_Ra_F)

    # Pr = 0.5
    Pr0_5_kappa     = data['0.5']['kappa'][:]
    Pr0_5_nu        = 0.5*Pr0_5_kappa
    Pr0_5_KE_dim    = data['0.5']['KE'][:]
    Pr0_5_KE_nondim = data['0.5']['KE_ND'][:]
    Pr0_5_Ra_F      = data['0.5']['Ra_F'][:]
    Pr0_5_Ra_BL     = data['0.5']['Ra_BL'][:]
    Pr0_5_Pr        = 0.5*np.ones_like(Pr0_5_Ra_F)

    # Pr = 0.1
    Pr0_1_kappa     = data['0.1']['kappa'][:]
    Pr0_1_nu        = 0.1*Pr0_1_kappa
    Pr0_1_KE_dim    = data['0.1']['KE'][:]
    Pr0_1_KE_nondim = data['0.1']['KE_ND'][:]
    Pr0_1_Ra_F      = data['0.1']['Ra_F'][:]
    Pr0_1_Ra_BL     = data['0.1']['Ra_BL'][:]
    Pr0_1_Pr        = 0.1*np.ones_like(Pr0_1_Ra_F)

    # Paper results: Table 6, Ra_F is too high by pi so divide to make it correct
    Pr1_kappa     = np.array([1., 2., 4., 6., 8., 12., 16., 24.])*1e12
    Pr1_nu        = 1.*Pr1_kappa
    Pr1_KE_dim    = np.array([36.22, 34.73, 33.42, 32.92, 31.62, 22.08, 12.02, 2.05])*1e38
    Pr1_KE_nondim = np.array([20057.7, 4807.6, 1156.7, 506.4, 273.5, 84.9, 26.0, 2.0])
    Pr1_Ra_F      = np.array([2.14e7, 2.68e6, 3.35e5, 9.93e4, 4.19e4, 1.24e4, 5.23e3, 1.55e3])/np.pi
    Pr1_Ra_BL     = 22.064159*Pr1_Ra_F # estimate for Ra_BL = mean(Pr2_Ra_BL/Pr2_Ra_F)*Pr1_Ra_F
    Pr1_Pr        = 1.*np.ones_like(Pr1_Ra_F)

    # generic Ra number array, smallest is at the end, biggest is at the front
    all_Ra = np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F,
                             Pr4_Ra_F, Pr0_1_Ra_F))
    min_Ra = np.amin(all_Ra); max_Ra = np.amax(all_Ra)
    Ra_F = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 32)

    # generic nu/kappa arrays, smallest is at the front
    all_kap = np.concatenate((Pr2_kappa, Pr0_25_kappa, Pr0_5_kappa, Pr1_kappa,
                              Pr4_kappa, Pr0_1_kappa))
    min_k = np.amin(all_kap); max_k = np.amax(all_kap)
    kappa = np.logspace(np.log10(min_k), np.log10(max_k), 32)
    all_nu = np.concatenate((Pr2_nu, Pr0_25_nu, Pr0_5_nu, Pr1_nu, Pr4_nu, Pr0_1_nu))
    min_n = np.amin(all_nu); max_n = np.amax(all_nu)
    nu = np.logspace(np.log10(min_n), np.log10(max_n), 32)

    all_KE = np.concatenate((Pr2_KE_dim, Pr0_25_KE_dim, Pr0_5_KE_dim, Pr1_KE_dim,
                             Pr4_KE_dim, Pr0_1_KE_dim))
    # unique kappa/nu/Ra arrays
    all_KE  = np.sort(np.array(list(set(all_KE))))
    all_kap = np.sort(np.array(list(set(all_kap))))
    all_nu  = np.sort(np.array(list(set(all_nu ))))
    all_Ra  = np.sort(np.array(list(set(all_Ra ))))
    N_nu = len(all_nu); N_kap = len(all_kap)

    # construct data cube
    KEdata = np.zeros((N_nu, N_kap))
    for _pr in data.keys():
        pr = float(_pr)
        for kk in range(len(data[_pr]['kappa'])):
            kap = data[_pr]['kappa'][kk]

            # find kappa index
            inu = None; ikap = None
            for ii in range(N_kap):
                if (kap == all_kap[ii]):
                    ikap = ii; break

            # find nu index
            nu = pr*kap
            for ii in range(N_nu):
                if (nu == all_nu[ii]):
                    inu = ii; break

            if (ikap is None):
                print("\ndid not find corresponding kappa index\n")
            if (inu is None):
                print("\ndid not find corresponding nu index\n")

            # assign data
            KEdata[inu, ikap] = data[_pr]['KE'][kk]

    # scale the data
    KEdata[:,:] /= 1.0e38
    all_KE[:]   /= 1.0e38
    all_nu[:]   /= 1.0e12
    all_kap[:]  /= 1.0e12

    # cutoff for low vs. high Rayleigh number based on non-dim KE plot
    Ra_F_crit = 2.e4 #1.e5 / np.pi

    # use a different non-dimensionalization?
    if (viscous_timescale):
        # use the viscous time instead of thermal time, so divide by Pr**2
        Pr0_5_KE_nondim /= 0.5**2
        Pr0_25_KE_nondim /= 0.25**2
        Pr4_KE_nondim /= 4.0**2
        Pr2_KE_nondim /= 2.0**2
        Pr1_KE_nondim /= 1.0**2
        Pr0_1_KE_nondim /= 0.1**2

    #######################################################
    # Kinetic Energy Plots
    #######################################################
    plt.figure(1, dpi=300)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})
    plt.subplot(111)

    cmap = 'jet'
    if (use_pcolormesh):
        X, Y = np.meshgrid(all_nu, all_kap, indexing='ij')
        im = plt.pcolormesh(X, Y, KEdata, cmap=cmap,
                            vmin=np.amin(KEdata), vmax=np.amax(KEdata))
        cbar = plt.colorbar(im, orientation='vertical')
        cbar.set_label(r"$10^{38}$ ergs")

        plt.gca().tick_params(axis='both', direction='inout', which='both')

    else: # scatter plot
        for i in range(N_nu):
            for j in range(N_kap):
                x = all_nu[i]; y = all_kap[j]
                color = get_color(KEdata[i,j], all_KE, cmap=cmap)
                if (color == 'k'):
                    if (str(x/y) in data.keys() or str(x/y) == '1.0'):
                        print("returned black color for nu = {}, kappa = {}, (Pr={})".format(
                           x, y, x/y))
                    continue

                plt.plot(x, y, color=color, marker='o')

        # need to get a "scalar mappable" going fist
        # also need "fake" data i.e. sm._A = []
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(
                                   vmin=np.amin(KEdata), vmax=np.amax(KEdata)))
        sm._A = []
        cbar = plt.colorbar(sm, orientation='vertical')
        cbar.set_label(r"$10^{38}$ ergs")

    if (logscale):
        plt.xscale('log'); plt.yscale('log')

    plt.xlim(np.amin(all_nu)-1, np.amax(all_nu)+1)
    plt.ylim(np.amin(all_kap)-1, np.amax(all_kap)+1)
    plt.xlabel(r"$\nu$ ($10^{12}$ cm$^2$/s)")
    plt.ylabel(r"$\kappa$ ($10^{12}$ cm$^2$/s)")
    plt.title(r"Dimensional Kinetic Energy")

    plt.tight_layout()

    if (save):
        if (eps):
            ext = ".eps"
        else:
            ext = ".png"
        plt.savefig(output+ext)
        print("saved image: {}".format(output+ext))
    else:
        plt.show()

    plt.close()

def read_datafile(filename, separator=None):
    """
    parse data file to extract calculated quantities
    """
    from collections import OrderedDict

    # allocate space
    data_values = OrderedDict()

    with open(filename, "r") as f:

        # only parse non-empty lines and lines that don't start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                fields = line.split(separator) # separate the fields

                # remove all whitespace from each field
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract specific values
                Pr       = fields[0]
                _kappa   = float(fields[1])
                _KE      = float(fields[2])
                _KE_ND   = float(fields[3])
                _Ra_F    = float(fields[4])
                _Ra_BL   = float(fields[5])
                _w_BL    = float(fields[6])
                _f_conv  = float(fields[7])
                _delta_V = float(fields[8])
                _Re      = float(fields[9])
                _Re_peak = float(fields[10])
                _S       = float(fields[11])
                _lavg_u  = float(fields[12])
                _lrms_u  = float(fields[13])
                _lavg_m  = float(fields[14])
                _lrms_m  = float(fields[15])
                _lavg_l  = float(fields[16])
                _lrms_l  = float(fields[17])

                # this is a new entry so initialize the data structure
                if (Pr not in data_values.keys()):
                    data_values[Pr] = OrderedDict({'kappa':[], 'KE':[], 'KE_ND':[],
                                                   'Ra_F':[], 'Ra_BL':[],
                                                   'w_BL':[], 'f_conv':[], 'delta_V':[],
                                                   'Re':[], 'Re_peak':[], 'S':[],
                                                   'lavg_upper':[], 'lavg_middle':[], 'lavg_lower':[],
                                                   'lrms_upper':[], 'lrms_middle':[], 'lrms_lower':[]})

                # add the data
                data_values[Pr]['kappa'].append(  _kappa)
                data_values[Pr]['KE'].append(     _KE)
                data_values[Pr]['KE_ND'].append(  _KE_ND)
                data_values[Pr]['Ra_F'].append(   _Ra_F)
                data_values[Pr]['Ra_BL'].append(  _Ra_BL)
                data_values[Pr]['w_BL'].append(   _w_BL)
                data_values[Pr]['f_conv'].append( _f_conv)
                data_values[Pr]['delta_V'].append(_delta_V)
                data_values[Pr]['Re'].append(     _Re)
                data_values[Pr]['Re_peak'].append(_Re_peak)
                data_values[Pr]['S'].append(      _S)
                data_values[Pr]['lavg_upper'].append( _lavg_u)
                data_values[Pr]['lavg_middle'].append(_lavg_m)
                data_values[Pr]['lavg_lower'].append( _lavg_l)
                data_values[Pr]['lrms_upper'].append( _lrms_u)
                data_values[Pr]['lrms_middle'].append(_lrms_m)
                data_values[Pr]['lrms_lower'].append( _lrms_l)

    # convert the lists into np arrays
    for pr in data_values.keys():
        for field in data_values[pr].keys():
            data_values[pr][field] = np.array(data_values[pr][field])

    return data_values

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(args['--save'], args['--output'], args['<data-file>'], args['--log'],
         args['--pcolormesh'], args['--eps'])

