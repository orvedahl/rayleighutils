"""
Plot various ME vs time

Usage:
    plot_energy_ME.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: energy_trace.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --quants=<q>         Comma separated list of quantities to use

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import GlobalAverage, build_file_list, ReferenceState
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, quants, **kwargs):

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    path = os.path.join(data_dir, "G_Avgs")
    files = build_file_list(start_file, end_file, path=path)

    if (quants is None):
        quant_list = None
    else:
        quant_list = list(quants)

    a = GlobalAverage(filename=files[0],path='',quantity=quant_list)
    ke_index  = a.lut[shortcut_lookup('ke')]     # Kinetic Energy (KE)
    me_index  = a.lut[shortcut_lookup('me')]     # Magnetic Energy (ME)
    pme_index = shortcut_lookup('pme')    # phi ME index
    sym_index = shortcut_lookup('mme')    # m=0 ME, <B>**2
    prime_index = shortcut_lookup('mep')    # ME' index = (B')**2

    # compile a time series of all the files
    data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                       GlobalAverage, axis=0, quantity=quant_list)
    alltime = Pm*np.array(time)
    alldays = alltime / (3600.*24.)

    # total KE/ME
    me = data[:,me_index]
    ke = data[:,ke_index]

    # other MEs
    if (pme_index is not None):
        me_zonal = data[:,a.lut[pme_index]] # total zonal ME
        me_merid = me - me_zonal            # total meridional ME
    else:
        me_zonal = np.zeros_like(alltime)
        me_merid = np.zeros_like(alltime)
        print("...Zonal ME could not be found")

    if (sym_index is not None):
        me_symm = data[:,a.lut[sym_index]] # m=0 ME
        me_asym = me - me_symm             # m/=0 ME
    else:
        me_symm = np.zeros_like(alltime)
        me_asym = np.zeros_like(alltime)
        print("...'m=0' ME could not be found")

    if (prime_index is not None):
        me_prime = data[:,a.lut[prime_index]] # ME' = (B')**2
    else:
        me_prime = np.zeros_like(alltime)
        print("...'prime' ME could not be found")

    print("\nFirst file: {}".format(files[-last_number_files:exclude_last][0]))
    print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    if (no_plots):
        return

    #Changing the plotting parameter somewhat depending on saveplot
    if (saveplot):
        plt.figure(1,figsize=(8.5, 4), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(8.5, 4), dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alltime,me,label=r'$ME_{total}$')
    plt.plot(alltime,ke,label=r'$KE_{total}$')
    plt.plot(alltime,me,label=r'$ME_{pert}\sim (B^\prime)^2$')
    plt.plot(alltime,me_zonal,label=r'$ME_{zonal}\sim B_\phi^2$')
    plt.plot(alltime,me_merid,label=r'$ME_{merid} \sim B_r^2 + B_\theta^2$')
    plt.plot(alltime,me_symm,label=r'$ME_{m=0} \sim <B>^2$')
    plt.plot(alltime,me_asym,label=r'$ME_{m\neq 0} = ME_{total} - ME_{m=0}$')
    plt.yscale('log')
    plt.xlabel('Time (simulation secs)')
    plt.ylabel('Energy Density')
    if (saveplot):
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, quants)

