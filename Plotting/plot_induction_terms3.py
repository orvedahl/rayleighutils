"""
Plot grid of azimuthal averages

Usage:
    plot_azavg.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: az_average.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-mean         Remove the mean [default: False]
    --Pm=<p>          Set Magnetic Prandtl for displaying time [default: 1]
    --single-time     Do not do an average over time [default: False]
    --list            List the available quanitites [default: False]
"""
from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
import matplotlib.pyplot as plt
import os
import sys
import numpy as np
from Utilities import azavg_util, time_utils, azavg_calculus
from Utilities.look_up_quantity import general_lookup, find_available

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_mean,
         list_contents, Pm, single_time):

    # build file list
    data_dir = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    quants = [  1,   2,   3, # velocity
              401, 402, 403] # B field

    # read first file to get meta data
    az = AzAverage(filename=files[0], path='', quantity=quants)
    nr = az.nr
    nth = az.ntheta
    radius = az.radius
    sintheta = az.sintheta
    costheta = az.costheta
    #costheta = np.cos(np.arcsin(sintheta))
    #costheta[0:nth/2] *= -1.0

    if (list_contents):
        vals = find_available(az.lut)
        print("\nFound the following values:")
        for i in vals:
            print("\t{:3d} --- {}".format(i, general_lookup(index=i)))
        print()
        sys.exit()

    # do time average, data is shape (nth, nr, nq)
    if (not single_time):
        print("\n...doing average...")
        data, avgtimes = time_utils.TimeAvg(files, AzAverage, axis=-1, data_attr="vals",
                                            quantity=quants)
    else:
        print("\n...choosing specific time...")
        tind = 0
        data = az.vals[:,:,:,tind]
        avgtimes = {"dt":0, "tstart":az.time[tind], "tend":az.time[tind], "nrec":az.niter}

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(Pm*avgtimes["tstart"]))
    print("Last  time: {} sec".format(Pm*avgtimes["tend"]))
    print("\nAveraged over {} sec".format(Pm*avgtimes["dt"]))
    print("Number of records {}".format(avgtimes["nrec"]))

    quantity = data[:,:,:]; nq = np.shape(quantity)[2]

    # remove the \ell=0 bit from all quantities
    if (no_mean):
        print("\n...removing \ell=0 from all quanitites...")
        for j in range(nq):
            for i in range(nr):
                quantity[:,i,j] -= np.mean(quantity[:,i,j])

    print("\n...setting up figure...")
    T = time_utils.Timer(); T.start()
    if (saveplot):
        fig = plt.figure(figsize=(8.5, 5.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        fig = plt.figure(figsize=(8.5, 5.0), dpi=80)
        plt.rcParams.update({'font.size': 14})

    main_ax = fig.add_subplot(1,1,1)
    main_ax.axis('off')
    T.stop(); dt = T.print_interval()

    tsize = 20
    lsize = 15

    theta = np.arccos(costheta)

    Ufield = (quantity[:,:,az.lut[1]], quantity[:,:,az.lut[2]], quantity[:,:,az.lut[3]])
    Bfield = (quantity[:,:,az.lut[401]], quantity[:,:,az.lut[402]], quantity[:,:,az.lut[403]])

    print("\n...calculating missing data...")
    calculated = {}

    print("\tcurl(B)...")
    T = time_utils.Timer(); T.start()
    Jfield = azavg_calculus.curl(Bfield, radius, theta)
    T.stop(); dt = T.print_interval()
    calculated['curlB'] = -Jfield[2]/Pm

    print("\tUxB...")
    T = time_utils.Timer(); T.start()
    UxB = azavg_calculus.cross(Ufield, Bfield)
    calculated['UxB'] = UxB[2]
    T.stop(); dt = T.print_interval()

    calculated['emf'] = -calculated['curlB'] - calculated['UxB']

    qcodes = ['UxB', 'curlB', 'emf']

    print("\n...plotting...")
    units = ''
    for i, qcode in enumerate(qcodes):
        A = fig.add_subplot(1,3,i+1)

        plt_data = calculated[qcode]

        azavg_util.plot_azav(fig, A, plt_data, radius, costheta, sintheta, r_bcz=0,
                         mycmap='RdBu_r', cbar=True, orientation='vertical',
                         boundsfactor=1.5, boundstype='rms',
                         units=units, fontsize=lsize,
                         underlay=[0], nlevs=6, show_contours=True)

        if (i == 0):
            plt.title(r"$\left(u \times B\right)_\phi$", fontsize=tsize)
        elif (i == 1):
            plt.title(r"$-\frac{1}{\mathrm{Pm}}\left(\nabla\times B\right)_\phi$", fontsize=tsize)
        elif (i == 2):
            plt.title(r"$\mathcal{E}_\phi$", fontsize=tsize)

    #plt.suptitle("Terms of $\partial_tA_\phi$ Equation", fontsize=tsize)

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_mean = args['--no-mean']
    list_contents = args['--list']
    Pm = float(args['--Pm'])
    single_time = args['--single-time']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_mean,
         list_contents, Pm, single_time)

