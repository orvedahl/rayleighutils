"""
Plot the kinetic energy versus rayleigh number

Usage:
    plot_KE_Ra.py [options] <data-file>

Options:
    --save           Save the image [default: False]
    --output=<o>     Save image as <o> [default: nondim_plots]
    --visc-time      Nondimensionalize using viscous timescale instead of thermal [default: False]
    --peclet         Plot the Peclet number Pe=Re*Pr instead of Re=U*L/nu [default: False]
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from NumericalToolsLocal import fitting
from NumericalToolsLocal import fit_RaPr

fit_args = {}
fit_args['error_method'] = 'bootstrap' # 'MC', 'Bootstrap', 'None', 'Jackknife'
#fit_args['Ntrials'] = 5000
fit_args['confidence'] = 0.68
fit_args['upper_lower_errors'] = False
fit_args['mcsigma'] = 0.15
fit_args['debug'] = False

def main(save, output, input_file, viscous_timescale, peclet_number):

    # read data file for output values
    data = read_datafile(input_file, separator=None)

    # extract the data
    # Pr = 2
    Pr2_kappa     = data['2']['kappa'][:]
    Pr2_KE_dim    = data['2']['KE'][:]
    Pr2_KE_nondim = data['2']['KE_ND'][:]
    Pr2_Ra_F      = data['2']['Ra_F'][:]
    Pr2_Ra_BL     = data['2']['Ra_BL'][:]
    Pr2_w_BL      = data['2']['w_BL'][:]
    Pr2_f_conv    = data['2']['f_conv'][:]
    Pr2_deltaV    = data['2']['delta_V'][:]
    Pr2_Re        = data['2']['Re'][:]
    Pr2_Re_peak   = data['2']['Re_peak'][:]
    Pr2_S         = data['2']['S'][:]
    Pr2_nu        = 2.*Pr2_kappa
    Pr2_lavg_upper  = data['2']['lavg_upper'][:]
    Pr2_lavg_middle = data['2']['lavg_middle'][:]
    Pr2_lavg_lower  = data['2']['lavg_lower'][:]
    Pr2_lrms_upper  = data['2']['lrms_upper'][:]
    Pr2_lrms_middle = data['2']['lrms_middle'][:]
    Pr2_lrms_lower  = data['2']['lrms_lower'][:]
    Pr2_Pr          = 2.*np.ones_like(Pr2_Ra_F)

    # Pr = 1
    myPr1_kappa     = data['1']['kappa'][:]
    myPr1_KE_dim    = data['1']['KE'][:]
    myPr1_KE_nondim = data['1']['KE_ND'][:]
    myPr1_Ra_F      = data['1']['Ra_F'][:]
    myPr1_Ra_BL     = data['1']['Ra_BL'][:]
    myPr1_w_BL      = data['1']['w_BL'][:]
    myPr1_f_conv    = data['1']['f_conv'][:]
    myPr1_deltaV    = data['1']['delta_V'][:]
    myPr1_Re        = data['1']['Re'][:]
    myPr1_Re_peak   = data['1']['Re_peak'][:]
    myPr1_S         = data['1']['S'][:]
    myPr1_nu        = 1.*myPr1_kappa
    myPr1_lavg_upper  = data['1']['lavg_upper'][:]
    myPr1_lavg_middle = data['1']['lavg_middle'][:]
    myPr1_lavg_lower  = data['1']['lavg_lower'][:]
    myPr1_lrms_upper  = data['1']['lrms_upper'][:]
    myPr1_lrms_middle = data['1']['lrms_middle'][:]
    myPr1_lrms_lower  = data['1']['lrms_lower'][:]
    myPr1_Pr          = 1.*np.ones_like(myPr1_Ra_F)

    # Pr = 4
    Pr4_kappa     = data['4']['kappa'][:]
    Pr4_KE_dim    = data['4']['KE'][:]
    Pr4_KE_nondim = data['4']['KE_ND'][:]
    Pr4_Ra_F      = data['4']['Ra_F'][:]
    Pr4_Ra_BL     = data['4']['Ra_BL'][:]
    Pr4_w_BL      = data['4']['w_BL'][:]
    Pr4_f_conv    = data['4']['f_conv'][:]
    Pr4_deltaV    = data['4']['delta_V'][:]
    Pr4_Re        = data['4']['Re'][:]
    Pr4_Re_peak   = data['4']['Re_peak'][:]
    Pr4_S         = data['4']['S'][:]
    Pr4_nu        = 4.*Pr4_kappa
    Pr4_lavg_upper  = data['4']['lavg_upper'][:]
    Pr4_lavg_middle = data['4']['lavg_middle'][:]
    Pr4_lavg_lower  = data['4']['lavg_lower'][:]
    Pr4_lrms_upper  = data['4']['lrms_upper'][:]
    Pr4_lrms_middle = data['4']['lrms_middle'][:]
    Pr4_lrms_lower  = data['4']['lrms_lower'][:]
    Pr4_Pr          = 4.*np.ones_like(Pr4_Ra_F)

    # Pr = 0.25
    Pr0_25_kappa     = data['0.25']['kappa'][:]
    Pr0_25_KE_dim    = data['0.25']['KE'][:]
    Pr0_25_KE_nondim = data['0.25']['KE_ND'][:]
    Pr0_25_Ra_F      = data['0.25']['Ra_F'][:]
    Pr0_25_Ra_BL     = data['0.25']['Ra_BL'][:]
    Pr0_25_w_BL      = data['0.25']['w_BL'][:]
    Pr0_25_f_conv    = data['0.25']['f_conv'][:]
    Pr0_25_deltaV    = data['0.25']['delta_V'][:]
    Pr0_25_Re        = data['0.25']['Re'][:]
    Pr0_25_Re_peak   = data['0.25']['Re_peak'][:]
    Pr0_25_S         = data['0.25']['S'][:]
    Pr0_25_nu        = 0.25*Pr0_25_kappa
    Pr0_25_lavg_upper  = data['0.25']['lavg_upper'][:]
    Pr0_25_lavg_middle = data['0.25']['lavg_middle'][:]
    Pr0_25_lavg_lower  = data['0.25']['lavg_lower'][:]
    Pr0_25_lrms_upper  = data['0.25']['lrms_upper'][:]
    Pr0_25_lrms_middle = data['0.25']['lrms_middle'][:]
    Pr0_25_lrms_lower  = data['0.25']['lrms_lower'][:]
    Pr0_25_Pr          = 0.25*np.ones_like(Pr0_25_Ra_F)

    # Pr = 0.5
    Pr0_5_kappa     = data['0.5']['kappa'][:]
    Pr0_5_KE_dim    = data['0.5']['KE'][:]
    Pr0_5_KE_nondim = data['0.5']['KE_ND'][:]
    Pr0_5_Ra_F      = data['0.5']['Ra_F'][:]
    Pr0_5_Ra_BL     = data['0.5']['Ra_BL'][:]
    Pr0_5_w_BL      = data['0.5']['w_BL'][:]
    Pr0_5_f_conv    = data['0.5']['f_conv'][:]
    Pr0_5_deltaV    = data['0.5']['delta_V'][:]
    Pr0_5_Re        = data['0.5']['Re'][:]
    Pr0_5_Re_peak   = data['0.5']['Re_peak'][:]
    Pr0_5_S         = data['0.5']['S'][:]
    Pr0_5_nu        = 0.5*Pr0_5_kappa
    Pr0_5_lavg_upper  = data['0.5']['lavg_upper'][:]
    Pr0_5_lavg_middle = data['0.5']['lavg_middle'][:]
    Pr0_5_lavg_lower  = data['0.5']['lavg_lower'][:]
    Pr0_5_lrms_upper  = data['0.5']['lrms_upper'][:]
    Pr0_5_lrms_middle = data['0.5']['lrms_middle'][:]
    Pr0_5_lrms_lower  = data['0.5']['lrms_lower'][:]
    Pr0_5_Pr          = 0.5*np.ones_like(Pr0_5_Ra_F)

    # Pr = 0.1
    Pr0_1_kappa     = data['0.1']['kappa'][:]
    Pr0_1_KE_dim    = data['0.1']['KE'][:]
    Pr0_1_KE_nondim = data['0.1']['KE_ND'][:]
    Pr0_1_Ra_F      = data['0.1']['Ra_F'][:]
    Pr0_1_Ra_BL     = data['0.1']['Ra_BL'][:]
    Pr0_1_w_BL      = data['0.1']['w_BL'][:]
    Pr0_1_f_conv    = data['0.1']['f_conv'][:]
    Pr0_1_deltaV    = data['0.1']['delta_V'][:]
    Pr0_1_Re        = data['0.1']['Re'][:]
    Pr0_1_Re_peak   = data['0.1']['Re_peak'][:]
    Pr0_1_S         = data['0.1']['S'][:]
    Pr0_1_nu        = 0.1*Pr0_1_kappa
    Pr0_1_lavg_upper  = data['0.1']['lavg_upper'][:]
    Pr0_1_lavg_middle = data['0.1']['lavg_middle'][:]
    Pr0_1_lavg_lower  = data['0.1']['lavg_lower'][:]
    Pr0_1_lrms_upper  = data['0.1']['lrms_upper'][:]
    Pr0_1_lrms_middle = data['0.1']['lrms_middle'][:]
    Pr0_1_lrms_lower  = data['0.1']['lrms_lower'][:]
    Pr0_1_Pr          = 0.1*np.ones_like(Pr0_1_Ra_F)

    # Paper results: Table 6, Ra_F is too high by pi so divide to make it correct
    Pr1_kappa     = np.array([1., 2., 4., 6., 8., 12., 16., 24.])*1e12
    Pr1_KE_dim    = np.array([36.22, 34.73, 33.42, 32.92, 31.62, 22.08, 12.02, 2.05])*1e38
    Pr1_KE_nondim = np.array([20057.7, 4807.6, 1156.7, 506.4, 273.5, 84.9, 26.0, 2.0])
    Pr1_Ra_F      = np.array([2.14e7, 2.68e6, 3.35e5, 9.93e4, 4.19e4, 1.24e4, 5.23e3, 1.55e3])/np.pi
    Pr1_Ra_BL     = 22.064159*Pr1_Ra_F # estimate for Ra_BL = mean(Pr2_Ra_BL/Pr2_Ra_F)*Pr1_Ra_F
    Pr1_w_BL      = np.array([4.96, 6.93, 9.83, 12.18, 14.09, 16.03, 19.06, 23.77]) # in Mm
    Pr1_f_conv    = np.array([0.948, 0.912, 0.847, 0.791, 0.736, 0.623, 0.442, 0.122])
    Pr1_deltaV    = np.array([1.323, 1.094, 0.799, 0.617, 0.452, 0.275, 0.266, 0.198])
    Pr1_Re        = np.array([229.8, 114.1, 56.3, 37.5, 27.5, 15.4, 9.0, 2.5])
    Pr1_Re_peak   = np.array([320.4, 166.5, 85.6, 58.2, 42.4, 24.0, 15.6, 4.5])
    Pr1_S         = np.array([1., 9374.7365, 6792.0897, 5492.2587, 4675.4939, 3519.9891, 2928.6206, 2174.4538])
    Pr1_nu        = 1.*Pr1_kappa
    Pr1_lavg_upper  = np.array([1e3, 17.36,  9.81,  7.25,  5.84,  5.51, 3.55, 3.76])
    Pr1_lavg_middle = np.array([1e3, 27.65, 18.59, 13.54, 10.12,  8.62, 3.27, 3.65])
    Pr1_lavg_lower  = np.array([1e3, 12.47,  8.50,  6.12,  4.92,  4.89, 1e3,  1e3])
    Pr1_lrms_upper  = np.array([1e3, 32.44, 18.49, 13.05, 10.02,  7.97, 5.63, 4.53])
    Pr1_lrms_middle = np.array([1e3, 37.08, 24.78, 18.55, 14.38, 11.62, 4.56, 4.31])
    Pr1_lrms_lower  = np.array([1e3, 17.61, 12.05,  8.70,  6.96,  6.25, 1e3,  1e3])
    Pr1_Pr          = 1.*np.ones_like(Pr1_Ra_F)

    # generic Ra number array, smallest is at the end, biggest is at the front
    min_Ra = np.amin(np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F)))
    max_Ra = np.amax(np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F)))
    Ra_F = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)
    min_Re = np.amin(np.concatenate((Pr2_Re, Pr0_25_Re, Pr0_5_Re, Pr1_Re, Pr4_Re, Pr0_1_Re)))
    max_Re = np.amax(np.concatenate((Pr2_Re, Pr0_25_Re, Pr0_5_Re, Pr1_Re, Pr4_Re, Pr0_1_Re)))
    Reynolds = np.logspace(np.log10(min_Re), np.log10(max_Re), 16)
    min_Ra = np.amin(np.concatenate((Pr2_Ra_BL, Pr0_25_Ra_BL, Pr0_5_Ra_BL, Pr1_Ra_BL, Pr4_Ra_BL, Pr0_1_Ra_BL)))
    max_Ra = np.amax(np.concatenate((Pr2_Ra_BL, Pr0_25_Ra_BL, Pr0_5_Ra_BL, Pr1_Ra_BL, Pr4_Ra_BL, Pr0_1_Ra_BL)))
    Ra_BL = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)

    # generic nu/kappa arrays, smallest is at the front
    min_k = np.amin(np.concatenate((Pr2_kappa, Pr0_25_kappa, Pr0_5_kappa, Pr1_kappa, Pr4_kappa, Pr0_1_kappa)))
    max_k = np.amax(np.concatenate((Pr2_kappa, Pr0_25_kappa, Pr0_5_kappa, Pr1_kappa, Pr4_kappa, Pr0_1_kappa)))
    kappa = np.logspace(np.log10(min_k), np.log10(max_k), 16)
    min_n = np.amin(np.concatenate((Pr2_nu, Pr0_25_nu, Pr0_5_nu, Pr1_nu, Pr4_nu, Pr0_1_nu)))
    max_n = np.amax(np.concatenate((Pr2_nu, Pr0_25_nu, Pr0_5_nu, Pr1_nu, Pr4_nu, Pr0_1_nu)))
    nu = np.logspace(np.log10(min_n), np.log10(max_n), 16)

    # cutoff for low vs. high Rayleigh number based on non-dim KE plot
    Ra_F_crit = 2.e4 #1.e5 / np.pi

    # use a different non-dimensionalization?
    if (viscous_timescale):
        Pr0_5_KE_nondim /= 0.5**2    # use the viscous time instead of thermal time, so divide by Pr**2
        Pr0_25_KE_nondim /= 0.25**2
        Pr4_KE_nondim /= 4.0**2
        Pr2_KE_nondim /= 2.0**2
        Pr1_KE_nondim /= 1.0**2
        Pr0_1_KE_nondim /= 0.1**2

    if (peclet_number): # convert all the Reynolds numbers to Peclet numbers, Pe=Re*Pr
        Pr0_5_Re *= 0.5
        Pr0_25_Re *= 0.25
        Pr4_Re *= 4.0
        Pr2_Re *= 2.0
        Pr1_Re *= 1.0
        Pr0_1_Re *= 0.1

        Pr0_5_Re_peak *= 0.5
        Pr0_25_Re_peak *= 0.25
        Pr4_Re_peak *= 4.0
        Pr2_Re_peak *= 2.0
        Pr1_Re_peak *= 1.0
        Pr0_1_Re_peak *= 0.1

    #######################################################
    # Kinetic Energy Plots
    #######################################################
    #plt.figure(1, figsize=(8,4), dpi=300)
    plt.figure(1, figsize=(12,4), dpi=300)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    xfont=14; yfont=14

    allRe  = np.concatenate((Pr0_25_Re, Pr0_5_Re,
                                  Pr1_Re, Pr2_Re, Pr4_Re, Pr0_1_Re))
    allRaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    nKE    = np.concatenate((Pr0_25_KE_nondim, Pr0_5_KE_nondim,
                                  Pr1_KE_nondim, Pr2_KE_nondim, Pr4_KE_nondim, Pr0_1_KE_nondim))
    allPr  = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
                                  Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    ind = np.where(allRaF > Ra_F_crit)
    fitRaF = allRaF[ind]
    fitKE  = nKE[ind]
    fitPr  = allPr[ind]
    fitRe  = allRe[ind]

    a0 = (-0.6, 0.3)
    #_params, _errors, _chi2, _chi2_red, _pte = fit_RaPr.fitting_RaPr(fitRaF, fitRe, fitPr,
    #                                             a0, seed=7, **fit_args)
    def fit_func(x, params):
        a,b = params
        return a + b*x
    xdata = np.log(fitRaF/fitPr**2)
    ydata = np.log(fitRe)
    _params, _errors = fitting.general_fit(xdata, ydata, fit_func, a0,
                                           seed=7, **fit_args)
    A = _params[0]
    _params[0] = np.exp(A)
    _errors[0] = np.exp(A)*_errors[0]
    print("\nFitting Reynolds vs. RaPr data:")
    #print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
    #                            _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    #print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
    #                            _params[2], _errors[2], 100*_errors[2]/abs(_params[2])))
    print("\tRa/Pr**2 power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
                                _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))

    # dimensional plot
    plt.subplot(122)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    #plt.axvline(x=Ra_F_crit, color='k', linestyle='-.')
    Ra_F2 = np.logspace(np.log10(50), np.log10(max_Ra), 16)
    plt.plot(Ra_F2, _params[0]*(Ra_F2)**(_params[1]), color='k', linestyle='--')
    plt.plot(Pr0_1_Ra_F/0.1**2, Pr0_1_Re, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F/0.25**2, Pr0_25_Re, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F/0.5**2, Pr0_5_Re, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F/1.**2, Pr1_Re, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F/2.**2, Pr2_Re, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F/4.**2, Pr4_Re, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.text(0.1, 0.9, r"$\mathbf{(b)}$",transform=plt.gca().transAxes,fontsize=16)
    #plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    #plt.xlim(2e-14, 2e-12)
    #plt.ylim(1e38, 1e40)
    #plt.xlabel(r"$\kappa^{-1}$ (cm$^{-2}$ s)")
    #plt.ylabel(r"Kinetic Energy (erg)")
    plt.xlim(3e1, 2e7)
    plt.ylim(1, 4e2)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}^{-2}$", fontsize=xfont)
    plt.ylabel(r"Reynolds Number", fontsize=yfont)

    #a0 = (-1.1, 0.666, -1.333)
    #_params, _errors, _chi2, _chi2_red, _pte = fit_RaPr.fitting_RaPr(fitRaF, fitKE, fitPr, a0, seed=127, **fit_args)
    #print("\nFitting nondimKE vs. RaPr data:")
    #print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    #print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_params[2], _errors[2], 100*_errors[2]/abs(_params[2])))
    #a = _params[0]; b = _params[1]; c = _params[2]

    # dimensional plot
    plt.subplot(121)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.axvline(x=Ra_F_crit, color='k', linestyle='-.')
    #plt.plot(Ra_F, _params[0]*(Ra_F)**(_params[1]), color='k', linestyle='--')
    #plt.plot(fitRaF, fitKE, color='k', marker='+', fillstyle='none', linestyle='')
    plt.plot(Pr0_1_Ra_F, Pr0_1_KE_dim, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F, Pr0_25_KE_dim, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F,  Pr0_5_KE_dim,  label='Pr = 0.5',  marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F,    Pr1_KE_dim,    label='Pr = 1',    marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F,    Pr2_KE_dim,    label='Pr = 2',    marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F,    Pr4_KE_dim,    label='Pr = 4',    marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.text(0.1, 0.9, r"$\mathbf{(a)}$",transform=plt.gca().transAxes,fontsize=16)
    plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(3e1, 2e7)
    plt.ylim(4e37, 1e40)
    #plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}^{-2}$")
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$", fontsize=xfont)
    #plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}^{0.689}\mathrm{Pr}^{0.313}$")
    plt.ylabel(r"Dimensional KE (erg)", fontsize=yfont)

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_KE_vs_Ra.eps")
        print("saved image: {}".format(output+"_KE_vs_Ra.eps"))
    else:
        plt.show()

    plt.close()

    #######################################################
    # Boundary Layer / Fractional Convective Flux Plot vs Ra*Pr
    #######################################################
    plt.figure(1, figsize=(12,4), dpi=300)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    xfont=14; yfont=14

    plt.subplot(121)
    _fconv = np.concatenate((Pr0_25_f_conv, Pr0_5_f_conv,
                                  Pr1_f_conv, Pr2_f_conv, Pr4_f_conv, Pr0_1_f_conv))
    _Pr = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
                                  Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    _RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    uRaF, ind = np.unique(_RaF, return_index=True)
    if (True):
        ind = np.where(_RaF > Ra_F_crit)
        _RaF   = _RaF[ind]
        _fconv = _fconv[ind]
        _Pr    = _Pr[ind]

    a0 = (1.7, -0.28)
    def fit_func(x, params):
        a,b = params
        return a + b*x
    xdata = np.log(_RaF*_Pr)
    ydata = np.log(1./(1.-_fconv))
    _params, _errors = fitting.general_fit(xdata, ydata, fit_func, a0,
                                           seed=63, **fit_args)
    A = _params[0]
    _params[0] = np.exp(A)
    _errors[0] = np.exp(A)*_errors[0]
    #_params, _errors, _chi2, _chi2_red, _pte = fit_RaPr.fitting_RaPr(
    #                                 _RaF, 1./(1.-_fconv), _Pr, a0, seed=63, **fit_args)
    print("\nFitting 1/(1-fconv) vs. RaPr data:")
    #print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
    #                           _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    #print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
    #                           _params[2], _errors[2], 100*_errors[2]/abs(_params[2])))
    print("\tRaPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
                               _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    a = _params[0]; b = _params[1]
    #plt.axvline(x=Ra_F_crit*1.0, color='k', linestyle='-.')
    plt.plot(uRaF, a*uRaF**b, color='k', linestyle='--', label=None)
    plt.plot(Pr0_1_Ra_F*0.1, 1./(1-Pr0_1_f_conv), label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F*0.25, 1./(1-Pr0_25_f_conv), marker='+', label='Pr = 0.25',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F*0.5, 1./(1-Pr0_5_f_conv), marker='D', label='Pr = 0.5',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F*1, 1./(1-Pr1_f_conv), marker='^', label='Pr = 1',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F*2, 1./(1-Pr2_f_conv), marker='x', label='Pr = 2',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F*4, 1./(1-Pr4_f_conv), marker='o', label='Pr = 4',
             color='r', fillstyle='none', linestyle='')
    plt.text(0.85, 0.9, r"$\mathbf{(a)}$",transform=plt.gca().transAxes,fontsize=16)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    #plt.legend(loc='lower left', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.8, 40)
    #plt.xlim(3e-3, 2e-1)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}$", fontsize=xfont)
    plt.ylabel(r"$1/(1-f_\mathrm{conv})$", fontsize=yfont)

    wBL   = np.concatenate((Pr0_25_w_BL, Pr0_5_w_BL,
                                  Pr1_w_BL, Pr2_w_BL, Pr4_w_BL, Pr0_1_w_BL))
    _Pr   = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
                                  Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    _RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    uRaF, ind = np.unique(_RaF, return_index=True)
    if (True):
        ind = np.where(_RaF > Ra_F_crit)
        _RaF = _RaF[ind]
        wBL  = wBL[ind]
        _Pr  = _Pr[ind]

    a0 = (4.2, -0.16)
    def fit_func(x, params):
        a,b = params
        return a + b*x
    xdata = np.log(_RaF*_Pr)
    ydata = np.log(wBL)
    _params, _errors = fitting.general_fit(xdata, ydata, fit_func, a0,
                                           seed=255, **fit_args)
    A = _params[0]
    _params[0] = np.exp(A)
    _errors[0] = np.exp(A)*_errors[0]
    #_params, _errors, _chi2, _chi2_red, _pte = fit_RaPr.fitting_RaPr(
    #                                      _RaF, wBL, _Pr, a0, seed=255, **fit_args)
    print("\nFitting w_BL vs. RaPr data:")
    print("\tscale factor = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
                               _params[0], _errors[0], 100*_errors[0]/abs(_params[0])))
    #print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
    #                           _params[2], _errors[2], 100*_errors[2]/abs(_params[2])))
    print("\tRaPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(
                               _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))

    a = _params[0]; b = _params[1]
    plt.subplot(122)
    plt.plot(uRaF,  a*uRaF**b, color='k', linestyle='--', label=None)
    H = 1. #H = 158.6 # Mm
    plt.plot(Pr0_1_Ra_F*0.1, Pr0_1_w_BL/H, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F*0.25, Pr0_25_w_BL/H, marker='+', label='Pr = 0.25',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F*0.5, Pr0_5_w_BL/H, marker='D', label='Pr = 0.5',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F*1, Pr1_w_BL/H, marker='^', label='Pr = 1',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F*2, Pr2_w_BL/H, marker='x', label='Pr = 2',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F*4, Pr4_w_BL/H, marker='o', label='Pr = 4',
             color='r', fillstyle='none', linestyle='')
    plt.text(0.85, 0.9, r"$\mathbf{(b)}$",transform=plt.gca().transAxes,fontsize=16)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower left', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(4, 30)
    #plt.xlim(1e2, 2e7)
    #plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$", fontsize=xfont)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}$", fontsize=xfont)
    plt.ylabel(r"Boundary Layer Width (Mm)", fontsize=yfont)
    #plt.ylabel(r"$w_\mathrm{BL}/H$")

   # plt.subplot(133)
   # plt.plot(uRaF,  a*uRaF**b*1.0**c, color='k', linestyle='--', label=None)
   # H = 1. #H = 158.6 # Mm
   # plt.plot(Pr0_25_Ra_F*np.sqrt(0.25), Pr0_25_w_BL/H, marker='+', #label='Pr = 0.25',
   #          color='m', fillstyle='none', linestyle='')
   # plt.plot(Pr0_5_Ra_F*np.sqrt(0.5), Pr0_5_w_BL/H, marker='D', #label='Pr = 0.5',
   #          color='c', fillstyle='none', linestyle='')
   # plt.plot(Pr1_Ra_F*np.sqrt(1.), Pr1_w_BL/H, marker='^', #label='Pr = 1',
   #          color='g', fillstyle='none', linestyle='')
   # plt.plot(Pr2_Ra_F*np.sqrt(2.), Pr2_w_BL/H, marker='x', #label='Pr = 2',
   #          color='b', fillstyle='none', linestyle='')
   # plt.plot(Pr4_Ra_F*np.sqrt(4.), Pr4_w_BL/H, marker='o', #label='Pr = 4',
   #          color='r', fillstyle='none', linestyle='')
   # plt.text(0.85, 0.9, r"$\mathbf{(c)}$",transform=plt.gca().transAxes,fontsize=16)
   # plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
   # #plt.legend(loc='lower left', frameon=True, fontsize='small', numpoints=1)
   # plt.xscale('log')
   # plt.yscale('log')
   # plt.ylim(4, 30)
   # #plt.xlim(1e2, 2e7)
   # plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}^{1/2}$", fontsize=xfont)
   # plt.ylabel(r"Boundary Layer Width (Mm)", fontsize=yfont)
   # #plt.ylabel(r"$w_\mathrm{BL}/H$")

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_wBL_fconv_RaPr.eps")
        print("saved image: {}".format(output+"_wBL_fconv_RaPr.eps"))
    else:
        plt.show()

    plt.close()

    return

    #######################################################
    # Fractional Convective Flux Plot vs Ra and (Ra*Pr)**power
    #######################################################
    plt.figure(1, figsize=(7,4), dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    plt.subplot(121)
    plt.plot(Pr0_1_Ra_F, Pr0_1_f_conv, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F, Pr0_25_f_conv, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_f_conv, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_f_conv, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_f_conv, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_f_conv, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.08, 1.5)
    plt.xlim(200, 4e7)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    plt.ylabel(r"$f_\mathrm{conv}$")

    plt.subplot(122)
    power = 1
    plt.axvline(x=Ra_F_crit*1.0, color='k', linestyle='-.')
    plt.plot((Pr1_Ra_F*1.0), 1.-(1.-Pr1_f_conv[1])*(Pr1_Ra_F*1.0/Pr1_Ra_F[1])**(-2./7.),
             color='k', linestyle='--', label=r'$1-\left(\mathrm{Ra}_F\mathrm{Pr}\right)^{-2/7}$')
    plt.plot((Pr0_1_Ra_F*0.1)**power, Pr0_1_f_conv, marker='v', #label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot((Pr0_25_Ra_F*0.25)**power, Pr0_25_f_conv, marker='+', #label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot((Pr0_5_Ra_F*0.5)**power, Pr0_5_f_conv, marker='D', #label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot((Pr1_Ra_F*1.0)**power, Pr1_f_conv, marker='^', #label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot((Pr2_Ra_F*2.0)**power, Pr2_f_conv, marker='x', #label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot((Pr4_Ra_F*4.0)**power, Pr4_f_conv, marker='o', #label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower center', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.08, 1.5)
    plt.xlim(200, 4e7)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}$")
    plt.ylabel(r"f$_\mathrm{conv}$")

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_fconv_RaPr.png")
        print("saved image: {}".format(output+"_fconv_RaPr.png"))
    else:
        plt.show()

    plt.close()

    #######################################################
    # Nusselt number based on f_conv
    #   f_conv = convective/total = (total - conduction)/total
    #          = 1 - cond/total
    #   Nu = total/conduction
    #   ---->  f_conv = 1 - 1/Nu
    #######################################################
    plt.figure(1, dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    print("\nTHIS NUSSELT NUMBER IS WRONG --- use calculate_Nu.py\n")
    def Nu(f):
        return 1./(1.-f)
    fconv = np.concatenate((Pr0_25_f_conv, Pr0_5_f_conv,
                                  Pr1_f_conv, Pr2_f_conv, Pr4_f_conv, Pr0_1_f_conv))
    _Pr   = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
                                  Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    _RaF  = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    a0 = (1.0, 0.28, 1.0)
    _params, _errors, _chi2, _chi2_red, _pte = fit_RaPr.fitting_RaPr(_RaF, Nu(fconv), _Pr, a0, seed=31, **fit_args)
    print("\nFitting Nusselt vs. RaPr data:")
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_params[2], _errors[2], 100*_errors[2]/abs(_params[2])))

    a = _params[0]; b = _params[1]; c = _params[2]
    #plt.plot((Pr1_Ra_F*1.0), Nu(Pr1_f_conv[1])*(Pr1_Ra_F*1.0/Pr1_Ra_F[1])**(2./7.),
    plt.plot(Ra_F**b, a*Ra_F**b, color='k', linestyle='--',
               label=None) #r'$\left(\mathrm{Ra}_F\mathrm{Pr}\right)^{2/7}$')
    plt.axvline(x=Ra_F_crit**b, color='k', linestyle='-.')
    plt.plot((Pr0_1_Ra_F**b*0.1**c), Nu(Pr0_1_f_conv), label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot((Pr0_25_Ra_F**b*0.25**c), Nu(Pr0_25_f_conv), label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot((Pr0_5_Ra_F**b*0.5**c), Nu(Pr0_5_f_conv), label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot((Pr1_Ra_F**b*1.0**c), Nu(Pr1_f_conv), label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot((Pr2_Ra_F**b*2.0**c), Nu(Pr2_f_conv), label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot((Pr4_Ra_F**b*4.0**c), Nu(Pr4_f_conv), label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(0.8, 25)
    plt.xlim(200**b, 1e7**b)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{BL}^{0.273}\mathrm{Pr}^{0.310}$")
    plt.ylabel(r"Nu = 1/(1-f)")

    if (save):
        plt.savefig(output+"_Nusselt.png")
        print("saved image: {}".format(output+"_Nusselt.png"))
    else:
        plt.show()

    plt.close()

    #######################################################
    # delta S plot, i.e. delta S = max(S)-min(S) = max(S)
    #######################################################
    plt.figure(1, figsize=(6,4), dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    _RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    S    = np.concatenate((Pr0_25_S, Pr0_5_S,
                                  Pr1_S, Pr2_S, Pr4_S, Pr0_1_S))
    _Pr  = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
                                  Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    a0 = (1e3, 0.186, -0.5)
    _params, _errors, _chi2, _chi2_red, _pte = fit_RaPr.fitting_RaPr(_RaF, S, _Pr, a0, seed=511, **fit_args)
    print("\nFitting Delta S vs. RaPr data:")
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(_params[2], _errors[2], 100*_errors[2]/abs(_params[2])))
    a = _params[0]; b = _params[1]; c = _params[2]
    test_plot = True
    if (test_plot):
        plt.subplot(121)
    uRaF, ind = np.unique(Ra_F, return_index=True)
    plt.plot(Ra_F[ind]**b, a*Ra_F[ind]**b, color='k', linestyle='--', label=None)
    plt.plot(Pr0_1_Ra_F**b*0.1**c, Pr0_1_S, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F**b*0.25**c, Pr0_25_S, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F**b*0.5**c, Pr0_5_S, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F**b*1**c, Pr1_S, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F**b*2**c, Pr2_S, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F**b*4**c, Pr4_S, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(700, 1.5e4)
    #plt.xlim(75**b, 3e6**b)
    plt.ylabel(r"$\Delta$ S")
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}^{0.689}\mathrm{Pr}^{0.313}$")
    plt.title("fit found\na={:.2f}, b={:.2f}, c={:.2f}".format(a,b,c))

    if (test_plot):
        plt.subplot(122)
        a = 1e0; b = 1.; c = 1.
        plt.plot(Ra_F[ind]**b, a*Ra_F[ind]**b, color='k', linestyle='--', label=None)
        plt.plot(Pr0_1_Ra_F**b*0.1**c, Pr0_1_S, label='Pr = 0.1', marker='v',
                 color='k', fillstyle='none', linestyle='')
        plt.plot(Pr0_25_Ra_F**b*0.25**c, Pr0_25_S, label='Pr = 0.25', marker='+',
                 color='m', fillstyle='none', linestyle='')
        plt.plot(Pr0_5_Ra_F**b*0.5**c, Pr0_5_S, label='Pr = 0.5', marker='D',
                 color='c', fillstyle='none', linestyle='')
        plt.plot(Pr1_Ra_F**b*1**c, Pr1_S, label='Pr = 1', marker='^',
                 color='g', fillstyle='none', linestyle='')
        plt.plot(Pr2_Ra_F**b*2**c, Pr2_S, label='Pr = 2', marker='x',
                 color='b', fillstyle='none', linestyle='')
        plt.plot(Pr4_Ra_F**b*4**c, Pr4_S, label='Pr = 4', marker='o',
                 color='r', fillstyle='none', linestyle='')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(700, 1.5e4)
        #plt.xlim(75**b, 3e6**b)
        plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}^{0.689}\mathrm{Pr}^{0.313}$")
        plt.ylabel(r"$\Delta$ S")
        plt.title("test plot,\na={:.2f}, b={:.2f}, c={:.2f}".format(a,b,c))

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_delta_S.png")
        print("saved image: {}".format(output+"_delta_S.png"))
    else:
        plt.show()

    plt.close()

    #######################################################
    # delta V plot
    #######################################################
    plt.figure(1, figsize=(6,4), dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    plt.plot(Pr0_1_Ra_F, Pr0_1_deltaV, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F, Pr0_25_deltaV, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_deltaV, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_deltaV, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_deltaV, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_deltaV, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(1e-4, 10)
    plt.xlim(200, 4e7)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    plt.ylabel(r"$\delta$ V")

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_delta_V.png")
        print("saved image: {}".format(output+"_delta_V.png"))
    else:
        plt.show()

    plt.close()

    #######################################################
    # Reynolds number Plots
    #######################################################
    if (peclet_number):
        # do a fit to the Pe vs. Ra_F data since it is pretty damn correlated

        Pe      = np.concatenate((Pr0_25_Re, Pr0_5_Re,
                                  Pr1_Re, Pr2_Re, Pr4_Re, Pr0_1_Pe))
        Ra      = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
        Pe_peak = np.concatenate((Pr0_25_Re_peak, Pr0_5_Re_peak,
                                  Pr1_Re_peak, Pr2_Re_peak, Pr4_Re_peak, Pr0_1_Re_peak))
        Ra_peak= np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
        ind = np.where(Ra >= Ra_F_crit)
        Pe = Pe[ind]
        Ra = Ra[ind]
        Pe_peak = Pe_peak[ind]
        Ra_peak = Ra_peak[ind]
        def fit_func(x, params):
            m,b = params
            return m*x + b
        Pe_params, Pe_errors, Pe_chi2, Pe_chi2_red, Pe_pte = fitting.fit_data(np.log10(Ra), np.log10(Pe), fit_func, (0.5, -1))
        print("\nFitting Peclet data:")
        print("\tslope = {} +/- {}".format(Pe_params[0], Pe_errors[0]))
        print("\tintercept = {} +/- {}".format(Pe_params[1], Pe_errors[1]))
        Pep_params, Pep_errors, Pep_chi2, Pep_chi2_red, Pep_pte = fitting.fit_data(np.log10(Ra_peak), np.log10(Pe_peak), fit_func, (0.5, -1))
        print("\nFitting Peak Peclet data:")
        print("\tslope = {} +/- {}".format(Pep_params[0], Pep_errors[0]))
        print("\tintercept = {} +/- {}".format(Pep_params[1], Pep_errors[1]))

    plt.figure(1, figsize=(7,4), dpi=200)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    plt.subplot(121)
    if (peclet_number):
        plt.plot(Ra_F, 10**fit_func(np.log10(Ra_F), Pe_params), marker='', linestyle='--', color='k')
    plt.axvline(x=Ra_F_crit, color='k', linestyle='-.')
    plt.plot(Pr0_1_Ra_F, Pr0_1_Re, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F, Pr0_25_Re, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_Re, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_Re, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_Re, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_Re, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(1, 300)
    plt.xlim(200, 4e7)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    if (peclet_number):
        plt.ylabel(r"Pe")
    else:
        plt.ylabel(r"Re")

    plt.subplot(122)
    if (peclet_number):
        plt.plot(Ra_F, 10**fit_func(np.log10(Ra_F), Pep_params), marker='', linestyle='--', color='k')
    plt.axvline(x=Ra_F_crit, color='k', linestyle='-.')
    plt.plot(Pr0_1_Ra_F, Pr0_1_Re_peak, label='Pr = 0.1', marker='v',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F, Pr0_25_Re_peak, label='Pr = 0.25', marker='+',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F, Pr0_5_Re_peak, label='Pr = 0.5', marker='D',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F, Pr1_Re_peak, label='Pr = 1', marker='^',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F, Pr2_Re_peak, label='Pr = 2', marker='x',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F, Pr4_Re_peak, label='Pr = 4', marker='o',
             color='r', fillstyle='none', linestyle='')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    #plt.legend(loc='lower right', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(1, 400)
    plt.xlim(200, 4e7)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}$")
    if (peclet_number):
        plt.ylabel(r"Peak Pe")
    else:
        plt.ylabel(r"Peak Re")

    plt.tight_layout()

    if (save):
        if (peclet_number):
            plt.savefig(output+"_Peclet.png")
            print("saved image: {}".format(output+"_Peclet.png"))
        else:
            plt.savefig(output+"_Reynolds.png")
            print("saved image: {}".format(output+"_Reynolds.png"))
    else:
        plt.show()

    plt.close()

def read_datafile(filename, separator=None):
    """
    parse data file to extract calculated quantities
    """
    from collections import OrderedDict

    # allocate space
    data_values = OrderedDict()

    with open(filename, "r") as f:

        # only parse non-empty lines and lines that don't start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                fields = line.split(separator) # separate the fields

                # remove all whitespace from each field
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract specific values
                Pr       = fields[0]
                _kappa   = float(fields[1])
                _KE      = float(fields[2])
                _KE_ND   = float(fields[3])
                _Ra_F    = float(fields[4])
                _Ra_BL   = float(fields[5])
                _w_BL    = float(fields[6])
                _f_conv  = float(fields[7])
                _delta_V = float(fields[8])
                _Re      = float(fields[9])
                _Re_peak = float(fields[10])
                _S       = float(fields[11])
                _lavg_u  = float(fields[12])
                _lrms_u  = float(fields[13])
                _lavg_m  = float(fields[14])
                _lrms_m  = float(fields[15])
                _lavg_l  = float(fields[16])
                _lrms_l  = float(fields[17])

                # this is a new entry so initialize the data structure
                if (Pr not in data_values.keys()):
                    data_values[Pr] = OrderedDict({'kappa':[], 'KE':[], 'KE_ND':[],
                                                   'Ra_F':[], 'Ra_BL':[],
                                                   'w_BL':[], 'f_conv':[], 'delta_V':[],
                                                   'Re':[], 'Re_peak':[], 'S':[],
                                                   'lavg_upper':[], 'lavg_middle':[], 'lavg_lower':[],
                                                   'lrms_upper':[], 'lrms_middle':[], 'lrms_lower':[]})

                # add the data
                data_values[Pr]['kappa'].append(  _kappa)
                data_values[Pr]['KE'].append(     _KE)
                data_values[Pr]['KE_ND'].append(  _KE_ND)
                data_values[Pr]['Ra_F'].append(   _Ra_F)
                data_values[Pr]['Ra_BL'].append(  _Ra_BL)
                data_values[Pr]['w_BL'].append(   _w_BL)
                data_values[Pr]['f_conv'].append( _f_conv)
                data_values[Pr]['delta_V'].append(_delta_V)
                data_values[Pr]['Re'].append(     _Re)
                data_values[Pr]['Re_peak'].append(_Re_peak)
                data_values[Pr]['S'].append(      _S)
                data_values[Pr]['lavg_upper'].append( _lavg_u)
                data_values[Pr]['lavg_middle'].append(_lavg_m)
                data_values[Pr]['lavg_lower'].append( _lavg_l)
                data_values[Pr]['lrms_upper'].append( _lrms_u)
                data_values[Pr]['lrms_middle'].append(_lrms_m)
                data_values[Pr]['lrms_lower'].append( _lrms_l)

    # convert the lists into np arrays
    for pr in data_values.keys():
        for field in data_values[pr].keys():
            data_values[pr][field] = np.array(data_values[pr][field])

    return data_values

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(args['--save'], args['--output'], args['<data-file>'], args['--visc-time'], args['--peclet'])

