"""
  Shell-Averages (Shell_Avgs) plotting example
  Reads in time steps 3 million through 3.3 million
  Plots average KE and v' vs. radius

  This example routine makes use of the ShellAverage
  data structure associated with the Shell_Avg output.
  Upon initializing a ShellAverage object, the 
  object will contain the following attributes:

    ----------------------------------
    self.niter                         : number of time steps
    self.nq                            : number of diagnostic quantities output
    self.nr                            : number of radial points
    self.qv[0:nq-1]                    : quantity codes for the diagnostics output
    self.radius[0:nr-1]                : radial grid

    self.vals[0:nr-1,0:3,0:nq-1,0:niter-1] : The spherically averaged diagnostics
                                             0-3 refers to moments (index 0 is mean, index 3 is kurtosis)    
    self.iters[0:niter-1]              : The time step numbers stored in this output file
    self.time[0:niter-1]               : The simulation time corresponding to each time step
    self.version                       : The version code for this particular output (internal use)
    self.lut                           : Lookup table for the different diagnostics output
   -------------------------------------

Usage:
    plot_energy_distro.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_distro.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --paper-data    Run using old data format [default: False]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list, ReferenceState
import matplotlib.pyplot as plt
import numpy as np
from Utilities.look_up_quantity import shortcut_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots, paper_data=False, **kwargs):
    # Set saveplot to True to save to a file. 
    # Set to False to view plots interactively on-screen.
    #saveplot = False 
    #savefile = 'energy_distro.pdf'  #If pdf gives issues, try .png instead.

    if (paper_data):
        alt = True
    else:
        alt = False

    #Time-average the data generated between timesteps 0 million and 10 million
    # Note that this averaging scheme assumes taht the radial grid didn't
    # change during the course of the run. 
    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
    icount = 0.0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellAverage(f,path='')
        if (i == 0):
            if (not paper_data):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
            else:
                data = np.zeros((a.nr,a.nq),dtype='float64')
            t_start = a.time[0]
        for j in range(a.niter):
            if (not paper_data):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
            else:
                data[:,:] = data[:,:]+a.vals[:,:,j]
            icount = icount+1.0
        t_end = a.time[-1]

    delta_time = (t_end - t_start)/3600./24.
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} days".format(t_start/3600./24.))
    print("Last  time: {} days".format(t_end/3600./24.))
    print("\nAveraged over {} days".format(delta_time))

    data = data/icount  # The average is now stored in data
    radius = a.radius
    rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1

    # get the density
    ref = ReferenceState(filename="reference", path=data_dir)
    rho = ref.density

    #For plot 1, we plot the three components of the kinetic energy as a function of radius
    rke_index = a.lut[shortcut_lookup('rke',alt=alt)]  #a.lut[126]  # KE associated with radial motion
    tke_index = a.lut[shortcut_lookup('tke',alt=alt)]  #a.lut[127]  # KE associated with theta motion
    pke_index = a.lut[shortcut_lookup('pke',alt=alt)]  #a.lut[128]  # KE associated with azimuthal motion

    if (not paper_data):
        rke = data[:,0,rke_index] # Grab appropriate slices of the data array
        tke = data[:,0,tke_index]
        pke = data[:,0,pke_index]
    else:
        rke = data[:,rke_index] # Grab appropriate slices of the data array
        tke = data[:,tke_index]
        pke = data[:,pke_index]

    #For plot 2, we plot the square root of the variance of each velocity component.
    #In this case, the mean used in the variance is computed on spherical shells.
    # Note that since we are grabbing the 2nd moment of the distribution, we
    # grab index 1, not 0.  Skewness is stored in index 2, and Kurtosis in 3.

    vr_index = a.lut[shortcut_lookup('vr',alt=alt)]  #a.lut[1]  # V_r
    vt_index = a.lut[shortcut_lookup('vt',alt=alt)]  #a.lut[2]  # V_theta
    vp_index = a.lut[shortcut_lookup('vp',alt=alt)]  #a.lut[3]  # V_phi

    # since this is the sqrt(2nd moment) --> these are the rms velocity components
    if (not paper_data):
        vr = np.sqrt(data[:,1,vr_index])*0.01 # Grab appropriate slices of the data array
        vt = np.sqrt(data[:,1,vt_index])*0.01 # convert to m/s
        vp = np.sqrt(data[:,1,vp_index])*0.01
    else:
        # need to calculate the 2nd moments using old format data
        # variance = <v**2> - <v>**2 = 2*KE/rho - <v>>**2
        vravg = data[:,vr_index]
        vtavg = data[:,vt_index]
        vpavg = data[:,vp_index]
        vr = np.sqrt(2*rke/rho - vravg**2)*0.01 # Grab appropriate slices of the data array
        vt = np.sqrt(2*tke/rho - vtavg**2)*0.01 # convert to m/s
        vp = np.sqrt(2*pke/rho - vpavg**2)*0.01

    if (no_plots):
        return

    lw = 1.5

    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(15,5),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(121)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(rnorm,rke,label = 'KE'+r'$_{rad}$',linewidth=lw)
    plt.plot(rnorm,tke,label = 'KE'+r'$_\theta$',linewidth=lw)
    plt.plot(rnorm,pke,label = 'KE'+r'$_\phi$',linewidth=lw)
    plt.xlim([0.75,1])
    plt.ylabel('Energy Density '+r'( erg cm$^{-3}$)')
    plt.xlabel('Radius '+r'(r/r$_{o}$)')
    legend = plt.legend(loc='upper left', shadow=True, ncol = 1) 

    plt.subplot(122)
    plt.plot(rnorm,vr,label = "v'"+r'$_{r}$',linewidth=lw)
    plt.plot(rnorm,vt,label = "v'"+r'$_\theta$',linewidth=lw)
    plt.plot(rnorm,vp,label = "v'"+r'$_\phi$',linewidth=lw)
    plt.xlim([0.75,1])
    plt.ylabel('Speed '+r'( m s$^{-1}$)')
    plt.xlabel('Radius '+r'(r/r$_{o}$)')
    legend = plt.legend(loc='upper left', shadow=True, ncol = 1) 
    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots, paper_data=args['--paper-data'])

