"""
plot the power spectrum for many different runs and read in the
power spectra from a datafile.

Usage:
    plot_many_spectra.py [options] <data-file>...

    <data-file> is a space separated list of files from which to read
    the spectra data. Minimum one file is required.

Options:
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: power_spectra.eps]
    --quantity=<q>  What are we plotting,'vrs', 's' [default: velocity]
"""

from __future__ import print_function
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def main(input_files, saveplot, savefile, quantity, **kwargs):

    # read data from file(s)
    data = {}
    for file in input_files:
        datatmp = read_input(file, minRaF=430.0)
        data.update(datatmp)

    # gather an array of all possible Rayleigh numbers
    Ra = np.array([])
    for pr in data.keys():
        for loc in data[pr].keys():
            Ra = np.concatenate((Ra, data[pr][loc]['Ra_F'][:]))
    Ra = np.sort(np.array(list(set(Ra)))) # make Ra unique (set) and sort it
    minRa = np.amin(Ra)
    maxRa = np.amax(Ra)
    color_ind = np.linspace(0.05,0.95,len(Ra))

    colormap = 'jet'

    # return color based on Rayleigh number
    def get_color(r, cmap='jet'):
        colormap = plt.get_cmap(cmap)
        icol = -1
        for i, ra in enumerate(Ra):  # for whatever reason np.where is not working robustly
            if (r == ra):
                icol = i
                break
        if (icol >= 0):
            col = colormap(color_ind[i]) # turn that into a color
        else:
            col = 'k'

        return col

    #####################################
    # setup the figure
    #####################################
    #Prandtl = ['0.5', '1.0', '2.0']
    Prandtl = ['0.25', '0.5', '1.0', '2.0', '4.0']
    #Prandtl = ['0.25', '0.5', '2.0', '4.0']
    nrows = len(Prandtl)
    ncols = 3

    # istart = 0 includes the \ell=0, i.e. all of them
    # istart = 1 starts with \ell=1
    # istart = 2 excludes both \ell=0 & \ell=1
    if (quantity.lower() == 'velocity'):
        ylim = (1e2, 1e10)
        #aspect = 0.25
        istart = 1
        xlim = (1e0, 600)
    elif (quantity.lower() == 'entropy' or quantity.lower() == 's'):
        ylim = (1e0, 1e7)
        #aspect = 0.3
        istart = 2
        xlim = (1e0, 600)
    elif (quantity.lower() == 'vr' or quantity.lower() == 'v_r'):
        ylim = (1e2, 1e9)
        #aspect = 0.3
        istart = 1
        xlim = (1e0, 600)
    elif (quantity.lower() == 'vrs'):
        ylim = (1e0, 1e8)
        #aspect = 0.28
        istart = 1
        xlim = (1e0, 600)
    elif (quantity.lower() == 'vhoriz'):
        ylim = (1e2, 1e10)
        #aspect = 0.25
        istart = 1
        xlim = (1e0, 600)
    else:
        ylim = None
        #aspect = 0.3
        istart = 1
        xlim = (1e0, 600)

    aspect = np.log10(xlim[1]/xlim[0])/np.log10(ylim[1]/ylim[0])
    fig, axarr = plt.subplots(nrows, ncols, dpi=300, figsize=(6.0,9.0),
                              subplot_kw=dict(adjustable='box-forced', aspect=aspect))
    for row, Pr in enumerate(Prandtl):
        for col in range(ncols):
            # get what column label
            if (col==0):
                loc = 'upper'
            elif (col==1):
                loc = 'middle'
            elif (col==2):
                loc = 'lower'

            # set title & labels that depend on parameters
            if (row == 0):
                pos_title = loc[0].upper() + loc[1:]
                axarr[row,col].set_title(pos_title + " Convection Zone", y=1.08, fontsize=10)
            if (col == ncols-1):
                textstr = "Pr = {}".format(Pr)
                axarr[row,col].text(1.05, 0.7, textstr, transform=axarr[row,col].transAxes,
                                    fontsize=10, rotation=-90)

            # turn off yaxis labels for interior plots
            if (col > 0):
                #axarr[row,col].axes.get_yaxis().set_ticklabels([])
                plt.setp(axarr[row,col].get_yticklabels(), visible=False)

            plt.setp(axarr[row,col].get_xticklabels(), fontsize=8)
            plt.setp(axarr[row,col].get_yticklabels(), fontsize=8)

            # plot data, reverse everything to make largest Ra plotted on top
            for i,r in enumerate(data[Pr][loc]['Ra_F'][::-1]):
                power = data[Pr][loc]['power'][::-1][i][:]
                lmax  = data[Pr][loc]['lmax'][::-1][i]
                ell = np.linspace(0, lmax, len(power), endpoint=True)
                axarr[row,col].plot(ell[istart:], power[istart:],
                                    color=get_color(r, cmap=colormap))

            # set limits
            axarr[row,col].set_xlim(xlim)
            axarr[row,col].set_ylim(ylim)
            axarr[row,col].set_xscale('log')
            axarr[row,col].set_yscale('log')

            # remove a few yticks to make it less busy
            if (col == 0):
                plt.setp(axarr[row,col].get_yticklabels()[::2], visible=False)
                #for label in axarr[row,col].get_yticklabels()[::2]:
                #    label.set_visible(False)

    # x & y label regardless of parameters
    for row in range(nrows):
        if (quantity.lower() == 'velocity'):
            axarr[row,0].set_ylabel("Velocity Power", fontsize=10)
        elif (quantity.lower() == 'entropy' or quantity.lower() == 's'):
            axarr[row,0].set_ylabel("Entropy Power", fontsize=10)
        elif (quantity.lower() == 'vr' or quantity.lower() == 'v_r'):
            axarr[row,0].set_ylabel("$V_r$ Power", fontsize=10)
        elif (quantity.lower() == 'vrs'):
            axarr[row,0].set_ylabel("$V_rS$ Power", fontsize=10)
        elif (quantity.lower() == 'vhoriz'):
            axarr[row,0].set_ylabel("$V_\mathrm{Horizontal}$ Power", fontsize=10)
        else:
            axarr[row,0].set_ylabel("{} Power".format(quantity), fontsize=10)
    for col in range(ncols):
        axarr[nrows-1,col].set_xlabel(r"Harmonic Degree $\ell$", fontsize=10)

    l=0.01; r=0.99; b=0.07; t=0.96; w=0.0; h=0.2
    plt.subplots_adjust(left=l, bottom=b, right=r, top=t, wspace=w, hspace=h)

    # add colorbar
    #  need to get a "scalar mappable" going first
    #  also need "fake" data i.e. sm._A = []
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=plt.Normalize(vmin=minRa, vmax=maxRa))
    sm._A = []
    if (quantity.lower() == 'velocity'):
        cb_x = 0.36; cb_y = 0.09
    elif (quantity.lower() == 'entropy' or quantity.lower() == 's'):
        cb_x = 0.352; cb_y = 0.10
    elif (quantity.lower() == 'vr' or quantity.lower() == 'v_r'):
        cb_x = 0.355; cb_y = 0.09
    elif (quantity.lower() == 'vrs'):
        cb_x = 0.355; cb_y = 0.09
    elif (quantity.lower() == 'vhoriz'):
        cb_x = 0.355; cb_y = 0.09
    else:
        cb_x = 0.35; cb_y = 0.10
    cb_w = 0.02; cb_h = 0.1
    cbaxes = fig.add_axes([cb_x, cb_y, cb_w, cb_h]) # position = (x,y,width,height)
    cm = fig.colorbar(sm, cax=cbaxes, ticks=[])     # turn off tick marks
    #cm.outline.set_color('white')                   # outline the thing in white
    #cm.outline.set_linewidth(1)                     # manually label low/high Ra
    cm.ax.text(-.56,1.04,r'High Ra$_F$', fontsize=6)
    cm.ax.text(-.56,-.15,r'Low Ra$_F$', fontsize=6)

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

def read_input(filename, separator=None, minRaF=0):
    """
    parse data file to extract spectra data
    """
    from collections import OrderedDict

    # allocate space
    data = OrderedDict()

    with open(filename, 'r') as f:

        # only parse non-empty lines and lines that dont start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                # split line and remove whitespace
                fields = line.split(separator)
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract the data fields
                Pr     = fields[0]
                _loc   = fields[1].lower() # make it lower case
                _Ra_F  = fields[2]
                _lmax  = fields[3]
                _sep   = fields[4]
                _power = fields[5:]

                # only add data if Ra_F > minRaF
                if (float(_Ra_F) < minRaF):
                    continue

                # this is a new entry so initialize the data structure
                if (Pr not in data.keys()):
                    data[Pr] = {'upper':OrderedDict(), 'lower':OrderedDict(), 'middle':OrderedDict()}
                    data[Pr]['upper']  = {'Ra_F':[], 'lmax':[], 'power':[]}
                    data[Pr]['middle'] = {'Ra_F':[], 'lmax':[], 'power':[]}
                    data[Pr]['lower']  = {'Ra_F':[], 'lmax':[], 'power':[]}

                # add the data
                data[Pr][_loc]['Ra_F'].append( float(_Ra_F))
                data[Pr][_loc]['lmax'].append( float(_lmax))
                data[Pr][_loc]['power'].append(      _power)

    # convert data into np arrays
    for Pr in data.keys():
        for loc in data[Pr].keys():
            for field in data[Pr][loc].keys():
                data[Pr][loc][field] = np.array(data[Pr][loc][field])

    return data

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    saveplot = args['--save']
    savefile = args['--output']
    input_files = args['<data-file>']
    quantity = args['--quantity']

    main(input_files, saveplot, savefile, quantity)

