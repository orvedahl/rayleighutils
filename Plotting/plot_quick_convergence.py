"""
plot quick and dirty convergence data

Usage:
     plot_quick_convergence.py [options] <datafile>

Options:
     --Ncol=<n>     Specify column of grid points [default: 0]
     --errcol=<e>   Specify column of error data [default: 1]
     --output=<o>   Set filename for saving [default: convergence.png]
     --save         Save the image [default: False]
     --no-curves    Do not plot O(dx**n) curves [default: False]

"""

import matplotlib.pyplot as plt
import numpy as np

def main(datafile, Ncol, errcol, output, save, no_curves):
    data  = np.loadtxt(datafile)
    N     = data[:,Ncol]
    error = data[:,errcol]

    # plot
    plt.plot(N, error, linestyle='', marker='x', color='r', label='data')
    if (not no_curves):
        plt.plot(N, error[-1]*(N/N[-1])**-2 , linestyle=':', color='k', label='$O(N^{-2})$')
        plt.plot(N, error[-1]*(N/N[-1])**-4 , linestyle='--', color='k', label='$O(N^{-4})$')
        plt.plot(N, error[-1]*(N/N[-1])**-6 , linestyle='-.', color='k', label='$O(N^{-6})$')
    plt.legend(loc='upper right')
    plt.xlabel('N')
    plt.ylabel('Error')
    plt.title('Convergence')
    plt.xscale('log')
    plt.yscale('log')
    if (save):
        plt.savefig(output)
        print("\nsaved: {}\n".format(output))
    else:
        plt.show()

if __name__=="__main__":
    from docopt import docopt
    args = docopt(__doc__)

    datafile = args['<datafile>']
    Ncol = int(args['--Ncol'])
    errcol = int(args['--errcol'])
    save = args['--save']
    output = args['--output']
    no_curves = args['--no-curves']

    main(datafile, Ncol, errcol, output, save, no_curves)

