"""
Plot total energy vs time

Usage:
    plot_energy_total.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_trace.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --magnetic      Plot the magnetic energy [default: False]
    --derivs        Plot the derivatives [default: False]
    --fft           FFT the time series [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import GlobalAverage, build_file_list, ReferenceState
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, magnetic, derivs, do_fft, **kwargs):

    # integration method
    method = 'cheb-end'

    # Build a list of all files ranging from iteration 0 million to 10 million
    #files = build_file_list(0,10000000,path='G_Avgs')
    if (data_dir[-1] != "/"):
        data_dir = data_dir + '/'
    files = build_file_list(start_file,end_file,path=data_dir+'G_Avgs')
    #The indices associated with our various outputs are stored in a lookup table
    #as part of the GlobalAverage data structure.  We define several variables to
    #hold those indices here:
    a = GlobalAverage(filename=files[0],path='')  # read a file to get the lookup table
    ke_index  = a.lut[shortcut_lookup('ke')]   # Kinetic Energy (KE)
    if (magnetic):
        me_index  = a.lut[shortcut_lookup('me')]   # Magnetic Energy (ME)

    #Next, we intialize some empy lists to hold those variables
    ke = []
    if (magnetic):
        me = []

    # We also define an empty list to hold the time.  This time
    # will be absolute time since the beginning of the run, 
    # not since the beginning of the first output file.  
    # In this case, the run was dimensional, so the time is recorded 
    # in seconds.  We will change that to days as we go.
    alldays = []
    alltime = []

    # Next, we loop over all files and grab the desired data
    # from the file, storing it in appropriate lists as we go.
    for f in files[-last_number_files:]:
        a = GlobalAverage(filename=f,path='')


        days = a.time/(3600.0*24.0)
        time = a.time
        for i,d in enumerate(days):
            alldays.append(d)
            alltime.append(time[i])
            ke.append(a.vals[i,ke_index])
            if (magnetic and (me_index is not None)):
                me.append(a.vals[i,me_index])

    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    # need to get the grid data so read the reference state
    reference = ReferenceState(filename="reference", path=data_dir)
    r_outer = np.max(reference.radius)
    r_inner = np.min(reference.radius)
    H = r_outer - r_inner
    volume = 4*np.pi*my_integrator(reference.radius**2, reference.radius, method=method, data_reversed=True)
    # get mass by integrating the density of the shell
    mass = 4*np.pi*my_integrator(reference.density*reference.radius**2, reference.radius, method=method, data_reversed=True)
    mean_KE_density = np.mean(ke)
    mean_KE_total = mean_KE_density*volume     # dimensional KE, [KE] = ergs

    print("\nAvg total KE density [simulation 'erg cm^-3'] : {}".format(mean_KE_density))
    print("Avg total KE [simulation 'erg']               : {}".format(mean_KE_total))
    if (magnetic):
        mean_ME_density = np.mean(me)
        mean_ME_total = mean_ME_density*volume     # dimensional ME, [ME] = ergs
        print("\nAvg total ME density [simulation 'erg cm^-3'] : {}".format(mean_ME_density))
        print("Avg total ME [simulation 'erg']               : {}".format(mean_ME_total))

    #Create two plots.
    # Plot 1:  Total KE and its breakdown.
    # Plot 2:  Breakdown of the mean KE
    if (no_plots):
        return

    #Changing the plotting parameter somewhat depending on saveplot
    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7.5,4),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alltime,ke,'red',label=r'KE$_{total}$')
    if (magnetic):
        plt.plot(alltime,me,'blue',label=r'ME$_{total}$')
    plt.yscale('log')
    plt.xlabel('Time (simulation secs)')
    plt.ylabel('Energy Density '+r'(simulation erg cm$^{-3}$)')
    if (saveplot):
        legend = plt.legend(loc='upper left', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='upper left', shadow=True, ncol = 2) 
    if (derivs):
        plt.twinx()
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        from NumericalToolsLocal.calculus.differentiate import derivative
        dkedt = derivative(ke, alltime, data_reversed=False)
        plt.plot(alltime,dkedt,'black', linestyle='--', label=r'$|$dKE$_{total}$/dt$|$')
        #plt.plot(alltime,np.abs(dkedt),'black', linestyle='--', label=r'$|$dKE$_{total}$/dt$|$')
        if (magnetic):
            dmedt = derivative(me, alltime, data_reversed=False)
            plt.plot(alltime,dmedt,'black', linestyle=':', label=r'$|$dME$_{total}$/dt$|$')
        plt.yscale('symlog',linthreshy=1e-2)
        plt.ylabel('dKE/dt')
        if (saveplot):
            legend = plt.legend(loc='upper right', shadow=True, ncol = 2, fontsize = 'x-small') 
        else:
            legend = plt.legend(loc='upper right', shadow=True, ncol = 2) 
    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

    if (do_fft):
        import NumericalToolsLocal.fft as my_fft

        # interp to uniform grid
        t_uniform, ke_uniform = my_fft.interp_uniform(ke, alltime)
        if (magnetic):
            t_uniform, me_uniform = my_fft.interp_uniform(me, alltime)

        # FFT the data
        fk, kfreq, ke_power, xwindow = my_fft.FFT(ke_uniform, t_uniform)
        if (magnetic):
            fk, kfreq, me_power, xwindow = my_fft.FFT(me_uniform, t_uniform)

        # plot it
        plt.clf()
        plt.plot(kfreq, ke_power, label='FFT(KE)', color='r', linestyle='-')
        if (magnetic):
            plt.plot(kfreq, me_power, label='FFT(ME)', color='b', linestyle='-')

        plt.legend(loc='upper right', shadow=True, ncol=2, fontsize='x-small')
        plt.ylabel("power")
        plt.xlabel("angular freq")
        plt.xscale('log')
        plt.yscale('log')

        plt.show()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    magnetic = args['--magnetic']
    derivs = args['--derivs']
    do_fft = args['--fft']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, magnetic, derivs, do_fft)

