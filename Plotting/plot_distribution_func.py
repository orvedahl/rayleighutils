"""
Plot distribution functions from Shell Slices

Usage:
    plot_distribution_func.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: distribution]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-plots        Suppress the plots [default: False]
    --list            List the possible radial indices [default: False]
    --radial-ind=<r>  Comma separated list of radial indices to plot [default: 0,3,6]
    --alt             Use alternate lookup table [default: False]
"""

from __future__ import print_function
import numpy as np
import env
from ReadRayleigh.diagnostic_reading import ShellSlice, build_file_list
import matplotlib.pyplot as plt
from matplotlib import ticker
from Utilities.look_up_quantity import shortcut_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots, rad_inds, list, alt, **kwargs):

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Slices')
    if (list):
        a = ShellSlice(filename=files[-1], path='')
        print("\nReading file: {}".format(files[-1]))
        print("\nPossible radial indices:")
        print("    rad-ind\tradial index")
        for i,ind in enumerate(a.inds):
            print("       {}\t   {}".format(i, ind))
        print()
        return
    print("\n...collecting data...")
    Nfiles = len(files[-last_number_files:])
    N_entries = 0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellSlice(filename=f, path='')
        if (i==0):
            # this approach assumes all files have the same number of time entries, a.niter
            # which tends to break when alt=True, in that case I do: files[-last-1:-1]
            data = np.zeros((a.nphi, a.ntheta, a.nr, a.nq, a.niter, Nfiles))
            t_start = a.time[0]
        data[:,:,:,:,:,i] = a.vals[:,:,:,:,:]
        N_entries += a.niter
        t_end = a.time[-1]

    delta_time = (t_end - t_start)/3600./24.
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(Nfiles))
    print("\nFirst time: {} days".format(t_start/3600./24.))
    print("Last  time: {} days".format(t_end/3600./24.))
    print("\nAveraged over {} days".format(delta_time))

    print("\nPossible radial indices: {}".format(a.inds))
    for i in rad_inds:
        print("\tplotting rad_index = {:3d} (radius = {})".format(a.inds[i], a.radius[i]))

    # grid data
    nth = a.ntheta; nphi = a.nphi
    sintheta = a.sintheta
    costheta = a.costheta
    theta = np.arccos(costheta)
    phi = np.linspace(-np.pi, np.pi, nth, endpoint=False) # this may be wrong?
    maxphi   = np.amax(phi);   minphi   = np.amin(phi)
    maxtheta = np.amax(theta); mintheta = np.amin(theta)
    dphi   = (maxphi - minphi)/nphi
    dtheta = (maxtheta - mintheta)/nth

    #Identify the variables indices for vr,vphi, and entropy
    vr_index = a.lut[shortcut_lookup('vr', alt=alt)]  #a.lut[1]
    s_index  = a.lut[shortcut_lookup('s', alt=alt)]   #a.lut[64]
    r_index  = rad_inds[0]

    # each element in histogram array a_ij only contributes weight_ij, not 1 to bin count
    total_area = 4.*np.pi
    _weights = np.zeros((nphi,nth))
    for i in range(nth):
        area = sintheta[i]*dphi*dtheta
        _weights[:,i] = area/total_area/N_entries

    # extract data and convert from cm/s to m/s
    vr = np.array([])
    entropy = np.array([])
    vrS = np.array([])
    weights = np.array([])
    Ntime = np.shape(data)[4]
    for j in range(Nfiles):
        for i in range(Ntime):
            radial_vel = data[:,:,r_index,vr_index,i,j]/100.  # go from cm/s --> m/s
            S = data[:,:,r_index,s_index,i,j]
            S = S - np.mean(S)                                # subtract ell=0
            vrS_tmp = radial_vel*S

            vr      = np.concatenate((vr,      radial_vel.flatten() ))
            entropy = np.concatenate((entropy, S.flatten()          ))
            vrS     = np.concatenate((vrS,     vrS_tmp.flatten()    ))
            weights = np.concatenate((weights, _weights.flatten()   )) # make all arrays the same size

    # now get upflow/downflow distributions
    Nvalues = len(vr)
    vr_upflow = []
    vr_downflow = []
    S_upflow = []
    S_downflow = []
    vrS_upflow = []
    vrS_downflow = []
    weight_upflow = []
    weight_downflow = []
    # add 'other' stuff
    vr_entrained = []
    S_entrained = []
    vrS_entrained = []
    weight_entrained = []
    for i in range(Nvalues):
        vri = vr[i]
        Si = entropy[i]
        vriSi = vrS[i]
        wi = weights[i]
        if (vri > 0):                  # upflows
            vr_upflow.append(vri)
            S_upflow.append(Si)
            vrS_upflow.append(vriSi)
            weight_upflow.append(wi)
        else:                          # downflows
            vr_downflow.append(vri)
            S_downflow.append(Si)
            vrS_downflow.append(vriSi)
            weight_downflow.append(wi)
        if (vriSi < 0):                # negative energy flux
            vr_entrained.append(vri)
            S_entrained.append(Si)
            vrS_entrained.append(vriSi)
            weight_entrained.append(wi)

    # generate distribution functions
    #    --histogram the data
    #    --get number of points for a given data value
    #    --multiply by area of those regions
    #    --divide by total area or region
    #    --result should be fraction with given data value
    #  note: the r**2 term will cancel in both area multiplications
    #  when using np.hist:
    #    --set weight = area_i / area_total and flatten data array
    #  when summing up all the histogram values, it should be 1
    vel_bins = 50 # number of bins to use for histogram
    s_bins = 50
    vrS_bins = 50
    vrange = [np.amin(vr),      np.amax(vr)]     # range of histogram x-axis
    srange = [np.amin(entropy), np.amax(entropy)]
    vrSrange = [np.amin(vrS),      np.amax(vrS)]

    if (no_plots):
        return

    # make plots
    print()
    plt.clf()
    hist, bins = np.histogram(vr, bins=vel_bins, range=vrange, weights=weights)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='total', color='b')
    print("\tsum of hist Vr: {}".format(np.sum(hist)))
    hist, bins = np.histogram(vr_upflow, bins=vel_bins, range=vrange, weights=weight_upflow)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='up-flows', color='g')
    print("\tsum of hist Vr upflow: {}".format(np.sum(hist)))
    hist, bins = np.histogram(vr_downflow, bins=vel_bins, range=vrange, weights=weight_downflow)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='down-flows', color='r')
    print("\tsum of hist Vr downflow: {}".format(np.sum(hist)))
    hist, bins = np.histogram(vr_entrained, bins=vel_bins, range=vrange, weights=weight_entrained)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='entrained', color='k')
    print("\tsum of hist Vr entrained: {}".format(np.sum(hist)))
    plt.title(r"$v_r$ distribution function")
    plt.xlabel(r"Binned $v_r$ (m/s)")
    plt.ylabel("Distribution")
    plt.legend(loc='upper left')
    if (saveplot):
        output = savefile + "_vr.png"
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    plt.clf()
    hist, bins = np.histogram(entropy, bins=s_bins, range=srange, weights=weights)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='total', color='b')
    print("\tsum of hist S: {}".format(np.sum(hist)))
    hist, bins = np.histogram(S_upflow, bins=s_bins, range=srange, weights=weight_upflow)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='up-flows', color='g')
    print("\tsum of hist S upflow: {}".format(np.sum(hist)))
    hist, bins = np.histogram(S_downflow, bins=s_bins, range=srange, weights=weight_downflow)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='down-flows', color='r')
    print("\tsum of hist S downflow: {}".format(np.sum(hist)))
    hist, bins = np.histogram(S_entrained, bins=s_bins, range=srange, weights=weight_entrained)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='entrained', color='k')
    print("\tsum of hist S entrained: {}".format(np.sum(hist)))
    plt.title(r"$S^\prime$ distribution function")
    plt.xlabel(r"Binned $S^\prime$ (erg g$^{-1}$ K$^{-1}$)")
    plt.ylabel("Distribution")
    plt.legend(loc='upper left')
    if (saveplot):
        output = savefile + "_Sprime.png"
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

    plt.clf()
    hist, bins = np.histogram(vrS, bins=vrS_bins, range=vrSrange, weights=weights)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='total', color='b')
    print("\tsum of hist vrS: {}".format(np.sum(hist)))
    hist, bins = np.histogram(vrS_upflow, bins=vrS_bins, range=vrSrange, weights=weight_upflow)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='up-flows', color='g')
    print("\tsum of hist vrS upflow: {}".format(np.sum(hist)))
    hist, bins = np.histogram(vrS_downflow, bins=vrS_bins, range=vrSrange, weights=weight_downflow)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='down-flows', color='r')
    print("\tsum of hist vrS downflow: {}".format(np.sum(hist)))
    hist, bins = np.histogram(vrS_entrained, bins=vrS_bins, range=vrSrange, weights=weight_entrained)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='entrained', color='k')
    print("\tsum of hist vrS entrained: {}".format(np.sum(hist)))
    plt.title(r"$v_rS^\prime$ distribution function")
    plt.xlabel(r"Binned $v_rS^\prime$ (m s$^{-1}$ erg g$^{-1}$ K$^{-1}$)")
    plt.ylabel("Distribution")
    plt.legend(loc='upper right')
    #plt.yscale('log')
    if (saveplot):
        output = savefile + "_vrSprime.png"
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    list = args['--list']
    alt = args['--alt']

    indices = (args['--radial-ind']).split(",")
    radial_ind = []
    for i in indices:
        radial_ind.append(int(i)) # make them integers

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, no_plots, radial_ind, list, alt)
