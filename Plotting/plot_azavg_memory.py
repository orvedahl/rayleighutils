"""
Plot many Azimuthal Average files using Matplotlib

Memory friendly version

Usage:
    plot_slices_pyplot_memory.py [options]

Options:
    --start=<s>        Starting file [default: 0]
    --end=<e>          Ending file [default: 100000000]
    --last=<l>         Use last <l> number of files (so files[-l:] is used) [default: 0]
    --data-dir=<d>     Specify location of data [default: ./]
    --save             Save an image [default: False]
    --dpi=<d>          Resolution of saved png [default: 200]
    --output=<o>       Save images as <o>.png [default: az_avg]
    --output-dir=<d>   Put output images in directory <d> [default: ./]
    --qcode=<q>        Plot quantity code q, 1=vr, 66=S' [default: 1]
    --scale=<s>        Set scale for saturating the plot [default: 1.5]
    --tind=<t>         Specify time index if only plotting one slice
    --list             List possible radii [default: False]
    --no-mean          Subtract off the mean for each quantity [default: False]
    --start-count=<c>  Starting iteration of output files [default: 0]

"""

from __future__ import print_function
import env
import NumericalToolsLocal.public as NT
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
from Utilities.time_utils import Timer
from Utilities import azavg_util
from Utilities.look_up_quantity import general_lookup
import os
import matplotlib.pyplot as plt
import numpy as np

def scale_data(indata, scale_factor=1.5, no_mean=False, data_lim=None):
    """
    rescale data so that
       1) indata has zero spherical mean
       2) indata is rescaled to have min at -1, max at +1
       3) indata "saturates" at absolute values > scale_factor*stdev(indata)

    indata is assumed to be dimensioned as [theta, r]
    """
    nth, nr = np.shape(indata)

    # remove spherical mean
    if (no_mean):
        for r in range(nr):
            indata[:,r] -= np.mean(indata[:,r])

    # saturate data at data_lim
    if (data_lim is None):
        sigma = np.std(indata)
        data_lim = scale_factor*sigma
    ind_above = np.where(indata >  data_lim)
    ind_below = np.where(indata < -data_lim)
    indata[ind_above] =  data_lim
    indata[ind_below] = -data_lim

    # rescale to be in [-1,1]
    indata /= data_lim

    return indata

def main(save, savefile, start_file, end_file, last_number_files, data_dir, output_dir,
         tind, qcode, scale, no_mean, start_count, list_contents, dpi):

    # parameters
    colorbar  = False
    draw_grid = True
    labels    = True
    scale_by_first = True   # scale colorbar of all images by first entry
    title    = 'Radial Magnetic Field'
    xlabel   = '__TIME__'
    #xlabel   = r'$r = r_o$'
    tfont    = 12   # title font
    xfont    = 10   # xlabel
    cfont    = 10   # colorbar

    time_it = Timer()
    time_it.start()

    if (tind is not None):
        tind = int(tind)

    # set output parameters
    if (save):
        if (not os.path.exists(output_dir)):
            os.makedirs(output_dir)
        # store inputs
        with open(os.path.join(output_dir, "pyplot_memory_input.txt"), 'w') as mf:
            n = "\n"
            mf.write(n)
            mf.write("-----------------------------------------------------------"+n)
            mf.write("Input parameters for plot_azavg_memory.py image generation"+n)
            mf.write("-----------------------------------------------------------"+n)
            mf.write(n)
            mf.write("start       = {}".format(start_file)+n)
            mf.write("end         = {}".format(end_file)+n)
            mf.write("last        = {}".format(last_number_files)+n)
            mf.write("data-dir    = {}".format(data_dir)+n)
            mf.write("output-dir  = {}".format(output_dir)+n)
            mf.write("output      = {}".format(savefile)+n)
            mf.write("qcode       = {}".format(qcode)+n)
            if (tind is not None):
                mf.write("tind        = {}".format(tind)+n)
            mf.write("scale       = {}".format(scale)+n)
            mf.write("no-mean     = {}".format(no_mean)+n)
            mf.write("start-count = {}".format(start_count)+n)
            mf.write("colorbar    = {}".format(colorbar)+n)
            mf.write("draw grid   = {}".format(draw_grid)+n)
            mf.write("labels      = {}".format(labels)+n)
            mf.write("scale first = {}".format(scale_by_first)+n)
            if (labels):
                mf.write("title       = {}".format(title)+n)
                mf.write("xlabel      = {}".format(xlabel)+n)
                mf.write("tfont       = {}".format(tfont)+n)
                mf.write("xfont       = {}".format(xfont)+n)
            if (colorbar):
                mf.write("cfont       = {}".format(cfont)+n)
            mf.write(n)

        # give output proper pathing
        savefile = os.path.join(output_dir, savefile)

    # build list of files
    path = os.path.join(data_dir, 'AZ_Avgs')
    files = build_file_list(start_file, end_file, path=path)
    files = files[-last_number_files:]
    ntotal = len(files)

    # read in first & last data files
    a = AzAverage(files[0], path='')
    b = AzAverage(files[-1], path='')

    print("\nFirst file = {}".format(files[0]))
    print("Last file = {}".format(files[-1]))
    print("Number of files = {}".format(ntotal))
    print("\nFirst time = {}".format(a.time[0]))
    print("Last time = {}".format(b.time[-1]))
    print("Number of records = ~{}".format(len(a.time)*ntotal))

    # extract quantities & radial slice
    qcode = a.lut[qcode]

    if (list_contents):
        print("\nPossible Quantities")
        print("\tqcode\tquantity name")
        for q in a.qv:
            print("\t{}\t{}".format(q, general_lookup(index=q)))
        print()

        return

    # get various meta data
    nr = a.nr; nth = a.ntheta
    radius = a.radius
    sintheta = a.sintheta
    costheta = a.costheta

    # get scaling factor from first time slice
    if (scale_by_first):
        indata = a.vals[:,:,qcode,0].copy()
        if (no_mean):
            for r in range(nr):
                indata[:,r] -= np.mean(indata[:,r])
        data_lim = scale*np.std(indata)
    else:
        data_lim = None

    #cmap = 'RdBu' # 'RdYlBu'
    cmap = 'RdBu_r'

    # setup window & figure
    fig = plt.figure(1, dpi=dpi, figsize=(4,6))

    print("\nLooping over time...")
    for i,f in enumerate(files):
        print("...starting file {} (out of {})".format(i+1, ntotal))

        a = AzAverage(f, path='')
        data = a.vals[:,:,qcode,:] # [ntheta, nr, nt]

        # allow ability to plot single time instance from each file
        if (tind is None):
            loop_index = range(a.niter)
        else:
            loop_index = [tind]

        # loop over all time steps in the file
        for i in loop_index:

            # scale the data
            quantity = scale_data(data[:,:,i], scale_factor=scale,
                                  no_mean=no_mean, data_lim=data_lim)

            # clear figure
            plt.clf()

            # make a projection
            ax = fig.add_subplot(1,1,1)

            units = ''
            azavg_util.plot_azav(fig, ax, quantity, radius, costheta, sintheta, r_bcz=0,
                                 mycmap=cmap, cbar=colorbar, orientation='vertical',
                                 boundsfactor=1.5, boundstype='rms', units=units,
                                 fontsize=xfont,
                                 underlay=[0], nlevs=6, show_contours=True)

            # set labels
            if (labels):
                ax.set_title(title, fontsize=tfont)
                if (xlabel == '__TIME__'):
                    xlabel = r'$t/t_\nu = {:.3f}$'.format(a.time[i])
                    ax.set_xlabel(xlabel, fontsize=xfont)
                    xlabel = '__TIME__'
                else:
                    ax.set_xlabel(xlabel, fontsize=xfont)

            # save/show
            if (save):
                output = savefile + "_{}.png".format(start_count)
                plt.savefig(output, dpi=dpi)
                print("\tsaved image {}".format(output))
            else:
                plt.show()
            start_count += 1

    # close the figure
    plt.close(plt.gcf())

    time_it.stop(); dt = time_it.interval()
    print("\nTotal time to make images = {} sec, {} min".format(dt, dt/60.))
    print("\n---Complete---\n")

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    save = args['--save']
    dpi = int(args['--dpi'])
    savefile = args['--output']
    data_dir = args['--data-dir']
    output_dir = args['--output-dir']
    tind = args['--tind']
    list_contents = args['--list']
    qcode = int(args['--qcode'])
    scale = float(args['--scale'])
    no_mean = args['--no-mean']
    start_count = int(args['--start-count'])

    main(save, savefile, start_file, end_file, last_number_files, data_dir, output_dir,
         tind, qcode, scale, no_mean, start_count, list_contents, dpi)

