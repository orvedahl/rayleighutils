"""
Plot KE of differential rotation vs time from AZ_Avgs data

Usage:
    plot_energy_KE.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: KE_differential_rotation.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --debug              Debug mode [default: False]
    --quants=<q>         Comma separated list of quantities to use

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list, ReferenceState
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import os
from NumericalToolsLocal.public import LegendreGrid
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, debug, quants, **kwargs):

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    path = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file,end_file,path=path)

    if (quants is None):
        quant_list = None
    else:
        quant_list = list(quants)

    a = AzAverage(filename=files[0],path='',quantity=quant_list)

    # compile a time series of all the files
    data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                  AzAverage, axis=-1, data_attr="vals", quantity=quant_list)
    alltime = Pm*np.array(time)

    # read reference state
    ref = ReferenceState(filename="reference", path=data_dir)
    density = ref.density

    print("\nFirst file: {}".format(files[-last_number_files:exclude_last][0]))
    print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    nt = len(alltime)
    nr = a.nr
    nth = a.ntheta
    radius = a.radius
    rad2 = radius*radius
    theta = np.arccos(a.costheta)
    sinth = a.sintheta

    # extract data
    vp_index = a.lut[shortcut_lookup('vp')]
    vphi2 = data[:,:,vp_index,:]**2         # size [nth,nr,nt]

    # volume integrating:
    #     int f * r**2*sinth*dth*dr*dphi / Vol
    #     int [int f dphi ] * r**2*sinth*dth*dr / Vol
    #     int [int f dphi/2pi] * r**2*sinth*dth*dr / (Vol/2pi)
    #     int [az_avg(f)] * r**2*sinth*dth*dr / (Vol/2pi)
    print("\nIntegrating over theta...")
    Leg = LegendreGrid(nth, a=-1, b=1)
    weights = Leg.weights; C = Leg.integ_coeff

    tmp = np.zeros((nr,nt))
    if (debug):
        tmp2 = np.zeros((nr,nt))
        for t in range(nt):
            for r in range(nr):
                # I think the sin(th) is taken care of in some u-substitution
                tmp[r,t] = C*np.sum(vphi2[:,r,t]*weights)
                tmp2[r,t] = my_integrator(sinth*vphi2[:,r,t], theta,
                                          method='trap', data_reversed=1)
    else:
        for t in range(nt):
            for r in range(nr):
                # I think the sin(th) is taken care of in some u-substitution
                tmp[r,t] = C*np.sum(vphi2[:,r,t]*weights)

    print("\nIntegrating over radius...")
    KE_diff_rot = np.zeros((nt))
    if (debug):
        KE_debug = np.zeros((nt))
        for t in range(nt):
            KE_diff_rot[t] = my_integrator(0.5*density[:]*rad2*tmp[:,t], radius,
                                           data_reversed=1, method='cheb-end')
            KE_debug[t] = my_integrator(0.5*density[:]*rad2*tmp2[:,t], radius,
                                        data_reversed=1, method='cheb-end')
    else:
        for t in range(nt):
            KE_diff_rot[t] = my_integrator(0.5*density[:]*rad2*tmp[:,t], radius,
                                           data_reversed=1, method='cheb-end')

    # normalize by volume, making it an average
    # don't include a factor of 2*pi, because these are Az_Avgs, where
    # a 2*pi has already been applied
    volume = 2./3.*(radius[0]**3 - radius[-1]**3)
    KE_diff_rot /= volume
    if (debug):
        KE_debug /= volume

    if (no_plots):
        return

    print("\nPlotting...")

    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7.5,4),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alltime,KE_diff_rot,label=r'$1/2\rho<v_\phi>^2$')
    if (debug):
        plt.plot(alltime,KE_debug,label=r'KE debug', linestyle='--')
    plt.yscale('log')
    plt.xlabel('Time (simulation secs)')
    plt.ylabel('Energy Density '+r'(simulation erg cm$^{-3}$)')
    if (saveplot):
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 

    if (debug):
        plt.twinx()
        plt.plot(alltime,(KE_debug-KE_diff_rot)/KE_diff_rot, linestyle='--', color='k')
        plt.ylabel("err")
        plt.yscale('symlog', linthreshy=1e-9)

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    debug = args['--debug']
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]


    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, debug, quants)

