"""
Calculate various things surrounding the non-dimensional Boussinesq
runs as a function of time

Usage:
    plot_boussinesq_timeseries.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: boussinesq_timeseries.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --Pr=<p>        Prandtl number, Pr=nu/kappa [default: 1]
    --Pm=<p>        Magnetic Prandtl number, Pr=nu/eta [default: 1]
    --Ek=<e>        Ekman number, Ek=nu/Omega/L**2 [default: 1]
    --aspect=<a>    Set aspect ratio [default: 0.4]
    --logx          Log scale the x-axis [default: False]
    --logy          Log scale the y-axis [default: False]
    --symlogy       Sym-Log scale the y-axis [default: False]
    --plot-peaks    Also plot the peak-number [default: False]
    --quants=<q>    Comma separated list of quantities to use
    --lut=<l>       Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list, ReferenceState
import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from NumericalToolsLocal.calculus.integrate import volume_avg
from NumericalToolsLocal.calculus.differentiate import derivative as my_deriv
import os,sys
from collections import OrderedDict
from Utilities.look_up_quantity import shortcut_lookup, general_lookup
from Utilities import calculate_Nu
from Utilities import time_utils
from Utilities.loop_progress import print_progress

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Pr, Prm, Ek,no_plots, aspect_ratio, quants,
         logx, logy, symlogy, plot_peaks, new_lut, **kwargs):

    Numbers = OrderedDict() # hold all output arrays

    # integration method
    method='cheb-end'

    path = os.path.join(data_dir, "Shell_Avgs")
    files = build_file_list(start_file, end_file, path=path)

    if (quants is None):
        quant_list = None
    else:
        quant_list = list(quants)

    a = ShellAverage(files[-last_number_files:][0], path='', quantity=quant_list)
    data, time = time_utils.TimeSeries(files[-last_number_files:], ShellAverage,
                                        quantity=quant_list)
    time *= Prm
    t_start = time[0]; t_end = time[-1]

    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} sec".format(t_start))
    print("Last  time: {} sec".format(t_end))

    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    radius = a.radius
    fpr = 4.0*np.pi*radius*radius # four pi r^2
    rnorm = radius/radius[0]      # normalize radius so that upper boundary is r=1

    # read reference state
    ref = ReferenceState(filename="reference", path=data_dir)

    print("\n-----------------------------------------------")

    rho = ref.density
    ke_index = a.lut[shortcut_lookup('ke')] # total KE
    ke  = data[:,0,ke_index,:]              # average over angles

    me_index = a.lut[shortcut_lookup('me')] # total ME
    me = data[:,0,me_index,:]               # average over angles

    Nu       = np.zeros_like(time)
    U2_tilde = np.zeros_like(time)
    B2_tilde = np.zeros_like(time)

    U_rms_squared = 2*ke/np.reshape(rho, (len(rho), 1)) # size [r, t]

    n_time = len(time)
    print("\n...looping over time, len(time) = {}".format(n_time))
    for i in range(n_time):
        print_progress("\tprogress = {:.2f} %".format(100.*(i+1)/(n_time)))

        Nu[i] = calculate_Nu.main(start_file, end_file, last_number_files,
                                      saveplot, savefile, data_dir, return_Nu=True,
                                      verbose=False, return_radial_funcs=False,
                                      data=data[...,i], ref_state=ref, lut=a.lut,
                                      aspect_ratio=aspect_ratio)

        # volume average of U_rms**2
        U2_tilde[i] = volume_avg(U_rms_squared[:,i], radius, method=method,
                                     data_reversed=True)

        # volume average of B_rms**2
        B2_tilde[i] = volume_avg(me[:,i], radius, method=method, data_reversed=True)
    print()

    print("\n...calculating Nu vs t\n")
    print("Average Nusselt Number, Nu = {}".format(np.mean(Nu)))
    print("                      Nu-1 = {}".format(np.mean(Nu-1.)))

    Numbers['Nu'] = Nu

    print("\n...calculating Re vs t\n")
    # calculate the Reynolds number
    #  get the rms velocity from the KE:
    #    KE = 0.5*rho*V_rms**2
    #    V_rms**2 = 2*KE/rho
    #  if U is nondimensionalized as
    #    U = nu/L*U^, U^ = simulation, nondimensional
    #  then the Reynolds number becomes
    #    Re = U*L/nu = (nu*U^/L)*L/nu = U^
    #  peak Reynolds number is
    #   peak Re = max(V_rms)*L / nu = max(U^_rms) = max( sqrt(2*KE/rho) )

    # get U_rms
    U_rms = np.sqrt(U_rms_squared)

    # Re = sqrt( vol_avg(U_rms**2) )
    # Re_peak = max( sqrt(U_rms**2) )
    Reynolds_num = np.sqrt(U2_tilde)       # size [t]
    Reynolds_peak = np.amax(U_rms, axis=0) # size [t]

    Re_num  = np.mean(Reynolds_num)
    Re_peak = np.mean(Reynolds_peak)
    print("Avg Reynolds number, Re = {}".format(Re_num))
    print("Peak Reynolds number, Re peak = {}".format(Re_peak))
    print("\nAvg Magnetic Reynolds number, Rem = Re*Pm = {}".format(Re_num*Prm))
    print("Peak Magnetic Reynolds number, Rem peak = {}".format(Re_peak*Prm))

    Numbers['Re'] = Reynolds_num
    Numbers['Re_peak'] = Reynolds_peak

    print("\n...calculating El vs t\n")
    # calculate the Elsasser number, El
    #      El = B**2 / (4*pi*rho*eta*Omega)
    #      B = sqrt(4*pi*rho*eta*Omega)*B^, B^ = simulation, nondimensional
    # so
    #      El = (B^)**2
    # from the source code,
    #      E_mag^ = (B^)**2 / Pm / Ek / 2
    #          Ek = nu / Omega / L**2
    #          Pm = nu / eta
    # so
    #      El = (B^)**2 = E_mag^ * 2 * Pm * Ek
    coeff = 2.0*Prm*Ek

    # make it have the right units
    Elsasser_num = B2_tilde*coeff
    Elsasser_peak = np.amax(me,axis=0)*coeff

    print("Elsasser number, El = {}".format(np.mean(Elsasser_num)))
    print("Peak Elsasser number, El peak = {}".format(np.mean(Elsasser_peak)))

    # also get the Lorentz number
    #     El = Lo**2 * Prm / Ek
    #     Lo = sqrt(Ek*El/Prm)
    Lorentz_num = np.sqrt(Elsasser_num * Ek / Prm)
    Lorentz_peak = np.sqrt(Elsasser_peak * Ek / Prm)

    print("\nAvg Lorentz number, Lo = {}".format(np.mean(Lorentz_num)))
    print("Peak Lorentz number, Lo peak = {}".format(np.mean(Lorentz_peak)))

    Numbers['El'] = Elsasser_num
    Numbers['El_peak'] = Elsasser_peak
    Numbers['Lo'] = Lorentz_num
    Numbers['Lo_peak'] = Lorentz_peak

    print("\n...plotting")
    plt.clf()
    linthreshy = 1e100
    for key in Numbers.keys():
        if ("peak" in key and (not plot_peaks)):
            continue
        plt.plot(time, Numbers[key], label=key)
        linthreshy = min(linthreshy, 0.5*np.mean(Numbers[key]))
    plt.legend(loc='best')
    plt.xlabel('Time', fontsize=14)
    plt.ylabel('Nondimensional Numbers', fontsize=14)

    if (logy):
        plt.yscale('log')
    if (symlogy):
        print("...using symlog with linthreshy = {}".format(linthreshy))
        plt.yscale('symlog', linthreshy=linthreshy)
    if (logx):
        plt.xscale('log')

    if (saveplot):
        plt.savefig(savefile, dpi=300)
        print("saved image: {}".format(savefile))
    else:
        plt.show()
    plt.close()
    print()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    Pr = float(args['--Pr'])
    Prm = float(args['--Pm'])
    Ek = float(args['--Ek'])
    aspect_ratio = float(args['--aspect'])
    no_plots = args['--no-plots']
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]
    logx = args['--logx']
    logy = args['--logy']
    symlogy = args['--symlogy']
    plot_peaks = args['--plot-peaks']
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Pr, Prm, Ek, no_plots, aspect_ratio, quants,
         logx, logy, symlogy, plot_peaks, new_lut)

