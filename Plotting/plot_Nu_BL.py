"""
Plot the Nusselt & Boundary Layer Thickness

Usage:
    plot_Nu_BL.py [options] <data-file>

Options:
    --save           Save the image [default: False]
    --output=<o>     Save image as <o> [default: nondim_plots_NuBL]
"""

from __future__ import print_function
import numpy as np
from numpy import pi
import matplotlib.pyplot as plt
import NumericalToolsLocal.fitting.fit_RaPr as fit_RaPr

fit_args = {}
fit_args['error_method'] = 'bootstrap' # 'MC', 'Bootstrap', 'None', 'Jackknife'
#fit_args['Ntrials'] = 5000
fit_args['confidence'] = 0.68
fit_args['upper_lower_errors'] = False
fit_args['mcsigma'] = 0.15
fit_args['debug'] = False
fit_args['full_output'] = False

def main(save, output, input_file):

    # read data file for output values
    data = read_datafile(input_file, separator=None)

    # extract the data
    # Pr = 2
    Pr2_kappa = data['2.0']['kappa'][:]
    Pr2_Ra_F  = data['2.0']['Ra_F'][:]
    Pr2_Ra_BL = data['2.0']['Ra_BL'][:]
    Pr2_w_BL  = data['2.0']['w_BL'][:]
    Pr2_Nu    = data['2.0']['Nu'][:]
    Pr2_Pr    = 2.*np.ones_like(Pr2_Ra_F)

    # Pr = 4
    Pr4_kappa = data['4.0']['kappa'][:]
    Pr4_Ra_F  = data['4.0']['Ra_F'][:]
    Pr4_Ra_BL = data['4.0']['Ra_BL'][:]
    Pr4_w_BL  = data['4.0']['w_BL'][:]
    Pr4_Nu    = data['4.0']['Nu'][:]
    Pr4_Pr    = 4.*np.ones_like(Pr4_Ra_F)

    # Pr = 0.25
    Pr0_25_kappa = data['0.25']['kappa'][:]
    Pr0_25_Ra_F  = data['0.25']['Ra_F'][:]
    Pr0_25_Ra_BL = data['0.25']['Ra_BL'][:]
    Pr0_25_w_BL  = data['0.25']['w_BL'][:]
    Pr0_25_Nu    = data['0.25']['Nu'][:]
    Pr0_25_Pr    = 0.25*np.ones_like(Pr0_25_Ra_F)

    # Pr = 0.5
    Pr0_5_kappa = data['0.5']['kappa'][:]
    Pr0_5_Ra_F  = data['0.5']['Ra_F'][:]
    Pr0_5_Ra_BL = data['0.5']['Ra_BL'][:]
    Pr0_5_w_BL  = data['0.5']['w_BL'][:]
    Pr0_5_Nu    = data['0.5']['Nu'][:]
    Pr0_5_Pr    = 0.5*np.ones_like(Pr0_5_Ra_F)

    # Pr = 0.1
    Pr0_1_kappa = data['0.1']['kappa'][:]
    Pr0_1_Ra_F  = data['0.1']['Ra_F'][:]
    Pr0_1_Ra_BL = data['0.1']['Ra_BL'][:]
    Pr0_1_w_BL  = data['0.1']['w_BL'][:]
    Pr0_1_Nu    = data['0.1']['Nu'][:]
    Pr0_1_Pr    = 0.1*np.ones_like(Pr0_1_Ra_F)

    # Paper results: Table 6, Ra_F is too high by pi so divide to make it correct
    Pr1_kappa = data['1.0']['kappa'][:] #np.array([1., 2., 4., 6., 8., 12., 16., 24.])*1e12
    Pr1_Ra_F  = data['1.0']['Ra_F'][:] #np.array([2.14e7, 2.68e6, 3.35e5, 9.93e4, 4.19e4, 1.24e4, 5.23e3, 1.55e3])/pi
    Pr1_Ra_BL = data['1.0']['Ra_BL'][:] #22.064159*Pr1_Ra_F # estimate for Ra_BL = mean(Pr2_Ra_BL/Pr2_Ra_F)*Pr1_Ra_F
    Pr1_w_BL  = data['1.0']['w_BL'][:] #np.array([4.96, 6.93, 9.83, 12.18, 14.09, 16.03, 19.06, 23.77]) # in Mm
    Pr1_Nu    = data['1.0']['Nu'][:] #np.array([4.96, 6.93, 9.83, 12.18, 14.09, 16.03, 19.06, 23.77]) # in Mm
    Pr1_Pr    = 1.*np.ones_like(Pr1_Ra_F)

    # adjust Nu, leave out Pr=0.1 case
    Pr0_25_Nu_fconv = np.array([5.57, 3.367, 2.427, 2.036, 1.481, 1.186, 1., 1.])
    Pr0_5_Nu_fconv  = np.array([6.25, 4.444, 3.521, 2.518, 1.945, 1.300])
    Pr2_Nu_fconv    = np.array([11.627, 6.849, 5.102, 3.846, 2.118, 1.512])
    Pr4_Nu_fconv    = np.array([18.56, 11.764, 7.352, 4.878, 3.134, 1.798, 1.107, 1.])

    diff_Pr = np.zeros((4, 6))
    diff_Pr[0,:] = Pr0_25_Nu[:6] - Pr0_25_Nu_fconv[:6]
    diff_Pr[1,:] = Pr0_5_Nu[:6] - Pr0_5_Nu_fconv[:6]
    diff_Pr[2,:] = Pr2_Nu[:6] - Pr2_Nu_fconv[:6]
    diff_Pr[3,:] = Pr4_Nu[:6] - Pr4_Nu_fconv[:6]

    #for i in range(4):
    #    print(i)
    #    print(diff_Pr[i,:])
    #print()
    #print(np.median(diff_Pr,axis=0))
    Pr1_Nu[:6] += np.median(diff_Pr, axis=0)
    Pr1_Nu[6:] += 0.01

    # generic Ra number array, smallest is at the end, biggest is at the front
    min_Ra = np.amin(np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F)))
    max_Ra = np.amax(np.concatenate((Pr2_Ra_F, Pr0_25_Ra_F, Pr0_5_Ra_F, Pr1_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F)))
    Ra_F = np.logspace(np.log10(min_Ra), np.log10(max_Ra), 16)

    # generic nu/kappa arrays, smallest is at the front
    min_k = np.amin(np.concatenate((Pr2_kappa, Pr0_25_kappa, Pr0_5_kappa, Pr1_kappa, Pr4_kappa, Pr0_1_kappa)))
    max_k = np.amax(np.concatenate((Pr2_kappa, Pr0_25_kappa, Pr0_5_kappa, Pr1_kappa, Pr4_kappa, Pr0_1_kappa)))
    kappa = np.logspace(np.log10(min_k), np.log10(max_k), 16)

    # cutoff for low vs. high Rayleigh number
    Ra_F_crit = 2.e4

    #######################################################
    # Nusselt / Boundary Layer
    #######################################################
    plt.figure(1, figsize=(12,4), dpi=300)
    if (save):
        plt.rcParams.update({'font.size':12})
    else:
        plt.rcParams.update({'font.size':14})

    xfont=14; yfont=14
    plt.subplot(121)

    #_Nu = np.concatenate((Pr0_25_Nu, Pr0_5_Nu,
    #                              Pr1_Nu, Pr2_Nu, Pr4_Nu, Pr0_1_Nu))
    #_Pr = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
    #                              Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    #_RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
    #                              Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    #uRaF, ind = np.unique(_RaF, return_index=True)

    # dont fit the Pr=1 data, since it has kinda made-up Nu numbers
    _Nu = np.concatenate((Pr0_25_Nu, Pr0_5_Nu, Pr2_Nu, Pr4_Nu, Pr0_1_Nu))
    _Pr = np.concatenate((Pr0_25_Pr, Pr0_5_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    _RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    uRaF, ind = np.unique(_RaF, return_index=True)

    a0 = (1.7, -0.28, -0.28)
    if (True):
        ind = np.where(_RaF*_Pr > Ra_F_crit)
        _RaF   = _RaF[ind]
        _Pr    = _Pr[ind]
        _Nu    = _Nu[ind]
    Ntrials = len(_Nu)
    _params, _errors = fit_RaPr.fitting_RaPr(_RaF, _Nu, _Pr, a0, seed=255,
                                             Ntrials=Ntrials, **fit_args)
    print("\nFitting Nu vs. Ra Pr data:")
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(\
               _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(\
               _params[2], _errors[2], 100*_errors[2]/abs(_params[2])))
    a = _params[0]; b = _params[1]; c = _params[2]
    #plt.axvline(x=Ra_F_crit*1.0, color='k', linestyle='-.')
    _RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    uRaF, ind = np.unique(_RaF, return_index=True)
    plt.plot(uRaF, a*uRaF**b*1.0**c, color='k', linestyle='--', label=None)
    plt.plot(Pr0_1_Ra_F*0.1, Pr0_1_Nu, marker='v', label='Pr = 0.1',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F*0.25, Pr0_25_Nu, marker='+', label='Pr = 0.25',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F*0.5, Pr0_5_Nu, marker='D', label='Pr = 0.5',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F*1, Pr1_Nu, marker='^', label='Pr = 1',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F*2, Pr2_Nu, marker='x', label='Pr = 2',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F*4, Pr4_Nu, marker='o', label='Pr = 4',
             color='r', fillstyle='none', linestyle='')
    #plt.plot(_Pr*_RaF,_Nu,marker='o',fillstyle='none',label='Fitted',linestyle='',color='k')
    plt.text(0.85, 0.9, r"$\mathbf{(a)}$",transform=plt.gca().transAxes,fontsize=16)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    #plt.legend(loc='lower left', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(1e2, 1e7)
    plt.ylim(0.8,40)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}$", fontsize=xfont)
    plt.ylabel(r"$\mathrm{Nu}$", fontsize=yfont)

    wBL   = np.concatenate((Pr0_25_w_BL, Pr0_5_w_BL,
                                  Pr1_w_BL, Pr2_w_BL, Pr4_w_BL, Pr0_1_w_BL))
    _Pr   = np.concatenate((Pr0_25_Pr, Pr0_5_Pr,
                                  Pr1_Pr, Pr2_Pr, Pr4_Pr, Pr0_1_Pr))
    _RaF = np.concatenate((Pr0_25_Ra_F, Pr0_5_Ra_F,
                                  Pr1_Ra_F, Pr2_Ra_F, Pr4_Ra_F, Pr0_1_Ra_F))
    uRaF, ind = np.unique(_RaF, return_index=True)
    a0 = (4.2, -0.16, -0.16)
    if (True):
        ind = np.where(_RaF*_Pr > 0.5*Ra_F_crit)
        _RaF = _RaF[ind]
        wBL  = wBL[ind]
        _Pr  = _Pr[ind]
    Ntrials = len(wBL)
    _params, _errors = fit_RaPr.fitting_RaPr(_RaF, wBL, _Pr, a0, seed=63,
                                             Ntrials=Ntrials, **fit_args)
    print("\nFitting w_BL vs. RaPr data:")
    print("\tRa power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(\
               _params[1], _errors[1], 100*_errors[1]/abs(_params[1])))
    print("\tPr power = {:.3e} +/- {:.3e}  ({:.1f} % error)".format(\
               _params[2], _errors[2], 100*_errors[2]/abs(_params[2])))

    a = _params[0]; b = _params[1]; c = _params[2]
    plt.subplot(122)

    plt.plot(uRaF,  a*uRaF**b*1.0**c, color='k', linestyle='--', label=None)
    H = 1. #H = 158.6 # Mm
    plt.plot(Pr0_1_Ra_F*0.1, Pr0_1_w_BL/H, marker='v', label='Pr = 0.1',
             color='k', fillstyle='none', linestyle='')
    plt.plot(Pr0_25_Ra_F*0.25, Pr0_25_w_BL/H, marker='+', label='Pr = 0.25',
             color='m', fillstyle='none', linestyle='')
    plt.plot(Pr0_5_Ra_F*0.5, Pr0_5_w_BL/H, marker='D', label='Pr = 0.5',
             color='c', fillstyle='none', linestyle='')
    plt.plot(Pr1_Ra_F*1, Pr1_w_BL/H, marker='^', label='Pr = 1',
             color='g', fillstyle='none', linestyle='')
    plt.plot(Pr2_Ra_F*2, Pr2_w_BL/H, marker='x', label='Pr = 2',
             color='b', fillstyle='none', linestyle='')
    plt.plot(Pr4_Ra_F*4, Pr4_w_BL/H, marker='o', label='Pr = 4',
             color='r', fillstyle='none', linestyle='')
    #plt.plot(_Pr*_RaF,wBL,marker='o',fillstyle='none',label='Fitted',linestyle='',color='k')
    plt.text(0.85, 0.9, r"$\mathbf{(b)}$",transform=plt.gca().transAxes,fontsize=16)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.legend(loc='lower left', frameon=True, fontsize='small', numpoints=1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(1e2, 1e7)
    plt.ylim(4, 30)
    plt.xlabel(r"$\mathrm{Ra}_\mathrm{F}\mathrm{Pr}$", fontsize=xfont)
    plt.ylabel(r"Boundary Layer Width (Mm)", fontsize=yfont)

    plt.tight_layout()

    if (save):
        plt.savefig(output+"_Nu_wBL.eps")
        print("saved image: {}".format(output+"_Nu_wBL.eps"))
    else:
        plt.show()

    plt.close()

    return

def read_datafile(filename, separator=None):
    """
    parse data file to extract calculated quantities
    """
    from collections import OrderedDict

    # allocate space
    data_values = OrderedDict()

    with open(filename, "r") as f:

        # only parse non-empty lines and lines that don't start with '#'
        for line in f:
            if (not(line.lstrip().startswith("#") or line.lstrip() == "")):
                line.replace("\n", "") # remove trailing whitespace
                line.replace("\r", "")

                fields = line.split(separator) # separate the fields

                # remove all whitespace from each field
                for field in fields:
                    field.replace(" ", "")
                    field.replace("\t", "")

                # extract specific values
                Pr     = fields[0]
                _kappa = float(fields[1])
                _Ra_F  = float(fields[2])
                _Ra_BL = float(fields[3])
                _w_BL  = float(fields[4])
                _w_BL2 = float(fields[5])
                _Nu    = float(fields[6])

                # this is a new entry so initialize the data structure
                if (Pr not in data_values.keys()):
                    data_values[Pr] = OrderedDict({'kappa':[],
                                                   'Ra_F':[], 'Ra_BL':[],
                                                   'w_BL':[], 'w_BL2':[],
                                                   'Nu':[]})

                # add the data
                data_values[Pr]['kappa'].append(_kappa)
                data_values[Pr]['Ra_F'].append( _Ra_F)
                data_values[Pr]['Ra_BL'].append(_Ra_BL)
                data_values[Pr]['w_BL'].append( _w_BL)
                data_values[Pr]['w_BL2'].append(_w_BL2)
                data_values[Pr]['Nu'].append(   _Nu)

    # convert the lists into np arrays
    for pr in data_values.keys():
        for field in data_values[pr].keys():
            data_values[pr][field] = np.array(data_values[pr][field])

    return data_values

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(args['--save'], args['--output'], args['<data-file>'])

