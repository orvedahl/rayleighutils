"""
plot time trace to see how far simulation has advanced

Usage:
     plot_simulation_output.py [options] <datafile>

Options:
     --output=<o>   Set filename for saving [default: convergence.png]
     --save         Save the image [default: False]
     --dt           Plot timestep [default: False]
     --logx         Plot x-axis on log scale [default: False]
     --logy         Plot y-axis on log scale [default: False]
     --log          Plot both axes on log scale [default: False]
     --no-plots     Print last time, don't plot [default: False]
     --Pm=<pm>      Magnetic Prandtl, multiple time by <pm> [default: 1]

"""

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np

def main(datafile, plot_dt, output, save, logx, logy, no_plots, Prm):

    data  = read_data(datafile)
    itr   = data[:,0]
    time = data[:,1]*Prm # convert from viscous to magnetic time
    dt = data[:,2]

    if (no_plots):
        print("\nRead from file = {}".format(datafile))
        print("\nLast iteration = {:.0f}".format(itr[-1]))
        print("\nLast timestep = {:.3e}".format(dt[-1]))
        print("\nLast time = {:.5f}".format(time[-1]))
        print()
        return

    # plot
    plt.plot(itr, time, linestyle='-', marker='', color='r', label='time')
    plt.legend(loc='upper left')
    plt.xlabel('Iteration')
    plt.ylabel('Simulation Time')
    if (logx):
        plt.xscale('log')
    if (logy):
        plt.yscale('log')
    if (plot_dt):
        plt.twinx()
        plt.plot(itr, dt, linestyle='-', marker='', color='b', label='dt')
        plt.ylabel('Timestep')
        plt.legend(loc='upper right')
        if (logy):
            plt.yscale('log')
    if (save):
        plt.savefig(output)
        print("\nsaved: {}\n".format(output))
    else:
        plt.show()

def read_data(datafile):
    data = []; count = 0; time = 0.
    with open(datafile, "r") as f:
        for line in f:
            if ("on iteration" in line.lower()):
                fields = line.split()

                itr = float(fields[3])
                dt = float(fields[6])
                if (count == 0):
                    time = dt
                else:
                    time = time + dt

                data.append([itr, time, dt])
                count += 1

    return np.array(data)

if __name__=="__main__":
    from docopt import docopt
    args = docopt(__doc__)

    datafile = args['<datafile>']
    save = args['--save']
    output = args['--output']
    logx = args['--logx']
    logy = args['--logy']
    log = args['--log']
    plot_dt = args['--dt']
    no_plots = args['--no-plots']
    Prm = float(args['--Pm'])

    if (log):
        logx = True; logy = True

    main(datafile, plot_dt, output, save, logx, logy, no_plots, Prm)

