"""
  Shell-Slice (Shell_Slices) plotting example
  - Reads in a single Shell_Slice file.
  - Plots vr, vphi, and entropy

  This example routine makes use of the ShellSlice
  data structure associated with the Shell_Slices output.
  Upon initializing a ShellSlice object, the 
  object will contain the following attributes:
    ----------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of shell slices output
    self.ntheta                                   : number of theta points
    self.nphi                                     : number of phi points
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radii of the shell slices output
    self.inds[0:nr-1]                             : radial indices of the shell slices output
    self.costheta[0:ntheta-1]                     : cos(theta grid)
    self.sintheta[0:ntheta-1]                     : sin(theta grid)
    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
                                                  : The shell slices 
    self.iters[0:niter-1]                         : The time step numbers stored in this output file
    self.time[0:niter-1]                          : The simulation time corresponding to each time step
    self.version                                  : The version code for this particular output (internal use)
    self.lut                                      : Lookup table for the different diagnostics output
    -------------------------------------

Usage:
    plot_shell_slice.py [options]

Options:
    --file=<i>        File to read, zero padding will happen if needed [default: 100000]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: shell_slice.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-plots        Suppress the plots [default: False]
    --radial-ind=<r>  Comma separated list of radial indices to plot [default: 0,3,6]
    --magnetic        Plot magnetic field [default: False]
"""

from __future__ import print_function
import numpy as np
import env
from ReadRayleigh.diagnostic_reading import ShellSlice
import matplotlib.pyplot as plt
from matplotlib import ticker
from Utilities.look_up_quantity import shortcut_lookup

def main(file_iteration, saveplot, savefile, data_dir, no_plots, magnetic, rad_inds, **kwargs):
    # Set saveplot to True to save to a .png file. 
    # Set to False to view plots interactively on-screen.
    #saveplot = True
    #savefile = 'shell_slice.png'

    #Read in our shell slice
    file_iteration = '{:08d}'.format(file_iteration)
    a = ShellSlice(filename=file_iteration,path=data_dir+'/Shell_Slices/')
    print("\nRead from file = {}".format(data_dir+'Shell_Slices/'+file_iteration))

    #Identify the variables indices for vr,vphi, and entropy
    #var_inds = [a.lut[1],a.lut[3],a.lut[64]]
    var_inds = [a.lut[shortcut_lookup('vr')],
                a.lut[shortcut_lookup('vp')],
                a.lut[shortcut_lookup('s')]]

    #rad_inds = [0,3,6]  # pick 3 depth to plot at
    tind = 0 # grab time index 0 (the first record of the file)

    #Tex can be enclosed in dollar signs within a string.  The r in front of the string is necessary for strings enclosing Tex
    units = [r'm s$^{-1}$', r'm s$^{-1}$', r'erg g$^{-1}$ K$^{-1}$']  
    vnames = [r'v$_r$', r'v$_\phi$', "S'"]
    ncol = len(var_inds)
    nrow = len(rad_inds)
    nplots = ncol*nrow

    print("\ttime = {} ({:.2e})".format(a.time[tind], a.time[tind]))

    if (no_plots):
        return

    ######################################
    # shell slice plots
    ######################################
    #Create the plots.  This first portion only handles the projection and colorbars.
    #Labeling comes further down.
    ind = 1
    f1 = plt.figure(figsize=(5.5*3, 5*3), dpi=80)
    for  j in range(nrow):
        for i in range(ncol):
            sslice = a.vals[:,:,rad_inds[j],var_inds[i],tind].reshape(a.nphi,a.ntheta)
            if (i == 2):
                sslice = sslice-np.mean(sslice) #subtract ell=0 from entropy
            else:
                sslice = sslice/100.0 # convert the velocity field to m/s
            sslice = np.transpose(sslice)
        
            # Set the projection
            ax1 = f1.add_subplot(nrow,ncol,ind, projection="mollweide")
        
            ind = ind+1

            twosigma = 2*np.std(sslice)  #limits are set to +/- twosigma

            contour_levels = twosigma*np.linspace(-1,1,256)
            image1 = ax1.imshow(sslice,vmin=-twosigma, vmax=twosigma,
                     extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
                     aspect=0.5, interpolation='bicubic')
            image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
            image1.axes.get_yaxis().set_visible(False)
            image1.set_cmap('RdBu_r')  # Red/Blue map
            #image1.set_cmap('RdYlBu_r')  # Red/Blue map

            cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
            cbar.set_label(units[i])

            tick_locator = ticker.MaxNLocator(nbins=5)
            cbar.locator = tick_locator
            cbar.update_ticks()

    # Next, set various parameters that control the plot layout
    # These parameters can also be set interactively when using plt.show()
    pbottom = 0.05
    pright = 0.95
    pleft = 0.15
    ptop = 0.95
    phspace = 0.1
    pwspace = 0.03

    plt.subplots_adjust(left = pleft, bottom = pbottom, right = pright, top = ptop, wspace=pwspace,hspace=phspace)

    # Add information about radial depth to the left margin
    rspace = (ptop-pbottom)/nrow
    rnorm = 6.96e10
    lilbit = 0.05
    xpos = 0.1*pleft
    for i in range(nrow):
        ypos = ptop-rspace*0.4-i*(rspace+phspace/(nrow-1))
        ratio = float(a.radius[rad_inds[i]]/rnorm)
        r_str = r'r/R$_\odot$ = %1.3f' % ratio
        f1.text(xpos,ypos,r_str,fontsize=16)
        ypos = ypos-lilbit
        ind_str = 'radial index: '+str(a.inds[rad_inds[i]])
        f1.text(xpos,ypos,ind_str,fontsize=16)

    #Label the plots (entropy, v_r etc.)
    cspace = (pright-pleft)/ncol
    for i in range(ncol):
        ypos = ptop
        xpos = pleft+cspace*0.47+i*cspace 
        f1.text(xpos,ypos,vnames[i],fontsize=20)

    #Save or display the figure
    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    file_iteration = int(args['--file'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    magnetic = args['--magnetic']

    indices = (args['--radial-ind']).split(",")
    radial_ind = []
    for i in indices:
        radial_ind.append(int(i))

    main(file_iteration, saveplot, savefile, data_dir, no_plots, magnetic, radial_ind)

