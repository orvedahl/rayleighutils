"""
Plot time traces at a specific point of the averaged induction terms

Usage:
    plot_induction_terms_points.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: boussinesq_timeseries.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --list-inds     List the possible location indices [default: False]
    --i-theta=<t>   Specify theta index [default: 0]
    --i-radius=<r>  Specify radial index [default: 0]
    --Pm=<p>        Magnetic Prandtl number, Pr=nu/eta [default: 1]
    --logx          Log scale the x-axis [default: False]
    --logy          Log scale the y-axis [default: False]
    --symlogy       Sym-Log scale the y-axis [default: False]
    --skip=<s>      Plot every <s> data point [default: 1]
    --lut=<l>       Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import Point_Probe, build_file_list, AzAverage
import matplotlib.pyplot as plt
import numpy as np
import os
from Utilities.look_up_quantity import lut_lookup, print_lut
from Utilities import time_utils
import NumericalToolsLocal.public as NT

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, skip,
         i_th, i_r, list_inds, Prm, no_plots, logx, logy, symlogy, new_lut, **kwargs):

    # read the Point Probes
    quants = [1628, 1626, 1630] # phi components of: -Ubar.gradBbar, Bbar.gradUbar, LapBbar
    path = os.path.join(data_dir, "Point_Probes")
    files = build_file_list(start_file, end_file, path=path)[-last_number_files:]
    P = Point_Probe(files[0], path='', quantity=quants)
    # data = [phi, th, r, nq, time]
    Pdata, Ptime = time_utils.TimeSeries(files, Point_Probe, quantity=quants, axis=-1)
    Ptime *= Prm
    t_start = Ptime[0]; t_end = Ptime[-1]

    print("First file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(t_start))
    print("Last  time: {} sec\n".format(t_end))

    if (list_inds):
        print("\nPossible Indices:")
        print("\tRadius\t\tArray Index\ti_r")
        for i,ir in enumerate(P.rad_inds):
            print("\t{}\t{}\t\t{}".format(P.radius[i], ir, i))
        print("\n\tcos(theta)\tArray Index\ti_th")
        for i,ith in enumerate(P.theta_inds):
            print("\t{:.8f}\t{}\t\t{}".format(P.costheta[i], ith, i))
        return

    # read the Azimuthal Averages
    quants = [803, 1669] # Bbar_phi, curl(EMF)_phi
    path = os.path.join(data_dir, "AZ_Avgs")
    files = build_file_list(start_file, end_file, path=path)[-last_number_files:]
    # data = [th, r, nq, time]
    A = AzAverage(files[0], path='', quantity=quants)
    Adata, Atime = time_utils.TimeSeries(files, AzAverage, quantity=quants, axis=-1)
    Atime *= Prm
    t_start = Atime[0]; t_end = Atime[-1]

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} sec".format(t_start))
    print("Last  time: {} sec".format(t_end))

    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    radius = A.radius
    fpr = 4.0*np.pi*radius*radius # four pi r^2
    rnorm = radius/radius[0]      # normalize radius so that upper boundary is r=1

    print("\n-----------------------------------------------")

    # pick a location
    i_phi = 0
    print("\nUsing location:\n")
    print("\t    radius = {:.6f}".format(P.radius[i_r]))
    print("\tcos(theta) = {:.6f} deg".format(P.costheta[i_th]*180/np.pi))
    print("\t       phi = {:.6f} deg".format(P.phi[i_phi]*180/np.pi))

    # Pdata = [phi, th, r, nq, time]
    phi_advection = Pdata[i_phi, i_th, i_r, P.lut[1628], :] # -<u>.grad(<B>)
    phi_shearing  = Pdata[i_phi, i_th, i_r, P.lut[1626], :] #  <B>.grad(<u>)
    phi_diffusion = Pdata[i_phi, i_th, i_r, P.lut[1630], :] # 1/Pm*Lap(<B>)
    advect_label  = r"$-[u\cdot\nabla B]_\phi$"
    shear_label   = r"$[B\cdot\nabla u]_\phi$"
    diff_label    = r"$\frac{1}{\mathrm{Pm}}[\nabla^2 B]_\phi$"

    # Adata = [th, r, nq, time]
    Bphi    = Adata[i_th, i_r, A.lut[803 ], :] # Bphi
    CurlEMF = Adata[i_th, i_r, A.lut[1669], :] # Curl(Emf)
    curlEMF_label = r"$[\nabla \times \mathcal{E}]_\phi$"

    # calculate time derivative
    Bphi_dot = NT.derivative(Bphi, Atime)
    Bdot_label = r"$\partial_t B_\phi$"

    print("\n...plotting")
    print("...using skip = {}".format(skip))
    plt.clf()
    plt.plot(Atime[::skip], Bphi_dot[::skip], label=Bdot_label, alpha=1.0)
    plt.plot(Ptime[::skip], phi_diffusion[::skip], label=diff_label, alpha=0.7)
    plt.plot(Ptime[::skip], phi_advection[::skip], label=advect_label, alpha=0.4)
    plt.plot(Atime[::skip], CurlEMF[::skip], label=curlEMF_label, alpha=0.7)
    plt.plot(Ptime[::skip], phi_shearing[::skip],  label=shear_label, alpha=0.6)
    plt.legend(loc='best')

    plt.xlabel('Magnetic Diffusion Time', fontsize=14)
    plt.ylabel('Mean Induction Terms', fontsize=14)

    if (logy):
        plt.yscale('log')
    if (symlogy):
        linthreshy = 1e-2
        print("...using symlog with linthreshy = {}".format(linthreshy))
        plt.yscale('symlog', linthreshy=linthreshy)
    if (logx):
        plt.xscale('log')

    if (saveplot):
        plt.savefig(savefile, dpi=300)
        print("saved image: {}".format(savefile))
    else:
        plt.show()
    plt.close()
    print()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    Prm = float(args['--Pm'])
    no_plots = args['--no-plots']
    i_th = int(args['--i-theta'])
    i_r  = int(args['--i-radius'])
    list_inds = args['--list-inds']
    logx = args['--logx']
    logy = args['--logy']
    symlogy = args['--symlogy']
    skip = int(args['--skip'])
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir, skip,
         i_th, i_r, list_inds, Prm, no_plots, logx, logy, symlogy, new_lut)

