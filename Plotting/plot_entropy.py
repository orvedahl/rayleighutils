"""

Usage:
    plot_entropy.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: energy_flux.png]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --alt           Run using old data format [default: False]

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list, ReferenceState
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from NumericalToolsLocal.calculus.integrate import volume_avg 
import os
from Utilities.look_up_quantity import shortcut_lookup, general_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, alt=False, **kwargs):

    # integration method
    method='cheb-end'

    #Time-average the data generated between timesteps 0 million and 10 million
    # Note that this averaging scheme assumes taht the radial grid didn't
    # change during the course of the run. 
    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
    icount = 0.0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellAverage(f,path='')
        if (i == 0):
            if (not alt):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
            else:
                data = np.zeros((a.nr,a.nq),dtype='float64')
            t_start = a.time[0]
        for j in range(a.niter):
            if (not alt):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
            else:
                data[:,:] = data[:,:]+a.vals[:,:,j]
            icount = icount+1.0
        t_end = a.time[-1]

    delta_time = (t_end - t_start)/3600./24.
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} days".format(t_start/3600./24.))
    print("Last  time: {} days".format(t_end/3600./24.))
    print("\nAveraged over {} days".format(delta_time))

    data = data/icount  # The average is now stored in data
    radius = a.radius
    fpr = 4.0*np.pi*radius*radius  # four pi r^2
    rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1

    # calculate the boundary layer thickness,
    # see Eqn 27 of Featherstone & Hindman 2016 ApJ 818
    s_index = a.lut[shortcut_lookup('s',alt=alt)]  #a.lut[64]
    if (not alt):
        entropy = data[:,0,s_index]
    else:
        entropy = data[:,s_index]
    max_S = np.amax(entropy)
    w_BL = my_integrator(max_S - entropy, radius, method=method, data_reversed=True)/max_S
    w_BL *= 1e-8 # convert from cm to Mm
    print("\nBoundary Layer thickness, w_BL = {} Mm".format(w_BL))
    delta_S = max_S - np.amin(entropy)
    print("\nDelta S = {} erg/g/K".format(delta_S))

    # entropy gradient
    dsdr_index = a.lut[shortcut_lookup('dsdr',alt=alt)]  #a.lut[70]
    if (not alt):
        entropy_dr = data[:,0,dsdr_index]
    else:
        entropy_dr = data[:,dsdr_index]

    if (no_plots):
        return

    #Create the plot

    lw = 1.5

    if (saveplot):
        plt.figure(1,figsize=(7.0, 5.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(7,5),dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.plot(rnorm, entropy, label=r'$\bar{S}$', linewidth=lw, color='r')
    plt.legend(loc='upper left', frameon=False)
    plt.ylabel(r'Mean Entropy (ergs g$^{-1}$ K$^{-1}$)')
    plt.twinx()
    plt.plot(rnorm, entropy_dr, label=r'$\partial_r\bar{S}$', linewidth=lw, color='b')
    plt.legend(loc='upper right', frameon=False)
    plt.ylabel(r'Mean Entropy Gradient')

    plt.xlabel('Radius '+r'(r/r$_{o}$)')
    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    alt = args['--alt']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, alt=args['--alt'])

