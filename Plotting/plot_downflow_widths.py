"""
Plot downflow widths from Shell Slices

Usage:
    plot_downflow_widths.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o> [default: distribution]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-plots        Suppress the plots [default: False]
    --list            List the possible radial indices [default: False]
    --rad-ind=<r>     What radial index to plot [default: 0]
    --alt             Use alternate lookup table [default: False]
"""

from __future__ import print_function
import numpy as np
import env
from ReadRayleigh.diagnostic_reading import ShellSlice, build_file_list
import matplotlib.pyplot as plt
from Utilities.look_up_quantity import shortcut_lookup

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, r_index, list, alt, **kwargs):

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Slices')
    if (list):
        a = ShellSlice(filename=files[-1], path='')
        print("\nReading file: {}".format(files[-1]))
        print("\nPossible radial indices:")
        print("    rad-ind\tradial index")
        for i,ind in enumerate(a.inds):
            print("       {}\t   {}".format(i, ind))
        print()
        return
    print("\n...collecting data...")
    Nfiles = len(files[-last_number_files:])
    N_entries = 0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellSlice(filename=f, path='')
        if (i==0):
            # this approach assumes all files have the same number of time entries, a.niter
            # which tends to break when alt=True, in that case I do: files[-last-1:-1]
            data = np.zeros((a.nphi, a.ntheta, a.nr, a.nq, a.niter, Nfiles))
            t_start = a.time[0]
        data[:,:,:,:,:,i] = a.vals[:,:,:,:,:]
        N_entries += a.niter
        t_end = a.time[-1]

    delta_time = (t_end - t_start)/3600./24.
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(Nfiles))
    print("\nFirst time: {} days".format(t_start/3600./24.))
    print("Last  time: {} days".format(t_end/3600./24.))
    print("\nAveraged over {} days".format(delta_time))

    # grid data
    nth = a.ntheta; nphi = a.nphi
    sintheta = a.sintheta
    costheta = a.costheta
    theta = np.arccos(costheta)
    phi = np.linspace(-np.pi, np.pi, nth, endpoint=False) # this may be wrong?
    maxphi   = np.amax(phi);   minphi   = np.amin(phi)
    maxtheta = np.amax(theta); mintheta = np.amin(theta)
    dphi   = (maxphi - minphi)/nphi
    dtheta = (maxtheta - mintheta)/nth

    # get variable indices
    vr_index = a.lut[shortcut_lookup('vr', alt=alt)]  #a.lut[1]
    radius = a.radius[r_index]

    # each element in histogram array a_ij only contributes weight_ij, not 1 to bin count
    total_area = 4.*np.pi
    _weights = np.zeros((nphi,nth))
    for i in range(nth):
        area = sintheta[i]*dphi*dtheta
        _weights[:,i] = area/total_area/N_entries

    # specify number of lines through longitude to draw, get the corresponding indices
    ncuts = 300
    cut_phi = True
    verbose = False

    # extract data
    print("\n...finding widths...")
    if (cut_phi):
        print("\tusing {} cuts (nphi = {})".format(ncuts, nphi))
    else:
        print("\tusing {} cuts (ntheta = {})".format(ncuts, nth))
    widths = []
    Ntime = np.shape(data)[4]
    for j in range(Nfiles):
        for i in range(1): #Ntime):
            radial_vel = data[:,:,r_index,vr_index,i,j]

            w = get_widths(radial_vel, theta, phi, radius, ncuts,
                           cut_phi=cut_phi, verbose=verbose)

            widths = widths + w

    #  when using np.hist:
    #    --set weight = area_i / area_total and flatten data array
    #  when summing up all the histogram values, it should be 1
    width_bins = 100 # number of bins to use for histogram
    wrange = [np.amin(widths),      np.amax(widths)]     # range of histogram x-axis

    print("\nResults:")
    avg = np.mean(widths); med = np.median(widths)
    print("\taverage width = {:.4e} cm ({:.2f} Mm)".format(avg, avg/1e8))
    print("\tmedian width  = {:.4e} cm ({:.2f} Mm)".format(med, med/1e8))
    if (cut_phi):
        circle = nth*dtheta*radius
    else:
        circle = nphi*dphi*radius # half the equatorial circumference
    print("\n\tLargest possible length = {:.4e} cm ({:.2f} Mm)".format(circle, circle/1e8))
    print("\tFraction of largest using median = {:.5f} ({:.3f} %)".format(med/circle, med/circle*100))

    print()

    if (no_plots): # return the median estimate in cm
        return med

    # make plots
    print()
    plt.clf()
    weights = np.ones_like(widths)
    hist, bins = np.histogram(widths, bins=width_bins, range=wrange, weights=weights)
    cntr = 0.5*(bins[:-1] + bins[1:])
    plt.plot(cntr, hist, linestyle='-', label='distribution', color='b')
    print("\tsum of hist widths: {}".format(np.sum(hist)))
    plt.axvline(x=avg, color='r', linestyle='--', label='average')
    plt.axvline(x=med, color='g', linestyle='--', label='median')

    plt.xscale('log')
    plt.yscale('log')

    plt.title(r"Downflow Width Distribution Function")
    plt.xlabel(r"Binned Downflow Width (cm)")
    plt.ylabel("Distribution")
    plt.legend(loc='upper right')
    if (saveplot):
        output = savefile + "_widths.png"
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

def get_widths(data, theta, phi, radius, ncuts, cut_phi=True, verbose=False):
    """
    --data is of form data(phi, theta)
    --theta is grid of theta values
    --phi is grid of phi values
    --radius is single number
    --ncuts is the number of cuts to take
    --cut_phi determines if cuts are in theta or phi

    returns a list of widths
    """
    # get the angular width of a single grid point
    nth = len(theta); nphi = len(phi)

    maxphi = np.amax(phi); minphi = np.amin(phi)
    dphi   = (maxphi - minphi)/nphi * np.ones_like(phi) # uniform grid

    dtheta = np.zeros_like(theta) # non-uniform grid
    dtheta[0]  = 0.5*(theta[0] + theta[1])
    dtheta[-1] = np.pi - 0.5*(theta[-1] + theta[-2])
    for i in range(1, nth-1):
        dtheta[i] = 0.5*(theta[i+1] - theta[i-1])

    # choose cuts at various longitude values
    if (cut_phi):
        cut_inds = np.arange(0, nphi, int(1.*nphi/ncuts))
        widths = []
        for p in cut_inds:
            cut = data[p, :]
            inds = []; downflow = []
            for t in range(nth):
                if (cut[t] < 0.): # this is a downflow
                    downflow.append(t)
                elif (len(downflow) > 0):
                    inds.append(downflow) # store indices of downflow
                    downflow = []         # reset downflow counter
            if (verbose):
                print("\tfound {} downflow lanes at phi = {}".format(len(inds), phi[p]))
            for I in inds:
                if (len(I) == 1):
                    # downflow is single grid point, so width is grid width
                    w = [dtheta[I[0]]*radius]
                else:
                    w = [abs(theta[I[-1]] - theta[I[0]])*radius]
                widths = widths + w # add the lists together

    # choose cuts at various latitude values
    else:
        cut_inds = np.arange(0, nth, int(1.*nth/ncuts))

        widths = []
        for t in cut_inds:
            th = theta[t]
            cut = data[:, t]
            inds = []; downflow = []
            for p in range(nphi):
                if (cut[p] < 0.): # this is a downflow
                    downflow.append(p)
                elif (len(downflow) > 0):
                    inds.append(downflow) # store indices of downflow
                    downflow = []         # reset downflow counter
            if (verbose):
                print("\tfound {} downflow lanes at theta = {}".format(len(inds), th))
            for I in inds:
                if (len(I) == 1):
                    # downflow is single grid point, so width is grid width
                    w = [dphi[I[0]]*radius*np.sin(th)]
                else:
                    w = [abs(phi[I[-1]] - phi[I[0]])*radius*np.sin(th)]
                widths = widths + w

    return widths

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    list = args['--list']
    alt = args['--alt']
    radial_ind = int(args['--rad-ind'])

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, radial_ind, list, alt)
