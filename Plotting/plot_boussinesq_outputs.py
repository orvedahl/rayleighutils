"""
Calculate various things surrounding the non-dimensional Boussinesq runs

Usage:
    plot_energy_flux.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save          Save an image [default: False]
    --output=<o>    Save images as <o> [default: boussinesq_]
    --data-dir=<d>  Specify location of data [default: ./]
    --no-plots      Suppress the plots [default: False]
    --Pr=<p>        Prandtl number, Pr=nu/kappa [default: 1]
    --Pm=<p>        Magnetic Prandtl number, Pr=nu/eta [default: 1]
    --Ek=<e>        Ekman number, Ek=nu/Omega/L**2 [default: 1]
    --reynolds      Calculate the Reynolds number [default: False]
    --nusselt       Calculate the Nusselt number [default: False]
    --elsasser      Calculate the Elsasser number [default: False]
    --aspect=<a>    Set aspect ratio [default: 0.4]
    --nusselt2      Use "F_conv/F_cond" method [default: False]
    --alt           Use alternate look up table [default: False]
    --quants=<q>    Comma separated list of quantities to use
    --lut=<l>       Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellAverage, build_file_list, ReferenceState
import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from NumericalToolsLocal.calculus.integrate import volume_avg
from NumericalToolsLocal.calculus.differentiate import derivative as my_deriv
import os,sys
from Utilities.look_up_quantity import shortcut_lookup, general_lookup
from Utilities import calculate_Nu
from Utilities import time_utils

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Pr, Prm, Ek, reynolds, nusselt, elsasser, no_plots, aspect_ratio, quants,
         new_lut, alt=False, nusselt2=False, **kwargs):

    if (alt):
        # the beginning code is there, but it isn't implemented everywhere...
        print("\nERROR: code not configured to use alternate look up table\n")
        sys.exit()

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    # integration method
    method='cheb-end'

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    path = os.path.join(data_dir, "Shell_Avgs")
    files = build_file_list(start_file, end_file, path=path)
#    icount = 0.0
#    for i,f in enumerate(files[-last_number_files:]):
#        a = ShellAverage(f,path='')
#        if (i == 0):
#            data = np.zeros((a.nr,4,a.nq),dtype='float64')
#            t_start = a.time[0]
#        for j in range(a.niter):
#            data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
#            icount = icount+1.0
#        t_end = a.time[-1]

    if (quants is None):
        quant_list = None
    else:
        quant_list = list(quants)

    a = ShellAverage(files[-last_number_files:][0], path='', quantity=quant_list)
    data, times = time_utils.TimeAvg(files[-last_number_files:], ShellAverage,
                                    quantity=quant_list)
    t_start = Prm*times['tstart']; t_end = Prm*times['tend']

    delta_time = Prm*times['dt']
    print("\nFirst file: {}".format(files[-last_number_files:][0]))
    print("Last  file: {}".format(files[-last_number_files:][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
    print("\nFirst time: {} sec".format(t_start))
    print("Last  time: {} sec".format(t_end))
    print("\nAveraged over {} sec".format(delta_time))

#    data = data/icount  # The average is now stored in data
    radius = a.radius
    fpr = 4.0*np.pi*radius*radius  # four pi r^2
    rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1

    # read reference state
    ref = ReferenceState(filename="reference", path=data_dir)

    print("\n-----------------------------------------------")

    if (nusselt or nusselt2):
#        # extract various fluxes
#        cf_index  = a.lut[shortcut_lookup('cond_flux',alt=alt)]     #122 Conductive energy flux
#        vhf_index = a.lut[shortcut_lookup('vol_heat_flux',alt=alt)] #121 The effective flux associated with volumetric heating
#        enf_index = a.lut[shortcut_lookup('enth_flux',alt=alt)]  #119 Enthalpy flux
#        vsf_index = a.lut[shortcut_lookup('visc_flux',alt=alt)]  #120 Viscous energy flux
#        kef_index = a.lut[shortcut_lookup('ke_flux',alt=alt)]    #117 Kinetic energy flux
#
#        cflux = data[:,0,cf_index] # Grab appropriate slices of the data array
#        hflux = data[:,0,vhf_index]
#        eflux = data[:,0,enf_index]
#        vflux =-data[:,0,vsf_index]
#        kflux = data[:,0,kef_index]
#
#        tflux = cflux+eflux+vflux+kflux+hflux # total flux
#
#        ### calculate fractional convective flux f_conv
#        ###     f_conv = 0 --> lack of convective heat transport
#        ###     f_conv = 1 --> convective is dominant over thermal conduction
#        ### see Eqn 26 of Featherstone & Hindman 2016 ApJ 818
#        ##numer = 4*np.pi*my_integrator((eflux+kflux+vflux)*radius*radius, radius,
#        ##                              method=method, data_reversed=True)
#        ##denom = 4*np.pi*my_integrator((eflux+kflux+vflux+cflux)*radius*radius, radius,
#        ##                              method=method, data_reversed=True)
#        ##f_conv = numer/denom
#        ##nusselt_number = 1./(1.-f_conv)
#        ##print("\nFractional Convective Flux, f_conv = {}".format(f_conv))
#        ##print("Nusselt Number, Nu = {}".format(nusselt_number))
#
#        # Nusselt number:
#        #        Nu = total flux / conductive flux
#        numer = my_integrator((tflux)*radius*radius, radius,
#                              method=method, data_reversed=True)
#        denom = my_integrator((cflux)*radius*radius, radius,
#                              method=method, data_reversed=True)
#        print("Nusselt Number, Nu = {}".format(numer/denom))
#
#        S = data[:,0,a.lut[shortcut_lookup('s',alt=alt)]] # entropy
#        dSdr = my_deriv(S, radius, data_reversed=True)
#        Nu = 1. - dSdr[0]/aspect_ratio  # evaluate dSdr at top of domain, dSdr[0] = top
        Nu, f_of_r = calculate_Nu.main(start_file, end_file, last_number_files,
                                    saveplot, savefile, data_dir, return_Nu=True,
                                    verbose=False, return_radial_funcs=True,
                                    data=data, ref_state=ref, lut=a.lut,
                                    aspect_ratio=aspect_ratio, use_wrong_method=nusselt2)
        print("Nusselt Number, Nu = {}".format(Nu))
        print("              Nu-1 = {}".format(Nu-1.))

        norm_Nu = True
        if (norm_Nu):
            for k in f_of_r['keys']:
                f_of_r[k] /= np.max(f_of_r[k])

        if (not no_plots):
            plt.clf()
            for k in f_of_r['keys']:
                plt.plot(radius/radius[0], f_of_r[k], label=k)
            plt.legend(loc='upper left')
            plt.show()

    if (reynolds):
        # calculate the Reynolds number
        #  get the rms velocity from the KE:
        #    KE = 0.5*rho*V_rms**2
        #    V_rms**2 = 2*KE/rho
        #  if U is nondimensionalized as
        #    U = nu/L*U^, U^ = simulation, nondimensional
        #  then the Reynolds number becomes
        #    Re = U*L/nu = (nu*U^/L)*L/nu = U^
        #  peak Reynolds number is
        #   peak Re = max(V_rms)*L / nu = max(U^_rms) = max( sqrt(2*KE/rho) )
        ke_index  = a.lut[shortcut_lookup('ke',alt=alt)]  #a.lut[125]  # total KE
        ke  = data[:,0,ke_index]          # average over angles
        rho = ref.density

        # get U_rms**2 and volume average it
        U_rms_squared = 2*ke/rho
        U2_tilde = volume_avg(U_rms_squared, radius, method=method, data_reversed=True)

        # get U_rms
        U_rms = np.sqrt(U_rms_squared)

        # Re = sqrt( vol_avg(U_rms**2) )
        # Re_peak = max( sqrt(U_rms**2) )
        Reynolds_num = np.sqrt(U2_tilde)
        Reynolds_peak = np.amax(U_rms)

        print("\nReynolds number, Re = {}".format(Reynolds_num))
        print("Peak Reynolds number, Re peak = {}".format(Reynolds_peak))
        print("\nMagnetic Reynolds number, Rem = Re*Pm = {}".format(Reynolds_num*Prm))
        print("Peak Magnetic Reynolds number, Rem peak = {}".format(Reynolds_peak*Prm))

    if (elsasser):
        # calculate the Elsasser number, El
        #      El = B**2 / (4*pi*rho*eta*Omega)
        #      B = sqrt(4*pi*rho*eta*Omega)*B^, B^ = simulation, nondimensional
        # so
        #      El = (B^)**2
        # from the source code,
        #      E_mag^ = (B^)**2 / Pm / Ek / 2
        #          Ek = nu / Omega / L**2
        #          Pm = nu / eta
        # so
        #      El = (B^)**2 = E_mag^ * 2 * Pm * Ek
        coeff = 2.0*Prm*Ek
        # first get the magnetic energy, integrated over all angles
        me_index = a.lut[shortcut_lookup('me',alt=alt)]
        me = data[:,0,me_index]

        # now integrate in radius, to make it a total volume integrated quantity
        B2_tilde = volume_avg(me, radius, method=method, data_reversed=True)

        # make it have the right units
        Elsasser_num = B2_tilde*coeff
        Elsasser_peak = np.amax(me)*coeff

        print("\nElsasser number, El = {}".format(Elsasser_num))
        print("Peak Elsasser number, El peak = {}".format(Elsasser_peak))

        # also get the Lorentz number
        #     El = Lo**2 * Prm / Ek
        #     Lo = sqrt(Ek*El/Prm)
        Lorentz_num = np.sqrt(Elsasser_num * Ek / Prm)
        Lorentz_peak = np.sqrt(Elsasser_peak * Ek / Prm)

        print("\nLorentz number, Lo = {}".format(Lorentz_num))
        print("Peak Lorentz number, Lo peak = {}".format(Lorentz_peak))

    print()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    Pr = float(args['--Pr'])
    Prm = float(args['--Pm'])
    Ek = float(args['--Ek'])
    reynolds = args['--reynolds']
    nusselt = args['--nusselt']
    elsasser = args['--elsasser']
    aspect_ratio = float(args['--aspect'])
    no_plots = args['--no-plots']
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         Pr, Prm, Ek, reynolds, nusselt, elsasser, no_plots, aspect_ratio, quants,
         new_lut, nusselt2=args['--nusselt2'], alt=args['--alt'])

