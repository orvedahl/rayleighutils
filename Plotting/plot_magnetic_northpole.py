"""
Read Br, Bth, Bphi from Shell Spectra to determine magnetic north pole

    1) get dipole spherical harmonic coefficients
         l=1,m=-1 is the y-dipole
         l=1,m=0  is the z-dipole
         l=1,m=+1 is the x-dipole
    2) calculate the dipole angles
         cos(theta) = z / (x**2+y**2+z**2)**0.5
         tan(phi)   = y / x

Usage:
    plot_shell_slice.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save            Save an image [default: False]
    --output=<o>      Save images as <o>.png [default: magpole_location]
    --data-dir=<d>    Specify location of data [default: ./]
    --no-plots        Suppress the plots [default: False]
    --rad-ind=<i>     Give radial index, or "max", "min", "2nd-small", "2nd-large" [default: 0]
    --Pm=<p>          Magnetic Prandtl number for time axis [default: 1]
    --Pr=<p>          Prandtl number for converting time axis [default: 1]
    --plot-tilt       Plot the tilt angle instead of latitude [default: False]
    --longitude       Also plot the longitude [default: False]
    --no-fix-time     Don't adjust time for saw-tooth effect [default: False]
    --store           Write results to a file for future reading [default: False]
    --read=<f>        Read stored results from file [default: ]
    --convert-time    After reading, convert time to magnetic diff times [default: False]
    --smooth          Plot smoothed data as well [default: False]
    --fft             Plot FFT of data [default: False]
    --debug           Debug mode [default: False]
"""

from __future__ import print_function
import numpy as np
import env
from Utilities.my_diagnostic_reading import mySpectralSpace
from ReadRayleigh.diagnostic_reading import build_file_list
import matplotlib.pyplot as plt
from Utilities.look_up_quantity import shortcut_lookup
from Utilities.loop_progress import print_progress
from Utilities.indices import find_index
from Utilities.spectral_utils import dipole_components, get_Poloidal
from NumericalToolsLocal.fft.signals import smooth
from NumericalToolsLocal.filesystem.file_io import read_data, save_data
from NumericalToolsLocal.fft.fft import FFT as myfft
from NumericalToolsLocal.public import interp
import sys

def main(start_file, end_file, last_number_files, saveplot, savefile,
         data_dir, no_plots, radial_ind, Pr, Prm, plot_tilt, no_fix_time,
         store_results, readfile, debug, longitude, smooth_data,
         convert_to_mag_diff_time, do_fft, **kwargs):

    quantity = 'br'

    # set some indices
    l1_ind = 1; m0_ind = 0; m1_ind = 1

    # read all the binary files and compile the data
    if (readfile == ''):
        if (data_dir[-1] != '/'):
            data_dir = data_dir + '/'

        files = build_file_list(start_file, end_file, path=data_dir+'Shell_Spectra')
        ntotal = len(files[-last_number_files:])

        b = mySpectralSpace(files[0], quantity)
        if (radial_ind in ["max", "min", "2nd-small", "2nd-large"]):
            radial_ind = find_index(b.radius, index=radial_ind)
        else:
            radial_ind = int(radial_ind)
        b = None

        for i,fname in enumerate(files[-last_number_files:]):
            print_progress("\tprogress = {:.2f} %".format(100.*i/ntotal))

            # extract data
            a = mySpectralSpace(fname, quantity) # a is size a[l, m, r, t]

            # store data
            if (i!=0):
                time = np.concatenate((time, a.time))  # data is size [m,t]
                data = np.concatenate((data, a.data[l1_ind,:,radial_ind,:]), axis=1)
            else:
                time = a.time
                data = a.data[l1_ind,:,radial_ind,:]

        print() # this resets the print_progress stuff

        time = np.array(time)
        lmax = a.lmax; nell = a.nell
        mmax = a.mmax; nm = a.nm
        radius = a.radius[radial_ind]  # specific radius
        Br = data[:, :] # size [m, t]

        # adjust the time so that it is monotonically increasing
        if (not no_fix_time):
            print("\nadjusting time to fix \"saw-tooth\" effect")

            ntime = len(time)
            indices = []; indices.append(0) # use index=0 as starting point
            for i in range(1,len(time)):    # find the breaks in time and
                if (time[i] < time[i-1]):   # the corresponding indices
                    indices.append(i)       # index: | 0 | 1 | 2 | X | 4 |
                                            # slice: 0   1   2   X   4   5
                                            # if X==i, X-1==i-1, then slice
                                            # is X, but adjustment time is X-1
            nindices = len(indices)
            for i in range(1,nindices):     # skip index=0 element
                s = indices[i]              # slice is X
                toffset = time[s-1]         # adjustment time is X-1
                if (i != nindices-1):       # manage the last slice separately
                    e = indices[i+1]
                else:
                    e = ntime               # this is ntime, not n+1 or n-1
                                            # which corresponds to the
                time[s:e] += toffset        # absence of "s:e+1" in favor of "s:e"
                                            # to avoid double counting time[e]

            if (nindices != 1):
                print("\tfound {} breaks in time".format(nindices-1)) # don't count 0 index
            else:
                print("\tfound no breaks in time")
            if (debug):
                plt.clf()
                plt.plot(time, linestyle='-', color='k', label='Time')
                plt.ylabel("time")
                plt.xlabel("index")
                plt.title("time")
                plt.legend()
                plt.show()

        # time comes out in viscous time, convert to magnetic time
        time *= Prm

        print("\nTotal number of files: {}".format(ntotal))

    # read results from file
    else:
        print("\nReading data from stored file {}".format(readfile))
        try:
            # data is returned as key=value pairs with data[key]=value
            # only send the keys to extract or all_keys=True
            readdata, keys = read_data(readfile)
            radius = readdata['radius']
            lmax   = readdata['lmax']
            nell   = readdata['nell']
            mmax   = readdata['mmax']
            nm     = readdata['nm']
            time   = readdata['time']
            Br     = readdata['Br']
            print("\t...success")
        except:
            print("\t...FAILED\n")
            sys.exit()

        if (convert_to_mag_diff_time):
            print("\tconverting to magnetic diffusion time\n")
            time *= Prm

    if (store_results):
        output = savefile+".magnorth_stored"
        print("\nStoring results to file {}".format(output))

        # save multiple arrays to single file
        # key=value pairs are stored as data[key]=value
        try:
            save_data(output, compress=True,
                      time=time, Br=Br,
                      radius=radius, lmax=lmax, nell=nell, mmax=mmax, nm=nm)
            print("\t...success")
        except:
            print("\t...FAILED\n")

    print("\nFirst time: {} simulation-secs".format(time[0]))
    print("Last  time: {} simulation-secs".format(time[-1]))
    print("\nTotal time: {} simulation-secs".format(time[-1]-time[0]))

    P = get_Poloidal(Br, radius, ell=1)       # size [m, t]

    method = 1
    dipoles = dipole_components(P, m0_ind, m1_ind, method=method)
    x_dipole = dipoles[0]
    y_dipole = dipoles[1]
    z_dipole = dipoles[2]
    total_dipole = dipoles[3]
    horiz_dipole = dipoles[4]

    # get spherical coordinates of dipole as func of time
    theta = np.arccos( z_dipole/total_dipole)
    phi   = np.arctan2(y_dipole, x_dipole) # returns phi in [-pi, pi]
    phi[np.where(phi < 0)] += 2.*np.pi     # so convert to [0, 2*pi]
    latitude = 0.5*np.pi - theta

    # convert to degrees
    latitude *= 180./np.pi
    theta    *= 180./np.pi
    phi      *= 180./np.pi

    if (smooth_data):
        window_length = None
        window = 'blackman' # 'flat'
        theta_s = smooth(theta, window_length=window_length, window=window)
        phi_s = smooth(phi, window_length=window_length, window=window)
        latitude_s = smooth(latitude, window_length=window_length, window=window)

    if (not no_plots):
        plt.clf()

        if (plot_tilt):
            plt.plot(time, theta, linestyle='-', color='r', label='Tilt', marker='x')
            if (smooth_data):
                plt.plot(time, theta_s, linestyle='-', color='k', label='Tilt_s', marker='x')
            plt.ylabel("Tilt (deg)")
            plt.ylim(-5, 185)
        else:
            plt.plot(time, latitude, linestyle='-', color='r', label='Latitude', marker='x')
            if (smooth_data):
                plt.plot(time, latitude_s, linestyle='-', color='k', label='Latitude_s', marker='x')
            plt.ylabel("Latitude (deg)")
            plt.ylim(-95, 95)
        plt.legend(loc='upper left') #, fontsize='small')
        #plt.legend(loc='best') #, fontsize='small')
        plt.xlabel("Magnetic Diffusion Time")
        plt.title("Dipole Location")

        if (longitude):
            plt.twinx()
            plt.plot(time, phi, linestyle='-', color='b', label='Longitude', marker='x')
            if (smooth_data):
                plt.plot(time, phi_s, linestyle='-', color='g', label='Longitude_s', marker='x')
            plt.legend(loc='upper right') #, fontsize='small')
            plt.ylabel("Longitude (deg)")
            plt.ylim(-5, 365)

        #Save or display the figure
        if (saveplot):
            output = savefile+".png"
            plt.savefig(output)
            print("saved image: {}".format(output))
        else:
            plt.show()

        plt.close()

        if (do_fft):
            interpolant = interp(latitude, time, method='cubic')
            t_uniform = np.linspace(time[0], time[-1], len(time), endpoint=1)
            lat_uniform = interpolant(t_uniform)
            fk, omega, power, xwin = myfft(lat_uniform, t_uniform)

            plt.plot(omega, power, linestyle='-', color='k', label='Power', marker='x')
            if (smooth_data):
                power_s = smooth(power, window_length=window_length, window=window)
                plt.plot(omega, power_s, linestyle='-', color='r', label='Smoothed Power', marker='x')
                interpolant = interp(latitude_s, time, method='cubic')
                t_uniform = np.linspace(time[0], time[-1], len(time), endpoint=1)
                lat_s_uniform = interpolant(t_uniform)
                fk, omega_s, power_s, xwin = myfft(latitude_s_uniform, t_uniform)
                plt.plot(omega, power_s, linestyle='-', color='b', label='Power_s', marker='x')

            plt.yscale('log')
            plt.xlabel(r"Magnetic Diffusion Frequency $\omega$")
            plt.ylabel("Power")
            plt.title("Power Spectrum of Latitude Data")

            #Save or display the figure
            if (saveplot):
                output = savefile+"_fft.png"
                plt.savefig(output)
                print("saved image: {}".format(output))
            else:
                plt.show()

    return

    ##############################################################
    # fun analysis bits
    ##############################################################
    plt.clf()
    plt.subplot(311)
    if (plot_tilt):
        plt.plot(time, theta, linestyle='-', color='k', label='Tilt')
        plt.ylabel("Tilt (deg)")
        plt.ylim(-5, 185)
    else:
        plt.plot(time, latitude, linestyle='-', color='k', label='Latitude')
        plt.ylabel("Latitude (deg)")
        plt.ylim(-95, 95)
    #plt.legend(loc='upper left') #, fontsize='small')
    #plt.xlabel("Magnetic Diffusion Time")
    plt.title("Dipole Location")
    plt.subplot(312)
    s = int(0.30*len(time)); e = int(0.42*len(time))
    if (plot_tilt):
        plt.plot(time[s:e], theta[s:e], linestyle='-', color='k', label='Tilt')
        plt.ylabel("Tilt (deg)")
        plt.ylim(-5, 185)
    else:
        plt.plot(time[s:e], latitude[s:e], linestyle='-', color='k', label='Latitude')
        plt.ylabel("Latitude (deg)")
        plt.ylim(-95, 95)
    plt.subplot(313)
    s = int(0.55*len(time)); e = int(0.59*len(time))
    if (plot_tilt):
        plt.plot(time[s:e], theta[s:e], linestyle='-', color='k', label='Tilt')
        plt.ylabel("Tilt (deg)")
        plt.ylim(-5, 185)
    else:
        plt.plot(time[s:e], latitude[s:e], linestyle='-', color='k', label='Latitude')
        plt.ylabel("Latitude (deg)")
        plt.ylim(-95, 95)
    plt.xlabel("Magnetic Diffusion Time")

    if (saveplot):
        output = savefile+"_zoom.png"
        plt.savefig(output)
        print("saved image: {}".format(output))
    else:
        plt.show()
    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file  = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    radial_ind = args['--rad-ind']
    Pr  = float(args['--Pr'])
    Prm = float(args['--Pm'])
    plot_tilt = args['--plot-tilt']
    no_fix_time = args['--no-fix-time']
    store_results = args['--store']
    readfile = args['--read']
    debug = args['--debug']
    longitude = args['--longitude']
    smooth_data = args['--smooth']
    convert_time = args['--convert-time']
    do_fft = args['--fft']

    main(start_file, end_file, last_number_files, saveplot, savefile,
         data_dir, no_plots, radial_ind, Pr, Prm, plot_tilt, no_fix_time,
         store_results, readfile, debug, longitude, smooth_data, convert_time, do_fft)

