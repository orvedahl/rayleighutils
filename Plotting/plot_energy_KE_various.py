"""
Plot various KE vs time

Usage:
    plot_energy_KE.py [options]

Options:
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --save               Save an image [default: False]
    --output=<o>         Save images as <o> [default: energy_trace.png]
    --data-dir=<d>       Specify location of data [default: ./]
    --Pm=<p>             Magnetic Prandtl number for converting time [default: 1]
    --no-plots           Suppress the plots [default: False]
    --quants=<q>         Comma separated list of quantities to use
    --lut=<l>            Use a different lut.py file

"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import GlobalAverage, build_file_list, ReferenceState
#import matplotlib; matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import numpy as np
from NumericalToolsLocal.calculus.integrate import integrate as my_integrator
from Utilities.look_up_quantity import shortcut_lookup, lut_lookup, nonzero_lut
import Utilities.time_utils as time_utils

def main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, quants, new_lut, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    path = os.path.join(data_dir, "G_Avgs")
    files = build_file_list(start_file, end_file, path=path)

    if (quants is None):
        quant_list = None
    else:
        quant_list = list(quants)

    a = GlobalAverage(filename=files[0],path='',quantity=quant_list)
    ke_index  = a.lut[shortcut_lookup('ke')]     # Kinetic Energy (KE)
    pke_index = shortcut_lookup('pke')    # phi KE index
    sym_index = shortcut_lookup('mke')    # m=0 KE, 0.5*rho*<v>**2
    prime_index = shortcut_lookup('kep')    # KE' index = 0.5*rho*(v')**2

    # compile a time series of all the files
    data, time = time_utils.TimeSeries(files[-last_number_files:exclude_last],
                                       GlobalAverage, axis=0, quantity=quant_list)
    alltime = Pm*np.array(time)
    alldays = alltime / (3600.*24.)

    # total KE/ME
    ke = data[:,ke_index]

    available_lut = nonzero_lut(a.lut).keys()

    # other KEs
    if (pke_index in available_lut):
        ke_zonal = data[:,a.lut[pke_index]] # total zonal KE
        ke_merid = ke - ke_zonal            # total meridional KE
        zonal_merid_KE = True
    else:
        zonal_merid_KE = False
        #ke_zonal = np.zeros_like(alltime)
        #ke_merid = np.zeros_like(alltime)
        print("...Zonal KE could not be found")

    if (sym_index in available_lut):
        ke_symm = data[:,a.lut[sym_index]] # m=0 KE
        ke_asym = ke - ke_symm             # m/=0 KE
    elif (406 in available_lut and 407 in available_lut and 408 in available_lut):
        ke_r_symm = data[:,a.lut[406]]
        ke_t_symm = data[:,a.lut[407]]
        ke_p_symm = data[:,a.lut[408]]

        ke_symm = ke_r_symm + ke_t_symm + ke_p_symm
        ke_asym = ke - ke_symm             # m/=0 KE

        ke_diff_rot = ke_p_symm

        symm_asym_KE = True
    else:
        symm_asym_KE = False
        print("...'m=0' KE could not be found")

    if (prime_index in available_lut):
        ke_prime = data[:,a.lut[prime_index]] # KE' = 0.5*rho*(v')**2
        prime_KE = True
    else:
        prime_KE = False
        print("...'prime' KE could not be found")

    print("\nFirst file: {}".format(files[-last_number_files:exclude_last][0]))
    print("Last  file: {}".format(files[-last_number_files:exclude_last][-1]))
    print("\nTotal number of files: {}".format(len(files[-last_number_files:exclude_last])))

    print("\nFirst time: {} simulation-secs".format(alltime[0]))
    print("Last  time: {} simulation-secs".format(alltime[-1]))
    print("\nTotal time: {} simulation-secs".format(alltime[-1]-alltime[0]))

    if (no_plots):
        return

    #Changing the plotting parameter somewhat depending on saveplot
    if (saveplot):
        plt.figure(1,figsize=(8.5, 4), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(8.5, 4), dpi=100)
        plt.rcParams.update({'font.size': 14})

    plt.subplot(111)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(alltime,ke,label=r'$KE_{total}$')
    if (prime_KE):
        plt.plot(alltime,ke,label=r'$KE_{pert}\sim (v^\prime)^2$')
    if (zonal_merid_KE):
        plt.plot(alltime,ke_zonal,label=r'$KE_{zonal}\sim v_\phi^2$')
        plt.plot(alltime,ke_merid,label=r'$KE_{merid} \sim v_r^2 + v_\theta^2$')
    if (symm_asym_KE):
        plt.plot(alltime,ke_symm,label=r'$KE_{m=0} \sim <v>^2$')
        plt.plot(alltime,ke_asym,label=r'$KE_{m\neq 0} = KE_{total} - KE_{m=0}$')
        plt.plot(alltime,ke_diff_rot,label=r'$KE_{diff rot} \sim <v_\phi>^2$')
    plt.yscale('log')
    plt.xlabel('Time (simulation secs)')
    plt.ylabel('Energy Density')
    if (saveplot):
        legend = plt.legend(loc='lower right', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='best', shadow=True, ncol = 2) 

    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    quants = args['--quants']
    if (quants is not None):
        quants = [int(a) for a in quants.split(",")]
    new_lut = args['--lut']

    main(start_file, end_file, last_number_files, exclude_last, saveplot, savefile, data_dir,
         no_plots, Pm, quants, new_lut)

