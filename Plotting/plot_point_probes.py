"""
Plots point probe data vs time

Usage:
    plot_point_probes.py [options]

Options:
    --start=<s>        Starting file [default: 0]
    --end=<e>          Ending file [default: 100000000]
    --last=<l>         Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save             Save an image [default: False]
    --output=<o>       Save images as <o> [default: point_probe.png]
    --data-dir=<d>     Specify location of data [default: ./]
    --quantities=<q>   Comma separated list of quantities to plot
    --Pm=<p>           Magnetic Prandtl number for converting time [default: 1]
    --no-plots         Suppress the plots [default: False]
    --lut=<l>          Use a different lut.py file
    --list             List available quanities [default: False]

"""

from __future__ import print_function
import env
import os
from ReadRayleigh.diagnostic_reading import Point_Probe, build_file_list, AzAverage
from Utilities import time_utils
from Utilities.look_up_quantity import lut_lookup, print_lut, nonzero_lut
import matplotlib.pyplot as plt
import numpy as np
import NumericalToolsLocal.public as NT

def main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, Pm, list_contents, new_lut, quants, **kwargs):

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    path = os.path.join(data_dir, "Point_Probes")
    files = build_file_list(start_file,end_file,path=path)
    files = files[-last_number_files:]

    if (list_contents):
        a = Point_Probe(filename=files[0], path='')
        print_lut(a.lut)
        return

    a = Point_Probe(filename=files[0], path='', quantity=quants)
    data, time = time_utils.TimeSeries(files, Point_Probe, axis=-1, data_attr="vals",
                                       quantity=quants)

    if (quants is None):
        quants = nonzero_lut(a.lut).keys()
    else:
        quants = list(quants)

    time = Pm*np.array(time) # convert from viscous to magnetic diffusion times

    print("\nFirst file: {}".format(files[0]))
    print("Last  file: {}".format(files[-1]))
    print("\nTotal number of files: {}".format(len(files)))
    print("\nFirst time: {} secs".format(time[0]))
    print("Last  time: {} secs".format(time[-1]))
    print("\nTotal time: {} secs".format(time[-1]-time[0]))

    # choose spatial location of point probe
    r_ind=0; t_ind=0; p_ind=0

    theta = np.arccos(a.costheta)
    rad = a.radius[r_ind]; th = theta[t_ind]; ph = a.phi[p_ind]
    lat = (0.5*np.pi - th)*180./np.pi
    lon = ph*180./np.pi
    print("\nPlotting point probe at")
    print("\t radius    = {}".format(rad))
    print("\t longitude = {}".format(lon))
    print("\t latitude  = {}".format(lat))
    print("\nAvailable coordinates are:")
    print("\t radius    = {}".format(a.radius))
    print("\t latitude  = {}".format((0.5*np.pi-theta)*180./np.pi))
    print("\t longitude = {}".format(a.phi*180./np.pi))

    if (no_plots):
        return

    plt.clf()
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

    for q in quants:
        plt.plot(time[:], data[p_ind,t_ind,r_ind,a.lut[q],:], label=lut_lookup(q))

    plt.yscale('symlog')
    plt.xlabel('Magnetic Diffusion Time')
    plt.ylabel('Point Probe')
    plt.legend(loc='best', shadow=True, ncol=2, fontsize='x-small') 
    plt.tight_layout()

    if (saveplot):
        plt.savefig(savefile, dpi=300)
        print("saved image: {}".format(savefile))
    else:
        plt.show()

    plt.close()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    saveplot = args['--save']
    savefile = args['--output']
    data_dir = args['--data-dir']
    no_plots = args['--no-plots']
    Pm = float(args['--Pm'])
    list_contents = args['--list']
    new_lut = args['--lut']

    if (args['--quantities'] is not None):
        quants = [int(a) for a in args['--quantities'].split(",")]
    else:
        quants = None

    main(start_file, end_file, last_number_files, saveplot, savefile, data_dir,
         no_plots, Pm, list_contents, new_lut, quants)

