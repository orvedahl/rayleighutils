"""
slightly modify python path in order to import from a relative path
"""

import os
import sys

# include one directory up
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
