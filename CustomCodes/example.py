"""
Example for adding your own code that uses the RayleighUtils libraries
"""

from __future__ import print_function  # this makes it run in either python 2 or python 3

import env # this allows you to import other RayleighUtils codes

# import any other packages you want to use
import numpy as np
import matplotlib.pyplot as plt

# import any NumericalToolsLocal packages: "import env" must appear before these imports
import NumericalToolsLocal.public as NT

# import any RayleighUtils packages: "import env" must appear before these imports
from ReadRayleigh.diagnostic_reading import AzAverage, build_file_list
# if you don't want to use the Fortran version, a pure python version can be used:
#from ReadRayleigh.diagnostic_reading_original import AzAverage, build_file_list

from Utilities.look_up_quantity import lut_lookup, print_lut
from Utilities import time_utils

###########################################################################

#--------------------------------------------------------------------------
# test the derivatives from NumericalToolsLocal
#--------------------------------------------------------------------------

def f(t):
    return np.exp(-t*t)*np.sin(np.pi*t)
def dfdt(t):
    return np.exp(-t*t)*np.pi*np.cos(np.pi*t) - 2*t*np.exp(-t*t)*np.sin(np.pi*t)

# get a uniform time grid between -2 and 3.5, inclusive
ntime = 256
G = NT.UniformGrid(ntime, a=-2, b=3.5, endpoints=True) # build the grid object
time = G.x # now extract the grid points from Grid object

# calculate a time derivative using 2nd order finite differences
dfdt_numerical = NT.derivative(f(time), time, method='fd', order=2)

error = dfdt(time) - dfdt_numerical

print("\nmaximum absolute difference in time derivative = {}".format(np.amax(np.abs(error))))

# plot it
plt.clf()
plt.plot(time, dfdt_numerical, marker='x', label='numerical', linestyle='')
plt.plot(time, dfdt(time), linestyle='-', color='r', label='analytic')
plt.legend(loc='best')
plt.show()

#--------------------------------------------------------------------------
# read Rayleigh data
#--------------------------------------------------------------------------
files = build_file_list(0, 1000000, path="./AZ_Avgs")

a = AzAverage(files[0], path='')
lut = a.lut
radius = a.radius
theta = np.arccos(a.costheta)
nr = a.nr
nth = a.ntheta

# print possible values in the look up table
print_lut(lut)

# get time series
data, time = time_utils.TimeSeries(files, AzAverage)

# get proper look up indices for diagnostic quantities
ke_index = lut_lookup("kinetic_energy")

# choose theta and radial index
th_ind = int(0.5*(nth-1))
r_ind  = int(0.5*(nr-1))

# extract the data
KE = data[th_ind, r_ind, lut[ke_index], :] # time series

# plot it
plt.clf()
plt.plot(time, KE, label='KE')
plt.legend(loc='best')
plt.show()

#--------------------------------------------------------------------------
# generate a radial grid similar to Rayleigh:
#     --zeros based chebyshev grid
#     --includes the endpoints
#     --grid is reversed, outer radius is first
#--------------------------------------------------------------------------
shell_depth  = 1.00 # L = r_o - r_i
aspect_ratio = 0.35 # chi = r_i / r_o

r_outer = shell_depth / (1. - aspect_ratio)
r_inner = aspect_ratio * r_outer

grid = NT.ChebGrid(nr, a=r_inner, b=r_outer, zeros=True, endpoints=True) # build grid object
r = grid.xab  # extract grid points
r = r[::-1]   # reverse the grid

# compare to radius from AzAverage
error = radius - r
print("maximum absolute difference in radial grids = {}".format(np.amax(np.abs(error))))
print()

