"""
Module to help with importing from two different versions of diagnostic_reading module
"""
from __future__ import print_function
import importlib

def diagnostics(imports, python=False, package=None):
    """
    import specific modules from the proper diagnostic_reading routine

        imports = ["ReferenceState", "GlobalAverage", "build_file_list"]
        diags = diagnostics(imports, python=0)
        ...
        ref = diags["ReferenceState"](...)
        avg = diags["GlobalAverage"](...)

    i.e., the "build_file_list" function is stored in the dictionary and
    accessed as
        build_file_list = diag_list["build_file_list"]
    and the "GlobalAverage" class is accessed as
        GlobalAverage = diags["GlobalAverage"]
    """

    # get proper import name
    diag_file = "diagnostic_reading"
    if (python):
        diag_file += "_python"

    if (package is not None):
        diag_file = package + "." + diag_file

    # import the module
    diag = importlib.import_module(diag_file)

    # import individual modules
    results = {}
    for i in imports:
        if (hasattr(diag, i)):
            results[i] = getattr(diag, i)
        else:
            print("\n\tERROR: could not import \"{}\" from module \"{}\"\n".format(i, diag_file))

    return results

