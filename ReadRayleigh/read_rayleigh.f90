!
! functions to read Rayleigh output data
!
! R. Orvedahl 6-30-2017

module read_rayleigh

   implicit none

   ! reference number written to files for determining the endian-ness
   integer, parameter :: rayleigh_reference_number = 314
   integer, parameter :: maxq = 5001   ! max number of quantities in lookup table
   integer, parameter :: n_ref_vals=10 ! number of reference state values

   contains

   !-----------------------------------------------------------------
   ! calculate the look up table
   !-----------------------------------------------------------------
   subroutine get_lut(quantities, nq, nlut, lut)
      integer, intent(in) :: nlut, nq, quantities(0:nq-1)
      integer, intent(out) :: lut(0:nlut-1)
      !f2py intent(in) :: nq, nlut
      !f2py intent(in) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut
      integer :: i, q
      lut(:) = 5000
      do i=0, nq-1
         q = quantities(i)
         if ((0 .le. q) .and. (q .le. nlut-1)) then ! lut indices are 0 to nlut-1, inclusive
            lut(q) = i
         endif
      enddo
   end subroutine get_lut

   !-----------------------------------------------------------------
   ! check endian-ness of file
   !-----------------------------------------------------------------
   subroutine check_endian(filename, endianness, reference_number)
      character(len=256), intent(in) :: filename
      integer, intent(in), optional :: reference_number
      character(len=16), intent(out) :: endianness

      !f2py intent(in) :: filename
      !f2py intent(in) :: reference_number
      !f2py optional :: reference_number
      !f2py intent(out) :: endianness

      integer :: ref_number, first_number, iounit=15

      endianness = trim("none") ! returned value if not little or big endian

      ! set the reference number
      if (.not. present(reference_number)) then
         ref_number = rayleigh_reference_number
      else
         ref_number = reference_number
      endif

      ! check for Big-Endian
      open(unit=iounit, file=trim(filename), form='unformatted', status='old', &
           access='stream', convert='BIG_ENDIAN')
      read(iounit) first_number
      close(iounit)
      if (first_number .eq. ref_number) then
         endianness = trim("BIG_ENDIAN")
      else
         ! failed, so check Little-Endian
         open(unit=iounit, file=filename, form='unformatted', status='old', &
              access='stream', convert='LITTLE_ENDIAN')
         read(iounit) first_number
         close(iounit)
         if (first_number .eq. ref_number) then
            endianness = trim("LITTLE_ENDIAN")
         endif
      endif

   end subroutine check_endian

   !-----------------------------------------------------------------
   ! read meta data from various files
   !-----------------------------------------------------------------
   subroutine read_meta(path, iteration, filetype, filename, version,&
                        nr, nth, nphi, nell, nm, nrec, nq, nlut, nrefs)
      character(len=256), intent(in) :: path
      integer, intent(in) :: iteration
      character(len=32), intent(in) :: filetype

      integer, intent(out) :: nr, nth, nphi, nell, nm, nrec
      integer, intent(out) :: nq, nlut, nrefs, version
      character(len=256), intent(out) :: filename

      !f2py intent(in) :: filetype, iteration, path
      !f2py intent(out) :: nr, nth, nphi, nell, nm, nrec
      !f2py intent(out) :: nq, nlut, version, filename

      integer :: iounit=15, first_number, lmax
      character(len=256) :: itrstr, prefix
      character(len=16) :: endian

      ! default values
      nr = -1; nth = -1; nphi = -1; nell = -1; nm = -1; nrec = -1; nq = -1
      version = -1
      nlut = maxq
      nrefs = n_ref_vals
      filename = "none"

      ! get filename
      if (trim(filetype) .eq. "reference") then
         filename = trim(path)

      else if (trim(filetype) .eq. "checkpoint") then
         if (iteration .lt. 0) then
            write(itrstr, '(i2.2)') abs(iteration)
            prefix = '/quicksave_'//trim(itrstr)
         else
            write(itrstr, '(i8.8)') iteration
            prefix = '/'//trim(itrstr)
         endif
         filename = trim(path)//trim(prefix)//'_grid_etc'

      else
         filename = trim(path)
      endif

      ! check endian-ness
      call check_endian(filename, endian)
      if (trim(filetype) .ne. "checkpoint") then
          open(unit=iounit, file=trim(filename), form='unformatted', status='old', &
               access='stream', convert=trim(endian))
      else
          ! checkpoint files have issues with the 'convert' keyword on Summit
          open(unit=iounit, file=trim(filename), form='unformatted', &
               status='old', access='stream') !, convert=trim(endian))
      endif

      ! read appropriate meta data, depending on the filetype
      if (trim(filetype) .eq. "g_avgs") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nq
      else if (trim(filetype) .eq. "shell_avgs") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nr
         read(iounit) nq
      else if (trim(filetype) .eq. "az_avgs") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nr
         read(iounit) nth
         read(iounit) nq
      else if (trim(filetype) .eq. "point_probes") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nr
         read(iounit) nth
         read(iounit) nphi
         read(iounit) nq
      else if (trim(filetype) .eq. "meridional_slices") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nr
         read(iounit) nth
         read(iounit) nphi
         read(iounit) nq
      else if (trim(filetype) .eq. "equatorial_slices") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nphi
         read(iounit) nr
         read(iounit) nq
      else if (trim(filetype) .eq. "shell_slices") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nth
         nphi = 2*nth
         read(iounit) nr
         read(iounit) nq
      else if (trim(filetype) .eq. "shell_spectra") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) lmax
         nell = lmax+1
         nm = nell
         read(iounit) nr
         read(iounit) nq
      else if (trim(filetype) .eq. "sph_modes") then
         read(iounit) first_number
         read(iounit) version
         read(iounit) nrec
         read(iounit) nell
         read(iounit) nr
         read(iounit) nq
      else if (trim(filetype) .eq. "reference") then
         read(iounit) first_number
         read(iounit) nr
      else if (trim(filetype) .eq. "checkpoint") then
         read(iounit) nr
      else
         write(*,*)
         write(*,*) "---ERROR: unrecognized filetype = "//trim(filetype)
         write(*,*) "   supported filetypes (case sensitive) = g_avgs, shell_avgs, az_avgs"
         write(*,*) "       point_probes, meridional_slices, equatorial_slices"
         write(*,*) "       shell_slices, shell_spectra, sph_modes, reference, checkpoint"
         write(*,*)
      endif

      close(iounit)

   end subroutine read_meta

   !-----------------------------------------------------------------
   ! read the reference file
   !-----------------------------------------------------------------
   subroutine read_reference(fname, nr, nrefs, ref_arr)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nr, nrefs

      double precision, intent(out) :: ref_arr(0:nr-1, 0:nrefs-1)

      !f2py intent(in) :: fname, nr, nrefs
      !f2py intent(out) :: ref_arr
      !f2py depend(nr,nrefs) :: ref_arr

      integer :: i, k, iounit=15, first_number, nr_meta
      character(len=16) :: endian

      ! check endian-ness
      call check_endian(fname, endian)
      open(unit=iounit, file=trim(fname), form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number   ! first line is the "reference number"
      read(iounit) nr_meta
      read(iounit) ((ref_arr(i,k), i=0,nr-1), k=0, nrefs-1)

      close(iounit)

   end subroutine read_reference

   !-----------------------------------------------------------------
   ! read the *_grid_etc file in the Checkpoints/ directory
   !-----------------------------------------------------------------
   subroutine read_grid(fname, nr, is_quicksave, sizes, times, radius)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nr
      logical, intent(in) :: is_quicksave

      integer, intent(out) :: sizes(0:3)
      double precision, intent(out) :: times(0:2)
      double precision, intent(out) :: radius(0:nr-1)

      !f2py intent(in) :: fname, is_quicksave, nr
      !f2py intent(out) :: sizes, times
      !f2py intent(out) :: radius
      !f2py depend(nr) :: radius

      integer :: nr_meta, lmax, grid_type, iter, i, iounit=15
      double precision :: dt, old_dt, time

      ! there is no "reference number" written first, so can't check endian-ness
      open(unit=iounit, file=trim(fname), form='unformatted', status='old')
      read(iounit) nr_meta
      read(iounit) grid_type
      read(iounit) lmax
      read(iounit) dt      ! dt in Rayleigh     --- this is the current dt
      read(iounit) old_dt  ! new_dt in Rayleigh --- not sure what this is, future dt?

      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) time
      if (is_quicksave) then
         ! we are loading a quicksave, need to get iteration
         read(iounit) iter
      endif
      close(iounit) 

      ! fill output arrays
      sizes(0) = nr
      sizes(1) = lmax
      sizes(2) = grid_type
      if (is_quicksave) then
          sizes(3) = iter
      else
          sizes(3) = 0
      endif

      times(0) = time
      times(1) = dt
      times(2) = old_dt

   end subroutine read_grid

   !-----------------------------------------------------------------
   ! read a Global Averages file
   !-----------------------------------------------------------------
   subroutine global_avg(fname, nrec, nq, nlut, time, iters, vals, quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nrec-1,0:nq-1)
      double precision, intent(out) :: time(0:nrec-1)

      !f2py intent(in) :: fname, nrec, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: vals
      !f2py depend(nrec, nq) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number
      integer :: i, k, nrec_meta, nq_meta, version
      character(len=16) :: endian

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      do k=0,nrec-1
          read(iounit) (vals(k,i), i=0,nq-1)
          read(iounit) time(k)
          read(iounit) iters(k)
      enddo
      close(iounit)

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine global_avg

   !-----------------------------------------------------------------
   ! read a Shell Average file
   !-----------------------------------------------------------------
   subroutine shell_avgs(fname, nrec, nr, nq, nlut, time, iters, vals, radius, &
                             quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nr, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nr-1,0:3,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)

      !f2py intent(in) :: fname, nrec, nr, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius
      !f2py depend(nr) :: radius
      !f2py intent(out) :: vals
      !f2py depend(nr,nq,nrec) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: i, j, k, m, nq_meta, nrec_meta, nr_meta
      character(len=16) :: endian

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nr_meta
      read(iounit) nq_meta

      if (version .eq. 1) then
         ! NotImplementedError
         close(iounit)
         write(*,*)'This version does not support reading of ShellAvg version=1'
         return
      endif

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      do m=0,nrec-1
          read(iounit) (((vals(i,j,k,m),i=0,nr-1),j=0,3),k=0,nq-1)
          read(iounit) time(m)
          read(iounit) iters(m)
      enddo
      close(iounit)

      ! adjust moments if version is old enough
      if ((version .gt. 1) .and. (version .le. 3)) then
         write(*,*) 'This ShellAverage file is version 2 or 3 and ntheta was not passed'
         write(*,*) 'The 2nd, 3rd and 4th moments are set to zero'
         vals(:,1,:,:) = 0.0d0
         vals(:,2,:,:) = 0.0d0
         vals(:,3,:,:) = 0.0d0
      endif

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine shell_avgs

   !-----------------------------------------------------------------
   ! read an Azimuthal Average file
   !-----------------------------------------------------------------
   subroutine az_avgs(fname, nrec, nr, nth, nq, nlut, time, iters, vals, &
                          radius, costheta, sintheta, quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nr, nth, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nth-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)
      double precision, intent(out) :: costheta(0:nth-1)
      double precision, intent(out) :: sintheta(0:nth-1)

      !f2py intent(in) :: fname, nrec, nr, nth, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius
      !f2py depend(nr) :: radius
      !f2py intent(out) :: costheta, sintheta
      !f2py depend(nth) :: costheta, sintheta
      !f2py intent(out) :: vals
      !f2py depend(nth,nr,nq,nrec) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: i, j, k, m, nq_meta, nrec_meta, nr_meta, nth_meta
      character(len=16) :: endian

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nr_meta
      read(iounit) nth_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) (costheta(i), i=0, nth-1)
      sintheta(:) = dsqrt(1.0d0 - costheta(:)*costheta(:))
      do m=0,nrec-1
          read(iounit) (((vals(i,j,k,m),i=0,nth-1),j=0,nr-1),k=0,nq-1)
          read(iounit) time(m)
          read(iounit) iters(m)
      enddo
      close(iounit)

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine az_avgs

   !-----------------------------------------------------------------
   ! read a Point Probe file
   !-----------------------------------------------------------------
   subroutine point_probe(fname, nrec, nphi, nth, nr, nq, nlut, time, iters, &
                              vals, radius, costheta, sintheta, phi, rad_ind, &
                              theta_ind, phi_ind, quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nphi, nth, nr, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nphi-1,0:nth-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)
      integer, intent(out) :: rad_ind(0:nr-1)
      double precision, intent(out) :: costheta(0:nth-1)
      double precision, intent(out) :: sintheta(0:nth-1)
      integer, intent(out) :: theta_ind(0:nth-1)
      double precision, intent(out) :: phi(0:nphi-1)
      integer, intent(out) :: phi_ind(0:nphi-1)

      !f2py intent(in) :: fname, nrec, nr, nth, nphi, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius, rad_ind
      !f2py depend(nr) :: radius, rad_ind
      !f2py intent(out) :: costheta, sintheta, theta_ind
      !f2py depend(nth) :: costheta, sintheta, theta_ind
      !f2py intent(out) :: phi, phi_ind
      !f2py depend(nphi) :: phi, phi_ind
      !f2py intent(out) :: vals
      !f2py depend(nphi,nth,nr,nq,nrec) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: n, i, j, k, m, nq_meta, nrec_meta, nr_meta, nth_meta, nphi_meta
      character(len=16) :: endian

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nr_meta
      read(iounit) nth_meta
      read(iounit) nphi_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) (rad_ind(i), i=0, nr-1)
      read(iounit) (costheta(i), i=0, nth-1)
      sintheta(:) = dsqrt(1.0d0 - costheta(:)*costheta(:))
      read(iounit) (theta_ind(i), i=0, nth-1)
      read(iounit) (phi(i), i=0, nphi-1)
      read(iounit) (phi_ind(i), i=0, nphi-1)
      do m=0,nrec-1
          read(iounit) ((((vals(n,i,j,k,m),n=0,nphi-1),i=0,nth-1),j=0,nr-1),k=0,nq-1)
          read(iounit) time(m)
          read(iounit) iters(m)
      enddo
      close(iounit)

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine point_probe

   !-----------------------------------------------------------------
   ! read a Meridional Slice file
   !-----------------------------------------------------------------
   subroutine meridional_slice(fname, nrec, nphi, nth, nr, nq, nlut, &
                              time, iters, vals, radius, costheta, sintheta, &
                              phi, phi_ind, quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nphi, nth, nr, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nphi-1,0:nth-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)
      double precision, intent(out) :: costheta(0:nth-1)
      double precision, intent(out) :: sintheta(0:nth-1)
      double precision, intent(out) :: phi(0:nphi-1)
      integer, intent(out) :: phi_ind(0:nphi-1)

      !f2py intent(in) :: fname, nrec, nr, nth, nphi, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius
      !f2py depend(nr) :: radius
      !f2py intent(out) :: costheta, sintheta
      !f2py depend(nth) :: costheta, sintheta
      !f2py intent(out) :: phi, phi_ind
      !f2py depend(nphi) :: phi, phi_ind
      !f2py intent(out) :: vals
      !f2py depend(nphi,nth,nr,nq,nrec) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: n, i, j, k, m, nq_meta, nrec_meta, nr_meta, nth_meta, nphi_meta
      character(len=16) :: endian
      double precision :: dphi

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nr_meta
      read(iounit) nth_meta
      read(iounit) nphi_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) (costheta(i), i=0, nth-1)
      sintheta(:) = dsqrt(1.0d0 - costheta(:)*costheta(:))

      read(iounit) (phi_ind(i), i=0, nphi-1)
      dphi = 2.0d0*dacos(-1.0d0)/(nth*2.0d0)
      do i=0,nphi-1
         phi(i) = phi_ind(i)*dphi
      enddo

      do m=0,nrec-1
          read(iounit) ((((vals(n,i,j,k,m),n=0,nphi-1),i=0,nth-1),j=0,nr-1),k=0,nq-1)
          read(iounit) time(m)
          read(iounit) iters(m)
      enddo
      close(iounit)

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine meridional_slice

   !-----------------------------------------------------------------
   ! read an Equatorial Slice file
   !-----------------------------------------------------------------
   subroutine equatorial_slice(fname, nrec, nphi, nr, nq, nlut, time, iters, &
                              vals, radius, phi, quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nphi, nr, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nphi-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)
      double precision, intent(out) :: phi(0:nphi-1)

      !f2py intent(in) :: fname, nrec, nphi, nr, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius
      !f2py depend(nr) :: radius
      !f2py intent(out) :: phi
      !f2py depend(nphi) :: phi
      !f2py intent(out) :: vals
      !f2py depend(nphi,nr,nq,nrec) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: i, j, k, m, nq_meta, nrec_meta, nr_meta, nphi_meta
      character(len=16) :: endian
      double precision :: dphi

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nphi_meta
      read(iounit) nr_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)

      dphi = 2.0d0*dacos(-1.0d0)/(nphi)
      do i=0,nphi-1
         phi(i) = i*dphi
      enddo

      do m=0,nrec-1
          read(iounit) (((vals(i,j,k,m),i=0,nphi-1),j=0,nr-1),k=0,nq-1)
          read(iounit) time(m)
          read(iounit) iters(m)
      enddo
      close(iounit)

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine equatorial_slice

   !-----------------------------------------------------------------
   ! read a Shell Slice file
   !-----------------------------------------------------------------
   subroutine shell_slice(fname, nrec, nphi, nth, nr, nq, nlut, time, &
                              iters, vals, radius, costheta, sintheta, rad_ind, &
                              quantities, lut, specific_inds)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nphi, nth, nr, nq, nlut
      integer, intent(in) :: specific_inds(0:2)

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:nphi-1,0:nth-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)    
      double precision, intent(out) :: radius(0:nr-1)
      integer, intent(out) :: rad_ind(0:nr-1)
      double precision, intent(out) :: costheta(0:nth-1)
      double precision, intent(out) :: sintheta(0:nth-1)

      !f2py intent(in) :: specific_inds
      !f2py intent(in) :: fname, nrec, nr, nth, nphi, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius, rad_ind
      !f2py depend(nr) :: radius, rad_ind
      !f2py intent(out) :: costheta, sintheta
      !f2py depend(nth) :: costheta, sintheta
      !f2py intent(out) :: phi
      !f2py depend(nphi) :: phi
      !f2py intent(out) :: vals
      !f2py depend(nphi,nth,nr,nq,nrec) :: vals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version, tind, qind, rind, qspec
      integer :: n, i, j, k, m, nq_meta, nrec_meta, nr_meta, nphi_meta, nth_meta
      integer :: before_slice, current_position, start_position, ierr
      integer :: q_size, rec_size, shell_size, offset
      character(len=16) :: endian
      logical :: single_slice

      ! extract specific indices if they were given
      if (specific_inds(0) .eq. -50000) then
         single_slice = .false.
      else
         single_slice = .true.
         tind  = specific_inds(0) ! time index
         qspec = specific_inds(1) ! quantity code
         rind  = specific_inds(2) ! radial index
      endif

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nth_meta
      nphi_meta = 2*nth_meta
      read(iounit) nr_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) (rad_ind(i), i=0, nr-1)
      read(iounit) (costheta(i), i=0, nth-1)
      sintheta(:) = dsqrt(1.0d0 - costheta(:)*costheta(:))

      if (.not. single_slice) then
         do m=0,nrec-1
             read(iounit) ((((vals(n,i,j,k,m),n=0,nphi-1),i=0,nth-1),j=0,nr-1),k=0,nq-1)
             read(iounit) time(m)
             read(iounit) iters(m)
         enddo
      else
         ! calculate size of data chunks
         shell_size = 8*nphi*nth       ! each shell slice is 8-byte double precision
         q_size     = nr*shell_size    ! there are nr slices per quantity
         rec_size   = nq*q_size        ! there are nq quantities per timestep
         rec_size   = rec_size + 8 + 4 ! add the time (double) & the iteration (integer)

         ! convert quantity code to quantity-array index
         qind = -50000
         do i=0,nq-1
            if (quantities(i) .eq. qspec) then
               qind = i
               exit
            endif
         enddo

         ! error trap
         if (qind .eq. -50000) then
            write(*,*)
            write(*,*) '---ERROR: quantity code not found, given code = ',qspec
            write(*,*) '   valid codes are:'
            write(*,*) quantities
            write(*,*)
         endif
         if (rind .gt. nr_meta-1) then ! nr_meta, b/c that is what file has, nr is user given
            write(*,*)
            write(*,*) '---ERROR: radial index too large, given index = ',rind
            write(*,*) '          number of radii in this file = ',nr_meta
            write(*,*) '          indexing is zero based'
            write(*,*)
         endif
         if (tind .gt. nrec_meta-1) then ! nrec_meta, b/c file has that, nrec is passed by user
            write(*,*)
            write(*,*) '---ERROR: time index too large, given index = ',tind
            write(*,*) '          number of records in this file = ',nrec_meta
            write(*,*) '          indexing is zero based'
            write(*,*)
         endif

         ! fseek positions
         start_position   = 0
         current_position = 1

         ! get offset to particular slice data starting from current spot in file
         offset = tind*rec_size + qind*q_size + rind*shell_size
         call fseek(iounit, offset, current_position, ierr)
         read(iounit) ((vals(n,i,0,0,0),n=0,nphi-1),i=0,nth-1)

         ! seek to the particular time data corresponding to the particular
         ! slice data starting from beginning of file
         !     meta data   --- 6 integers specifying array sizes
         !     quantities  --- nq quantities, integers
         !     radii       --- nr radii, double precision
         !     radial inds --- nr radial indices, integers
         !     costheta    --- nth angles,  double precision
         ! integers = 4 bytes & double precision = 8 bytes
         before_slice = 6*4 + nq*4 + nr*8 + nr*4 + nth*8
         offset = (tind+1)*rec_size + before_slice - 12
         call fseek(iounit, offset, start_position, ierr)
         read(iounit) time(0)
         read(iounit) iters(0)
      endif
      close(iounit)

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine shell_slice

   !-----------------------------------------------------------------
   ! calculate the power from a Shell Spectra data cube
   !-----------------------------------------------------------------
   subroutine calculate_power(vals_r, vals_i, nell, nm, nr, nq, nrec, power)
      integer, intent(in) :: nrec, nell, nm, nr, nq
      double precision, intent(in) :: vals_r(0:nell-1,0:nm-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(in) :: vals_i(0:nell-1,0:nm-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: power(0:nell-1,0:nr-1,0:nq-1,0:nrec-1,0:2)

      !f2py intent(in) :: vals_r, vals_i, nell, nm, nr, nq, nrec
      !f2py depend(nell,nm,nr,nq,nrec) :: vals_r, vals_i
      !f2py intent(out) :: power
      !f2py depend(nell,nr,nq,nrec) :: power

      integer :: k,q,j,m

      ! calculate power
      do k=0,nrec-1
         do q=0,nq-1
            do j=0,nr-1
               ! m = 0 power
               power(:,j,q,k,1) = power(:,j,q,k,1) + &
                                  vals_r(:,0,j,q,k)**2 + vals_i(:,0,j,q,k)**2
               ! m /= 0 power
               do m=1,nm-1
                  power(:,j,q,k,2) = power(:,j,q,k,2) + &
                                  vals_r(:,m,j,q,k)**2 + vals_i(:,m,j,q,k)**2
               enddo

               ! total power
               power(:,j,q,k,0) = power(:,j,q,k,1) + power(:,j,q,k,2)
            enddo
         enddo
      enddo

   end subroutine calculate_power

   !-----------------------------------------------------------------
   ! read a Shell Spectra file
   !-----------------------------------------------------------------
   subroutine shell_spectra(fname, nrec, nell, nm, nr, nq, nlut, time, iters, &
                            vals_r, vals_i, power, radius, lmax, mmax, rad_ind, &
                            quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nell, nm, nr, nq, nlut

      integer, intent(out) :: lmax, mmax
      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals_r(0:nell-1,0:nm-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: vals_i(0:nell-1,0:nm-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: power(0:nell-1,0:nr-1,0:nq-1,0:nrec-1,0:2)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)
      integer, intent(out) :: rad_ind(0:nr-1)

      !f2py intent(in) :: fname, nrec, nr, nth, nphi, nq, nlut
      !f2py intent(out) :: lmax, mmax
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius, rad_ind
      !f2py depend(nr) :: radius, rad_ind
      !f2py intent(out) :: phi
      !f2py depend(nphi) :: phi
      !f2py intent(out) :: vals_r, vals_i
      !f2py depend(nell,nm,nr,nq,nrec) :: vals_r, vals_i
      !f2py intent(out) :: power
      !f2py depend(nell,nr,nq,nrec) :: power
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: i, j, k, m, l, q, nq_meta, nrec_meta, nr_meta, nell_meta, nm_meta
      character(len=16) :: endian

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) lmax
      nell_meta = lmax+1
      nm_meta = nell_meta
      mmax = nm_meta-1
      read(iounit) nr_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) (rad_ind(i), i=0, nr-1)

      do m=0,nrec-1
          read(iounit) ((((vals_r(i,j,k,l,m),i=0,nell-1),j=0,nm-1),k=0,nr-1),l=0,nq-1)
          read(iounit) ((((vals_i(i,j,k,l,m),i=0,nell-1),j=0,nm-1),k=0,nr-1),l=0,nq-1)
          read(iounit) time(m)
          read(iounit) iters(m)
      enddo
      close(iounit)

      if (version .lt. 4) then
         ! m>0 power is too high by factor of 2, divide complex amplitude by sqrt(2)
         vals_r(:,1:nm-1,:,:,:) = vals_r(:,1:nm-1,:,:,:) / dsqrt(2.0d0)
         vals_i(:,1:nm-1,:,:,:) = vals_i(:,1:nm-1,:,:,:) / dsqrt(2.0d0)
      endif

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

      ! calculate power
      do k=0,nrec-1
         do q=0,nq-1
            do j=0,nr-1
               ! m = 0 power
               power(:,j,q,k,1) = power(:,j,q,k,1) + &
                                  vals_r(:,0,j,q,k)**2 + vals_i(:,0,j,q,k)**2
               ! m /= 0 power
               do m=1,nm-1
                  power(:,j,q,k,2) = power(:,j,q,k,2) + &
                                  vals_r(:,m,j,q,k)**2 + vals_i(:,m,j,q,k)**2
               enddo

               ! total power
               power(:,j,q,k,0) = power(:,j,q,k,1) + power(:,j,q,k,2)
            enddo
         enddo
      enddo

   end subroutine shell_spectra

   !-----------------------------------------------------------------
   ! read a SPH Modes file
   !-----------------------------------------------------------------
   subroutine sph(fname, nrec, nm, nell, nr, nq, nlut, time, iters, &
                       vals, lvals, radius, rad_ind, quantities, lut)
      character(len=256), intent(in) :: fname
      integer, intent(in) :: nrec, nell, nm, nr, nq, nlut

      integer, intent(out) :: lut(0:nlut-1)
      integer, intent(out) :: quantities(0:nq-1)
      integer, intent(out) :: iters(0:nrec-1)
      double precision, intent(out) :: vals(0:1,0:nm-1,0:nell-1,0:nr-1,0:nq-1,0:nrec-1)
      double precision, intent(out) :: time(0:nrec-1)
      double precision, intent(out) :: radius(0:nr-1)
      integer, intent(out) :: rad_ind(0:nr-1)
      integer, intent(out) :: lvals(0:nell-1)

      !f2py intent(in) :: fname, nrec, nell, nm, nr, nq, nlut
      !f2py intent(out) :: time, iters
      !f2py depend(nrec) :: time, iters
      !f2py intent(out) :: radius, rad_ind
      !f2py depend(nr) :: radius, rad_ind
      !f2py intent(out) :: vals
      !f2py depend(nm,nell,nr,nq,nrec) :: vals
      !f2py intent(out) :: lvals
      !f2py depend(nell) :: lvals
      !f2py intent(out) :: quantities
      !f2py depend(nq) :: quantities
      !f2py intent(out) :: lut
      !f2py depend(nlut) :: lut

      integer :: iounit=15, first_number, version
      integer :: i, k, nq_meta, nrec_meta, nr_meta, nell_meta
      integer :: p, qv, rr, nm_loc, lval, lv
      character(len=16) :: endian

      ! check endianness
      call check_endian(fname, endian)
      open(unit=iounit, file=fname, form='unformatted', status='old', &
           access='stream', convert=trim(endian))

      ! read contents of file
      read(iounit) first_number
      read(iounit) version
      read(iounit) nrec_meta
      read(iounit) nell_meta
      read(iounit) nr_meta
      read(iounit) nq_meta

      read(iounit) (quantities(i), i=0, nq-1)
      read(iounit) (radius(i), i=0, nr-1)
      read(iounit) (rad_ind(i), i=0, nr-1)
      read(iounit) (lvals(i), i=0, nell-1)
      vals(:,:,:,:,:,:) = 0.0d0

      do i=0,nrec-1
         do qv=0,nq-1
            do p=0,1
               do rr=0,nr-1
                  do lv=0,nell-1
                     lval = lvals(lv)
                     nm_loc = lval+1
                     if (p .eq. 0) then
                        read(iounit) (vals(0,k,lv,rr,qv,i),k=0,nm_loc-1) ! real part
                     else
                        read(iounit) (vals(1,k,lv,rr,qv,i),k=0,nm_loc-1) ! imag part
                     endif
                  enddo
               enddo
            enddo
         enddo
         read(iounit) time(i)
         read(iounit) iters(i)
      enddo
      close(iounit)

      if (version .lt. 4) then
         ! m>0 power is too high by factor of 2, divide complex amplitude by sqrt(2)
         vals(:,1:nm-1,:,:,:,:) = vals(:,1:nm-1,:,:,:,:) / dsqrt(2.0d0)
      endif

      ! generate the lookup table
      call get_lut(quantities, nq, nlut, lut)

   end subroutine sph

end module read_rayleigh

