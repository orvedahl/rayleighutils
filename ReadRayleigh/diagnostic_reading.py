"""
Module to read Rayleigh diagnostic files.

The diagnostic_reading.py file that ships with the source code
uses pure python to do all the reading and various calculations.

This version uses Fortran to read the binary files. Python is
used as a wrapper to those reading routines that behaves in a
similar manor as the routines that are shipped with the source code.

Classes:
    --ReferenceState
    --GlobalAverage
    --ShellAverage
    --AzAverage
    --Point_Probe
    --Meridional_Slice
    --Equatorial_Slice
    --ShellSlice
    --SPH_Modes
    --ShellSpectra

Other routines:
    --build_file_list

R. Orvedahl June 1, 2017
"""
from __future__ import print_function
import numpy as np
import os
from collections import OrderedDict
from read_rayleigh import read_rayleigh

def get_meta(fname, ftype, itr=None):
    """
    return the meta data of a file
    """
    metadata = OrderedDict()

    # iteration is only used for checkpoint files
    if (itr is None):
        itr = 0

    # call Fortran routine
    meta = read_rayleigh.read_meta(fname, itr, ftype.lower())

    # unpack/store the data
    filename = meta[0]
    metadata['version'] = meta[1]
    metadata['nr']      = meta[2]
    metadata['nth']     = meta[3]
    metadata['nphi']    = meta[4]
    metadata['nell']    = meta[5]
    metadata['nm']      = meta[6]
    metadata['nrec']    = meta[7]
    metadata['nq']      = meta[8]
    metadata['nlut']    = meta[9]
    metadata['nrefs']   = meta[10]

    return filename, metadata

class Grid_Info:
    """
    Read in a grid file
    ------------------------------------------
    self.tpar        : list holding [dt, old_dt]
    self.gpars       : list holding [nr, lmax, grid_type, itr]
    self.radius      : radius array
    self.simtime     : simulation time
    """

    def __init__(self, filename, iteration, path='.'):

        # construct the filename, exclude the iteration bit
        _fname = os.path.join(path, filename)
        send_path = os.path.split(_fname)[0]

        if (iteration < 0):
            is_quicksave = True
        else:
            is_quicksave = False

        # read meta data
        fname, meta = get_meta(send_path, "checkpoint", itr=iteration)

        # allocate space
        nr = meta['nr']
        radius = np.zeros((nr))

        # read data
        sizes, times, radius = read_rayleigh.read_grid(fname, nr, is_quicksave)

        self.gpars   = sizes
        self.simtime = times[0]
        self.tpar    = times[1:]
        self.radius  = radius

class ReferenceState:
    """
    Read in the Reference State
    ------------------------------------------
    self.n_r         : number of radial points
    self.radius      : radial coordinates
    self.density     : density
    self.dlnrho      : logarithmic derivative of density
    self.d2lnrho     : d_by_dr of dlnrho
    self.pressure    : pressure
    self.temperature : temperature
    self.dlnt        : logarithmic derivative of temperature
    self.dsdr        : entropy gradient (radial)
    self.entropy     : entropy
    self.gravity     : gravity
    """

    def __init__(self, filename, path='.'):

        # construct the filename
        _fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(_fname, "reference")

        # allocate space
        nr = meta['nr']; nrefs = meta['nrefs']
        ref_arr = np.zeros((nr, nrefs))
        names = np.array(['']*nrefs)

        # read data
        ref_arr = read_rayleigh.read_reference(fname, nr, nrefs)

        self.radius      = ref_arr[:,0]
        self.density     = ref_arr[:,1]
        self.dlnrho      = ref_arr[:,2]
        self.d2lnrho     = ref_arr[:,3]
        self.pressure    = ref_arr[:,4]
        self.temperature = ref_arr[:,5]
        self.dlnt        = ref_arr[:,6]
        self.dsdr        = ref_arr[:,7]
        self.entropy     = ref_arr[:,8]
        self.gravity     = ref_arr[:,9]
        self.names = ["radius", "density", "dlnrho", "d2lnrho", "pressure",
                      "temperature", "dlnt", "dsdr", "entropy", "gravity"]

class GlobalAverage:
    """
    Rayleigh GlobalAverage Structure
    ------------------------------------------
    self.niter                  : number of time steps
    self.nq                     : number of diagnostic quantities output
    self.qv[0:nq-1]             : quantity codes for the diagnostics output
    self.vals[0:niter-1,0:nq-1] : The globally averaged diagnostics 
    self.iters[0:niter-1]       : The time step numbers stored in this output file
    self.time[0:niter-1]        : The simulation time corresponding to each time step
    self.version                : The version code for this particular output (internal use)
    self.lut                    : Lookup table for the different diagnostics output
    """

    def __init__(self, filename, path='G_Avgs', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "g_avgs")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((nrec, nq))

        # read data
        time, iters, vals, qv, lut = read_rayleigh.global_avg(fname, nrec, nq, nlut)

        self.niter = nrec
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals
            self.lut = lut
        else:
            # return only specified quantities
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            # fancy array indexing using the existing lut
            ind = lut[quantity]
            self.vals = vals[:,ind]

            # now generate a new lut based on new quantity array
            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

        self.iters = iters
        self.time = time
        self.version = meta['version']

class ShellAverage:
    """
    Rayleigh Shell Average Structure
    ------------------------------------------
    self.niter                         : number of time steps
    self.nq                            : number of diagnostic quantities output
    self.nr                            : number of radial points
    self.qv[0:nq-1]                    : quantity codes for the diagnostics output
    self.radius[0:nr-1]                : radial grid
    self.vals[0:n-1,0:3,0:nq-1,0:niter-1] : The spherically averaged diagnostics
                                            0-3 are the moments (index 0 is mean, 1 is
                                            variance, 2 is skewness,index 3 is kurtosis)
    self.iters[0:niter-1]              : The time step numbers stored in this output file
    self.time[0:niter-1]               : The simulation time corresponding to each time step
    self.version                       : Version for this particular output (internal use)
    self.lut                           : Lookup table for the different diagnostics output
    """
    def __init__(self, filename, path='Shell_Avgs', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "shell_avgs")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nr = meta['nr']
        radius = np.zeros((nr))
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((nr, 4, nq, nrec))

        # read data
        fname = fname.strip()
        time, iters, vals, radius, qv, lut = read_rayleigh.shell_avgs(\
                                                fname, nrec, nr, nq, nlut)

        self.niter = nrec
        self.nr = nr
        self.radius = radius
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals[:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

class AzAverage:
    """
    Rayleigh AzAverage Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of radial points
    self.ntheta                                   : number of theta points
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radial grid
    self.costheta[0:ntheta-1]                     : cos(theta grid)
    self.sintheta[0:ntheta-1]                     : sin(theta grid)
    self.vals[0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] : The phi-averaged diagnostics 
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version for this output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """

    def __init__(self, filename, path='AZ_Avgs', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "az_avgs")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nr = meta['nr']; nth = meta['nth']
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((nth, nr, nq, nrec))
        radius = np.zeros((nr))
        costheta = np.zeros((nth)); sintheta = np.zeros((nth))

        # read data
        time, iters, vals, radius, costheta, sintheta, qv, lut = \
                               read_rayleigh.az_avgs(fname, nrec, nr, nth, nq, nlut)

        self.niter = nrec
        self.nr = nr
        self.ntheta = nth
        self.radius = radius
        self.costheta = costheta
        self.sintheta = sintheta
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals[:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

class Point_Probe:
    """
    Rayleigh Point Probes Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of radial points
    self.ntheta                                   : number of theta points
    self.nphi                                     : number of phi points sampled
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radial grid
    self.costheta[0:ntheta-1]                     : cos(theta grid)
    self.sintheta[0:ntheta-1]                     : sin(theta grid)
    self.phi[0:nphi-1]                            : phi values (radians)
    self.phi_indices[0:nphi-1]                    : phi indices (from 1 to nphi)
    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] : The meridional slices 
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version for this output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """
    def __init__(self, filename, path='Point_Probes', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "point_probes")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nphi = meta['nphi']; nth = meta['nth']; nr = meta['nr']
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((nphi, nth, nr, nq, nrec))
        radius = np.zeros((nr)); rad_ind = np.zeros((nr))
        costheta = np.zeros((nth)); sintheta = np.zeros((nth))
        phi = np.zeros((nphi)); phi_ind = np.zeros((nphi))
        theta_ind = np.zeros((nth))

        # read data
        time, iters, vals, radius, costheta, sintheta, \
              phi, rad_ind, theta_ind, phi_ind, qv, lut = read_rayleigh.point_probe(\
                                                 fname, nrec, nphi, nth, nr, nq, nlut)

        self.niter = nrec
        self.nr = nr
        self.ntheta = nth
        self.nphi = nphi
        self.radius = radius
        self.rad_inds = rad_ind - 1 # convert from 1-based to 0-based
        self.costheta = costheta
        self.sintheta = sintheta
        self.theta_inds = theta_ind - 1 # convert from 1-based to 0-based
        self.phi = phi
        self.phi_inds = phi_ind - 1 # convert from 1-based to 0-based
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals[:,:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

class Meridional_Slice:
    """
    Rayleigh Meridional Slice Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of radial points
    self.ntheta                                   : number of theta points
    self.nphi                                     : number of phi points sampled
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radial grid
    self.costheta[0:ntheta-1]                     : cos(theta grid)
    self.sintheta[0:ntheta-1]                     : sin(theta grid)
    self.phi[0:nphi-1]                            : phi values (radians)
    self.phi_indices[0:nphi-1]                    : phi indices (from 1 to nphi)
    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] : The meridional slices 
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version for this output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """

    def __init__(self, filename, path='Meridional_Slices', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "meridional_slices")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nphi = meta['nphi']; nth = meta['nth']; nr = meta['nr']
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((nphi, nth, nr, nq, nrec))
        radius = np.zeros((nr))
        costheta = np.zeros((nth)); sintheta = np.zeros((nth))
        phi = np.zeros((nphi)); phi_ind = np.zeros((nphi))

        # read data
        time, iters, vals, radius, costheta, sintheta, \
              phi, phi_ind, qv, lut = read_rayleigh.meridional_slice(\
                                         fname, nrec, nphi, nth, nr, nq, nlut)

        self.niter = nrec
        self.nr = nr
        self.ntheta = nth
        self.nphi = nphi
        self.radius = radius
        self.costheta = costheta
        self.sintheta = sintheta
        self.phi = phi
        self.phi_inds = phi_ind - 1 # convert from 1-based to 0-based
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals[:,:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

class Equatorial_Slice:
    """
    Rayleigh Equatorial Slice Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of radial points
    self.nphi                                     : number of phi points
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radial grid
    self.vals[0:phi-1,0:nr-1,0:nq-1,0:niter-1]    : The equatorial_slices
    self.phi[0:nphi-1]                            : phi values (in radians)
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version for this output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """

    def __init__(self, filename, path='Equatorial_Slices', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "equatorial_slices")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nphi = meta['nphi']; nr = meta['nr']
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((nphi, nr, nq, nrec))
        radius = np.zeros((nr))
        phi = np.zeros((nphi))

        # read data
        time, iters, vals, radius, phi, qv, lut = read_rayleigh.equatorial_slice(\
                                  fname, nrec, nphi, nr, nq, nlut)

        self.niter = nrec
        self.nr = nr
        self.nphi = nphi
        self.radius = radius
        self.phi = phi
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals[:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

class ShellSlice:
    """
    Rayleigh Shell Slice Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of shell slices output
    self.ntheta                                   : number of theta points
    self.nphi                                     : number of phi points
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radii of the shell slices output
    self.inds[0:nr-1]                             : radial indices of the shell slices output
    self.costheta[0:ntheta-1]                     : cos(theta grid)
    self.sintheta[0:ntheta-1]                     : sin(theta grid)
    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
                                                  : The shell slices 
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version for this output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """

    def __init__(self, filename, path='Shell_Slices', rec0=False, slice_spec=[],
                 quantity=None):
        """
        rec0       --- return only the first time record
        slice_spec --- list that specifies time index, quantity, and radial index
                       for example the first radial index, second time index of entropy
                       is spec=[1, 64, 0], the time index is first, quantity code is second,
                       and the radial index is last.
        quantity   --- list of quantity codes to return. Returns full time and radial
                       information, but only for the specified quantity codes.
        """

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "shell_slices")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nphi = meta['nphi']; nth = meta['nth']; nr = meta['nr']
        if (rec0 or len(slice_spec)==3):
            nrec = 1
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        radius = np.zeros((nr)); rad_ind = np.zeros((nr))
        costheta = np.zeros((nth)); sintheta = np.zeros((nth))
        vals = np.zeros((nphi, nth, nr, nq, nrec))
        if (len(slice_spec)==3):
            spec = np.array(slice_spec)
        else:
            spec = np.array([-50000, -50000, -50000])

        # read data
        time, iters, vals, radius, costheta, sintheta, rad_ind, qv, lut = \
                     read_rayleigh.shell_slice(fname, nrec, nphi, nth, nr, nq, nlut, spec)

        rad_ind = rad_ind - 1 # convert from 1-based to 0-based

        # extract specific radius & quantity
        if (len(slice_spec) == 3):
            tind  = slice_spec[0] # same order as the Fortran code
            qcode = slice_spec[1]
            rind  = slice_spec[2]

            # fix the look up table and the array sizes
            nq = 1; nr = 1
            lut[:] = 1000
            lut[qcode] = 0
            qv       = np.zeros((nq), dtype=np.int32)
            _radius  = np.zeros((nr), dtype=np.float64)
            _rad_ind = np.zeros((nr), dtype=np.int32)

            # extract the proper quantity & radius info
            qv[0] = qcode
            _radius[0] = radius[rind]
            _rad_ind[0] = rad_ind[rind]
            radius = _radius; rad_ind = _rad_ind

            # reshape the values data
            vals = np.reshape(vals[:,:,0,0,0], (nphi, nth, 1,1,1))

        self.niter = nrec
        self.nr = nr
        self.ntheta = nth
        self.nphi = nphi
        self.radius = radius
        self.inds = rad_ind
        self.costheta = costheta
        self.sintheta = sintheta
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if ((quantity is None) or (len(slice_spec) == 3)): # don't undo slice_spec stuff
            self.nq = nq                                   # i.e., you requested a specific
            self.qv = qv                                   # slice, so don't return
            self.vals = vals                               # multiple quantities
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals[:,:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

def CalculatePower(vals):
    """
    given the complex "vals" array from the ShellSpectra, calculate the "lpower" attribute
    """
    nell, nm, nr, nq, nt = np.shape(vals)
    power = np.empty((nell, nr, nq, nt, 3), dtype=np.float64)
    power = read_rayleigh.calculate_power(np.real(vals), np.imag(vals), nell, nm, nr, nq, nt)
    return power

class ShellSpectra:
    """
    Rayleigh Shell Spectrum Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of shell slices output
    self.nell                                     : number of ell values
    self.nm                                       : number of m values
    self.lmax                                     : maximum spherical harmonic degree l
    self.mmax                                     : maximum spherical harmonic degree m
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radii of the shell slices output
    self.inds[0:nr-1]                             : radial indices of the shell slices output
    self.vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1] 
                                                  : The complex spectra of the shells output 
    self.lpower[0:lmax,0:nr-1,0:nq-1,0:niter-1,3] : power as function of ell (sum over m)
                                                  :    0:total,1:m=0, 2:total-m=0 power
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version for this output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """

    def __init__(self, filename, path='Shell_Spectra', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "shell_spectra")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nell = meta['nell']; nm = meta['nm']; nr = meta['nr']
        self.time = np.empty((nrec)); self.iters = np.empty((nrec))
        self.qv = np.empty((nq)); self.lut = np.empty((nlut))
        self.vals = np.empty((nell, nm, nr, nq, nrec), dtype=np.complex128)
        self.lpower = np.empty((nell, nr, nq, nrec, 3))
        self.radius = np.empty((nr)); self.inds = np.empty((nr))

        # read data
        self.time, self.iters, self.vals.real, self.vals.imag, self.lpower, self.radius, \
               lmax, mmax, self.inds, self.qv, self.lut = \
                         read_rayleigh.shell_spectra(fname, nrec, nell, nm, nr, nq, nlut)

        self.inds -= 1 # convert from 1-based to 0-based

        # store the integer data
        self.niter = nrec
        self.nr = nr
        self.nell = nell
        self.nm = nm
        self.lmax = lmax
        self.mmax = mmax
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = self.lut[quantity]
            self.vals = self.vals[:,:,:,ind,:]
            self.lpower = self.lpower[:,:,ind,:,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

class SPH_Modes:
    """
    Rayleigh Shell Spectrum Structure
    ------------------------------------------
    self.niter                                    : number of time steps
    self.nq                                       : number of diagnostic quantities output
    self.nr                                       : number of shell slices output
    self.nell                                     : number of ell values
    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
    self.radius[0:nr-1]                           : radii of the shell slices output
    self.inds[0:nr-1]                             : radial indices of the shell slices output
    self.lvals[0:nell-1]                          : ell-values output
    self.vals[0:lmax,0:nell-1,0:nr-1,0:nq-1,0:niter-1] 
                                                  : The complex spectra of the SPH modes output
                                                  :  (lmax denotes max l-value output; not
                                                  :   the simulation lmax)
    self.iters[0:niter-1]                         : The time step numbers
    self.time[0:niter-1]                          : The simulation time
    self.version                                  : Version code for output (internal use)
    self.lut                                      : Lookup table for different diag outputs
    """
    def __init__(self, filename, path='SPH_Modes', quantity=None):

        # construct the filename
        fname = os.path.join(path, filename)

        # read meta data
        fname, meta = get_meta(fname, "sph_modes")

        # allocate space
        nrec = meta['nrec']; nq = meta['nq']; nlut = meta['nlut']
        nm = meta['nm']; nell = meta['nell']; nr = meta['nr']
        time = np.zeros((nrec)); iters = np.zeros((nrec))
        qv = np.zeros((nq)); lut = np.zeros((nlut))
        vals = np.zeros((2, nm, nell, nr, nq, nrec))
        vals_complex = np.zeros((nm, nell, nr, nq, nrec), dtype=np.complex128)
        lvals = np.zeros((nell))
        radius = np.zeros((nr)); rad_ind = np.zeros((nr))

        # read data
        time, iters, vals, lvals, radius, rad_ind, qv, lut = read_rayleigh.sph(\
                                          fname, nrec, nm, nell, nr, nq, nlut)

        # Fortran returns real & imag part as separate float arrays,
        # combine them into single complex array
        vals_complex.real = vals[0,:,:,:,:,:]
        vals_complex.imag = vals[1,:,:,:,:,:]
        vals = None

        self.niter = nrec
        self.nr = nr
        self.nell = nell
        self.radius = radius
        self.inds = rad_ind - 1 # convert from 1-based to 0-based
        self.lvals = lvals
        self.iters = iters
        self.time = time
        self.version = meta['version']
        if (quantity is None):
            self.nq = nq
            self.qv = qv
            self.vals = vals_complex
            self.lut = lut
        else:
            quantity = list(quantity)
            self.nq = len(quantity)
            self.qv = np.array(quantity, dtype=np.int32)

            ind = lut[quantity]
            self.vals = vals_complex[:,:,:,ind,:]

            self.lut = read_rayleigh.get_lut(self.qv, self.nq, nlut)

def build_file_list(istart, iend, path='.', diter=-1, ndig=8, special=False):
    """
    Generate a list of files in the given path. The filenames are
    assumed to have the form:
        00005000, 00010000, 00015000, etc.
    such that they can be converted to an integer.

    If diter < 1 (default), then we use the equivalent of the 'ls' command
    to generate the file list. The filenames are still assumed to be integers
    as shown above.

    If diter >= 1, the length is set by "ndig" and the difference between
    each file iteration is given by "diter".

    The special keyword is included for backwards compatability. I cannot
    find where it is actually used in the Rayleigh source code.

    The returned file list contains all files in [istart, iend], inclusive.
    """
    files = []
    if (diter < 1):
        # Examine the directory and grab all files that fall between istart and iend
        allfiles = os.listdir(path)
        allfiles.sort()
        for f in allfiles:
            if ( ('special' in f) and special ):
                fint = int(f[0:7])
                if ( (fint >= istart ) and (fint <= iend)  ):
                    files.append(os.path.join(path, f))
            if ( (not 'special' in f) and not(special) ):
                fint = int(f)
                if ( (fint >= istart ) and (fint <= iend)  ):
                    files.append(os.path.join(path, f))
    else:
        # Generate filename manually (no ls)
        i = istart
        digmod = "%0"+str(ndig)+"d"
        while (i <= iend):
            fiter = digmod % i           
            if (special):
                fiter=fiter+'_special'
            files.append(os.path.join(path, fiter))
            i = i+diter
    return files

