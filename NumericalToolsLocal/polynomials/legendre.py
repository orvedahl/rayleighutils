"""
Module to handle various Legendre operations

    P_0 = 1
    P_1 = x

    (n+1)*P_{n+1} = (2*n+1)*x*P_{n} - n*P_{n-1}
"""

from __future__ import print_function
import numpy as np
from .common import clenshaw, legendre_to_chebyshev
from ..misc import grids
from ..misc.config import config

f90_support = config.getboolean("installed-options", "f90support")

if (f90_support):
    from ..f90 import legendref90

def DEVEL_integrate(fx, x, domain, endpoints=False, axis=-1):
    return

def DEVEL_derivative(fx, x, domain, endpoints=False, axis=-1):
    return

class Legendre():

    def __init__(self, f, a=-1.0, b=1.0, endpoints=False):

        self.a = a   # lower boundary
        self.b = b   # upper boundary

        self.fx = f        # function value at grid points, increasing array indices
        self.Num = len(f)  # are assumed to correspond to increasing grid values
                           # so if the function is f(x) = x then the ordering is
                           # assumed to be f_0 < f_1 < ... < f_i < ... < f_n-2 < f_n-1

        self.endpoints = endpoints

        # generate the grid
        self.grid = grids.LegendreGrid(self.Num, a=self.a, b=self.b,
                                       endpoints=self.endpoints)

        self.x = self.grid.x
        self.deriv_coeff = self.grid.deriv_coeff
        self.integ_coeff = self.grid.integ_coeff

        # get Legendre coefficients
        self.a_k = self._get_coeffs()

        # storage
        self.dfdx = None            # first derivative coefficients
        self.def_integral = None    # value of definite integral over whole domain
        self.Fx = None              # indefinite integral coefficients

    def _get_coeffs(self):
        """
        return Legendre coefficients a_k
           f(x) = sum a_k & P_k(x)

           a_k = (2*k+1)/2 * int_{-1}^{1} f(x)*P_k(x) dx
               = (2*k+1)/2 * sum_n w(x_n)*f(x_n)*P_k(x_n)
        """
        if (f90_support):
            coeffs = legendref90.legendref90.get_coeffs(self.Num, self.x, self.Num,\
                                                    self.grid.weights, self.fx)
        else:
            coeffs = np.zeros((self.Num))
            for l in range(self.Num):
                C = (2.*l + 1.)/2.
                Pl, dPldx = self.grid._eval_nth_legendre(self.x, l)

                coeffs[l] = C*np.sum(self.grid.weights*self.fx*Pl)

        return coeffs

    def _from_ab(self, r):
        return self.grid._from_ab(r)

    def _to_ab(self, x):
        return self.grid._to_ab(x)

    def deriv(self, x, return_instance=False):
        """
        derivative of Legendre series at x
          f(x)  = sum a_k * P_k(x)
          f'(x) = sum a_k * P'_k(x) = sum b_k * P_k(x)
          relate b_k to a_k using a three term recurrsion
          see Boyd's 2001 book equation A.41 of Appendix A
        """
        y = self._from_ab(x)
        C = self.deriv_coeff

        # ----EXPERIMENTAL WAY
        #     f(x) = sum_k=0^N alpha_k * P_k(x)
        #
        #     f'(x) = sum_k=1^N alpha_k P'_k(x) = sum_k=0^N alpha'_k P_k(x), with alpha'_N = 0
        #
        #     use:
        #         P_k = (P'_{k+1} - P'_{k-1}) / (2*k+1)   k>=1
        #
        #     to find:
        #         alpha'_N = 0
        #
        #         alpha'_{N-1} = (2*N-1)*alpha_N
        #
        #         alpha'_{k-1} = (2*k-1) * [alpha_k + alpha'_{k+1}/(2*k+3)]   k=N-1,N-2,...,1
        #
        beta = np.zeros_like(self.a_k)
        N = len(beta)
        beta[N-1] = 0.
        beta[N-2] = (2*N-3)*self.a_k[N-1]
        for k in range(N-2, 0, -1):
            beta[k-1] = (2.*k-1.) * (self.a_k[k] + beta[k+1]/(2.*k+3.))
        dfdx = C*clenshaw(y, beta, func='Pn')
        # ----EXPERIMENTAL WAY

        # ----CURRENT WAY
        # evaluate on supplied grid using different basis functions
        #dfdx = C*clenshaw(y, self.a_k, func='dPndx')
        # ----CURRENT WAY

        if (return_instance):
            return Legendre(dfdx, a=self.a, b=self.b, endpoints=self.endpoints)
        else:
            return dfdx

    def integ(self):
        """
        definite integral of Legendre series over whole domain

          int_a^b f(x) dx = sum_k weights_k * f(x_k)

          weights_k = integration weights for Legendre polynomials
        """
        if (self.def_integral is not None):
            # save some work if it was already calculated
            return self.def_integral

        # use indefinite integral to calculate definite integral
        if (not self.endpoints):
            self.def_integral = self.integ_coeff*np.sum(self.fx*self.grid.weights)
        else:
            self.def_integral = self.antideriv(self.b) - self.antideriv(self.a)

        return self.def_integral

    def antideriv(self, x, return_instance=False):
        """
        indefinite integral of Legendre series

          int_a^x P_n(x) dx = P_{n+1}/(2*n+1) - P_{n-1}/(2*n+1)
        """
        y = self._from_ab(x)
        C = self.integ_coeff

        if (self.Fx is None):
            coeffs = np.zeros_like(self.a_k)
            N = len(coeffs)
            for k in range(2,N-1):
                coeffs[k] = self.a_k[k-1]/(2.*k-1.) - self.a_k[k+1]/(2.*k+3.)
            coeffs[ 0 ] = -self.a_k[1]/3.
            coeffs[ 1 ] = self.a_k[0] - self.a_k[2]/5.
            coeffs[N-1] = self.a_k[N-2]/(2.*N-3.)

            # set F(a) = F(-1) = 0
            if (not self.endpoints):
                # using f(-1) = sum a_k * P_k(-1) = sum a_k * (-1)**k
                p = np.ones_like(coeffs)
                p[1::2] *= -1.0
                Const = -np.sum(p*coeffs)
            else:
                # using f(xlo) = sum a_k * P_k(xlo)
                Const = -clenshaw(self.grid.xlo, coeffs, func='Pn')
            coeffs[0] += Const

            self.Fx = coeffs

        Fx = C*clenshaw(y, self.Fx, func='Pn')

        if (return_instance):
            return Legendre(Fx, a=self.a, b=self.b, endpoints=self.endpoints)
        else:
            return Fx

    def __call__(self, x):
        """
        evaluate f(x) = sum_0^n a_k * P_k(x) at the given x points
        """
        y = self._from_ab(x)
        return clenshaw(y, self.a_k, func='Pn')

def antiderivative(fx, x, domain, endpoints=False):
    """
    indefinite integral of data on a legendre grid evaluated at given x pts
        F(x) = int_a^x f(t) dt
    constant chosen s.t. F(a) = 0
    """
    leg = Legendre(fx, a=domain[0], b=domain[1], endpoints=endpoints)
    Fx = leg.antideriv(x)
    return Fx

def integral(fx, domain, endpoints=False):
    """
    definite integral of data on a legendre grid, returns single number
        I = int_a^b f(x) dx
    """
    leg = Legendre(fx, a=domain[0], b=domain[1], endpoints=endpoints)
    I = leg.integ()
    return I

def derivative(fx, x, domain, endpoints=False):
    """
    1st deriv of data on a legendre grid evaluated at given x points
    """
    leg = Legendre(fx, a=domain[0], b=domain[1], endpoints=endpoints)
    dfdx = leg.deriv(x)
    return dfdx

# VERY BETA right now...
def dfdx_using_chebyshev(fx, x, domain, endpoints=False):
    """
    1st deriv of data on a legendre grid evaluated at given x points
    """
    a = domain[0]; b = domain[1]

    # build Legendre object
    leg = Legendre(fx, a=a, b=b, endpoints=endpoints)

    # build Chebyshev grid and interpolate Legendre data onto it
    cheb_grid = grids.ChebGrid(len(fx), a=a, b=b, endpoints=endpoints, zeros=True)
    cheb = Chebyshev(leg(cheb_grid.xab), a=a, b=b, zeros=True, endpoints=endpoints)

    # use Chebyshev to evaluate derivative at given x points
    dfdx = cheb.deriv(x)

    return dfdx

# VERY BETA right now...
def DM(xgrid):
    """
    1st derivative differentiation matrix
    """
    N = len(xgrid)
    Pn, dPndx = grids.evaluate_Pn(xgrid, N)
    inv_dPndx = 1./dPndx

    # assume xgrid is 1d
    xi = xgrid[np.newaxis, :]
    xj = xgrid[:, np.newaxis]
    xdiff = xi - xj

    Js = np.arange(N, dtype=np.int32)
    dm = np.zeros((N,N))

    for i in range(N):
        J = np.delete(Js, i) # avoid i=j entries
        dm[i,J] = dPndx[i]*inv_dPndx[J]*xdiff[i,J]

    return dm

def is_grid_legendre(grid, domain, tol=5e-14):
    """
    determine if grid is a legendre grid. grid is assumed in [a,b]
    """
    # grid is assumed to be monotonically increasing
    if (grid[1] < grid[0]):
        grid = grid[::-1]

    N = len(grid); a = domain[0]; b = domain[1]

    # try traditional grid
    leg_grid = grids.LegendreGrid(N, a=a, b=b, endpoints=False)
    y = leg_grid.x
    x = leg_grid._from_ab(grid)

    # if difference between incoming grid and the legendre
    # grid is larger than the tolerance, then the grid is not legendre
    if (np.any(np.greater(np.abs(x-y), tol))):
        is_legendre_trad = False
    else:
        is_legendre_trad = True
    max_error_trad = np.amax(np.abs(x-y))

    # try endpoints=True grid
    leg_grid = grids.LegendreGrid(N, a=a, b=b, endpoints=True)
    y_end = leg_grid.x
    x_end = leg_grid._from_ab(grid)
    if (np.any(np.greater(np.abs(x_end-y_end), tol))):
        is_legendre_end = False
    else:
        is_legendre_end = True
    max_error_end = np.amax(np.abs(x_end-y_end))

    # a bunch of logic...
    if (is_legendre_trad or is_legendre_end):
        is_legendre = True
        if (is_legendre_trad):
            endpoints = False
        else:
            endpoints = True
    else:
        is_legendre = False
        endpoints = None

    results = {'legendre':is_legendre, 'endpoints':endpoints,
               'error':max_error_trad, 'endpoints-error':max_error_end}
    return results

