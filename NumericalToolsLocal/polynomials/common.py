"""
Module to hold various routines used by all polynomial types
"""

from __future__ import print_function
import numpy as np
from scipy.special import gamma as Gamma
from sys import exit

def clenshaw(x, c, func='Tn'):
    """
    evaluate the sum at the given x,
       f(x) = sum_0^n c_k * F_k(x)
    using the expansion coefficients c_k and the known
    three term recurrence relation for F_k(x). the keyword
    "func" determines which basis functions are used

    --x is array like that specifies evaluation points
    --c is array of coefficients

    Clenshaw Recurrence:
       1) define function expansion
            f(x) = sum_{0}^{N} c_k * F_k(x)

       2) define known recurrence relation
            F_{n+1} = alpha(n,x)*F_{n} + beta(n,x)*F_{n-1}

            Cheb 1st kind, T_n       : alpha = 2*x, beta = -1
            Cheb 2nd kind, U_n       : alpha = 2*x, beta = -1
            Legendre,      P_n       : alpha = (2*n+1)*x/(n+1), beta = -n/(n+1)
            d(Legendre)/dx = dP_n/dx : alpha = (2*n+1)*x/n, beta = -(n+1)/n
            d(Cheb)/dx = dT_n/dx     : alpha = (n+1)*2*x/n, beta = -(n+1)/(n-1)

       3) define
            a) y_{N+2} = y_{N+1} = 0
            b) y_k = alpha(k,x)*y_{k+1} + beta(k+1,x)*y_{k+2} + c_{k}

       4) backsolve and plug in to get final sum (lots of stuff cancels)
            f(x) = c_{0}*F_{0}(x) + y_{1}*F_{1}(x) + beta(1,x)*F_{0}(x)*y_{2}
            note: y_{0} is not used

            Cheb 1st kind, T_n: F_0 = 1, F_1 = x
            Cheb 2nd kind, U_n: F_0 = 1, F_1 = 2*x
            Legendre,      P_n: F_0 = 1, F_1 = x
    """
    func = func.lower()
    supported = ["tn", "un", "pn", "dpndx", "dtndx"]
    if (func not in supported):
        print("\n---ERROR: clenshaw, function not supported, func = {}".format(func))
        print("\tsupported funcs = {}".format(supported))
        exit()

    x = np.asarray(x); c = np.asarray(c); N = np.shape(c)[0]

    if (len(np.shape(x)) > 1): # larger than 1d array
        print("\n---ERROR: clenshaw, x must be scalar or 1d array, shape(x) = {}".format(\
                  np.shape(x)))
        exit()

    # define alpha, beta, and first two basis functions
    if (func == 'tn'):
        # Chebyshev of 1st kind
        def get_alpha(n, x):
            return 2.*x
        def get_beta(n, x):
            return -np.ones_like(x)
        F0 = np.ones_like(x); F1 = x

    elif (func == 'dtndx'):
        # Chebyshev of 1st kind
        def get_alpha(n, x):
            return 2.*x*(n+1.)/float(n)
        def get_beta(n, x):
            if (n != 1):
                return -(n+1.)/(n-1.)*np.ones_like(x)
            else:
                return -2.0*np.ones_like(x)
        F0 = np.zeros_like(x); F1 = np.ones_like(x)

    elif (func == 'un'):
        # Chebyshev of 2nd kind
        def get_alpha(n, x):
            return 2.*x
        def get_beta(n, x):
            return -np.ones_like(x)
        F0 = np.ones_like(x); F1 = 2.*x

    elif (func == 'pn'):
        # Legendre
        def get_alpha(n, x):
            return (2.*n+1.)*x/(n+1.)
        def get_beta(n, x):
            return -n/(n+1.)*np.ones_like(x)
        F0 = np.ones_like(x); F1 = x

    elif (func == 'dpndx'):
        # 1st derivative of Legendre
        def get_alpha(n, x):
            return (2.*n+1.)*x/float(n)
        def get_beta(n, x):
            return -(n+1.)/float(n)*np.ones_like(x)
        F0 = np.zeros_like(x); F1 = np.ones_like(x)

    if (len(np.shape(c)) > 1):
        # modify the dimensions of c
        d = np.reshape(c, np.shape(c)+(1,))
        bkp1 = bkp2 = np.zeros_like(c[0])     # initialize: b_{k+1} = b_{k+2} = 0
        I = np.ones_like(x)
        I = np.reshape(I, np.shape(I)+(1,))
        fx = np.zeros_like(np.inner(d[0], I)) # remove the 'l' dimension, replace with 'x'

        if (len(np.shape(x)) == 0):
            x = np.array([x])
        M = np.shape(x)[0]

        for j in range(M):
            bkp1[...] = 0.0
            bkp2[...] = 0.0
            for k in range(N-1, 0, -1):            # loop from N-1 --> 1, inclusive
                alpha = get_alpha(k,x[j])          # evaluate alpha/beta at k & x
                beta  = get_beta(k+1,x[j])
                bk = c[k] + bkp1*alpha + bkp2*beta # recursion
                bkp2 = bkp1                        # after loop, b[2] will be stored in bkp2
                bkp1 = bk                          # after loop, b[1] will be stored in bkp1
            beta = get_beta(1,x[j])
            fx[...,j] = c[0]*F0[j] + bkp1*F1[j] + bkp2*beta*F0[j]

        # make the spatial dimension first
        fx = np.swapaxes(fx, -1, 0)

    else:
        bkp1 = bkp2 = 0.0
        for k in range(N-1, 0, -1):
            alpha = get_alpha(k,x)
            beta  = get_beta(k+1,x)
            bk = c[k] + alpha*bkp1 + beta*bkp2
            bkp2 = bkp1
            bkp1 = bk
        beta = get_beta(1,x)
        fx = F0*c[0] + F1*bkp1 + beta*F0*bkp2

    return fx

# VERY BETA right now...
def legendre_to_chebyshev(alpha):
    """
    given the coefficients of a legendre series

        f(x) = sum_n=0^N alpha_n * P_n(x)

    convert to coefficients of a chebyshev series

        f(x) = sum_n=0^N beta_n * T_n(x)

    using matrix method:

        beta = M . alpha
    """
    alpha = np.asarray(alpha)
    N = len(alpha)
    M = np.zeros((N,N))

    for j in range(0,N,2):     # 0 <= j <= N-1, but only even j
        L = _alt_Gamma(0.5*j+0.5)/_alt_Gamma(0.5*j+1.0)/np.pi
        for i in range(0,j+1): # 0 <= i <= j
            M[i,j] = L

    for j in range(N):         # 0 <= j <= N-1
        for i in range(1,j+1): # 0 <  i <= j
            if ((i+j)%2 == 1):
                continue
            # only do this when (i+j) is even
            L = _alt_Gamma((j-i)*0.5+0.5)/_alt_Gamma((j+i)*0.5+1.0)
            M[i,j] = L*2./np.pi

    beta = np.dot(M, alpha)
    return beta

def _alt_Gamma(z, eps=1e-1):
    """
    alternate way of calculating the Gamma function
    """
    z = np.asarray(z)
    if (len(np.shape(z)) == 0):
        Nmax = int(z/eps)
    else:
        Nmax = int(np.amax(z)/eps)
    result = -np.log(z)
    for i in range(1,Nmax):
        inv_i = 1./i
        result += z*np.log(1. + inv_i) - np.log(1. + z*inv_i)
    result = np.exp(result)
    return result

