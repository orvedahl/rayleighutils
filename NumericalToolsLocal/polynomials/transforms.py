"""
Module to perform various polynomial transforms including

    Legendre Transforms
    Chebyshev Transforms
"""

from __future__ import print_function
import numpy as np
from .common import clenshaw
from ..fft import fft
from ..misc import grids
from ..misc.config import config
from ..misc.array import SwapAxis, ReverseAxis

f90_support = config.getboolean("installed-options", "f90support")

if (f90_support):
    from ..f90 import legendref90
else:
    raise ImportError("No f90 support, cannot use Fortran-Legendre-Transforms")

Plm       = None # 2D array of P_l^m(x), m=0
L_xgrid   = None # zeros of Legendre polynomials
L_weights = None # integration weights for Legendre polynomials
Legendre_Initialized = False

C_xgrid   = None # Chebyshev grid points based on zeros/extrema
C_grid    = None # Chebyshev grid object
C_zeros   = None # using zeros or extrema
C_domain  = None # domain of Cheb grid
Chebyshev_Initialized = False

####################################################################################
# Legendre Transforms
####################################################################################
def InitializeLegendreTransform(N, lmax):
    """
    initialize the legendre grid, weights and Plm arrays
    """
    global L_xgrid, L_weights, Plm, Legendre_Initialized
    L_xgrid, L_weights = legendref90.legendref90.roots(N)
    Plm  = legendref90.legendref90.compute_plm(L_xgrid, N, lmax)
    Legendre_Initialized = True

def FinalizeLegendreTransform():
    """
    deallocate the legendre grid, weights and Plm arrays
    """
    global L_xgrid, L_weights, Plm, Legendre_Initialized
    Plm       = None
    L_xgrid   = None
    L_weights = None
    Legendre_Initialized = False

def LegendreTransform(data_in, axis=-1):
    """
    forward transform of data: given F(x), find C s.t. F(x) = sum C(l)*P_l^m(x)

    data_in (physical grid data) may be 1d, 2d, 3d, or 4d:

        F(x      ) = sum C(l      )*P_l(x)
        F(x,t    ) = sum C(l,t    )*P_l(x)
        F(x,y,t  ) = sum C(l,y,t  )*P_l(x)
        F(x,y,z,t) = sum C(l,y,z,t)*P_l(x)
    """
    global L_weights, Plm, Legendre_Initialized

    if (not Legendre_Initialized):
        raise ValueError("Must first call InitializeLegendreTransform to initialize arrays")

    # get physical/spectral shapes and error trap
    nx, nl = np.shape(Plm)
    if (np.shape(data_in)[axis] != nx):
        raise ValueError("x-shape of Plm array must be consistent with data[axis] shape")

    # make the to-be-transformed axis first
    data = SwapAxis(data_in, axis, 0)
    shape = np.shape(data)

    # make data be a 4D array
    if (len(shape) == 1):
        data = np.reshape(data, (shape[0],1,1,1))

    elif (len(shape) == 2):
        data = np.reshape(data, (shape[0],shape[1],1,1))

    elif (len(shape) == 3):
        data = np.reshape(data, (shape[0],shape[1],shape[2],1))

    elif (len(shape) > 4):
        raise ValueError("Legendre Transforms not coded for > 4D data sets")

    _nx, ny, nz, nt = np.shape(data)
    coeffs = legendref90.legendref90.legendre_transform_4d(data, L_weights, Plm,
                                                           nx, ny, nz, nt, nl)

    # restore incoming order of axes
    coeffs = SwapAxis(coeffs, 0, axis)

    return coeffs

def iLegendreTransform(data_in, axis=-1):
    """
    reverse transform of data: given C(l), find F s.t. F(x) = sum C(l)*P_l^m(x)

    data_in (spectral coefficients) may be 1d, 2d, 3d, or 4d:

        F(x      ) = sum C(l      )*P_l(x)
        F(x,t    ) = sum C(l,t    )*P_l(x)
        F(x,y,t  ) = sum C(l,y,t  )*P_l(x)
        F(x,y,z,t) = sum C(l,y,z,t)*P_l(x)
    """
    global Plm, Legendre_Initialized

    if (not Legendre_Initialized):
        raise ValueError("Must first call InitializeLegendreTransform to initialize arrays")

    # get physical/spectral shapes and error trap
    nx, nl = np.shape(Plm)
    if (np.shape(data_in)[axis] != nl):
        raise ValueError("l-shape of Plm array must be consistent with data[axis] shape")

    # make the to-be-transformed axis first
    data = SwapAxis(data_in, axis, 0)
    shape = np.shape(data)

    # make data be a 4D array
    if (len(shape) == 1):
        data = np.reshape(data, (shape[0],1,1,1))

    elif (len(shape) == 2):
        data = np.reshape(data, (shape[0],shape[1],1,1))

    elif (len(shape) == 3):
        data = np.reshape(data, (shape[0],shape[1],shape[2],1))

    elif (len(shape) > 4):
        raise ValueError("Inverse Legendre Transforms not coded for > 4D data sets")

    _nx, ny, nz, nt = np.shape(data)
    fx = legendref90.legendref90.i_legendre_transform_4d(data, Plm, nx, ny, nz, nt, nl)

    # restore incoming order of axes
    fx = SwapAxis(fx, 0, axis)

    return fx

####################################################################################
# Chebyshev Transforms
####################################################################################
def InitializeChebyshevTransform(N, a=-1, b=1, zeros=True, endpoints=False):
    """
    initialize the chebyshev grid and arrays
    """
    global C_xgrid, C_zeros, C_domain, C_grid, Chebyshev_Initialized

    C_grid = grids.ChebGrid(N, a=a, b=b, zeros=zeros, endpoints=endpoints)

    C_xgrid  = C_grid.xab
    C_zeros  = zeros
    C_domain = [a,b]
    Chebyshev_Initialized = True

def FinalizeChebyshevTransform():
    """
    deallocate the chebyshev grid and arrays
    """
    global C_xgrid, C_zeros, C_domain, C_grid, Chebyshev_Initialized
    C_xgrid  = None
    C_zeros  = None
    C_domain = None
    C_grid   = None
    Chebyshev_Initialized = False

def ChebyshevTransform(data_in, axis=-1):
    """
    forward transform of data: given F(x), find C s.t. F(x) = sum C(m)*T_m(x)
    """
    global C_zeros, C_xgrid, Chebyshev_Initialized

    if (not Chebyshev_Initialized):
        raise ValueError("Must first call InitializeChebyshevTransform to initialize arrays")

    # make transform axis first
    data = SwapAxis(data_in, axis, 0)

    N = len(C_xgrid)

    if (C_zeros):
        dct_type = 2
        coeffs = fft.DCT(data, type=dct_type, norm=None, window='none', axis=0)
        coeffs /= N      # adjust the normalization
        coeffs[0] *= 0.5 # first coefficient is special

    else:
        k = N - 1 - np.arange(N-1)
        A = np.hstack((data[0:N-1], data[k]))
        F = fft.iFFTc(A, n=None, axis=0)
        coeffs = np.hstack((F[0], 2*F[1:N-1], F[N-1]))
        coeffs = np.real(coeffs)

    # restore incoming order of axes
    coeffs = SwapAxis(coeffs, 0, axis)

    return coeffs

def iChebyshevTransform(data_in, axis=-1):
    """
    inverse transform of data: given F(x), find C s.t. F(x) = sum C(m)*T_m(x)
    """
    global C_zeros, C_grid, Chebyshev_Initialized

    if (not Chebyshev_Initialized):
        raise ValueError("Must first call InitializeChebyshevTransform to initialize arrays")

    # make transform axis first
    data = SwapAxis(data_in, axis, 0)

    fx = clenshaw(ReverseAxis(C_grid.x), data, func='Tn')

    # restore incoming order of axes
    fx = SwapAxis(fx, 0, axis)

    return fx

