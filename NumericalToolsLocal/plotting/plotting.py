"""
Module for various plotting tasks

Methods:
    --wrapper to pcolormesh that takes care of index order
    --return a plotting color given a data value, data range and colormap
    --center a colorbar on a given data value
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from numpy import ma
from matplotlib import cbook
from matplotlib.colors import Normalize, SymLogNorm

def discrete_cmap(N, cmap='jet'):
    """
    make an N-bin discrete colormap based on the specified cmap
    """
    base = plt.cm.get_cmap(cmap)
    color_list = base(np.linspace(0,1,N))
    cmap_name = base.name + str(N)

    return base.from_list(cmap_name, color_list, N)

def get_color(x, xmin=None, xmax=None, log=False, cmap='jet'):
    """
    convert x in [xmin,xmax] into a color for plotting
    if x is outside this range, returns colors associated
    with xmin & xmax, which ever is closer
    """
    mapcolor = plt.get_cmap(cmap)               # get colormap
    if (log):
        xnorm = Normalize(vmin=xmin, vmax=xmax) # get scaling function
    else:
        lthresh = 1e-2*np.amax(x)
        xnorm = SymLogNorm(lthresh, vmin=xmin, vmax=xmax)
    col = mapcolor(xnorm(x))                    # apply colormap & scaling to x
    return col

def plot2d(x, y, data, order='xy', **kwargs):
    """
    wrapper to pcolormesh

    input values
    -------------
    x      -  1d array of x-axis values
    y      -  1d array of y-axis values
    data   -  2d array of data values on the grid
    order  -  string specifying the order of the data array
                'xy' --> data[x,y]
                'yx' --> data[y,x]

    other recognized options to pcolormesh passed using kwargs:

      cmap - specify the colormap
      vmin - specify lower bound on data
      vmax - specify upper bound on data

    returns the image object using pcolormesh
    """
    supported_order = ['xy', 'yx']
    if (order not in supported_order):
        print("\n---ERROR: order not recognized for plot2d, order = {}".format(order))
        print("\tsupported order = {}".format(supported_order))

    # generate grid
    X, Y = np.meshgrid(x, y, indexing='ij')

    # plot data on grid making sure it has the proper ordering
    if (order == 'yx'):
        data = np.transpose(data)
    image = plt.pcolormesh(X, Y, data, **kwargs)

    return image

class MidPointNorm(Normalize):    
    """

    -----> Found this code on stackoverflow written by Annan <-----

    Center the colorbar by subclassing Normalize. To use it:

        norm = MidPointNorm(midpoint=3)
        imshow(X, norm=norm)

    """

    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
        Normalize.__init__(self, vmin, vmax, clip)
        self.midpoint = midpoint

    def __call__(self, value, clip=None):

        if clip is None:
            clip = self.clip

        result, is_scalar = self.process_value(value)

        self.autoscale_None(result)
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if not (vmin < midpoint < vmax):
            raise ValueError("midpoint must be between maxvalue and minvalue.")       
        elif vmin == vmax:
            result.fill(0) # Or should it be all masked? Or 0.5?
        elif vmin > vmax:
            raise ValueError("maxvalue must be bigger than minvalue")
        else:
            vmin = float(vmin)
            vmax = float(vmax)
            if clip:
                mask = ma.getmask(result)
                result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                  mask=mask)

            # ma division is very slow; we can take a shortcut
            resdat = result.data

            # First scale to -1 to 1 range, than to from 0 to 1.
            resdat -= midpoint            
            resdat[resdat>0] /= abs(vmax - midpoint)            
            resdat[resdat<0] /= abs(vmin - midpoint)

            resdat /= 2.
            resdat += 0.5
            result = ma.array(resdat, mask=result.mask, copy=False)                

        if is_scalar:
            result = result[0]            

        return result

    def inverse(self, value):

        if not self.scaled():
            raise ValueError("Not invertible until scaled")

        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if cbook.iterable(value):
            val = ma.asarray(value)
            val = 2 * (val-0.5)  
            val[val>0]  *= abs(vmax - midpoint)
            val[val<0] *= abs(vmin - midpoint)
            val += midpoint
            return val
        else:
            val = 2 * (val - 0.5)
            if val < 0: 
                return  val*abs(vmin-midpoint) + midpoint
            else:
                return  val*abs(vmax-midpoint) + midpoint

