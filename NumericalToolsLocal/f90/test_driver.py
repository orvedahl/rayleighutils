"""
Test the Fortran reading routines and their wrappers.

Usage:
    test_driver.py [options]

Options:
    --data=<d>   What data to use in the test [default: shavg]
    --call=<c>   What routine to use in the call [default: wrap]
    --list       List allowed input parameters [default: False]
"""

from __future__ import print_function
import os
import sys
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import time
import numpy as np
import math
import legendref90 # fortran routines
import NumericalTools.public as NT

def main():

    verbose = False

    print("\n...using fortran routines\n")

    lmax = 511         # what l to evaluate P_l(x)
    m = 3*(lmax+1)/2 # number of grid points to use

    if (False):
        print("\nNumericalTools-based routines")

        print("\n...testing f(x) = sin(x)")
        grid = NT.LegendreGrid(m)
        x = grid.x
        fx = np.sin(x)

        L = NT.Legendre(fx)

        #          module file - module name - function
        Pl, dPldx = legendref90.legendref90.eval_nth_legendre(x, m, lmax)

        # get NT version
        NT_Pl, NT_dPldx = grid._eval_nth_legendre(x, lmax)

        if (verbose):
            print('x\t\tError in P_l(x)\t\tError in dPdx(x)')
            for i in range(m):
                print("{:+.6f}\t{:+.6e}\t\t{:+.6e}".format(x[i], Pl[i]-NT_Pl[i],
                      dPldx[i]-NT_dPldx[i]))

        w = L.grid.weights
        stime = time.time()
        coeffs = legendref90.legendref90.get_coeffs(m, x, m, w, fx)
        etime = time.time()
        if (verbose):
            print("\ncoeffs Error")
            for i in range(m):
                print("\t{:+.6e}".format(coeffs[i]-L.a_k[i]))
        print("\n\tmax error in coeffs = {}".format(np.amax(np.abs(coeffs[:]-L.a_k[:]))))
        print("\n\tTime to do transform = {} seconds\n".format(etime-stime))

    else:
        print("\nRayleigh-based routines")
        xgrid, weights = legendref90.legendref90.roots(m) # 1d arrays
        print("\n...testing f(x) = sin(x)")
        fx = np.sin(xgrid)
        Plx = legendref90.legendref90.compute_plm(xgrid, m, lmax) # 2d array
        stime = time.time()
        a_k = legendref90.legendref90.legendre_transform(fx, weights, Plx, m, lmax+1, 1)
        etime = time.time()
        f_x = legendref90.legendref90.i_legendre_transform(a_k, Plx, lmax+1, m, 1)
        if (verbose):
            print('x\t\tError in f(x)')
            for i in range(m):
                print("{:+.6f}\t{:+.6e}".format(xgrid[i], fx[i]-f_x[i,0]))
        print("\n\tmax error in f(x) = {}".format(np.amax(np.abs(fx[:]-f_x[:,0]))))
        print("\n\tTime to do transform = {} seconds\n".format(etime-stime))

        print("\n...testing f(x) = x**3 + x**2 + x + 1")
        fx = xgrid**3 + xgrid**2 + xgrid + 1.0
        ak = np.zeros((lmax+1))
        ak[0] = 4.0/3.0 / Alm(0,0)
        ak[1] = 8.0/5.0 / Alm(1,0)
        ak[2] = 2.0/3.0 / Alm(2,0)
        ak[3] = 2.0/5.0 / Alm(3,0)

        stime = time.time()
        a_k = legendref90.legendref90.legendre_transform(fx, weights, Plx, m, lmax+1, 1)
        etime = time.time()
        f_x = legendref90.legendref90.i_legendre_transform(a_k, Plx, lmax+1, m, 1)
        if (verbose):
            print('l\tError in Coeffs')
            for i in range(lmax+1):
                print("{:d}\t{:+.6e}".format(i, ak[i]-a_k[i,0]))
        print("\n\tmax error in f(x)   = {}".format(np.amax(np.abs(fx[:]-f_x[:,0]))))
        print("\tmax error in coeffs = {}".format(np.amax(np.abs(ak[:]-a_k[:,0]))))
        print("\n\tTime to do transform = {} seconds\n".format(etime-stime))

def Alm(l,m):
    coeff = 2*l+1
    coeff /= 4.*np.pi
    if (m != 0):
        coeff *= math.factorial(l-m)/math.factorial(l+m)
    coeff = np.sqrt(coeff)
    return coeff

if __name__ == "__main__":
    from docopt import docopt
    args = docopt(__doc__)

    main()

