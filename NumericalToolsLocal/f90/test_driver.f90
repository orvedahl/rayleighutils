!
! fortran version of python_driver.py
!

program test

   use legendref90

   implicit none

   integer :: m, i, lmax, j, nx, nl, nrhs, tmpi
   double precision :: amp, alpha, beta, err, diff, max_err
   double precision, allocatable :: Pl(:), dPldx(:), x(:), P3(:), dP3dx(:), w(:)
   double precision, allocatable :: Plx(:,:), coeffs(:,:), fx(:,:), true_coeffs(:)
   double precision, allocatable :: A(:,:), B(:,:), C(:,:), true_C(:,:), numerical_fx(:)

   !----------evaluate P_3(x) at the legendre zero grid points-----------
   if (.true.) then
      write(*,*) '...testing P_3(x) at various grid points...'
      m = 8
      allocate(Pl(0:m-1), dPldx(0:m-1), x(0:m-1), P3(0:m-1), dP3dx(0:m-1))
      x(0) = -0.960289
      x(1) = -0.796666
      x(2) = -0.525532
      x(3) = -0.183434
      x(4) =  0.183434
      x(5) =  0.525532
      x(6) =  0.796666
      x(7) =  0.960289

      call Y_ell_m0(3, x, m, P3, dP3dx) ! analytic solutions

      call eval_nth_legendre(x, m, 3, Pl, dPldx) ! numerical solution
      write(*,*)
      write(*,*) 'length of x = ', shape(x)
      write(*,*) 'l = 3'
      write(*,*) '  Error[Pl(x)]              Error[dPldx(x)]'
      amp = ((2.0d0*3 + 1)/4.0d0/dacos(-1.0d0))**0.5d0 ! this converts regular Pl to Alm*Pl
      do i=0,m-1
         write(*,*) amp*Pl(i)-P3(i), amp*dPldx(i)-dP3dx(i)
      enddo
      deallocate(Pl, dPldx, x, P3, dP3dx)
   endif

   !---------test DGEMM call--------
   if (.true.) then
      write(*,*) '...testing DGEMM call...'
      nx = 3; nl = 2; nrhs = 4
      allocate(A(nx,nl), B(nx,nrhs), C(nl,nrhs), true_C(nl,nrhs))
      A(1,1) = 1.0d0; A(1,2) = 4.0d0
      A(2,1) = 2.0d0; A(2,2) = 5.0d0
      A(3,1) = 3.0d0; A(3,2) = 6.0d0

      B(1,1) = 1.0d0; B(1,2) = 3.0d0; B(1,3) = 14.0d0; B(1,4) = 4.0d0
      B(2,1) = 2.0d0; B(2,2) = 2.0d0; B(2,3) = 5.0d0;  B(2,4) = 2.0d0
      B(3,1) = 3.0d0; B(3,2) = 1.0d0; B(3,3) = 6.0d0;  B(3,4) = 3.0d0

      true_C(1,1) = 14.0d0; true_C(1,2) = 10.0d0; true_C(1,3) = 42.0d0;   true_C(1,4) = 17.0d0
      true_C(2,1) = 32.0d0; true_C(2,2) = 28.0d0; true_C(2,3) = 117.0d0;  true_C(2,4) = 44.0d0

      C(:,:) = 0.0d0
      alpha = 1.0d0; beta = 0.0d0
      call dgemm('T', 'N', nl, nrhs, nx, alpha, A, nx, B, nx, beta, C, nl)
      err = -1.0d100
      write(*,*)'i, j, error' 
      do i=1,2
         do j=1,4
            diff = abs(C(i,j) - true_C(i,j))
            write(*,*) i, j,diff
            if (diff .gt. err) then
               err = diff
            endif
         enddo
         write(*,*)
      enddo
      write(*,*) 'testing DGEMM, max error = ', err
      deallocate(A, B, C, true_C)
   endif

   if (.true.) then
      write(*,*) '...compare transform coefficients...'
      !---------get expansion coefficients--------

      lmax = 31        ! coefficient size

      m = 3*(lmax+1)/2 ! theta grid size

      allocate(Plx(0:m-1,0:lmax), fx(0:m-1,0:0), x(0:m-1), w(0:m-1), numerical_fx(0:m-1), &
               coeffs(0:lmax,0:0), true_coeffs(0:lmax), P3(0:m-1), dP3dx(0:m-1))

      call roots(m, x, w) ! build grid
      call compute_plm(x, m, lmax, Plx)
      tmpi = 3 ! ell
      call Y_ell_m0(tmpi, x, m, P3, dP3dx) ! analytic solutions
      write(*,*)'...first ensure grids agree...'
      write(*,*) 'Plx(i,3)                  P3(i)                      P3(i)-Plx(i,3)'
      max_err = 1e-19
      do i=0,m-1
         !write(*,*) Plx(i,tmpi), P3(i), (P3(i)-Plx(i,tmpi))
         max_err = max(abs(P3(i)-Plx(i,tmpi)), max_err)
      enddo

      write(*,*)
      write(*,*) 'found max error = ', max_err
      write(*,*)

      ! function values and true coeffs
      fx(:,0) = x**3 + x**2 + x + 1.0d0
      true_coeffs(:) = 0.0d0
      true_coeffs(0) = 4.0d0/3.0d0 ! these are the f(x) = sum a_l*P_l(x)
      true_coeffs(1) = 8.0d0/5.0d0
      true_coeffs(2) = 2.0d0/3.0d0
      true_coeffs(3) = 2.0d0/5.0d0

      ! convert to f(x) = sum a_l*Alm*P_l(x) = sum b_l * P~_l(x)
      do i=0,lmax
         true_coeffs(i) = true_coeffs(i)/Alm(i,0)
      enddo
  
      ! numerically get coeffs
      coeffs(:,:) = 0.0d0
      call legendre_transform(fx, w, Plx, m, lmax+1, 1, coeffs)

      write(*,*)
      write(*,*) 'physical grid = ', m
      write(*,*) 'spectral grid = ', lmax
      write(*,*)
      write(*,*) '        ell  Error[coeff(l)]           numerical coeff            true coeffs'
      max_err = 1e-19
      do i=0,lmax
         !write(*,*) i, coeffs(i,0)-true_coeffs(i), coeffs(i,0), true_coeffs(i)
         max_err = max(abs(coeffs(i,0)-true_coeffs(i)), max_err)
      enddo
      write(*,*)
      write(*,*) 'found max error = ', max_err
      write(*,*)

      ! numerically invert for physical grid
      call i_legendre_transform(coeffs, Plx, lmax+1, m, 1, numerical_fx)

      write(*,*) '        Error[f(x)]           numerical f(x)            true f(x)'
      max_err = 1e-19
      do i=0,m-1
         !write(*,*) numerical_fx(i)-fx(i,0), numerical_fx(i), fx(i,0)
         max_err = max(abs(fx(i,0)-numerical_fx(i)), max_err)
      enddo
      write(*,*)
      write(*,*) 'found max error = ', max_err
      write(*,*)

      deallocate(Plx, fx, x, w, coeffs, true_coeffs, P3, dP3dx, numerical_fx)
   endif

   contains

   subroutine Y_ell_m0(ell, x, n, P, dPdx)
      ! true solution
      integer, intent(in):: n, ell
      double precision, intent(in):: x(n)
      double precision, intent(out):: P(n), dPdx(n)
      double precision :: Pm1(n), Pm2(n)
      integer :: i

      ! first get Legendre Polynomials
      if (ell .eq. 0) then
          P(:) = 1.0d0
          dPdx(:) = 0.0d0
      else if (ell .eq. 1) then
          P(:) = x(:)
          dPdx(:) = 1.0d0
      else
          Pm1(:) = 0.0d0
          P(:) = 1.0d0
          do i=0,ell-1
             Pm2(:) = Pm1(:)
             Pm1(:) = P(:)
             P(:) = ((2.0d0*dble(i)+1.0d0)*x(:)*Pm1(:) - dble(i)*Pm2(:))/dble(i+1)
          enddo
          dPdx(:) = ell*(x(:)*P(:) - Pm1(:))/(x(:)*x(:) - 1.0d0)
      endif

      ! now make them "Associated"
      P(:)    = P(:)*Alm(ell,0)
      dPdx(:) = dPdx(:)*Alm(ell,0)

   end subroutine Y_ell_m0

   function Alm(l,m) result(coeff)
       integer :: l,m
       double precision :: coeff
       coeff = (2*l+1)
       coeff = coeff/4.0d0/dacos(-1.0d0)
       if (m .ne. 0) then
          coeff = coeff*factorial(l-m)/factorial(l+m)
       endif
       coeff = (coeff)**0.5d0
   end function Alm

   function factorial(N) result(fact)
       integer :: N, fact, i
       fact=1
       do i=1,N
          fact = fact*i
       enddo
   end function factorial

end program test

