!
! functions to quickly evaluate legendre polynomials
!
! R. Orvedahl 8-7-2017

module legendref90

   implicit none

   contains

   !-----------------------------------------------------------------
   ! compute Alm*Pl(x, l), written in general form to account for m/=0
   !    Alm**2 = (2*l+1)/(4*pi) * (l-m)! / (l+m)!
   !-----------------------------------------------------------------
   subroutine compute_plm(x, n, lmax, Pl)
      integer, intent(in) :: n, lmax
      double precision, intent(in) :: x(0:n-1)

      double precision, intent(out) :: Pl(0:n-1,0:lmax) ! size(n, lmax+1)

      !f2py intent(in) :: x, n, lmax
      !f2py intent(out) :: Pl
      !f2py depend(n) :: x
      !f2py depend(n,lmax) :: Pl

      integer :: i, m, l, mv
      double precision :: ratio, amp, pi, tmp, xi

      pi = dacos(-1.0d0)

      do m=1,1 ! 1-based array indexing

         mv = m - 1 ! convert to azimuthal integer

         ! first do the l=m & l=m+1 pieces
         ratio = 1.0d0
         call compute_factorial_ratio(mv, ratio)
         amp = ((mv+0.5d0)/(2.0d0*pi))**0.5d0
         amp = amp*ratio
         do i=0,n-1
            xi = x(i)
            tmp = 1.0d0 - xi*xi
            if (mod(mv,2) .eq. 1) then
               Pl(i,mv) = -amp*tmp**(mv/2+0.5d0) ! odd m
            else
               Pl(i,mv) =  amp*tmp**(mv/2)       ! even m
            endif

            ! l=m+1 part
            if (mv .lt. lmax) then
               Pl(i,mv+1) = Pl(i,mv)*xi*(2.0d0*mv+3)**0.5d0
            endif
         enddo

         ! now do the l>m+1
         do l=mv+2,lmax
            do i=0,n-1
               xi = x(i)
               amp = (l-1)**2-mv*mv
               amp = amp / (4.0d0*(l-1)**2-1.0d0)
               amp = amp**0.5d0
               tmp = Pl(i,l-1)*xi - amp*Pl(i,l-2)
               amp = (4.0d0*l*l-1.0d0)/(l*l-mv*mv)
               Pl(i,l) = tmp*amp**0.5d0
            enddo
         enddo
      enddo

   end subroutine compute_plm

   !-----------------------------------------------------------------
   ! compute factorial ratio = sqrt( (2m)! / 4**m / m! / m! )
   !-----------------------------------------------------------------
   subroutine compute_factorial_ratio(m, ratio)
      integer, intent(in) :: m
      double precision, intent(out) :: ratio
      !f2py intent(in) :: m
      !f2py intent(out) :: ratio
      integer :: i
      ratio = 1.0d0
      do i=1,m
         ratio = ratio*((i-0.5d0)/i)**0.5d0
      enddo
   end subroutine compute_factorial_ratio

   !-----------------------------------------------------------------
   ! forward transform: given f(x), return a_l s.t. f(x) = sum a_l * P_l(x)
   !-----------------------------------------------------------------
   subroutine legendre_transform_1d(fin, weights, Pl, nx, nl, coeffs)
      ! input:
      !       fin, array size(nx)
      !       weights, array size(nx)
      !       Pl, array size(nx,nl)
      ! output:
      !       coeffs, array size(nl)
      integer, intent(in) :: nx, nl
      double precision, intent(in) :: weights(0:nx-1), fin(0:nx-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: coeffs(0:nl-1)
      !f2py intent(in) :: nx, nl, weights, fin, Pl
      !f2py intent(out) :: coeffs
      !f2py depend(nx) :: weights, fin
      !f2py depend(nx,nl) :: Pl
      !f2py depend(nl) :: coeffs
      integer :: i, nrhs
      double precision :: alpha, beta, twopi
      double precision :: iPl(0:nx-1,0:nl-1), fx(0:nx-1,0:0), coeff(0:nl-1,0:0)
      twopi = 2.0d0*dacos(-1.0d0)
      ! compute Pl*weights
      do i=0,nl-1
         iPl(:,i) = twopi*weights(:)*Pl(:,i)
      enddo
      alpha = 1.0d0
      beta  = 0.0d0
      nrhs = 1
      fx(:,0) = fin(:)
      call dgemm('T', 'N', nl, nrhs, nx, alpha, iPl, nx, fx, nx, beta, coeff, nl)
      coeffs(:) = coeff(:,0)
   end subroutine legendre_transform_1d

   !-----------------------------------------------------------------
   ! inverse transform: given a_l, return f s.t. f(x) = sum a_l * P_l(x)
   !-----------------------------------------------------------------
   subroutine i_legendre_transform_1d(coeff, Pl, nl, nx, fout)
      ! input:
      !       coeff, array size(nl)
      !       Pl, array size(nx,nl)
      ! output:
      !       fout, array size(nx)
      integer, intent(in) :: nx, nl
      double precision, intent(in) :: coeff(0:nl-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: fout(0:nx-1)
      !f2py intent(in) :: nx, nl, nrhs, coeff, Pl
      !f2py intent(out) :: fout
      !f2py depend(nx) :: fout
      !f2py depend(nx,nl) :: Pl
      !f2py depend(nl) :: coeff
      integer :: nrhs
      double precision :: alpha, beta, fx(0:nx-1,0:0), coeffs(0:nl-1,0:0)
      alpha = 1.0d0
      beta  = 0.0d0
      nrhs = 1
      coeffs(:,0) = coeff(:)
      call dgemm('N', 'N', nx, nrhs, nl, alpha, Pl, nx, coeffs, nl, beta, fx, nx)
      fout(:) = fx(:,0)
   end subroutine i_legendre_transform_1d

   !-----------------------------------------------------------------
   ! forward transform: given f(x,t), return a_l s.t. f(x,t) = sum a_l(t) * P_l(x)
   !-----------------------------------------------------------------
   subroutine legendre_transform_2d(fx, weights, Pl, nx, nl, nrhs, coeffs)
      ! input:
      !       fx, array size(nx,nrhs)
      !       weights, array size(nx)
      !       Pl, array size(nx,nl)
      ! output:
      !       coeffs, array size(nl,nrhs)
      integer, intent(in) :: nx, nl, nrhs
      double precision, intent(in) :: weights(0:nx-1), fx(0:nx-1,0:nrhs-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: coeffs(0:nl-1,0:nrhs-1)
      !f2py intent(in) :: nx, nl, nrhs, weights, fx, Pl
      !f2py intent(out) :: coeffs
      !f2py depend(nx) :: weights
      !f2py depend(nx,nrhs) :: fx
      !f2py depend(nx,nl) :: Pl
      !f2py depend(nl,nrhs) :: coeffs
      integer :: i
      double precision :: alpha, beta, iPl(0:nx-1,0:nl-1), twopi
      twopi = 2.0d0*dacos(-1.0d0)
      ! compute Pl*weights
      do i=0,nl-1
         iPl(:,i) = twopi*weights(:)*Pl(:,i)
      enddo
      alpha = 1.0d0
      beta  = 0.0d0
      call dgemm('T', 'N', nl, nrhs, nx, alpha, iPl, nx, fx, nx, beta, coeffs, nl)
   end subroutine legendre_transform_2d

   !-----------------------------------------------------------------
   ! inverse transform: given a_l(t), return f s.t. f(x,t) = sum a_l(t) * P_l(x)
   !-----------------------------------------------------------------
   subroutine i_legendre_transform_2d(coeffs, Pl, nl, nx, nrhs, fx)
      ! input:
      !       coeffs, array size(nl,nrhs)
      !       Pl, array size(nx,nl)
      ! output:
      !       fx, array size(nx,nrhs)
      integer, intent(in) :: nx, nl, nrhs
      double precision, intent(in) :: coeffs(0:nl-1,0:nrhs-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: fx(0:nx-1,0:nrhs-1)
      !f2py intent(in) :: nx, nl, nrhs, coeffs, Pl
      !f2py intent(out) :: fx
      !f2py depend(nx,nrhs) :: fx
      !f2py depend(nx,nl) :: Pl
      !f2py depend(nl,nrhs) :: coeffs
      double precision :: alpha, beta
      alpha = 1.0d0
      beta  = 0.0d0
      call dgemm('N', 'N', nx, nrhs, nl, alpha, Pl, nx, coeffs, nl, beta, fx, nx)
   end subroutine i_legendre_transform_2d

   !-----------------------------------------------------------------
   ! forward transform: given f(y,x,t), return a_l s.t. f(y,x,t) = sum a_l(y,t) * P_l(x)
   !-----------------------------------------------------------------
   subroutine legendre_transform_3d(fx, weights, Pl, nx, nl, ny, nrhs, coeffs)
      ! input:
      !       fx, array size(nx,nrhs,ny)
      !       weights, array size(nx)
      !       Pl, array size(nx,nl)
      ! output:
      !       coeffs, array size(nl,nrhs,ny)
      integer, intent(in) :: nx, nl, nrhs, ny
      double precision, intent(in) :: weights(0:nx-1), fx(0:nx-1,0:nrhs-1,0:ny-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: coeffs(0:nl-1,0:nrhs-1,0:ny-1)
      !f2py intent(in) :: nx, nl, ny, nrhs, weights, fx, Pl
      !f2py intent(out) :: coeffs
      !f2py depend(nx) :: weights
      !f2py depend(ny,nx,nrhs) :: fx
      !f2py depend(nx,nl) :: Pl
      !f2py depend(ny,nl,nrhs) :: coeffs
      integer :: i
      double precision :: alpha, beta, iPl(0:nx-1,0:nl-1), twopi
      twopi = 2.0d0*dacos(-1.0d0)
      ! compute Pl*weights
      do i=0,nl-1
         iPl(:,i) = twopi*weights(:)*Pl(:,i)
      enddo
      alpha = 1.0d0
      beta  = 0.0d0
      do i=0,ny-1
         call dgemm('T', 'N', nl, nrhs, nx, alpha, iPl, nx, fx(:,:,i), nx, beta, &
                    coeffs(:,:,i), nl)
      enddo
   end subroutine legendre_transform_3d

   !-----------------------------------------------------------------
   ! inverse transform: given a_l(y,t), return f s.t. f(y,x,t) = sum a_l(y,t) * P_l(x)
   !-----------------------------------------------------------------
   subroutine i_legendre_transform_3d(coeffs, Pl, nl, nx, ny, nrhs, fx)
      ! input:
      !       coeffs, array size(nl,nrhs,ny)
      !       Pl, array size(nx,nl)
      ! output:
      !       fx, array size(nx,nrhs,ny)
      integer, intent(in) :: nx, nl, nrhs, ny
      double precision, intent(in) :: coeffs(0:nl-1,0:nrhs-1,0:ny-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: fx(0:nx-1,0:nrhs-1,0:ny-1)
      !f2py intent(in) :: nx, nl, ny, nrhs, coeffs, Pl
      !f2py intent(out) :: fx
      !f2py depend(ny,nx,nrhs) :: fx
      !f2py depend(nx,nl) :: Pl
      !f2py depend(ny,nl,nrhs) :: coeffs
      integer :: i
      double precision :: alpha, beta
      alpha = 1.0d0
      beta  = 0.0d0
      do i=0,ny-1
         call dgemm('N', 'N', nx, nrhs, nl, alpha, Pl, nx, coeffs(:,:,i), nl, beta, &
                    fx(:,:,i), nx)
      enddo
   end subroutine i_legendre_transform_3d

   !-----------------------------------------------------------------
   ! forward transform: given f(x,y,z,t), return a_l s.t. f(x,y,z,t) = sum a_l(y,z,t) * P_l(x)
   !-----------------------------------------------------------------
   subroutine legendre_transform_4d(fx, weights, Pl, nx, ny, nz, nt, nl, coeffs)
      ! input:
      !       fx, array size(nx,ny,nz,nt)
      !       weights, array size(nx)
      !       Pl, array size(nx,nl)
      ! output:
      !       coeffs, array size(nl,ny,nz,nt)
      integer, intent(in) :: nx, nl, nt, ny, nz
      double precision, intent(in) :: weights(0:nx-1), fx(0:nx-1,0:ny-1,0:nz-1,0:nt-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: coeffs(0:nl-1,0:ny-1,0:nz-1,0:nt-1)
      !f2py intent(in) :: nx, ny, nz, nt, nl, weights, fx, Pl
      !f2py intent(out) :: coeffs
      !f2py depend(nx) :: weights
      !f2py depend(nx,ny,nz,nt) :: fx
      !f2py depend(nx,nl) :: Pl
      !f2py depend(nl,ny,nz,nt) :: coeffs
      integer :: i,j
      double precision :: alpha, beta, iPl(0:nx-1,0:nl-1), twopi
      twopi = 2.0d0*dacos(-1.0d0)
      ! compute Pl*weights
      do i=0,nl-1
         iPl(:,i) = twopi*weights(:)*Pl(:,i)
      enddo
      alpha = 1.0d0
      beta  = 0.0d0
      do j=0,nt-1
         do i=0,nz-1
            call dgemm('T', 'N', nl, ny, nx, alpha, iPl, nx, fx(:,:,i,j), nx, beta, &
                       coeffs(:,:,i,j), nl)
         enddo
      enddo
   end subroutine legendre_transform_4d

   !-----------------------------------------------------------------
   ! inverse transform: given a_l(y,z,t), return f s.t. f(x,y,z,t) = sum a_l(y,z,t) * P_l(x)
   !-----------------------------------------------------------------
   subroutine i_legendre_transform_4d(coeffs, Pl, nx, ny, nz, nt, nl, fx)
      ! input:
      !       coeffs, array size(nl,nt,ny,nz)
      !       Pl, array size(nx,nl)
      ! output:
      !       fx, array size(nx,nt,ny,nz)
      integer, intent(in) :: nx, nl, nt, ny, nz
      double precision, intent(in) :: coeffs(0:nl-1,0:ny-1,0:nz-1,0:nt-1)
      double precision, intent(in) :: Pl(0:nx-1,0:nl-1)
      double precision, intent(out) :: fx(0:nx-1,0:ny-1,0:nz-1,0:nt-1)
      !f2py intent(in) :: nx, nl, nz, ny, nt, coeffs, Pl
      !f2py intent(out) :: fx
      !f2py depend(nx,ny,nz,nt) :: fx
      !f2py depend(nx,nl) :: Pl
      !f2py depend(nl,ny,nz,nt) :: coeffs
      integer :: i,j
      double precision :: alpha, beta
      alpha = 1.0d0
      beta  = 0.0d0
      do j=0,nt-1
         do i=0,nz-1
            call dgemm('N', 'N', nx, ny, nl, alpha, Pl, nx, coeffs(:,:,i,j), nl, beta, &
                       fx(:,:,i,j), nx)
         enddo
      enddo
   end subroutine i_legendre_transform_4d

   !-----------------------------------------------------------------
   ! evaluate P_l and d(P_l)/dx at the given grid points
   !-----------------------------------------------------------------
   subroutine eval_nth_legendre(x, m, l, Pl, dPldx)
      integer, intent(in) :: m, l
      double precision, intent(in) :: x(0:m-1)

      double precision, intent(out) :: Pl(0:m-1), dPldx(0:m-1)

      !f2py intent(in) :: m, l, x
      !f2py intent(out) :: Pl, dPldx
      !f2py depend(m) :: x, Pl, dPldx

      integer :: i
      double precision :: Pl_minus1(0:m-1), Pl_minus2(0:m-1)

      Pl_minus1(:) = 0.d0
      Pl(:) = 1.0d0

      ! use recursion relation
      do i=0,l-1
         Pl_minus2(:) = Pl_minus1(:)
         Pl_minus1(:) = Pl(:)
         Pl(:) = ((2.0d0*dble(i)+1.0d0)*x(:)*Pl_minus1(:) - dble(i)*Pl_minus2(:))/dble(i+1)
      enddo

      ! get derivative
      dPldx(:) = l*(x(:)*Pl(:) - Pl_minus1(:))/(x(:)*x(:) - 1.d0)

   end subroutine eval_nth_legendre

   !-----------------------------------------------------------------
   ! coefficients in the expansion f(x) = sum a_l * P_l(x)
   !-----------------------------------------------------------------
   subroutine get_coeffs(n, x, m, weights, fx, coeffs)
      integer, intent(in) :: n, m
      double precision, intent(in) :: x(0:m-1), weights(0:n-1), fx(0:n-1)

      double precision, intent(out) :: coeffs(0:n-1)

      !f2py intent(in) :: n, x, m, weights, fx
      !f2py intent(out) :: coeffs
      !f2py depend(n) :: weights, fx, coeffs
      !f2py depend(m) :: x

      integer :: i
      double precision :: Pl(0:m-1), dPldx(0:m-1), C

      do i=0,n-1
         C = dble(i) + 0.5d0
         call eval_nth_legendre(x, m, i, Pl, dPldx)

         coeffs(i) = C*sum(weights(:)*fx(:)*Pl(:))
      enddo

   end subroutine get_coeffs

   !-----------------------------------------------------------------
   ! Legendre grid points ordered as x(i) < x(i+1) and x in (-1,1)
   !-----------------------------------------------------------------
   subroutine roots(n, x, weights)
      integer, intent(in) :: n
      double precision, intent(out) :: x(0:n-1), weights(0:n-1)

      !f2py intent(in) :: n
      !f2py intent(out) :: x, weights
      !f2py depend(n) :: x, weights

      integer :: i, n_roots
      logical :: converged
      double precision :: midpoint, scaling, eps, ith_root(0:0), pi
      double precision :: Np_half, new_guess, delta, Pl(0:0), dPldx(0:0)

      pi = dacos(-1.0d0)

      midpoint = 0.0d0
      scaling  = 1.0d0
      n_roots  = (n+1)/2

      Np_half = dble(n) + 0.5d0

      eps = 3.0d-15

      do i=0,n_roots-1
         ith_root(0) = dcos(pi*(dble(i)+0.75d0)/Np_half)
         converged = .false.
         do
            ! evaluate Pl at single root
            call eval_nth_legendre(ith_root, 1, n, Pl, dPldx)

            new_guess = ith_root(0) - Pl(0)/dPldx(0)
            delta = dabs(ith_root(0) - new_guess)
            ith_root(0) = new_guess
            if (delta .le. eps) then
               converged = .true.
            endif

            x(i)     = midpoint - scaling*ith_root(0)
            x(n-1-i) = midpoint + scaling*ith_root(0)

            weights(i)     = 2.0d0*scaling/((1.-ith_root(0)*ith_root(0))*dPldx(0)*dPldx(0))
            weights(n-1-i) = weights(i)

            if (converged) exit
         enddo
      enddo

   end subroutine roots

end module legendref90

