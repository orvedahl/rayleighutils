"""
Configuration handling.

Values can be returned as
    value = config.get("section", "variable")
    type(value) == string
  or
    value = config.getboolean("section", "variable")
    type(value) == boolean
  or
    value = config.getint("section", "variable")
    type(value) == int
  or
    value = config.getfloat("section", "variable")
    type(value) == float
"""

import os
import sys

if (sys.version_info[0] == 2):
    # using Python 2
    from ConfigParser import ConfigParser
else:
    # using Python 3
    from configparser import ConfigParser

#NumToolsDir = os.environ['NumericalToolsDir']

# create configuration file object
config = ConfigParser()

# read defaults from installation directory
#config.read(os.path.join(NumToolsDir, 'numtools.cfg'))

# read defaults from user's home directory
#config.read(os.path.expanduser('~/.numtools/numtools.cfg'))

# read defaults from local directory
#config.read('numtools.cfg')

# read local file
config.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), "numtools.cfg"))

# override any issues that the installation process detected
#config.read(os.path.join(NumToolsDir, '__installed_options.cfg'))

