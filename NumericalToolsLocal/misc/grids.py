"""
Module to handle various grids:

    --Legendre grids
        -traditional zeros grid, (a,b)
        -zeros grid that is scaled to include endpoints, [a,b]

    --Chebyshev grids
        -traditional zeros grid, (a,b)
        -traditional extrema grid, [a,b]
        -zeros grid that is scaled to include endpoints, [a,b]

    --Uniform grid
        -[a, b) -- include only the first boundary
                      b = x[-1] + dx
        -(a, b] -- include only the last boundary
                      a = x[ 0] - dx
        -(a, b) -- include neither boundary
                      a = x[ 0] - dx/2
                      b = x[-1] + dx/2
        -[a, b] -- include both boundaries

    --Random grid
        -[a, b) -- include only the first boundary

"""
from __future__ import print_function
import numpy as np
from ..misc.config import config
f90_support = config.getboolean("installed-options", "f90support")

if (f90_support):
    from ..f90 import legendref90

def to_ab(x, a, b, xlo, xhi, endpoints):
    """linear transformation from (-1,1) --> [a,b] or (a,b)
        x is assumed in (-1,1)
        r is assumed in (a,b) or [a,b] depending on endpoints
    """
    x = np.asarray(x)
    if (endpoints):
        r = (x - xlo)*(b - a)/(xhi - xlo) + a
    else:
        r = 0.5*(b - a)*x + 0.5*(b + a)
    return r

def from_ab(r, a, b, xlo, xhi, endpoints):
    """linear transformation from [a,b] or (a,b) --> (-1,1)
        r is assumed in (a,b) or [a,b] depending on endpoints
        x is assumed in (-1,1)
    """
    r = np.asarray(r)
    if (endpoints):
        x = (r - a)*(xhi - xlo)/(b - a) + xlo
    else:
        x = 2.*r/(b - a) - (b + a)/(b - a)
    return x

class Grid():
    """basic grid"""

    def __init__(self, N, a=-1.0, b=1.0):

        self.a = a            # lower boundary
        self.b = b            # upper boundary
        self.N = N            # number of grid points
        self._generate_grid() # get the grid points

        self.info = {}
        self.info['a'] = 'lower boundary, float'
        self.info['b'] = 'upper boundary, float'
        self.info['N'] = 'number of grid points, int'

        self.info['x'] = 'grid points, array'

    def _generate_grid(self):
        raise NotImplementedError()

class RandomGrid(Grid):
    """Random grid"""

    def __init__(self, N, a=-1.0, b=1.0, seed=43):

        self.seed = seed
        Grid.__init__(self, N, a=a, b=b)

        self.info['seed'] = 'random number generator seed, int'

    def _generate_grid(self):
        rand = np.random.RandomState(seed=self.seed)
        x = rand.uniform(low=self.a, high=self.b, size=self.N)
        self.x = np.sort(x)
        return

class UniformGrid(Grid):
    """Uniform grid"""

    def __init__(self, N, a=-1.0, b=1.0, endpoints=False, lendpoint=True, uendpoint=False):
        self.lendpoint = lendpoint # is the left  endpoint included
        self.uendpoint = uendpoint # is the right endpoint included
        self.endpoints = endpoints # include both endpoints, overrides l/u-endpoint
        if (self.endpoints):
            self.lendpoint = True
            self.uendpoint = True

        Grid.__init__(self, N, a=a, b=b)

        self.info['lendpoint'] = 'is the lower boundary included, boolean'
        self.info['uendpoint'] = 'is the upper boundary included, boolean'
        self.info['endpoints'] = 'are both boundaries included, boolean'
        self.info['dx'] = 'grid spacing, float'

    def _generate_grid(self):
        if (self.lendpoint and self.uendpoint):
            # include both endpoints
            x, dx = np.linspace(self.a, self.b, num=self.N, endpoint=True, retstep=True)

        elif(self.lendpoint):
            # only include left endpoint
            x, dx = np.linspace(self.a, self.b, num=self.N, endpoint=False, retstep=True)

        elif(self.uendpoint):
            # only include right endpoint
            x, dx = np.linspace(self.a, self.b, num=self.N, endpoint=False, retstep=True)
            x += dx

        else:
            # neither endpoint is included
            x, dx = np.linspace(self.a, self.b, num=self.N, endpoint=False, retstep=True)
            x += 0.5*dx

        # store grid and grid spacing
        self.dx = dx
        self.x  = x
        return

class ChebGrid(Grid):
    """Chebyshev grid"""

    def __init__(self, N, a=-1.0, b=1.0, zeros=True, endpoints=False):

        self.zeros = zeros   # zeros or extrema

        if (self.zeros):
            self.endpoints = endpoints # this is only relevant for the zeros grid
        else:
            self.endpoints = False

        Grid.__init__(self, N, a=a, b=b)

        if (self.endpoints):
            self.deriv_coeff = (self.xhi - self.xlo)/(self.b - self.a)
        else:
            self.deriv_coeff = 2.0/(self.b - self.a)
        self.integ_coeff = 1./self.deriv_coeff

        self.xab = self._to_ab(self.x) # convert x to [a,b] or (a,b)

        self.info['zeros'] = 'is the grid a zeros grid, boolean'
        self.info['endpoints'] = 'are the endpoints included in the zeros grid, boolean'
        self.info['deriv_coeff'] = 'scaling coefficient when differentiating, float'
        self.info['integ_coeff'] = 'scaling coefficient when integrating, float'
        self.info['theta'] = 'grid points in theta space, array'
        self.info['xab'] = 'grid points in [a,b] space, array'
        self.info['xlo'] = 'first chebyshev grid point, float'
        self.info['xhi'] = 'last chebyshev grid point, float'
        self.info['_to_ab(x)'] = 'method to convert into [a,b] space, callable'
        self.info['_from_ab(r)'] = 'method to convert from [a,b] space, callable'

    def _generate_grid(self):
        if (self.zeros):
            x, theta = self._roots(self.N, return_theta=1)
            self.grid_type = 'zeros'
        else:
            x, theta = self._extrema(self.N, return_theta=1)
            self.grid_type = 'extrema'

        # store both grids
        self.theta = theta          # theta in (0, pi)
        self.x = x                  # x in (-1,1)

        # store first and last chebyshev grid point
        self.xlo = x[ 0]
        self.xhi = x[-1]

        return

    def _to_ab(self, x):
        """linear transformation from (-1,1) --> [a,b] or (a,b)
           x is assumed in (-1,1)
           r is assumed in (a,b) or [a,b] depending on endpoints
        """
        return to_ab(x, self.a, self.b, self.xlo, self.xhi, self.endpoints)

    def _from_ab(self, r):
        """linear transformation from [a,b] or (a,b) --> (-1,1)
           r is assumed in (a,b) or [a,b] depending on endpoints
           x is assumed in (-1,1)
        """
        return from_ab(r, self.a, self.b, self.xlo, self.xhi, self.endpoints)

    def _roots(self, Npts, return_theta=False):
        """
        Chebyshev zeros grid
            x_k = cos(pi*k/Npts + pi/2/Npts)
            k in [0,Npts-1]
            x=-1 & x=1 are not included
            ordered as x[i] < x[i+1]
        """
        x = np.zeros((Npts)); theta = np.zeros((Npts))
        dctheta = np.pi/Npts
        ctheta0 = dctheta*0.5
        arg = ctheta0
        for i in range(Npts): # i in [0,Npts)
            theta[i] = arg
            x[i] = np.cos(arg)
            arg += dctheta
        x = x[::-1]; theta = theta[::-1]
        if (return_theta):
            return x, theta
        else:
            return x

    def _extrema(self, Npts, return_theta=False):
        """
        Chebyshev extrema grid
            x_k = cos(pi*k/(Npts-1))
            k in [0,Npts-1]
            x=-1 & x=1 are both included
            ordered as x[i] < x[i+1]
        """
        x = np.zeros((Npts)); theta = np.zeros((Npts))
        dctheta = np.pi / (Npts - 1)
        ctheta0 = 0.
        arg = ctheta0
        for i in range(Npts):
            theta[i] = arg
            x[i] = np.cos(arg)
            arg += dctheta
        x = x[::-1]; theta = theta[::-1]
        if (return_theta):
            return x, theta
        else:
            return x

class LegendreGrid(Grid):
    """Legendre grid"""

    def __init__(self, N, a=-1.0, b=1.0, endpoints=False):

        self.endpoints = endpoints

        Grid.__init__(self, N, a=a, b=b)

        if (self.endpoints):
            self.deriv_coeff = (self.xhi - self.xlo)/(self.b - self.a)
        else:
            self.deriv_coeff = 2.0/(self.b - self.a)
        self.integ_coeff = 1./self.deriv_coeff

        self.xab = self._to_ab(self.x) # convert x to [a,b] or (a,b)

        self.info['weights'] = 'weights for integration, array'
        self.info['endpoints'] = 'are the endpoints included in the grid, boolean'
        self.info['deriv_coeff'] = 'scaling coefficient when differentiating, float'
        self.info['integ_coeff'] = 'scaling coefficient when integrating, float'
        self.info['xab'] = 'grid points in [a,b] space, array'
        self.info['xlo'] = 'first legendre grid point, float'
        self.info['xhi'] = 'last legendre grid point, float'
        self.info['_to_ab(x)'] = 'method to convert into [a,b] space, callable'
        self.info['_from_ab(r)'] = 'method to convert from [a,b] space, callable'

    def _generate_grid(self):
        x, weights = self._roots(self.N)

        self.x = x
        self.weights = weights

        # store first and last legendre grid point
        self.xlo = x[ 0]
        self.xhi = x[-1]

        return

    def _to_ab(self, x):
        """linear transformation from (-1,1) --> [a,b] or (a,b)
           x is assumed in (-1,1)
           r is assumed in (a,b) or [a,b] depending on endpoints
        """
        return to_ab(x, self.a, self.b, self.xlo, self.xhi, self.endpoints)

    def _from_ab(self, r):
        """linear transformation from [a,b] or (a,b) --> (-1,1)
           r is assumed in (a,b) or [a,b] depending on endpoints
           x is assumed in (-1,1)
        """
        return from_ab(r, self.a, self.b, self.xlo, self.xhi, self.endpoints)

    def _roots(self, Npts):
        """
        Legendre grid points
            ordered as x[i] < x[i+1] and x in (-1,1)
        """
        x, w = LegendreRoots(Npts)
        return x, w

    def _eval_nth_legendre(self, x, n):
        """
        evaluate n-th Legendre at the given grid point
          P_{n+1} = (2*n+1)*x/(n+1)*P_n - n/(n+1)*P_n-1
        or
          P_{m} = ( (2*m-1)*x*P_{m-1} - (m-1)*P_{m-2} ) /m
        """
        pn, deriv_pn = evaluate_Pn(x, n)
        return pn, deriv_pn

def evaluate_Pn(x, n):
    """
    evaluate n-th Legendre at the given grid point
        P_{n+1} = (2*n+1)*x/(n+1)*P_n - n/(n+1)*P_n-1
    or
        P_{m} = ( (2*m-1)*x*P_{m-1} - (m-1)*P_{m-2} ) /m
    """
    x = np.asarray(x)
    if (len(np.shape(x)) > 0): # x is array
        length = len(x)
    else:
        length = 1             # x is scalar

    if (f90_support):
        pn, deriv_pn = legendref90.legendref90.eval_nth_legendre(x, length, n)

    else:
        pn_minus1 = 0.0
        pn = 1.0

        # use recursion relation
        for j in range(n):
            pn_minus2 = pn_minus1
            pn_minus1 = pn
            pn = ((2.*j + 1.)*x*pn_minus1 - j*pn_minus2)/float(j+1.)

        # get derivative
        deriv_pn = n*(x*pn-pn_minus1)/(x*x - 1.)

    return pn, deriv_pn

def LegendreRoots(Npts):
    """
    Legendre grid points
        ordered as x[i] < x[i+1] and x in (-1,1)
    """
    if (f90_support):
        x, w = legendref90.legendref90.roots(Npts)

    else:
        x = np.zeros((Npts)); w = np.zeros((Npts))
        midpoint = 0.0
        scaling =  1.0
        n_roots = (Npts + 1)/2

        eps = 3.e-15

        # this is a Newton-Rhapson find for the roots
        for i in range(n_roots):
            ith_root = np.cos(np.pi*(i+0.75)/(Npts+0.5))
            converged = False; iters = 0
            while (not converged):
                pn, deriv_pn = evaluate_Pn(ith_root, Npts)

                new_guess = ith_root - pn/deriv_pn
                delta = np.abs(ith_root - new_guess)
                ith_root = new_guess
                if (delta <= eps):
                    converged = True

                x[i] = midpoint - scaling*ith_root
                x[Npts-1-i] = midpoint + scaling*ith_root

                w[i] = 2.*scaling/((1.-ith_root*ith_root)*deriv_pn*deriv_pn)
                w[Npts-1-i] = w[i]
                iters += 1

    return x, w

