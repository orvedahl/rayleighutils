"""
Interface to handle various numpy array operations
"""
from __future__ import print_function
import numpy as np

class Array():
    """
    wrap numpy arrays with some useful operations
    """

    def __init__(self, A):

        self.data = A
        self.original_shape = np.shape(self.data)
        self.shape = np.shape(self.data)
        self.swaps = []

    def swapaxes(self, ax0, ax1):
        """swap location of axes ax0 and ax1"""
        self.data = SwapAxis(self.data, ax0, ax1)
        self.swaps.append((ax0, ax1))
        self.shape = np.shape(self.data)

    def resetaxes(self):
        """undo all previous axis swaps, return to original shape"""
        for swaps in self.swaps[::-1]:
            self.swapaxes(swaps[1], swaps[0])
        self.swaps = []
        self.shape = np.shape(self.data)

def ReverseAxis(x, axis=-1):
    """
    reverse the axis of a multidimensional array
    """
    x = np.asarray(x)
    shape = np.shape(x)
    if (len(shape) > 0): # x = array of 1d or larger
        x = SwapAxis(x, -1, axis)
        x = x[...,::-1]
        x = SwapAxis(x, axis, -1)
    return x

def MultiplyAxis(A, b, axis=-1):
    """
    multiply a particular axis of n-D array A by the 1D vector b
       A    --- n dimensional array
       b    --- 1D array
       axis --- axis over which the multiplication is performed
    """
    ndim = np.ndim(A)
    b = np.asarray(b)

    if (len(np.shape(b)) < 1): # b is a pure number, make it a 1D array
        shape = np.shape(A)
        N = shape[axis]
        b = b*np.ones((N))

    # get array used to reshape 1D array to have singleton dimensions
    # except for the given axis where it is -1 to signify use of
    # entire length of elements along that axis. numpy will figure
    # out proper value in order to use all elements along that axis
    dim = np.ones((1, ndim),np.int32).ravel()
    dim[axis] = -1

    # reshape b and perform elementwise multiplication
    return A*np.reshape(b, dim)

def SwapAxis(array, axis0, axis1):
    """
    move axes of array, other axes remain untouched

    B.shape
        (2,3,4,5,6)

    # swap 2nd and last axes:
    A = SwapAxis(B, 1, -1)
    A.shape
        (2,6,4,5,3)

    # undo that swap
    A = SwapAxis(A, -1, 1)
    A.shape
        (2,3,4,5,6)
    """
    if (len(np.shape(array)) != 0):
        return np.swapaxes(array, axis0, axis1)
    else:
        return array

def Tile(th, r, shape):
    """
    convert 1d arrays to arrays with given shape
    """
    Th, R = np.meshgrid(th, r, indexing='ij')

    newshape = np.asarray(shape)
    newshape[2:] = 1
    newshape = tuple(newshape)

    Th = np.reshape(Th, newshape)
    R  = np.reshape( R, newshape)

    return Th, R

