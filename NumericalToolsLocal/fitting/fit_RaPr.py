"""
Module to fit data of the form

    f(Ra, Pr) = A*(Ra**b)*(Pr**c)

Usage:
    fit_RaPr.py [options]

Options:
    --a=<a>       Set true a value [default: 10]
    --b=<b>       Set true b value [default: 0.125]
    --c=<c>       Set true c value [default: -1]
    --noise=<n>   Set standard deviation of noisy data [default: 2]
    --method=<m>  Choose error estimation, MC, Boot, None [default: None]
"""

from __future__ import print_function
from .fitting import general_fit
import scipy.stats as stats
import numpy as np
import sys

def fitting_RaPr(Ra, fRa, Pr, a0, error_method='MC', full_output=True, **kwargs):
    """
    fit data of the form a*(Ra**b)*(Pr**c)

    Ra  : vector of Rayleigh numbers
    fRa : function value at each Ra
    Pr  : vector of Prandtl numbers
    a0  : (a,b,c) initial guess
    method : MC, Boot, None = error estimates from
               Monte Carlo, Bootstrap, np.leastsq returned errors
    """

    #def fit_func(x, params, Pr):
    #    a,b,c = params
    #    # this is really fitting f(Ra,Pr) = (e**a)*(Ra**b)*(Pr**c)
    #    return a + b*x + c*np.log(Pr)

    #xdata = np.log(Ra); ydata = np.log(fRa)

    #_params, _errors, info = general_fit(xdata, ydata, fit_func, a0,
    #                error_method=error_method, full_output=True, func_args=(Pr,), **kwargs)

    def fit_func(x, params):
        a,b,c = params
        # this is really fitting f(Ra,Pr) = (e**a)*(Ra**b)*(Pr**c)
        _Ra = x[0]; _Pr = x[1]
        return a + b*_Ra + c*np.log(_Pr)

    xdata = np.zeros((2,len(Ra))) # pack dependent variables into single array
    xdata[0,:] = np.log(Ra)
    xdata[1,:] = Pr
    ydata = np.log(fRa)

    _params, _errors, info = general_fit(xdata, ydata, fit_func, a0,
                    error_method=error_method, full_output=True, func_args=(), **kwargs)

    _chi2 = info['chi_squared']
    _chi2_red = info['reduced_chi2']
    _pte = info['pte']

    # convert back to f(Ra,Pr) = a*(Ra**b)*(Pr**c)
    A = _params[0]
    _params[0] = np.exp(A)
    _errors[0] = np.exp(A)*_errors[0]
    if (full_output):
        return _params, _errors, _chi2, _chi2_red, _pte
    else:
        return _params, _errors

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    a = float(args['--a'])
    b = float(args['--b'])
    c = float(args['--c'])
    noise = float(args['--noise'])
    method = args['--method']

    # set seed for random number generator
    np.random.seed(43)

    Ra025 = np.logspace(np.log10(5e2), np.log10(3e6), 5)
    nRa025=len(Ra025)
    rand = noise*np.random.standard_normal(nRa025)
    f025 = a*(Ra025)**b*(0.25)**c + rand

    Ra05 = np.logspace(np.log10(5e2), np.log10(3e6), 7)
    nRa05=len(Ra05)
    rand = noise*np.random.standard_normal(nRa05)
    f05 = a*(Ra05)**b*(0.5)**c + rand

    Ra1 = np.logspace(np.log10(5e2), np.log10(3e6), 6)
    nRa1=len(Ra1)
    rand = noise*np.random.standard_normal(nRa1)
    f1 = a*(Ra1)**b*(1.0)**c + rand

    Ra2 = np.logspace(np.log10(5e2), np.log10(3e6), 5)
    nRa2=len(Ra2)
    rand = noise*np.random.standard_normal(nRa2)
    f2 = a*(Ra2)**b*(2.0)**c + rand

    Ra4 = np.logspace(np.log10(5e2), np.log10(3e6), 7)
    nRa4=len(Ra4)
    rand = noise*np.random.standard_normal(nRa4)
    f4 = a*(Ra4)**b*(4.0)**c + rand

    Raf = np.concatenate((f025,  f05,  f1,  f2,  f4))
    Rax = np.concatenate((Ra025, Ra05, Ra1, Ra2, Ra4))
    nmax = len(Rax)
    pr=np.ones((nmax))
    pr[0:nRa025] = 0.25
    pr[nRa025:nRa025+nRa05] = 0.5
    pr[nRa025+nRa05:nRa025+nRa05+nRa1] = 1.0
    pr[nRa025+nRa05+nRa1:nRa025+nRa05+nRa1+nRa2] = 2.0
    pr[nRa025+nRa05+nRa1+nRa2:nRa025+nRa05+nRa1+nRa2+nRa4] = 4.0

    a0 = (8, 0.1, -0.5)
    _params, _errors, _chi2, _chi2_red, _pte = fitting_RaPr(Rax, Raf, pr, a0, error_method=method)

    print("\nInputs:")
    print("\ta        = {}".format(a))
    print("\tb        = {}".format(b))
    print("\tc        = {}".format(c))
    print("\tnoise    = {}".format(noise))
    print("\n\tmethod   = {}".format(method))

    a = _params[0]; b = _params[1]; c = _params[2]
    da = _errors[0]; db = _errors[1]; dc = _errors[2]
    print("\nInitial Guess:")
    print("\ta        = {}".format(a0[0]))
    print("\tb        = {}".format(a0[1]))
    print("\tc        = {}".format(a0[2]))

    print("\nFitting Parameters:")
    print("\ta +/- da = {} +/- {} ({:.2f}%)".format(a, da, 100*abs(da/a)))
    print("\tb +/- db = {} +/- {} ({:.2f}%)".format(b, db, 100*abs(db/b)))
    print("\tc +/- dc = {} +/- {} ({:.2f}%)".format(c, dc, 100*abs(dc/c)))
    print("\n\tchi2 = {}".format(_chi2))
    print("\treduced chi2 = {}".format(_chi2_red))
    print("\tpte = {}".format(_pte))
    print()

    import matplotlib.pyplot as plt
    plt.clf()
    plt.subplot(121)
    plt.plot(Ra025, f025, marker='+', color='m', linestyle='')
    plt.plot(Ra05,   f05, marker='D', color='c', linestyle='')
    plt.plot(Ra1,     f1, marker='^', color='g', linestyle='')
    plt.plot(Ra2,     f2, marker='x', color='b', linestyle='')
    plt.plot(Ra4,     f4, marker='o', color='r', linestyle='')
    plt.xlabel("Ra"); plt.ylabel("F(Ra, Pr)")
    plt.yscale('log'); plt.xscale('log')
    plt.subplot(122)
    plt.plot(Ra025**b*0.25**c, f025, marker='+', color='m', linestyle='')
    plt.plot(Ra05**b*0.5**c,   f05, marker='D', color='c', linestyle='')
    plt.plot(Ra1**b*1.**c,     f1, marker='^', color='g', linestyle='')
    plt.plot(Ra2**b*2.**c,     f2, marker='x', color='b', linestyle='')
    plt.plot(Ra4**b*4.**c,     f4, marker='o', color='r', linestyle='')
    plt.plot(Rax**b*pr**c, a*Rax**b*pr**c, marker='', color='k', linestyle='-')
    plt.suptitle(method)
    plt.xlabel("Ra^b Pr^c"); plt.ylabel("F(Ra, Pr)")
    plt.yscale('log'); plt.xscale('log')
    plt.show()

