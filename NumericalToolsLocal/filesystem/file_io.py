"""
Module for filesystem manipulations

Routines:
    --use HDF5 to read/write data to file with or without compression
    --manipulate filenames, add/change file extensions, insert text
    --simple tree walk to return all filenames in the top directory
"""

from __future__ import print_function
import os
import numpy as np
import h5py
from collections import OrderedDict

def save_data(filename, compress=None, **kwargs):
    """
    save data to an HDF5 file
    """
    if (type(compress) is bool): # set default if boolean flag is used
        if (compress):
            compress = "gzip"
        else:
            compress = None

    # create file,
    # it closes automagically when using the with...as...:
    with h5py.File(filename, 'w') as hf:

        # save data
        for dataset in kwargs.keys():
            data = kwargs[dataset]
            if (len(np.shape(data)) != 0): # don't compress scalar data
                comp = compress
            else:
                comp = None
            hf.create_dataset(dataset, data=data, compression=comp)

    return

def read_data(filename, list_contents=False):
    """
    read data from HDF5 file
    """
    data = OrderedDict() # output data storage

    # open file
    with h5py.File(filename, 'r') as hf:

        # read data into output dictionary
        for dataset in hf.keys():
            try:
                data[dataset] = hf[dataset][:] # read array data
            except:
                try:
                    data[dataset] = hf[dataset][()] # read scalar data
                except:
                    print("Could not read data {} from file {}".format(dataset, filename))
                    continue

    # print list of contents in file
    if (list_contents):
        print("\nListing datasets from {}".format(filename))
        print("-------------------------------------------")
        for i, dataset in enumerate(data.keys()):
            print("\t{:2d}-th dataset is {}".format(i, dataset))
        print("-------------------------------------------\n")

    return data, data.keys()

def outputname(filename, insert=None, ext=None, verbose=False):
    """
    parse a filename and insert text before the extension

    input values
    -------------
    filename  ---  string, filename to be parsed
    insert    ---  string (optional), text to be inserted between
                   basename and extension
    ext       ---  string (optional), set the ending filename's extension
                   should include the ".", for example ext=".eps" or ext=".png"
    verbose   ---  boolean (optional), print status information

    return value
    -------------
    output    ---  string, resulting filename
    """
    if (verbose):
        print("...incoming filename is {}".format(filename))
    # extract the basename and the extension
    split = os.path.splitext(filename)
    basename = split[0]
    extension = split[1]

    # change the extension
    if (ext is not None):
        extension = ext
        if (verbose):
            print("...changing extension to {}".format(ext))

    # insert text if necessary
    if (insert is not None):
        basename = basename + insert
        if (verbose):
            print("...inserting text = {}".format(insert))

    # reconstruct the new output name
    output = basename + extension
    if (verbose):
        print("...final name = {}".format(output))

    return output

def treewalk(top_dir, include_ext=[], exclude_dirs=[]):
   """
   walk a directory tree and return full path of all files that were found

   include_ext is a list of file extensions to include. for example to
   find all fortran files, pass include_ext=["f90", "F90", "f", "F"]

   exclude_dirs is a list of directories to exclude. for example to
   exclude the "build" directory, pass exclude_dirs=["build"]
   """
   if (len(include_ext) == 0):
       include_all_files = True
   else:
       include_all_files = False
       include_ext = tuple(include_ext)

   filelist = []
   for root, dirs, files in os.walk(top_dir):

       # remove the excluded directories
       dirs[:] = [d for d in dirs if d not in exclude_dirs]

       # only include the correct file extensions
       if (include_all_files):
           for f in files:
               _f = os.path.abspath(os.path.join(root, f))
               filelist.append(_f)
       else:
           for f in files:
               _f = os.path.abspath(os.path.join(root, f))
               if (_f.endswith(include_ext)):
                   filelist.append(_f)

   return filelist

