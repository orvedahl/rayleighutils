
# record the version
__version__ = '2.0.0'

# public calculus routines
from .calculus import differentiate
from .calculus import integrate

# public fft routines
from .fft import fft
from .fft import signals

# public filesystem routines
from .filesystem import file_io

# public fitting routines
from .fitting import fitting
from .fitting import fit_RaPr

# public interpolation routines
from .interpolation import interp

# public polynomials routines
from .polynomials import chebyshev, legendre

# public plotting routines
from .plotting import plotting

# public miscelaneous routines
from .misc import parser
from .misc import grids

