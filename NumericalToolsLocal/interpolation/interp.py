"""
Module for performing interpolation

Supported options, default values listed in []:
    --func, what radial basis function to use [multiquadric]
    --epsilon, what shape factor to use with rbf method [None]

Methods include:
    --method=rbf, radial basis function, default method
        available RBFs and their approximate accuracy
        using the default shape parameter
            linear        2nd order
            multiquadric  2nd order
            thin-plate    3rd order
            cubic         4th order
            quintic       6th order (default RBF)
            inverse       bad scaling for functions that were tested
            gaussian      bad scaling for functions that were tested
    --method=nearest,   1st order
    --method=zero,      1st order
    --method=linear,    2nd order
    --method=slinear,   2nd order
    --method=quadratic, 3rd order
    --method=cubic,     4th order
"""

from __future__ import print_function
import numpy as np
from scipy.interpolate import interp1d as spinterp1d
from .rbf_interpolation import Rbf
from sys import exit

def interp(data, grid, method='rbf', axis=-1, **kwargs):
    """
    method to perform interpolation, return a callable function
    """

    supported = ['rbf', 'linear', 'nearest', 'zero', 'slinear', 'quadratic', 'cubic']

    method = method.lower()
    if (method not in supported):
        print("\n---ERROR: unsupported interpolation method = {}".format(method))
        print("\t\tsupported methods = {}".format(supported))
        exit()

    # choose integrator and calling args based on method
    if (method != 'rbf'):
        f_interp = spinterp1d(grid, data, kind=method, axis=axis)

    else:
        func = kwargs.pop("function", "quintic")
        epsilon = kwargs.pop("epsilon", None)
        f_interp = Rbf(grid, data, function=func)

    return f_interp

