"""
Build a data cube for particular data types

Usage:
    data_cube.py [options]

Options:
    --start=<s>        Starting file [default: 0]
    --end=<e>          Ending file [default: 100000000]
    --last=<l>         Use last <l> number of files (so files[-l:] is used) [default: 0]
    --data-dir=<d>     Specify location of data [default: ./]
    --Pm=<p>           Use to convert to magnetic time [default: 1]
    --output=<o>       Store data cube to file named <o>
    --quantities=<q>   Comma separated list of quantities to store, can be names or indices
    --lut=<l>          Use a different lut.py file
    --python           Use pure python diagnostic_reading file [default: False]

"""

from __future__ import print_function
import env
from sys import exit
from Utilities.look_up_quantity import lut_lookup, nonzero_lut
from Utilities.time_utils import Timer, TimeSeries
from ReadRayleigh.import_diagnostics import diagnostics
import NumericalToolsLocal.public as NT

def read_data_cube(filename):

    T = Timer(); T.start()
    try:
        data, keys = NT.read_data(filename)
        print("\t...success")
    except:
        data, keys = None, None
        print("\t...FAILED")
    T.stop(); dt = T.print_interval()

    return data, keys

def build_data_cube(start_file, end_file, last_number_files, data_dir, Pm, output, quantities,
         python_diag, new_lut, **kwargs):

    # import reading routines
    imports = ["build_file_list", "ShellSpectra", "ShellSlice", "AzAverage", "GlobalAverage",
               "ShellAverage", "Meridional_Slice", "Equatorial_Slice", "Point_Probe"]
    diags = diagnostics(imports, python=python_diag, package="ReadRayleigh")
    build_file_list  = diags[imports[0]]
    ShellSpectra     = diags[imports[1]]
    ShellSlice       = diags[imports[2]]
    AzAverage        = diags[imports[3]]
    GlobalAverage    = diags[imports[4]]
    ShellAverage     = diags[imports[5]]
    Meridional_Slice = diags[imports[6]]
    Equatorial_Slice = diags[imports[7]]
    Point_Probe      = diags[imports[8]]

    # change lut
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    # build quantity list
    if (quantities is None):
        quant_list = None
    else:
        quantities = quantities.replace(" ",  "") # remove white space
        quantities = quantities.replace("\n", "")
        quantities = quantities.replace("\r", "")
        quantities = quantities.replace("\t", "")
        quantities = quantities.split(",")        # split it
        quant_list = []
        for q in quantities:
            try:
                ind = int(q)
            except:
                ind = lut_lookup(q)
                if (ind is None):
                    raise ValueError("ERROR: quantity \"{}\" was not found".format(q))
            quant_list.append(ind)

    # map directory name to the routine that opens those files
    file_opener = {}                #  file opener, time_axis, data_attr, other attr to save
    file_opener['Shell_Spectra']    = [ShellSpectra, -1, "vals",
                                       "lmax", "mmax", "nr", "radius"]
    file_opener['Shell_Slices']     = [ShellSlice,   -1, "vals",
                                       "nphi", "ntheta", "nr", "costheta", "radius"]
    file_opener['Shell_Avgs']       = [ShellAverage, -1, "vals",
                                       "nr", "radius"]
    file_opener['AZ_Avgs']          = [AzAverage, -1, "vals",
                                       "ntheta", "nr", "costheta", "radius"]
    file_opener['G_Avgs']           = [GlobalAverage, 0, "vals"]
    file_opener['Meridional_Slice'] = [Meridional_Slice, -1, "vals",
                                       "nphi", "ntheta", "nr", "phi", "costheta", "radius"]
    file_opener['Equatorial_Slice'] = [Equatorial_Slice, -1, "vals",
                                       "nphi", "nr", "phi", "radius"]
    file_opener['Point_Probe']      = [Point_Probe, -1, "vals",
                                       "nphi", "ntheta", "nr", "phi", "costheta", "radius"]

    file_type = None
    for key in file_opener.keys():
        if (key in data_dir):
            file_type = key
            break
    if (file_type is None):
        print("\nERROR: cannot determine file_type, try using the --data-dir=<d> option\n")
        return

    # build file list
    files = build_file_list(start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # generate output filename
    if (output is None):
        output = "data_cube_" + file_type
    output = NT.outputname(output, ext=".stored")

    a = file_opener[file_type][0](files[0], path='', quantity=quant_list)
    available_quants = nonzero_lut(a.lut)

    print("\nResults:")
    print("-----------------------------------------")
    print("Data directory = {}".format(data_dir))
    print("File opener    = {}".format(file_type))
    print("\nQuantity list (found {}):".format(len(available_quants.keys())))
    for q in available_quants.keys():
        print("\t{:4d} --- {}".format(q, available_quants[q]))
    print("-----------------------------------------")

    print("\nCompiling data cube...")
    opener    = file_opener[file_type][0]
    axis      = file_opener[file_type][1]
    data_attr = file_opener[file_type][2]
    attrs     = file_opener[file_type][3:]
    data, time = TimeSeries(files, opener, axis=axis, data_attr=data_attr,
                            quantity=quant_list)
    time *= Pm

    # store data
    print("\nStoring results to file {}".format(output))
    
    storage = {"time":time, "lut":a.lut, "data":data, "datatype":file_type}
    for attr in attrs:
        storage[attr] = getattr(a, attr)

    T = Timer(); T.start()
    try:
        NT.save_data(output, compress=True, **storage)
        print("\t...success")
        success = True
    except:
        print("\t...FAILED")
        success = False
    T.stop(); dt = T.print_interval()

    if (not success):
        exit()

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    data_dir = args['--data-dir']
    Pm = float(args['--Pm'])
    output = args['--output']
    quantities = args['--quantities']
    new_lut = args['--lut']
    python_diag = args['--python']

    build_data_cube(start_file, end_file, last_number_files, data_dir, Pm, output, quantities,
         python_diag, new_lut)

