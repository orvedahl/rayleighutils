"""
Module to list the available quantities in the lut

Usage:
    list_lut.py [options]

Options:
    --start=<s>        Starting file [default: 0]
    --end=<e>          Ending file [default: 100000000]
    --last=<l>         Use last <l> number of files (so files[-l:] is used) [default: 0]
    --data-dir=<d>     Specify location of data [default: ./]
    --lut=<l>          Use a different lut.py file
    --iter=<i>         Which file to read lut from [default: 0]
    --python           Use pure python diagnostic_reading file [default: False]
"""
from __future__ import print_function
import env
import os
from look_up_quantity import lut_lookup, print_lut, nonzero_lut
from time_utils import Timer
from ReadRayleigh.import_diagnostics import diagnostics
from collections import OrderedDict

def list_lut(start_file, end_file, last_number_files, data_dir, new_lut, file_iter,
             python_diag, **kwargs):
    """
    list the possible values in the lut given a directory
    that contains the data files
    """

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        from Utilities.look_up_quantity import change_lut
        change_lut(new_lut, verbose=1)

    # import reading
    imports = ["build_file_list", "ShellSpectra", "ShellSlice", "AzAverage", "GlobalAverage",
               "ShellAverage", "Meridional_Slice", "Equatorial_Slice", "Point_Probe"]
    diags = diagnostics(imports, python=python_diag, package="ReadRayleigh")

    files = diags["build_file_list"](start_file, end_file, path=data_dir)
    files = files[-last_number_files:]

    # map directory name to the routine that opens those files
    file_opener = {}
    file_opener['Shell_Spectra'] = diags["ShellSpectra"]
    file_opener['Shell_Slices'] = diags["ShellSlice"]
    file_opener['Shell_Avgs'] = diags["ShellAverage"]
    file_opener['AZ_Avgs'] = diags["AzAverage"]
    file_opener['G_Avgs'] = diags["GlobalAverage"]
    file_opener['Meridional_Slice'] = diags["Meridional_Slice"]
    file_opener['Equatorial_Slice'] = diags["Equatorial_Slice"]
    file_opener['Point_Probe'] = diags["Point_Probe"]

    file_type = None
    for key in file_opener.keys():
        if (key in data_dir):
            file_type = key
            break
    if (file_type is None):
        print("\nERROR: cannot determine file_type from directory name\n")
        return

    print("\nResults:")
    print("-----------------------------------------")
    print("Data directory = {}".format(data_dir))
    print("File opener = {}".format(file_type))
    print("-----------------------------------------")
    nfiles = len(files)
    print("\nFound {} files...".format(nfiles))

    # only read a few files:
    inds = [file_iter]
    for i in inds:
        print("\nReading file = {}".format(files[i]))
        a = file_opener[file_type](filename=files[i], path='')
        print_lut(a.lut, not_found=5000)

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    data_dir   = args['--data-dir']
    new_lut    = args['--lut']
    file_iter  = int(args['--iter'])
    python_diag = args['--python']

    list_lut(start_file, end_file, last_number_files, data_dir, new_lut, file_iter,
             python_diag)

