"""
Print progress percent for loops using only single line

R. Orvedahl 12-19-2016

Usage:
    loop_progress.py [options]

Options:
    --num=<n>     Loop over <n> values [default: 500000]
"""

from __future__ import print_function
import sys

def print_progress(progress_string):
    """
    print string to screen by overwriting previous line
    """
    sys.stdout.write("\r"+progress_string)
    sys.stdout.flush()
    return

def progress_bar(iteration, total, prefix='\tprogress:', suffix='complete',
                 decimals=1, length=50, fill='#'):
    """
    Call in a loop to create terminal progress bar
    params:
        iteration - Required : current iteration (Int)
        total     - Required : total iterations (Int)
        prefix    - Optional : prefix string (Str)
        suffix    - Optional : suffix string (Str)
        decimals  - Optional : positive number of decimals in percent complete (Int)
        length    - Optional : character length of bar (Int)
        fill      - Optional : bar fill character (Str)
    example:
        progress_bar(0, 256, prefix="Progress:", suffix="Complete", length=50)
        for i in range(256):
            ....
            progress_bar(i+1, 256, prefix="Progress:", suffix="Complete", length=50)
    produces output:
        Progress: |######################################------| 90.0% Complete
    """
    if (total > 0):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
    else:
        percent = ("{0:." + str(decimals) + "f}").format(100)
        filledLength = int(length)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r{} |{}| {}% {}'.format(prefix, bar, percent, suffix), end='\r')

    # Print New Line on Complete
    if iteration == total: 
        print()

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    num = int(args['--num'])

    for i in range(num):
        outstring = "\tloop progress = {:.2f}".format(100.*i/num)
        print_progress(outstring)
    print("\n1st post-loop-print needs special care\n")

