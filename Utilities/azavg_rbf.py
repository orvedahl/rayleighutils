"""
Interpret data in the meridional plane using Radial Basis Functions.

Data is assumed to be function of radius and latitude:

    r in [r_i, r_o]  and  theta in [0, pi]

"""

from __future__ import print_function, division

import sys
import numpy as np
#from scipy.special import xlogy
from scipy import linalg
from six import callable, get_method_function, get_function_code

class Rbf(object):
    """
    Rbf(*args)

    A class for radial basis function approximation/interpolation to
    N-dimensional scattered data.

    Parameters
    ----------
    *args : arrays
        x, y, z, ..., d, where x, y, z, ... are coordinates of the nodes
        and d is the array of values at the nodes
    function : string, optional
        The radial basis function, based on the radius, r, given by the norm
        (default is Euclidean distance; the default is 'multiquadric'::

        'multiquadric': sqrt((r/self.epsilon)**2 + 1)
        'inverse': 1.0/sqrt((r/self.epsilon)**2 + 1)
        'gaussian': exp(-(r/self.epsilon)**2)
        'linear': r
        'cubic': r**3
        'quintic': r**5
        'thin-plate': r**2*log(r)
        'septic': r**7
        'nonic': r**9
    epsilon : float, optional
        Adjustable constant for gaussian or multiquadrics functions
        - defaults to approximate average distance between nodes (which is
        a good start).
    smooth : float, optional
        Values greater than zero increase the smoothness of the approximation.
        0 is for interpolation (default), the function will always go through
        nodal points in this case.

    Examples
    --------
    >>> import Rbf_interpolation
    >>> x,y,z,d = np.random.rand(4, 50)
    >>> rbfi = Rbf(x, y, z, d) # RBF interpolator instance
    >>> xi = yi = zi = np.linsapce(0, 1, 20)
    >>> di = rbfi(xi, yi, zi)  # interpolated values
    >>> di.shape
    (20,)

    """

    def _euclidean_norm(self, x1, x2):
        return np.sqrt(((x1 - x2)**2).sum(axis=0))

    ########################################################################
    # functions and their derivatives
    ########################################################################
    def _hfx_gaussian(self, r):
        return np.exp(-(r/self.epsilon)**2)
    def _hdfdx_gaussian(self, r):
        return -2*r/self.epsilon/self.epsilon*self._hfx_gaussian(r)
    def _hd2fdx2_gaussian(self, r):
        return (-4*r**2/self.epsilon**4 - 2/self.epsilon**2)*self._hfx_gaussian(r)

    # a broad class of functions fit the form phi(r) = (1+r**2)**alpha
    def _beta(self, r):
        return 1 + (r/self.epsilon)**2
    def _phi(self, r, alpha):
        return self._beta(r)**alpha
    def _phi_prime(self, r, alpha):
        return 2*r*alpha/self.epsilon**2*self._phi(r,alpha)/self._beta(r)
    def _phi_primeprime(self, r, alpha):
        A = (2*r*alpha/self._beta(r)/self.epsilon**2)**2
        B = 2*alpha/self.epsilon**2*(1 - 2*r/self.epsilon**2/self._beta(r))
        return _phi(self,r,alpha)*(A+B)

    def _hfx_multiquadric(self, r):
        return self._phi(r, 0.5)
    def _hdfdx_multiquadric(self, r):
        return self._phi_prime(r, 0.5)
    def _hd2fdx2_multiquadric(self, r):
        return self._phi_primeprime(r, 0.5)

    def _hfx_inverse_multiquadric(self, r):
        return self._phi(r, -0.5)
    def _hdfdx_inverse_multiquadric(self, r):
        return self._phi_prime(r, -0.5)
    def _hd2fdx2_inverse_multiquadric(self, r):
        return self._phi_primeprime(r, -0.5)

    def _hfx_linear(self, r): # first order
        return r
    def _hdfdx_linear(self, r):
        return np.zeros_like(r)
    def _hd2fdx2_linear(self, r):
        return np.zeros_like(r)

    def _hfx_thin_plate(self, r): # 2nd order
        phi = np.zeros_like(r)
        ind = np.where(r!=0.)
        phi[ind] = r[ind]**2*np.log(r[ind])
        return phi
    def _hdfdx_thin_plate(self, r):
        phi = np.zeros_like(r)
        ind = np.where(r!=0.)
        phi[ind] = 2*r[ind]*np.log(r[ind]) + r[ind]
        return phi
    def _hd2fdx2_thin_plate(self, r):
        phi = np.zeros_like(r)
        one = np.ones_like(r)
        ind = np.where(r!=0.)
        phi[ind] = 3*one + 2*np.log(r[ind])
        return phi

    def _hfx_cubic(self, r): # third order
        return r**3
    def _hdfdx_cubic(self, r):
        return 3*r**2
    def _hd2fdx2_cubic(self, r):
        return 6*r

    def _hfx_quintic(self, r): # fifth order
        return r**5
    def _hdfdx_quintic(self, r):
        return 5*r**4
    def _hd2fdx2_quintic(self, r):
        return 20*r**3

    def _hfx_septic(self, r): # seventh order
        return r**7
    def _hdfdx_septic(self, r):
        return 7*r**6
    def _hd2fdx2_septic(self, r):
        return 42*r**5

    def _hfx_nonic(self, r): # ninth order
        return r**9
    def _hdfdx_nonic(self, r):
        return 9*r**8
    def _hd2fdx2_nonic(self, r):
        return 72*r**7

    def _init_function(self, r):
        if (isinstance(self.function, str)):
            # function was a string, map it to the desired RBF
            self.function = self.function.lower()
            _mapped = {'inverse':'inverse_multiquadric',
                       'inverse multiquadric':'inverse_multiquadric',
                       'thin-plate':'thin_plate'}
            if (self.function in _mapped):
                self.function = _mapped[self.function]

            func_name = "_hfx_" + self.function
            deriv_name = "_hdfdx_" + self.function
            sec_deriv_name = "_hd2fdx2_" + self.function

            failed = False
            if (hasattr(self, func_name)):
                self._function = getattr(self, func_name)
            else:
                failed = True
            if (hasattr(self, deriv_name)):
                self._deriv_function = getattr(self, deriv_name)
            else:
                failed = True
            if (hasattr(self, sec_deriv_name)):
                self._sec_deriv_function = getattr(self, sec_deriv_name)
            else:
                failed = True

            if (failed):
                functionlist = [x[3:] for x in dir(self) if x.startswith('_h_')]
                _passed = "Unrecognized function = {}\n".format(self.function)
                raise ValueError(_passed + "Function must be callable or one of " +
                                 ", ".join(functionlist))
        else:
            functionlist = [x[3:] for x in dir(self) if x.startswith('_hfx_')]
            raise ValueError("Function must be one of " + ", ".join(functionlist))

        a0 = self._function(r)
        if (a0.shape != r.shape):
            raise ValueError("Callable must take array and return array of same shape")

        b0 = self._deriv_function(r)

        c0 = self._sec_deriv_function(r)

        return a0, b0, c0

    def __init__(self, *args, **kwargs):
        # extract individual coordinates and function values
        self.xi = np.asarray([np.asarray(a, dtype=np.float64).flatten()
                             for a in args[:-1]])
        self.N = self.xi.shape[-1]
        self.di = np.asarray(args[-1]).flatten()

        if (not all([x.size == self.di.size for x in self.xi])):
            raise ValueError("All arrays must be equal length")

        # extract the various keyword arguments
        self.norm = self._euclidean_norm
        r = self._call_norm(self.xi, self.xi) # get matrix of distances
        self.epsilon = kwargs.pop("epsilon", None)
        if (self.epsilon is None):
            # default is average distance between nodes based on bounding hypercube
            dim = self.xi.shape[0]
            ximax = np.amax(self.xi, axis=1)
            ximin = np.amin(self.xi, axis=1)
            edges = ximax-ximin
            edges = edges[np.nonzero(edges)]
            self.epsilon = np.power(np.prod(edges)/self.N, 1.0/edges.size)
        self.smooth = kwargs.pop("smooth", 0.0)

        self.function = kwargs.pop("function", "multiquadric")

        # save every other option that was passed
        for item, value in kwargs.items():
            setattr(self, item, value)

        # use RBF function and evaluate at the matrix of distances
        # to get the `A' matrix. use this and the given function values
        # to solve for the weights
        A, dA, d2A = self._init_function(r)
        self.A  = A - np.eye(self.N)*self.smooth
        self.dA = dA
        self.d2A = d2A
        self.nodes = linalg.solve(self.A, self.di)

    def _make_multidimensional(self, x, first=True):
        x = np.asarray(x)
        if (first):
            x = x[...,np.newaxis,:]
        else:
            x = x[...,:,np.newaxis]
        return x

    def _array_diff(self, x1, x2):
        x1 = self._make_multidimensional(x1, first=False)
        x2 = self._make_multidimensional(x2, first=True )
        return np.squeeze(x1-x2)

    def _array_add(self, x1, x2):
        x1 = self._make_multidimensional(x1, first=False)
        x2 = self._make_multidimensional(x2, first=True )
        return np.squeeze(x1+x2)

    def _array_multiply(self, x1, x2):
        x1 = self._make_multidimensional(x1, first=False)
        x2 = self._make_multidimensional(x2, first=True )
        return np.squeeze(x1*x2)

    def _array_divide(self, x1, x2):
        x1 = self._make_multidimensional(x1, first=False)
        x2 = self._make_multidimensional(x2, first=True )
        return np.squeeze(x1/x2)

    def _call_norm(self, x1, x2):
        # make arrays multi dimensional
        # such that x1-x2 is an array
        x1 = self._make_multidimensional(x1, first=False)
        x2 = self._make_multidimensional(x2, first=True )
        return self.norm(x1, x2)

    def __call__(self, *args, **kwargs):

        deriv = kwargs.pop("deriv", None)

        # evaluate the interpolant at the given points
        args = [np.asarray(x) for x in args]
        if (not all([x.shape == y.shape for x in args for y in args])):
            raise ValueError("Array lengths must be equal")
        shp = args[0].shape
        xa = np.asarray([a.flatten() for a in args], dtype=np.float64)
        r = self._call_norm(xa, self.xi)
        if (deriv is None):
            # regular function evaluation
            return np.dot(self._function(r), self.nodes).reshape(shp)

        elif (deriv == "1d"):
            # simple derivative of the basis function
            x_minus_xi = self._array_diff(xa, self.xi)
            ddx = self._deriv_function(r) * x_minus_xi / r
            ddx = np.dot(ddx, self.nodes)
            return ddx.reshape(shp)

        elif (deriv == "r"):
            # get radial derivative
            x = xa[0]; y = xa[1]
            xi = self.xi[0]; yi = self.xi[1]
            x2  = self._array_multiply(x,x)
            y2  = self._array_multiply(y,y)
            xxi = self._array_multiply(x,xi)
            yyi = self._array_multiply(y,yi)
            print((x2+y2)[np.where(x2+y2<0)])
            rad = np.sqrt(x2+y2)
            N = (x2 + y2 - xxi - yyi) / rad / r
            ddr = self._deriv_function(r) * N
            ddr = np.dot(ddr, self.nodes)
            return ddr.reshape(shp)

        elif (deriv == "th"):
            # get theta derivative
            x = xa[0]; y = xa[1]
            xi = self.xi[0]; yi = self.xi[1]
            xiy = self._array_multiply(xi,y)
            xyi = self._array_multiply(x,yi)
            M = (xiy - xyi) / r
            ddth = self._deriv_function(r) * M
            ddth = np.dot(ddth, self.nodes)
            return ddth.reshape(shp)

        elif (deriv == "r2"):
            # get second radial derivative
            x = xa[0]; y = xa[1]
            xi = self.xi[0]; yi = self.xi[1]
            x2  = self._array_multiply(x,x)
            y2  = self._array_multiply(y,y)
            xxi = self._array_multiply(x,xi)
            yyi = self._array_multiply(y,yi)
            rad = np.sqrt(x2+y2)
            N = (x2 + y2 - xxi - yyi) / rad / r

            d2dr2 = self._deriv_function(r)*(1 - N**2)/r \
                  + N**2*self._sec_deriv_function(r)

            d2dr2 = np.dot(d2dr2, self.nodes)
            return d2dr2.reshape(shp)

        elif (deriv == "th2"):
            # get second theta derivative
            x = xa[0]; y = xa[1]
            xi = self.xi[0]; yi = self.xi[1]
            xy  = self._array_multiply(x,y)
            xiy = self._array_multiply(xi,y)
            xyi = self._array_multiply(x,yi)
            xxi = self._array_multiply(x,xi)
            yyi = self._array_multiply(y,yi)
            M = (xiy - xyi) / r

            d2dth2 = self._deriv_function(r)*(xxi + yyi - M**2)/r \
                   + M**2*self._sec_deriv_function(r)

            d2dth2 = np.dot(d2dth2, self.nodes)
            return d2dth2.reshape(shp)

import matplotlib.pyplot as plt
from azavg_calculus import Tile
import NumericalToolsLocal.public as NT

def f(x):
    return 4.*(np.sin(2*x) + 0.1)
def dfdx(x):
    return 8.*np.cos(2*x)
def convert_xy(x,y):
    r  = np.sqrt(x*x + y*y)
    th = np.arctan2(y,x)
    return r,th
def F(x,y):
    r,th = convert_xy(x,y)
    return np.exp(-r*r)*np.sin(2*th)
def dFdr(x,y):
    r,th = convert_xy(x,y)
    return -2*r*F(x,y)
def d2Fdr2(x,y):
    r,th = convert_xy(x,y)
    return -2*r*dFdr(x,y) - 2*F(x,y)
def dFdth(x,y):
    r,th = convert_xy(x,y)
    return np.exp(-r*r)*2*np.cos(2*th)
def d2Fdth2(x,y):
    r,th = convert_xy(x,y)
    return -np.exp(-r*r)*4*np.sin(2*th)

testing = "2d"
deriv = None # "r", "th", "r2", "th2"
deriv = "r"
show = False

if (testing == "2d"):
    Nr = 16; lmax = 63
    rlo = 1; rhi = 4

    # coarse grid
    Nth = int(1.5*(lmax+1))
    G = NT.ChebGrid(Nr, a=rlo, b=rhi, endpoints=1, zeros=1)
    r1d = G.xab[::-1]
    G = NT.LegendreGrid(Nth)
    th1d = np.arccos(G.x)

    Th, R = Tile(th1d, r1d, (len(th1d), len(r1d)))
    X = R*np.cos(Th)
    Y = R*np.sin(Th)

    Nreval = 4*Nr
    rand = np.random.RandomState(seed=44)
    reval  = np.sort(rand.uniform(low=rlo, high=rhi, size=Nreval))
    Ntheval = 4*Nth
    theval = np.sort(rand.uniform(low=-1, high=1, size=Ntheval))
    theval = np.arccos(theval)

    Th_eval, R_eval = Tile(theval, reval, (len(theval), len(reval)))
    X_eval = R_eval*np.cos(Th_eval)
    Y_eval = R_eval*np.sin(Th_eval)

    funcs = ["inverse", "gaussian", "multiquadric", "nonic"]
    funcs = ["gaussian", "nonic"]
    funcs = ["nonic"]

    fx_eval = {None:F, "r":dFdr, "th":dFdth, "r2":d2Fdr2, "th2":d2Fdth2}

    print("\nCoarse resolution: Nr = {}, Nth = {}".format(Nr, Nth))
    print(  "  Eval resolution: Nr = {}, Nth = {}".format(Nreval, Ntheval))
    print("\nType of differentiation: {}".format(deriv))

    for func in funcs:

        interp_f = Rbf(X, Y, F(X,Y), function=func)

        interp_fx = interp_f(X_eval, Y_eval, deriv=deriv)

        _fx_eval = fx_eval[deriv](X_eval,Y_eval)
        max_F = np.amax(_fx_eval)
        min_F = np.amin(_fx_eval)
        error = np.abs(_fx_eval - interp_fx)
        print("RBF = {}".format(func))
        print("\tmin func value = {}".format(min_F))
        print("\tmax func value = {}".format(max_F))
        print("\tmin error = {}".format(np.amin(error)))
        print("\tmax error = {}".format(np.amax(error)))
        print("\tmin error = {}".format(np.amin(error)))
        print("\tavg error = {}".format(np.mean(error)))
        print("\tmed error = {}".format(np.median(error)))
        print("\tstd error = {}".format(np.std(error)))
        print()

elif (testing == "1d"):
    xlo = 0.0; xhi = 2*np.pi; N = 32
    xcoarse = np.linspace(xlo, xhi, N, endpoint=1)
    dx = xcoarse[1] - xcoarse[0]

    Neval = 256
    rand = np.random.RandomState(seed=44)
    xeval = np.sort(rand.uniform(low=xlo, high=xhi, size=Neval))

    fx = f(xcoarse)
    dfx = dfdx(xcoarse)
    if (deriv is not None):
        fx_eval = dfdx(xeval)
    else:
        fx_eval = f(xeval)

    funcs = ["inverse", "gaussian", "multiquadric", "nonic"]
    funcs = ["gaussian", "nonic"]

    print("\nCoarse resolution = {}, evaluation resolution = {}\n".format(N, Neval))

    for func in funcs:
        interp_f = Rbf(xcoarse, fx, function=func, epsilon=np.sqrt(N)*dx)

        interp_fx = interp_f(xeval,deriv=deriv)

        error = np.abs(fx_eval - interp_fx)
        print("RBF = {}".format(func))
        #print("\tmax error = {}".format(np.amax(error)))
        #print("\tmin error = {}".format(np.amin(error)))
        print("\tavg error = {}".format(np.mean(error)))
        print("\tmed error = {}".format(np.median(error)))
        print("\tstd error = {}".format(np.std(error)))
        print()

        if (show):
            plt.clf()
            plt.title("RBF = {}".format(func))
            if (deriv is not None):
                plt.plot(xcoarse, dfx, marker='o', color='k', linestyle='', label='nodes')
            else:
                plt.plot(xcoarse, fx, marker='o', color='k', linestyle='', label='nodes')
            plt.plot(xeval, interp_fx, color='r', linestyle='-', label='Rbf')
            plt.plot(xeval, fx_eval, color='g', linestyle='-', label='true')
            plt.legend(loc='best')
            plt.twinx()
            plt.plot(xeval, error, color='k', linestyle='--')
            plt.show()

