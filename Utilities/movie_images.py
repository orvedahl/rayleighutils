"""

    Make images for movies by repeatedly calling
    the proper plotting routine

Usage:
    movie_images.py [options]

Options:
    --start=<s>      Starting file [default: 0]
    --end=<e>        Ending file [default: 100000000]
    --output=<o>     Save images as <o>_iteration.png [default: movie_]
    --data-dir=<d>   Specify location of data [default: ./]
    --plot=<p>       What to plot: flux, slice, azavg, spectrum, energy_distro [default: slice]
    --magnetic       Plot magnetic field quantities [default: False]
    --Lnorm=<l>      Normalize 4*pi*r**2*Flux by <l> [default: 1]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import build_file_list

def main(start, end, output, data_dir, what_to_plot, magnetic, Lnorm):

    if (what_to_plot == 'slice'):
        plot_dir = 'Shell_Slices'
        import plot_shell_slice as plotter
    elif (what_to_plot == 'flux'):
        plot_dir = 'Shell_Avgs'
        import plot_energy_flux as plotter
    elif (what_to_plot == 'azavg'):
        plot_dir = 'AZ_Avgs'
        import plot_azavg as plotter
    elif (what_to_plot == 'spectrum'):
        plot_dir = 'Shell_Spectra'
        import plot_spectrum as plotter
    elif (what_to_plot == 'energy_distro'):
        plot_dir = 'Shell_Avgs'
        import plot_energy_distro as plotter
    else:
        print("\n---ERROR: unrecognized choice for what to plot: {}\n".format(what_to_plot))
        sys.exit()

    # plotting arguments that are used by every plotter
    args = {"saveplot":True, "data_dir":data_dir, "magnetic":magnetic,
            "rad_inds":[0,3,6], "last_number_files":0, "Lnorm":Lnorm,
            "no_plots":False, "avg_file":"time_averaged_azavg", "dont_timeavg":False,
            "savefig":True}

    # build file list
    files = build_file_list(start, end, path=data_dir+'/'+plot_dir)

    # loop over files and plot
    for f in files:
        # extract the file iteration
        iter = int(f.split("/")[-1])

        basename = output+str(iter)+'.png'

        # update the calling arguments for each plotting routine
        args["savefile"] = basename
        args["file_iteration"] = iter
        args["start_file"] = iter
        args["end_file"] = iter

        # call plotter with the apropriate arguments
        plotter.main(**args)

    print("\nSummary:\n")
    print("\tData directory = {}".format(data_dir))
    print("\tWhat to plot = {}".format(what_to_plot))
    print("\tOutput basename = {}".format(output))
    print("\tStarting iter = {}".format(start))
    print("\tEnding iter = {}".format(end))
    print("\n---Complete---\n")

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start = int(args['--start'])
    end   = int(args['--end'])
    basename = args['--output']
    data_dir = args['--data-dir']
    what_to_plot = args['--plot']
    magnetic = args['--magnetic']
    Lnorm = float(args['--Lnorm'])

    main(start, end, basename, data_dir, what_to_plot, magnetic, Lnorm)

