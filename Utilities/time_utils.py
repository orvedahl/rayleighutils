"""
Module to hold various utilities for dealing with time series data

Methods:
    --Timer: simple class to handle timing various tasks

    --Average: define how to take an average, default to standard mean

    --CombineAverages: given two separate data sets defined using
        the average (xbar_i) and the length (len_i), calculate
        the average of the combined data set:
            xbar = sum(x_1 + x_2) / (len(x1) + len(x2))

    --TimeAvg: calculate time average of a list of files. supply
        the the list of files, the method used to open each file
        and the axis that holds the time data.

    --TimeSeries: combine many diagnostic files together to get
        a continuous time series fo the data. supply the list
        of files, the method to open each file, and the axis
        of the time data.
"""
from __future__ import print_function
import numpy as np
import env
from loop_progress import print_progress, progress_bar
import time as timer
import NumericalToolsLocal.public as NT

class Timer():
    """
    time an event
    """
    def __init__(self):
        self.stime = None
        self.etime = None
        self.dt = None

    def start(self):
        self.stime = timer.time()

    def stop(self):
        self.etime = timer.time()

    def interval(self):
        self.dt = self.etime - self.stime
        return self.dt

    def print_interval(self):
        dt = self.interval()
        if (dt < 60):
            print("\n\tElapsed time = {:.3f} sec\n".format(dt))
        else:
            print("\n\tElapsed time = {:.3f} sec, {:.3f} min\n".format(dt, dt/60.))
        return dt

def Average(data, method='mean', axis=0, time=None):
    """
    define an average over the given axis
    """
    if (method == 'mean'):
        avg = np.mean(data, axis=axis)

    elif (method == 'int' and time is not None):
        # avg = 1/T * int_0^T f(t) dt
        #
        # this method skips the in-between points:
        #         .                           .
        #     .xxxxxxx.   .       .       .xxxx
        # .xxxxxxxxxxxxxxxx   .xxxxxxx.xxxxxxxx
        # xxxxxxxxxxxxxxxxx   xxxxxxxxxxxxxxxxx
        # 0---1---2---3---4---5---6---7---8---9
        #
        # if domain is split into [x0,...,x4] and [x5,...,x9]
        # then pass one will do int_x0^x4 and pass two will do int_x5^9
        # but there is a missing chunk: int_x4^x5
        #
        # produces only a small difference, but still don't have good way to fix it
        avg = NT.integrate(data, time, method='trap', axis=axis)
        T = time[-1] - time[0]
        avg /= T

    elif (method == 'int' and time is None):
        print("\n---ERROR: Average with method='int' requires time argument\n")
        avg = None
    else:
        print("\n---ERROR: unrecognized Average method = {}\n".format(method))
        avg = None

    return avg

def CombineAverages(xbar1, len1, xbar2, len2):
    """
    given two separate data sets
        xbar1 = F(x1)/len1
        xbar2 = F(x2)/len2
    calculate avg of whole data set
        xbar = F(x1+x2)/(len1+len2)
             = (len1*xbar1 + len2*xbar2) / (len1+len2)
    where F is some kind of averaging operation
    that is linear in its arguments, i.e.,
        F(a+b) = F(a) + F(b)
    """
    xbar1 = np.asarray(xbar1); xbar2 = np.asarray(xbar2)

    length = len1 + len2
    xbar = (float(len1)*xbar1 + float(len2)*xbar2) / float(length)

    return xbar, length

def TimeAvg(files, file_opener, axis=-1, data_attr="vals", method="mean", **opener_kwargs):
    """
    time average many diagnostic files, assumed to be sorted.
    axis specifies the "time" axis
    """
    T = Timer(); T.start()
    # read first file & allocate space
    a = file_opener(files[0], path='', **opener_kwargs)
    nrec_old = a.niter
    nfiles = len(files)

    # allow user to specify class location of data
    if (hasattr(a, data_attr)):
        file_data = getattr(a, data_attr)
    else:
        print("\n---ERROR: file does not have data attribute = {}\n".format(data_attr))
        return None, None

    # get average of first file
    data = Average(file_data, axis=axis, method=method, time=a.time)
    time_start = a.time[0]
    time_end   = a.time[-1]

    for i, f in enumerate(files[1:]):
        print_progress("\tprogress = {:.2f} %".format(100.*(i+1)/(nfiles-1)))
        a = file_opener(f, path='', **opener_kwargs)
        nrec = a.niter
        file_data = getattr(a, data_attr)

        # add average of this file to average of previous files
        avg = Average(file_data, axis=axis, method=method, time=a.time)
        data, nrec_old = CombineAverages(avg, nrec, data, nrec_old)

        time_end = a.time[-1]

        # free memory
        a = None; file_data = None
    print()
    T.stop(); interval = T.print_interval()

    # calculate the averaging time interval
    dt = time_end - time_start

    times = {"dt":dt, "tstart":time_start, "tend":time_end, "nrec":nrec_old}

    return data, times

def TimeAvg2(files, file_opener, axis=-1, data_attr="vals", method="mean", **opener_kwargs):
    """
    time average many diagnostic files, assumed to be sorted.
    axis specifies the "time" axis
    """
    # get a time series
    data, time = TimeSeries(files, file_opener, axis=axis, data_attr=data_attr,
                            **opener_kwargs)

    # average full time series
    avg = Average(data, time=time, method=method, axis=axis)

    t_start = time[0]; t_end = time[-1]; dt = t_end - t_start; nrec = len(time)
    times = {"dt":dt, "tstart":t_start, "tend":t_end, "nrec":nrec}

    # free memory
    data = None; time = None

    return avg, times

def TimeAvgSeries(files, file_opener, axis=-1, data_attr="vals", method="mean",
                  **opener_kwargs):
    """
    compile a time series and compute time average of diagnostic files.
    The files are assumed to be in sorted order, axis specifies the "time" axis,
    data_attr specifies the class attribute that holds the data
    """
    # get a time series
    data, time = TimeSeries(files, file_opener, axis=axis, data_attr=data_attr,
                            **opener_kwargs)

    # average full time series
    avg = Average(data, time=time, method=method, axis=axis)

    t_start = time[0]; t_end = time[-1]; dt = t_end - t_start; nrec = len(time)
    times = {"dt":dt, "tstart":t_start, "tend":t_end, "nrec":nrec}

    return avg, times, data, time


def TimeSeries(files, file_opener, axis=-1, data_attr="vals", **opener_kwargs):
    """
    Compile a time series of many diagnostic files, assumed to be sorted.
    Assumes nrec does not increase over the file list, it could decrease.

    axis specifies the "time" axis
    """
    T = Timer(); T.start()
    # read first file & allocate space
    a = file_opener(files[0], path='', **opener_kwargs)
    nrec = a.niter
    nfiles = len(files)

    # adjust shape of output data array
    ntime = nfiles*nrec

    # sometimes the first file and last file have smaller number of records
    # and it throws off the total count estimate. so try reading the
    # second file and the second to last file
    try:
        b = file_opener(files[1], path='', **opener_kwargs)
        nrecb = b.niter
        newtime = nrecb*nfiles
        if (newtime > ntime):
            ntime = newtime   # override old ntime if necessary
        b = None
    except:
        pass
    try:
        b = file_opener(files[-2], path='', **opener_kwargs)
        nrecb = b.niter
        newtime = nrecb*nfiles
        if (newtime > ntime):
            ntime = newtime   # override old ntime if necessary
        b = None
    except:
        pass

    # allow user to specify class location of data
    if (hasattr(a, data_attr)):
        file_data = getattr(a, data_attr)
    else:
        print("\n---ERROR: file does not have data attribute = {}\n".format(data_attr))
        return None, None

    dtype = file_data.dtype

    shape = np.shape(file_data)
    newshape = list(shape)
    newshape[axis] = ntime     # increase size of time axis
    newshape = tuple(newshape)
    data = np.zeros(newshape, dtype=dtype)  # allocate space
    time = np.zeros((ntime))

    # fill in data from first file
    istart = 0; iend = nrec; ntotal_recs = 0

    # slice(None) is equivalent to ":" when indexing arrays
    slc1 = [slice(None)]*len(newshape) # for accessing all entries in output data
    slc2 = [slice(None)]*len(shape)    # for accessing all entries in file's data
    slc1[axis] = slice(istart, iend)   # modify only the time axis
    slc2[axis] = slice(0, nrec)
    slc1t = tuple(slc1); slc2t = tuple(slc2)

    time[istart:iend] = a.time
    data[slc1t] = file_data[slc2t]
    ntotal_recs += nrec

    progress_bar(0, nfiles-1)
    for i,f in enumerate(files[1:]):
        a = file_opener(f, path='', **opener_kwargs)
        file_data = getattr(a, data_attr)

        # fill output data array
        nrec = a.niter
        istart = iend
        iend = istart + nrec

        # istart, iend, & nrec are different for every file
        # so remake the slice objects
        slc1[axis] = slice(istart, iend)
        slc2[axis] = slice(0, nrec)

        # convert slices to tuples
        slc1t = tuple(slc1); slc2t = tuple(slc2)

        # store data
        try:
            time[istart:iend] = a.time
        except ValueError:
            print("\n---ERROR: time array dimensions not the same")
            print(f)
            print(len(time[istart:iend]), len(a.time), nrec)
            print(istart, iend)
            print(a.time)
        data[slc1t] = file_data[slc2t]
        ntotal_recs += nrec

        # free memory
        a = None; file_data = None

        progress_bar(i+1, nfiles-1)
    T.stop(); interval = T.print_interval()

    if (ntotal_recs < ntime):
        slc1[axis] = slice(0, ntotal_recs)
        slc1t = tuple(slc1)
        data = data[slc1t]
        time = time[0:ntotal_recs]

    return data, time

def debugging(*args):
    print("debugging....")
    for a in args:
        print("\t{}".format(a))

