"""
Calculate thermal boundary layer thickness from Shell_Avg files

    1) use <S>:
               w_BL = int( max(<S>) - <S> )dr / max(<S>)

    2) find r_BL s.t.
               F_conductive(r_BL) = F_convective(r_BL)
               w_BL = r_o - r

Usage:
    calculate_BL.py [options]

Options:
    --start=<s>       Starting file [default: 0]
    --end=<e>         Ending file [default: 100000000]
    --last=<l>        Use last <l> number of files (so files[-l:] is used) [default: 0]
    --method=<m>      Which method from above to use [default: 1]
    --init-guess=<r>  Inital guess for BL thickness. Only used for Newton Find method
    --verbose         Print verbose information [default: False]
    --save            Save the image [default: False]
    --output=<o>      Save image as <o> [default: calculate_BL.png]
    --data-dir=<d>    Specify location of data [default: ./]
    --alt             Use older look up table [default: False]
"""

from __future__ import print_function
from look_up_quantity import shortcut_lookup
import numpy as np
from scipy.optimize import newton as newton_find
import env
from ReadRayleigh.diagnostic_reading import ReferenceState, build_file_list, ShellAverage
from NumericalToolsLocal import interp
from NumericalToolsLocal import integrate
my_integrator = integrate.integrate
volume_avg    = integrate.volume_avg

def main(start_file, end_file, last_number_files, save, output, data_dir,
         BL_method, verbose=False, init_guess=None, alt=False, **kwargs):

    # integration method
    method = 'cheb-end'

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'

    # read Reference file
    print(data_dir)
    ref = ReferenceState(filename="reference", path=data_dir)
    radius = ref.radius
    Nr = len(radius)
    rhobar = ref.density
    Tbar   = ref.temperature
    router = radius[0]   # radius is backwards...
    rinner = radius[-1]
    H = router - rinner

    # read Shell_Avgs file
    files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
    icount = 0.0
    for i,f in enumerate(files[-last_number_files:]):
        a = ShellAverage(f,path='')
        if (i == 0):
            if (not alt):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
                t_start = a.time[0]
            else:
                data = np.zeros((a.nr,a.nq),dtype='float64')
                t_start = a.time[0]
        if (not alt):
            for j in range(a.niter):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
                icount = icount+1.0
        else:
            for j in range(a.niter):
                data[:,:] = data[:,:]+a.vals[:,:,j]
                icount = icount+1.0
        t_end = a.time[-1]
    data = data/icount  # average is now stored in data

    delta_time = (t_end - t_start)
    if (verbose):
        print("\nFirst file: {}".format(files[-last_number_files:][0]))
        print("Last  file: {}".format(files[-last_number_files:][-1]))
        print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
        print("\nFirst time: {} secs".format(t_start))
        print("Last  time: {} secs".format(t_end))
        print("\nAveraged over {} secs\n".format(delta_time))

    if (BL_method == 1):
        i_s = a.lut[shortcut_lookup('s', alt=alt)]  # entropy
        if (not alt):
            S = data[:,0,i_s]
        else:
            S = data[:,i_s]

        from NumericalToolsLocal import chebyshev
        roots = chebyshev.roots(len(radius))[::-1]
        extrema = chebyshev.extrema(len(radius))[::-1]
        b = 1./(1.-0.35); a = b*0.35
        print(a,b)
        print(radius[-1], radius[0])
        rad = chebyshev.from_ab_space(radius, a, b)
        print(extrema[:3], extrema[-3:])
        print(roots[:3], roots[-3:])
        print(0.5*(roots[:3]+extrema[:3]), 0.5*(roots[-3:]+extrema[-3:]))
        print(rad[:3], rad[-3:])

        max_S = np.amax(S)
        Bdry_width = my_integrator(max_S - S, radius, method=method, data_reversed=True)
        Bdry_width /= max_S

    elif (BL_method == 2):
        # extract the appropriate fluxes
        i_cond_flux = a.lut[shortcut_lookup('cond_flux', alt=alt)] # Conductive flux
        i_conv_flux = a.lut[shortcut_lookup('conv_flux', alt=alt)] # Convective flux
        if (not alt):
            cond_flux = data[:,0,i_cond_flux]
            conv_flux = data[:,0,i_conv_flux]
        else:
            cond_flux = data[:,i_cond_flux]
            conv_flux = data[:,i_conv_flux]

        # get interpolant for each flux in order to get more exact bdry layer estimate
        F_cond_RBF = interp.interp(cond_flux, radius, method='rbf')
        F_conv_RBF = interp.interp(conv_flux, radius, method='rbf')

        # use secant method to solve F_cond_RBF(r) - F_conv_RBF(r) = 0
        # these are callable functions so can't simply subtract them
        # make your own func that does the subtraction
        def F(r):
            return F_cond_RBF(r) - F_conv_RBF(r)

        if (init_guess is not None):
            init_guess = float(init_guess)
            if (init_guess > 0):
                r_guess = router - init_guess
            else:
                # init_guess is interpreted as a fraction between 0,1
                # so r_guess = (1-|init_guess|)*router
                init_guess = abs(init_guess)
                r_guess = router*(1.-init_guess)
            multiple_finds = False
        else:
            multiple_finds = True
            r_guesss = []
            for d in [2., 3., 4., 8., 10., 16., 20., 32., 64.]:
                r_guesss.append(router - H/d)

        if (not multiple_finds):
            # single root find
            r_BL = newton_find(F, r_guess, maxiter=100)
            if (verbose):
                print("\tinitial guess = {:.6e}".format(r_guess))
                print("\tNewton result = {:.6e}".format(r_BL))
                print("\tNewton/r_o    = {:.6e}".format(r_BL/router))
                print("\t---> BL width = {:.6e}".format(router - r_BL))
                print("\t---> wBL/r_o  = {:.6e}\n".format(1 - r_BL/router))
        else:
            # a few different root finds
            r_BLs = []
            for r_guess in r_guesss:
                try:
                    _r_BL = newton_find(F, r_guess, maxiter=100)
                    r_BLs.append(_r_BL)
                    if (verbose):
                        print("\tinitial guess = {:.6e}".format(r_guess))
                        print("\tNewton result = {:.6e}".format(_r_BL))
                        print("\tNewton/r_o    = {:.6e}".format(_r_BL/router))
                        print("\t---> BL width = {:.6e}".format(router - _r_BL))
                        print("\t---> wBL/r_o  = {:.6e}\n".format(1 - _r_BL/router))
                except:
                    pass
            r_BL = np.median(r_BLs) # use median to avoid outliers

        print("Newton-Find found r_BL = {}".format(r_BL))

        Bdry_width = router - r_BL

    print("\nInputs:")
    print("\n\tData Dir = {}".format(data_dir))
    print("\tMethod = {}".format(BL_method))
    print("\nBoundary Layer Thickness (cm) = {}".format(Bdry_width))
    print("\nBoundary Layer Thickness (Mm) = {}\n".format(Bdry_width/1e8))

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(int(args['--start']), int(args['--end']), int(args['--last']),
             args['--save'], args['--output'], args['--data-dir'], int(args['--method']),
             init_guess=args['--init-guess'], verbose=args['--verbose'],
             alt=args['--alt'])

