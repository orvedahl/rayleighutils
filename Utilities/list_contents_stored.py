"""
List what datasets are contained in a file that was saved using NumericalToolsLocal

Usage:
    list_contents_stored.py [options] <file>

Options:
    --data-dir=<d>  Specify location of data [default: ./]

"""

from __future__ import print_function
import env
import os
import sys
import numpy as np
from NumericalToolsLocal.public import read_data

def main(filename, data_dir, **kwargs):

    # make full filename
    fname = os.path.join(data_dir, filename)

    # try to read file
    try:
        data, keys = read_data(fname)
    except:
        print("\n--> FAILED to read file = {}\n".format(fname))
        sys.exit()

    print("\n-----------------------------------------")
    print("Data directory = {}".format(data_dir))
    print("Filename = {}".format(filename))
    print("-----------------------------------------")
    print("\nContents of file...")
    has_time = False
    for k in keys:
        if (k in ["t", "time", "times"]):
            has_time = True
            itime = k
        if (len(np.shape(data[k])) > 0):
            value = "shape = {}".format(np.shape(data[k]))
        else:
            value = "value = {}".format(data[k])
        print("\t{:>10s} = {:>25s}, {}".format(k, type(data[k]), value))
    print()
    if (has_time):
        time = data[itime]
        sh = np.shape(time)
        if (len(sh) > 0):
            print("\tFirst time = {}".format(time[0]))
            print("\tLast time  = {}".format(time[-1]))
        else:
            print("\tTime = {}".format(time))
    else:
        print("\tNo time-like entry found")
    print()

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    filename = args['<file>']
    data_dir = args['--data-dir']

    main(filename, data_dir)

