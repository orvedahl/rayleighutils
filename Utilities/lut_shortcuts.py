"""
Module to hold the appropriate shortcuts for accessing the look up table

For example, to access the kinetic energy:

    ke_index = lut_lookup_name('ke')

instead of using the full name:

    ke_index = lut_lookup_name('kinetic_energy')

R. Orvedahl Oct-18-2017
"""

from collections import OrderedDict

shortcuts = OrderedDict()

# Full Kinetic Energies
shortcuts['ke']  = 'kinetic_energy'
shortcuts['rke'] = 'radial_ke'
shortcuts['tke'] = 'theta_ke'
shortcuts['pke'] = 'phi_ke'

# Mean Kinetic Energies
shortcuts['mke']  = 'mkinetic_energy'
shortcuts['mrke'] = 'radial_mke'
shortcuts['mtke'] = 'theta_mke'
shortcuts['mpke'] = 'phi_mke'

# Primed Kinetic Energies
shortcuts['kep'] = 'pkinetic_energy'

# Full Velocity
shortcuts['vr'] = 'v_r'
shortcuts['vt'] = 'v_theta'
shortcuts['vp'] = 'v_phi'

shortcuts['v2'] = 'vsq'

# Mass flux
shortcuts['rhovr'] = 'rhov_r'
shortcuts['rhovt'] = 'rhov_theta'
shortcuts['rhovp'] = 'rhov_phi'

# Thermodynamics
shortcuts['t'] = 'entropy'
shortcuts['s'] = 'entropy'
shortcuts['tm'] = 'entropy_m'
shortcuts['tp'] = 'entropy_p'

shortcuts['dtdr'] = 'entropy_dr'
shortcuts['dsdr'] = 'entropy_dr'

# Fluxes
shortcuts['cond_flux']     = 'cond_flux_r'
shortcuts['conv_flux']     = 'enth_flux_radial'
shortcuts['vol_heat_flux'] = 'vol_heat_flux'
shortcuts['enth_flux']     = 'enth_flux_radial'
shortcuts['visc_flux']     = 'visc_flux_r'
shortcuts['ke_flux']       = 'ke_flux_radial'

# Full Magnetic Energy
shortcuts['me']  = 'magnetic_energy'
shortcuts['rme'] = 'radial_me'
shortcuts['tme'] = 'theta_me'
shortcuts['pme'] = 'phi_me'

# Mean Magnetic Energy
shortcuts['mme']  = 'mmagnetic_energy'
shortcuts['mrme'] = 'radial_mme'
shortcuts['mtme'] = 'theta_mme'
shortcuts['mpme'] = 'phi_mme'

# Primed Magnetic Energy
shortcuts['mep'] = 'pmagnetic_energy'

# Magnetic Field
shortcuts['br'] = 'b_r'
shortcuts['bt'] = 'b_theta'
shortcuts['bp'] = 'b_phi'

# Custom
shortcuts['my_nu']    = 'my_nusselt'
shortcuts['my_visc']  = 'my_viscous_heating'
shortcuts['my_ohmic'] = 'my_ohmic_heating'
