from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ShellSpectra
from look_up_quantity import general_lookup
import numpy as np
import os

class myPowerSpectrum():
    """Rayleigh Power Spectrum Structure
    ----------------------------------
    self.niter                                    : number of time steps
    self.nr                                       : number of radii at which power spectra are available
    self.lmax                                     : maximum spherical harmonic degree l
    self.radius[0:nr-1]                           : radii of the shell slices output
    self.inds[0:nr-1]                             : radial indices of the shell slices output
    self.power[0:lmax,0:nr-1,0:niter-1,0:2]       : the velocity power spectrum.  The third
                                                  : index indicates (0:total,1:m=0, 2:total-m=0 power)
    self.mpower[0:lmax,0:nr-1,0:niter-1,0:2]      : the magnetic power spectrum
    self.iters[0:niter-1]                         : The time step numbers stored in this output file
    self.time[0:niter-1]                          : The simulation time corresponding to each time step
    self.magnetic                                 : True if mpower exists
    """
    #Power Spectrum Class - generated using shell spectra files
    def __init__(self, infile, quantity, alt=False, magnetic=False):
        self.magnetic = magnetic
        self.spectra_file_init(infile, quantity, alt=alt)

    def spectra_file_init(self, sfile, quantity, alt=False):

        a = ShellSpectra(filename=sfile,path='./')
        lmax = a.lmax
        nr = a.nr
        nt = a.niter

        self.lmax = lmax
        self.nr = nr
        self.niter = nt
        self.radius = a.radius
        self.inds = a.inds
        self.iters = a.iters
        self.time = a.time

        self.lut = a.lut

        if (quantity.lower() == 'vrs'):
            vrindex = a.lut[general_lookup(q='vr', alt=alt)]
            sindex  = a.lut[general_lookup(q='s',  alt=alt)]

            #the last index indicates 0:full power, 1:m0 power, 2:full-m0
            power = np.zeros((lmax+1,nr,nt,3),dtype='float64')

            # Next we grab one radial index and one time instance of each variable
            dims = (a.nell,a.nm,nr,nt)
            vr = np.reshape(a.vals[:,:,:,vrindex,:],dims)
            s  = np.reshape(a.vals[:,:,:,sindex, :],dims)

            for k in range(nt):
                for j in range(nr):
                    data = np.real(vr[:,0,j,k]*np.conj(s[:,0,j,k]))
                    power[:,j,k,1] += data
                    power[:,j,k,0] += data
                    for m in range(1,a.nm):
                        data = np.real(vr[:,m,j,k]*np.conj(s[:,m,j,k]))
                        power[:,j,k,0] += 0.5*data
                    power[:,j,k,2] = power[:,j,k,0]-power[:,j,k,1]

        elif (quantity.lower() == 'vhoriz'):
            vpindex = a.lut[general_lookup(q='vp', alt=alt)]
            vtindex = a.lut[general_lookup(q='vt', alt=alt)]

            #the last index indicates 0:full power, 1:m0 power, 2:full-m0
            power = np.zeros((lmax+1,nr,nt,3),dtype='float64')

            # Next we grab one radial index and one time instance of each variable
            dims = (a.nell,a.nm,nr,nt)
            vp = np.reshape(a.vals[:,:,:,vpindex,:],dims)
            vt = np.reshape(a.vals[:,:,:,vtindex,:],dims)

            for k in range(nt):
                for j in range(nr):
                    data =  np.real(vt[:,0,j,k])**2 + np.imag(vt[:,0,j,k])**2
                    data += np.real(vp[:,0,j,k])**2 + np.imag(vp[:,0,j,k])**2
                    power[:,j,k,1] += data
                    power[:,j,k,0] += data
                    for m in range(1,a.nm):
                        data =  np.real(vt[:,m,j,k])**2 + np.imag(vt[:,m,j,k])**2
                        data += np.real(vp[:,m,j,k])**2 + np.imag(vp[:,m,j,k])**2
                        power[:,j,k,0] += 0.5*data
                    power[:,j,k,2] = power[:,j,k,0]-power[:,j,k,1]

        else:
            # We use the lookup table to find index for desired quantity
            index = a.lut[general_lookup(q=quantity, alt=alt)]

            #the last index indicates 0:full power, 1:m0 power, 2:full-m0
            power = np.zeros((lmax+1,nr,nt,3),dtype='float64')

            # Next we grab one radial index and one time instance of each variable
            dims = (a.nell,a.nm,nr,nt)
            data = np.reshape(a.vals[:,:,:, index, :],dims)

            for k in range(nt):
                for j in range(nr):
                    power[:,j,k,1] += np.real(data[:,0,j,k])**2 + np.imag(data[:,0,j,k])**2
                    # there is a bug in current Rayleigh, need to adjust m>0 power by 2
                    power[:,j,k,0] += np.real(data[:,0,j,k])**2 + np.imag(data[:,0,j,k])**2
                    for m in range(1,a.nm):
                        power[:,j,k,0] += 0.5*(np.real(data[:,m,j,k])**2 + np.imag(data[:,m,j,k])**2)
                    power[:,j,k,2] = power[:,j,k,0]-power[:,j,k,1]

        self.power = power

        return

class mySpectralSpace():
    """return Rayleigh data as a function of spherical harmonic (l,m)
    """
    def __init__(self, infile, quantity, alt=False):
        self.alt = alt
        self.spectra_file_init(infile, quantity)

    def spectra_file_init(self, sfile, quantity):

        a = ShellSpectra(filename=sfile,path='./')
        lmax = a.lmax
        mmax = a.mmax
        nr = a.nr
        nm = a.nm
        nell = a.nell
        nt = a.niter

        self.mmax = mmax
        self.lmax = lmax
        self.nr = nr
        self.nm = nm
        self.nell = nell
        self.niter = nt
        self.radius = a.radius
        self.inds = a.inds
        self.iters = a.iters
        self.time = a.time

        self.lut = a.lut

        # We use the lookup table to find index for desired quantity
        index = a.lut[general_lookup(q=quantity, alt=self.alt)]

        # extract and reshape the data
        dims = (a.nell,a.nm,nr,nt)
        data = np.reshape(a.vals[:,:,:, index, :],dims)

        self.data = data

        return

