"""
Module to hold various utilities for dealing with spectral data, i.e., data(l,m)

Methods:
    --Timer: simple class to handle timing various tasks

    --Average: given two separate data sets defined using

    --TimeAvg: calculate time average of a list of files. supply

    --TimeSeries: combine many diagnostic files together to get
"""
from __future__ import print_function
import numpy as np
import env
from loop_progress import print_progress

def mag_sq(A, conj=False):
    """magnitude squared of a quantity"""
    if (not conj):
        return np.abs(A)**2
    else:
        return np.abs(np.conj(A))**2

def Alm(l,m):
    """
    spherical harmonic normalization
       Alm**2 = (2*l+1)/4/pi * (l-m)! / (l+m)!
    l is assumed >0
    m can be negative
    """
    A = (2*l+1.)/4/np.pi
    if (m >= 0):
        A *= np.math.factorial(l-m)
        A /= np.math.factorial(l+m)
    else:
        A *= np.math.factorial(l+m)
        A /= np.math.factorial(l-m)
    A = np.sqrt(A)
    return A

def GaussCoeffs(Clm, l, m):
    """
    convert from spherical harmonic coefficients to Gauss coefficients
    """
    if (m==0):
        glm = Alm(l,m)*np.real(Clm)
        hlm = 0.*np.imag(Clm)
    else:
        A = Alm(l,m)
        glm =  2.*A*np.real(Clm)
        hlm = -2.*A*np.imag(Clm)
    return glm, hlm

def calc_axisymmetric_energy(Br, Bth, Bphi):
    """incoming B should be size (l,t) and m=0"""

    nell, nt = np.shape(Br)
    axisym_E = np.zeros((nt), dtype=np.float64)

    # just use spherical harmonic coefficients
    # *****this relies on the fact that C_l^{-m} = (-1)^m (C_l^m)*
    for l in range(nell):
        axisym_E[:] += mag_sq(Br[l,:]) + mag_sq(Bth[l,:]) + mag_sq(Bphi[l,:])
    #axisym_E[:] += np.sum(mag_sq(Br[:,:]) + mag_sq(Bth[:,:]) + mag_sq(Bphi[:,:]), axis=0)

    # size of [t], it is actually B**2 and should be real
    return axisym_E

def calc_total_energy(Br, Bth, Bphi, lmax=None):
    """incoming B should be size (l,m,t)"""

    nell, nm, nt = np.shape(Br)
    total_energy = np.zeros((nt), dtype=np.float64)

    if (lmax is not None):
        # loop over first lmax ell-values
        nell = lmax

    for l in range(nell):
        # don't do complex conjugate of m=0 stuff, it is already real
        total_energy[:] += mag_sq(Br[l,0,:]) + mag_sq(Bth[l,0,:]) + mag_sq(Bphi[l,0,:])
        for m in range(1,nm):
            # include m>0 and m<0 using the conj(B)
            total_energy[:] += mag_sq(Br[l,m,:])  +mag_sq(Bth[l,m,:])  +mag_sq(Bphi[l,m,:])
            total_energy[:] += mag_sq(Br[l,m,:],1)+mag_sq(Bth[l,m,:],1)+mag_sq(Bphi[l,m,:],1)

    # size of [t], it is actually B**2 and should be real
    return total_energy

def calc_ell_energy(Br, Bth, Bphi, m0, *args):
    """incoming B should be size (m,t) and a specific ell
       m0   --- index for m=0
       args --- indices for all m!=0 components
    """
    nm, nt = np.shape(Br)
    Energy = np.zeros((nt), dtype=np.float64)

    # just use spherical harmonic coefficients
    # *****this relies on the fact that C_l^{-m} = (-1)^m (C_l^m)*
    Energy[:] += mag_sq(Br[m0,:]) + mag_sq(Bth[m0,:]) + mag_sq(Bphi[m0,:])
    for m in args:
        Energy[:] += mag_sq(Br[m,:])   + mag_sq(Bth[m,:])   + mag_sq(Bphi[m,:])
        Energy[:] += mag_sq(Br[m,:],1) + mag_sq(Bth[m,:],1) + mag_sq(Bphi[m,:],1)

    # size of [t], it is actually B**2 and should be real
    return Energy

def calc_dipole_energy(Br, Bth, Bphi, m0, m1):
    """incoming B should be size (m,t) and l=1"""

    nm, nt = np.shape(Br)
    dipole_E = np.zeros((nt), dtype=np.float64)

    # just use spherical harmonic coefficients
    # *****this relies on the fact that C_l^{-m} = (-1)^m (C_l^m)*
    dipole_E[:] += mag_sq(Br[m0,:])   + mag_sq(Bth[m0,:])   + mag_sq(Bphi[m0,:])
    dipole_E[:] += mag_sq(Br[m1,:])   + mag_sq(Bth[m1,:])   + mag_sq(Bphi[m1,:])
    dipole_E[:] += mag_sq(Br[m1,:],1) + mag_sq(Bth[m1,:],1) + mag_sq(Bphi[m1,:],1)

    # size of [t], it is actually B**2 and should be real
    return dipole_E

def calc_quadrupole_energy(Br, Bth, Bphi, m0, m1, m2):
    """incoming B should be size (m,t) and l=2"""
    quad_E = 0.
    nm, nt = np.shape(Br)
    quad_E = np.zeros((nt), dtype=np.float64)

    # just use spherical harmonic coefficients
    # *****this relies on the fact that C_l^{-m} = (-1)^m (C_l^m)*
    quad_E[:] += mag_sq(Br[m0,:])   + mag_sq(Bth[m0,:])   + mag_sq(Bphi[m0,:])
    quad_E[:] += mag_sq(Br[m1,:])   + mag_sq(Bth[m1,:])   + mag_sq(Bphi[m1,:])
    quad_E[:] += mag_sq(Br[m2,:])   + mag_sq(Bth[m2,:])   + mag_sq(Bphi[m2,:])
    quad_E[:] += mag_sq(Br[m1,:],1) + mag_sq(Bth[m1,:],1) + mag_sq(Bphi[m1,:],1)
    quad_E[:] += mag_sq(Br[m2,:],1) + mag_sq(Bth[m2,:],1) + mag_sq(Bphi[m2,:],1)

    # size of [t], it is actually B**2 and should be real
    return quad_E

def get_Poloidal(Br, r, ell=1):
    """
    B = del x (del x P rhat) + (del x T rhat)
    use Br to get P:
         Br = l*(l+1)*P/r**2
         P = r**2*Br/l/(l+1)
    incoming Br is size [m,t]
    """

    P = np.zeros_like(Br) # size [m,t]

    #P[0,:,:] = 0.0          # \ell=0 bit, slightly unsure of this...
    #for l in range(1,nell): # all other \ell values
    #    ll_plus_one = l*(l+1)
    #    P[l,:,:] = r*r*Br[l,:,:]/ll_plus_one
    ll_plus_one = ell*(ell+1.0)
    P[:,:] = r*r*Br[:,:]/ll_plus_one
    return P

def dipole_components(Poloidal, m0, m1, method=1, zero_tol=1e-30):
    """incoming Poloidal should be size (m,t) and l=1"""

    if (method == 1):
        # just use spherical harmonic coefficients
        # *****this relies on the fact that C_l^{-m} = (-1)^m (C_l^m)*
        #      x = C_x = -sqrt(2)*Real(C_{1}^{ 1}) =    (C_{1}^{-1} - C_{1}^{1})/sqrt(2)
        #      y = C_y = +sqrt(2)*Imag(C_{1}^{ 1}) = -i*(C_{1}^{-1} + C_{1}^{1})/sqrt(2)
        #      z = C_z =          Real(C_{1}^{ 0}), C_1^0 is real anyway
        x = -np.sqrt(2)*np.real(Poloidal[m1, :])
        y =  np.sqrt(2)*np.imag(Poloidal[m1, :])
        z =             np.real(Poloidal[m0, :])
    elif (method == 2):
        # extract dipole components in terms of gauss coefficients
        #      g11 = x, g10 = z, h11 = y = g1-1
        #      g_lm =  2*A_lm*Re(C_lm)
        #      h_lm = -2*A_lm*Im(C_lm)
        #      g_l0 =    A_l0*Re(C_l0)
        x, y  = GaussCoeffs(Poloidal[m1,:], 1, 1)
        z, _z = GaussCoeffs(Poloidal[m0,:], 1, 0)
    else:
        raise ValueError("dipole components, method = {} not recognized".format(method))

    tot = np.sqrt(x**2 + y**2 + z**2)
    hor = np.sqrt(x**2 + y**2)

    # ensure zeros are taken care of
    tot[np.where(tot<zero_tol)] = zero_tol
    hor[np.where(hor<zero_tol)] = zero_tol

    return (x, y, z, tot, hor)

