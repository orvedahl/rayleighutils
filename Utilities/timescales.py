"""

Calculate various timescales

Usage:
    plot_entropy.py [options]

Options:
    --data-dir=<d>    Specify location of data [default: ./]
    --Lnorm=<l>       Set the luminosity ergs/sec [default: 3.846e33]
    --cp=<c>          Set specific heat at constant pressure [default: 3.5e8]
    --kappa=<k>       Diffusivity in cm**2/sec [default: 2e12]
    --Length=<L>      Specify length for diffusive timescale in cm [default: 1e8]
    --rad-ind=<i>     What radial index to use [default: 64]
"""
import env
from ReadRayleigh.diagnostic_reading import ReferenceState
import numpy as np
from NumericalToolsLocal import integrate
my_integrator = integrate.integrate
volume_avg = integrate.volume_avg

def main(data_dir, Luminosity, rad_ind, cp, kappa, Length):

    ref = ReferenceState(filename="reference", path=data_dir)

    # extract background quantities of interest
    nr = ref.nr
    radius = ref.radius
    S = ref.entropy
    dSdr = ref.dsdr
    rho = ref.density
    T = ref.temperature
    P = ref.pressure
    grav = ref.gravity

    # shortcuts
    rtop = np.amax(radius); rbot = np.amin(radius)
    Ptop = np.amin(P); Pbot = np.amax(P)
    H = rtop - rbot

    # calculate the Q heating factor and the associated Flux
    #   Q = alpha*(P(r) - P(rtop))
    #   L = 4*pi*alpha * integral(  (P(r) - P(rtop))*r**2 dr)
    integrand = radius*radius*(P - Ptop)
    integrated = my_integrator(integrand, radius, data_reversed=True, method='cheb-end')
    alpha = Luminosity / 4. / np.pi / integrated
    Q = alpha*(P - Ptop)
    Flux_Q = my_integrator(Q*radius*radius, radius, indefinite=True,
                           data_reversed=True, method='cheb-end')/radius/radius

    ###############
    # timescales
    ###############

    # time to heat from 0 to delta_S due to heating function
    delta_S = 1e4
    time_Q = delta_S*rho[rad_ind]*T[rad_ind]/Q[rad_ind]

    # freefall time, due to gravity
    ur = 10.e2   # cm/s
    S1 = 500.   # ergs / g / K
    time_freefall = abs(ur / (S1/cp) / grav[rad_ind])

    # diffusive time
    time_diffuse = Length**2 / kappa

    print("\nusing radius = {} cm ({} Mm)".format(radius[rad_ind], radius[rad_ind]*1e-8))
    print("\nTimescales:")
    print("\n\tInternal Heating:")
    print("\t\tdelta S = {} ergs/g/K".format(delta_S))
    print("\t\ttime = {:.2f} sec ({:.2f} days)".format(time_Q, time_Q/86400.))

    print("\n\tFreefall:")
    print("\t\tS_1 = {} ergs/g/K".format(S1))
    print("\t\tu_r = {} cm/s".format(ur))
    print("\t\ttime = {:.2f} sec ({:.2f} days)".format(time_freefall, time_freefall/86400.))

    print("\n\tDiffusive:")
    print("\t\tkappa = {}".format(kappa))
    print("\t\tlength = {} cm ({} Mm)".format(Length, Length*1e-8))
    print("\t\ttime = {:.2f} sec ({:.2f} days)".format(time_diffuse, time_diffuse/86400.))

    print("")

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    kappa = float(args['--kappa'])
    Length = float(args['--Length'])
    data_dir = args['--data-dir']
    rad_ind = int(args['--rad-ind'])
    Lnorm = float(args['--Lnorm'])
    cp = float(args['--cp'])

    main(data_dir, Lnorm, rad_ind, cp, kappa, Length)

