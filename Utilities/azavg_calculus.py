"""
Module to calculate various differential operators on Rayleigh
data that depends only on radius and latitude. These are
mostly the AZ_Avgs data (Meridional_Slices probably depend on phi)
"""
from __future__ import print_function
import numpy as np
import NumericalToolsLocal.public as NT
from NumericalToolsLocal.polynomials.legendre import antiderivative

# differentiation methods for radial & theta directions
method_r = 'fd'
method_t = 'fd'
order = '2'

# IF DOING 'fd' differentiation, data_reversed does not matter
# so set it to False when doing shapes like (th,r,...)
# The differentiation code is reversing the wrong dimension, I think...
rev_r  = False
rev_th = False

def SwapAxis(array, axis0, axis1):
    """
    move axes of array, other axes remain untouched

    B.shape
        (2,3,4,5,6)

    # swap 2nd and last axes:
    A = SwapAxis(B, 1, -1)
    A.shape
        (2,6,4,5,3)

    # undo that swap
    A = SwapAxis(A, -1, 1)
    A.shape
        (2,3,4,5,6)
    """
    return np.swapaxes(array, axis0, axis1)

def Tile(th, r, shape):
    """
    convert 1d arrays to arrays with given shape
    """
    Th, R = np.meshgrid(th, r, indexing='ij')

    newshape = np.asarray(shape)
    newshape[2:] = 1
    newshape = tuple(newshape)

    Th = np.reshape(Th, newshape)
    R  = np.reshape( R, newshape)

    return Th, R

def AdotGradB(A, B, r, th):
    """
    compute C = A.del(B) in spherical coordinates for vectors A & B

    all arrays are assumed to only depend on r & theta
    so all azimuthal derivatives are zero

    C.rhat = Ar*dr(Br) + Ath/r*dth(Br) - (Ath*Bth + Aphi*Bphi)/r + 0*dphi(Br)

    C.that = Ar*dr(Bth) + Ath/r*dth(Bth) + Ath*Br/r - Aphi*Bphi*cot(th)/r + 0*dphi(Bth)

    C.phat = Ar*dr(Bphi) + Ath/r*dth(Bphi) + Aphi*Br/r + Aphi*Bth*cot(th)/r + 0*dphi(Bphi)

    r & th are 1d arrays and assumed to be
        r  in [r_outer, r_inner]
        th in [pi, 0] = [south pole, north pole]

    ordering is assumed to be A_i(theta, r) and A = [A_r, A_th, A_phi]
    """

    Ar, Ath, Aphi = A[0], A[1], A[2]
    Br, Bth, Bphi = B[0], B[1], B[2]
    nth, nr = np.shape(Ar)[0], np.shape(Ar)[1]

    th2d, r2d = Tile(th, r, np.shape(Ar))

    Cr   = np.zeros_like(Ar)
    Cth  = np.zeros_like(Ar)
    Cphi = np.zeros_like(Ar)

    grad_Br   = gradient(Br, r, th)
    grad_Bth  = gradient(Bth, r, th)
    grad_Bphi = gradient(Bphi, r, th)

    cotan = np.cos(th2d)/np.sin(th2d)

    Cr   = Ar*grad_Br[  0] + Ath*grad_Br[  1] - (Ath*Bth + Aphi*Bphi)/r2d
    Cth  = Ar*grad_Bth[ 0] + Ath*grad_Bth[ 1] + Ath*Br/r2d - Aphi*Bphi*cotan/r2d
    Cphi = Ar*grad_Bphi[0] + Ath*grad_Bphi[1] + Aphi*Br/r2d + Aphi*Bth*cotan/r2d

    return (Cr, Cth, Cphi)

def gradient(f, r, th):
    """
    calculate the gradient in spherical coords

    grad(f) = dr(f).rhat + 1/r*dth(f).that + 0.phat

    r & th are 1d arrays and assumed to be
        r  in [r_outer, r_inner]
        th in [pi, 0] = [south pole, north pole]

    assumed ordering is f(th,r)
    """
    nth, nr = np.shape(f)[0], np.shape(f)[1]

    global method_r, method_t, order, rev_r, rev_th

    Gr   = np.zeros_like(f)
    Gth  = np.zeros_like(f)
    Gphi = np.zeros_like(f) 

    # make differentiation axis last
    f  = SwapAxis(f,  1, -1)
    Gr = SwapAxis(Gr, 1, -1)

    # radial component
    for i in range(nth):
        Gr[i,...,:] = NT.derivative(f[i,...,:], r, data_reversed=rev_r, method=method_r, order=order)

    # swap back
    f  = SwapAxis(f,  -1, 1)
    Gr = SwapAxis(Gr, -1, 1)

    # make diff axis last
    f   = SwapAxis(f,   0, -1)
    Gth = SwapAxis(Gth, 0, -1)

    # theta component
    for i in range(nr):
        # since we evaluate the deriv at costheta, we need to 
        # multiply by d(costh)/dth to make this a proper d/dth deriv
        #Gth[:,i] = Leg.derivative(f[:,i], costh, domain)*(-sinth)/r[i]

        Gth[i,...,:] = NT.derivative(f[i,...,:], th, data_reversed=rev_th, method=method_t, order=order)/r[i]

    # swap back
    f   = SwapAxis(f,   -1, 0)
    Gth = SwapAxis(Gth, -1, 0)

    # phi component
    #Gphi[:,:] = 0.0

    # return a 3-coordinate vector
    return (Gr, Gth, Gphi)

def divergence(F, r, th):
    """
    calculate the divergence in spherical coords

    div(F) = 1/r**2*dr(F_r * r**2) + 1/r/sin(th)*dth(F_th*sin(th)) + 0*dphi()

    r & th are 1d arrays and assumed to be
        r  in [r_outer, r_inner]
        th in [pi, 0] = [south pole, north pole]

    assumed ordering is F_i(th,r) with F = [F_r, F_th]
    """
    Fr, Fth = F[0], F[1]
    nth, nr = np.shape(Fr)

    global method_r, method_t

    div = np.zeros_like(Fr)

    # deal with radial component first
    for i in range(nth):
        div[i,:] = NT.derivative(r**2*Fr[i,:], r, data_reversed=1, method=method_r)/r**2

    sinth = np.sin(th)
    costh = np.cos(th)

    # now include theta component
    for i in range(nr):
        dFdth = NT.derivative(Fth[:,i]*sinth, th, data_reversed=1, method=method_t)
        dFdth /= sinth

        div[:,i] += dFdth/r[i]

    # now include phi component
    # div[:,:] += 0.0

    return div

def laplacian(A, r, th):
    """
    compute laplacian of vector quantity in spherical coordinates

    all arrays are assumed to only depend on r & theta
    so all azimuthal derivatives are zero

    lap(A).rhat = lap(Ar) - 2*Ar/r**2 - 2/r**2*dAthdth - 2/r**2*cot(th)*Ath

    lap(A).that = lap(Ath) - Ath/r**2/sin(th)**2 + 2/r**2*dArdth

    lap(A).phat = lap(Aphi) - Aphi/r**2/sin(th)**2

    r & th are 1d arrays and assumed to be
        r  in [r_outer, r_inner]
        th in [pi, 0] = [south pole, north pole]

    ordering is assumed to be F_i(theta, r) and F = [F_r, F_th, F_phi]
    """

    Ar, Ath, Aphi = A[0], A[1], A[2]
    nth, nr = np.shape(Ar)[0], np.shape(Ar)[1]

    dArdth  = np.zeros_like(Ar)
    dAthdth = np.zeros_like(Ath)

    global method_t, order, rev_th

    # make sure differentiation axis is last
    dArdth  = SwapAxis(dArdth,  0, -1)
    dAthdth = SwapAxis(dAthdth, 0, -1)
    Ar  = SwapAxis(Ar,  0, -1)
    Ath = SwapAxis(Ath, 0, -1)

    # calculate 1st derivatives in theta
    for i in range(nr):
        dArdth[i,...,:]  = NT.derivative(Ar[i,...,:],  th, data_reversed=rev_th, method=method_t, order=order)
        dAthdth[i,...,:] = NT.derivative(Ath[i,...,:], th, data_reversed=rev_th, method=method_t, order=order)

    # move axes back
    dArdth  = SwapAxis(dArdth,  -1, 0,)
    dAthdth = SwapAxis(dAthdth, -1, 0,)
    Ar  = SwapAxis(Ar,  -1, 0,)
    Ath = SwapAxis(Ath, -1, 0,)

    th2d, r2d = Tile(th, r, np.shape(Ar))

    Lr = lap(Ar, r, th) - 2.*Ar/r2d**2 - 2./r2d**2*dAthdth - 2./r2d**2*np.cos(th2d)*Ath/np.sin(th2d)

    Lth = lap(Ath, r, th) - Ath/r2d**2/np.sin(th2d)**2 + 2./r2d**2*dArdth

    Lphi = lap(Aphi, r, th) - Aphi/r2d**2/np.sin(th2d)**2

    return (Lr, Lth, Lphi)

def lap(f, r, th):
    """
    compute laplacian of scalar in spherical coordinates

    F is assumed to only depend on r & theta
    so all azimuthal derivatives are zero

    lap(F) = d2Fdr2 + 2/r*dFdr + 1/r**2*(d2Fdth2 + cot(th)*dFdth)

    r & th are 1d arrays and assumed to be
        r  in [r_outer, r_inner]
        th in [pi, 0] = [south pole, north pole]

    ordering is assumed to be F(theta, r)
    """

    nth, nr = np.shape(f)[0], np.shape(f)[1]

    d2Fdr2  = np.zeros_like(f) # second derivatives
    d2Fdth2 = np.zeros_like(f)
    dFdr  = np.zeros_like(f)   # first derivatives
    dFdth = np.zeros_like(f)

    global method_r, method_t, order, rev_th, rev_r

    # make diff axis last
    f      = SwapAxis(f,      1, -1)
    dFdr   = SwapAxis(dFdr,   1, -1)
    d2Fdr2 = SwapAxis(d2Fdr2, 1, -1)

    # calculate derivatives in radius
    for i in range(nth):
        dFdr[i,...,:]   = NT.derivative(   f[i,...,:], r, data_reversed=rev_r, method=method_r, order=order)
        d2Fdr2[i,...,:] = NT.derivative(dFdr[i,...,:], r, data_reversed=rev_r, method=method_r, order=order)

    # swap back
    f      = SwapAxis(f,      -1, 1)
    dFdr   = SwapAxis(dFdr,   -1, 1)
    d2Fdr2 = SwapAxis(d2Fdr2, -1, 1)

    # make diff axis last
    f       = SwapAxis(f,       0, -1)
    dFdth   = SwapAxis(dFdth,   0, -1)
    d2Fdth2 = SwapAxis(d2Fdth2, 0, -1)

    # calculate derivatives in theta
    for i in range(nr):
        dFdth[i,...,:]   = NT.derivative(    f[i,...,:], th, data_reversed=rev_th, method=method_t, order=order)
        d2Fdth2[i,...,:] = NT.derivative(dFdth[i,...,:], th, data_reversed=rev_th, method=method_t, order=order)

    # swap back
    f       = SwapAxis(f,       -1, 0)
    dFdth   = SwapAxis(dFdth,   -1, 0)
    d2Fdth2 = SwapAxis(d2Fdth2, -1, 0)

    # broadcast r & th to 2D
    th, r = Tile(th, r, np.shape(f))

    # calculate the laplacian
    L = d2Fdr2 + 2./r*dFdr + 1./r**2*d2Fdth2 + np.cos(th)/r**2*dFdth/np.sin(th)

    return L

def curl(A, r, th):
    """
    compute curl of vector quantity in spherical coordinates

    all arrays are assumed to only depend on r & theta
    so all azimuthal derivatives are zero

    curl(A).rhat = dth(Aphi)/r + cot(th)*Aphi/r - 0*dphi()

    curl(A).that = -dr(Aphi) - Aphi/r + 0*dphi()

    curl(A).phat = dr(Ath) + Ath/r - dth(Ar)/r

    r & th are 1d arrays and assumed to be
        r  in [r_outer, r_inner]
        th in [pi, 0] = [south pole, north pole]

    ordering is assumed to be F_i(theta, r) and F = [F_r, F_th, F_phi]
    """

    Ar, Ath, Aphi = A[0], A[1], A[2]
    nth, nr = np.shape(Ar)[0], np.shape(Ar)[1]

    dAphidth = np.zeros_like(Aphi)
    dAphidr  = np.zeros_like(Aphi)
    dArdth = np.zeros_like(Ar)
    dAthdr = np.zeros_like(Ath)

    global method_t, order, rev_th, rev_r

    # make diff axis last
    Aphi     = SwapAxis(Aphi,     0, -1)
    Ar       = SwapAxis(Ar,       0, -1)
    dAphidth = SwapAxis(dAphidth, 0, -1)
    dArdth   = SwapAxis(dArdth,   0, -1)

    # calculate 1st derivatives in theta
    for i in range(nr):
        dAphidth[i,...,:] = NT.derivative(Aphi[i,...,:], th, data_reversed=rev_th, method=method_t, order=order)
        dArdth[i,...,:]   = NT.derivative(Ar[i,...,:],   th, data_reversed=rev_th, method=method_t, order=order)

    # swap back
    Aphi     = SwapAxis(Aphi,     -1, 0)
    Ar       = SwapAxis(Ar,       -1, 0)
    dAphidth = SwapAxis(dAphidth, -1, 0)
    dArdth   = SwapAxis(dArdth,   -1, 0)

    # make diff axis last
    Aphi    = SwapAxis(Aphi,    1, -1)
    Ath     = SwapAxis(Ath,     1, -1)
    dAphidr = SwapAxis(dAphidr, 1, -1)
    dAthdr  = SwapAxis(dAthdr,  1, -1)

    # calculate 1st derivatives in radius
    for i in range(nth):
        dAphidr[i,...,:] = NT.derivative(Aphi[i,...,:], r, data_reversed=rev_r, method=method_r, order=order)
        dAthdr[i,...,:]  = NT.derivative(Ath[i,...,:],  r, data_reversed=rev_r, method=method_r, order=order)

    # swap back
    Aphi    = SwapAxis(Aphi,    -1, 1)
    Ath     = SwapAxis(Ath,     -1, 1)
    dAphidr = SwapAxis(dAphidr, -1, 1)
    dAthdr  = SwapAxis(dAthdr,  -1, 1)

    th2d, r2d = Tile(th, r, np.shape(Ar))

    cotan = np.cos(th2d)/np.sin(th2d)

    Cr   = dAphidth / r2d + cotan*Aphi/r2d

    Cth  = -dAphidr - Aphi/r2d

    Cphi = dAthdr + Ath/r2d - dArdth/r2d

    return (Cr, Cth, Cphi)

def cross(A, B):
    """
    compute cross product of vector quantity in spherical coordinates

    all arrays are assumed to only depend on r & theta
    so all azimuthal derivatives are zero

    AxB.rhat = Ath*Bphi - Aphi*Bth

    AxB.that = Aphi*Br - Ar*Bphi

    AxB.phat = Ar*Bth - Ath*Br

    ordering is assumed to be F_i(theta, r) and F = [F_r, F_th, F_phi]
    """

    Ar, Ath, Aphi = A[0], A[1], A[2]
    Br, Bth, Bphi = B[0], B[1], B[2]

    Cr   = Ath*Bphi - Aphi*Bth
    Cth  = Aphi*Br - Ar*Bphi
    Cphi = Ar*Bth - Ath*Br

    return (Cr, Cth, Cphi)

def PoloidalToroidal(B, r, th, order=0):
    """
    compute the scalar potentials of the azimuthal B field s.t.

        B = curl(T*r*rhat) + curl(curl(P*r*rhat))

        div(B) = 0

        B = B_phi*phat + curl(A_phi*phat)

    ***only returns P and A_phi, does not compute T***
    --------

        T = -indef_int(Bphi) dth

        sin(th)*A_phi = r*indef_int(sin(th)*Br) dth

        r*A_phi = -indef_int(r*Bth) dr

        P = -indef_int(A_phi) dth

    all arrays are assumed to only depend on r & theta
    so all azimuthal derivatives are zero

    ordering is assumed to be B_i(theta, r) and B = [B_r, B_th, B_phi]
    """
    Br, Bth, Bphi = B[0], B[1], B[2]
    nth, nr = np.shape(Br)[0], np.shape(Br)[1]

    dth = np.zeros((nth)); dr = np.zeros((nr))
    Psi  = np.zeros_like(Br); Psi2 = np.zeros_like(Br)
    dPsidr = np.zeros_like(Br)
    dPsidt = np.zeros_like(Br)
    P = np.zeros_like(Br); P2 = np.zeros_like(Br)
    A = np.zeros_like(Br)

    th2d, r2d = Tile(th, r, np.shape(Br))
    sint = np.sin(th2d)
    cost = np.cos(th2d)

    dPsidr = -r2d*sint*Bth
    dPsidt = r2d*r2d*sint*Br

    # dx_{i} = x_{i} - x_{i-1}
    dth[1:nth] = th[1:nth] - th[0:nth-1]; dth[0] = 0.0
    dr[1:nr]   =  r[1:nr]  -  r[0:nr-1];   dr[0] = 0.0

    # start at outer shell and north pole, proceed inward and southward
    # dF/dx = (F_{i} - F_{i-1}) / dx_{i} = RHS_{i}
    if (order >= 0):
        for i in range(1,nr):
            Psi[1:nth,i,...] = Psi[1:nth,i-1,...] + dPsidr[1:nth,i,...]*dr[i]
        for i in range(1,nth):
            Psi[i,1:nr,...]  = Psi[i-1,1:nr,...]  + dPsidt[i,1:nr,...]*dth[i]

    if (order <= 0):
        # start at inner shell and south pole, proceed outward and northward
        dth[0:nth-1] = th[0:nth-1] - th[1:nth]; dth[nth-1] = 0.0
        dr[0:nr-1]   =  r[0:nr-1]  -  r[1:nr];   dr[nr-1]  = 0.0
        for i in range(nr-2,-1,-1):
            Psi2[0:nth-1,i,...] = Psi2[0:nth-1,i+1,...] + dPsidr[0:nth-1,i,...]*dr[i]
        for i in range(nth-2,-1,-1):
            Psi2[i,0:nr-1,...]  = Psi2[i+1,0:nr-1,...]  + dPsidt[i,0:nr-1,...]*dth[i]

    if (order == 0):
        # Psi[0,0] is wrong for Psi, should be okay for Psi2?
        # Psi[-1,-1] is wrong for Psi2, should be okay for Psi?

        #Psi[0,0] = Psi2[0,0]; Psi2[-1,-1] = Psi[0,0]
        Psi = 0.5*(Psi + Psi2)
    elif (order < 0):
        Psi = Psi2
    else:
        # this is order > 0, Psi needs no modification
        pass

    # now extract P and A
    A = Psi/(r2d*sint)

    # forward integrate for P
    if (order >= 0):
        dth[1:nth] = th[1:nth] - th[0:nth-1]; dth[0] = 0.0
        for i in range(1,nth):
            P[i,1:nr,...] = P[i-1,1:nr,...] + (   -A[i,1:nr,...])*dth[i]

    # reverse integrate for P
    if (order <= 0):
        dth[0:nth-1] = th[0:nth-1] - th[1:nth]; dth[nth-1] = 0.0
        for i in range(nth-2,-1,-1):
            P2[i,0:nr-1,...] = P2[i+1,0:nr-1,...] + (   -A[i,0:nr-1,...])*dth[i]

    if (order == 0):
        #P[0,0] = P2[0,0]; P2[-1,-1] = P[0,0]
        P = 0.5*(P + P2)
    elif (order < 0):
        P = P2
    else:
        # this is order > 0, P needs no modification
        pass

    return (A, P)

