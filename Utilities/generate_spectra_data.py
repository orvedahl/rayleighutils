"""
Run spectra code to generate multiple spectra datafiles

Usage:
    generate_spectra_data.py [options]

Options:
    --quantity=<q>    What quantity to do [default: velocity]
    --output=<o>      Output will be <quantity>_<o>_Pr---.dat [default: spectra]
    --output-dir=<d>  Output will <d>/<output> [default: ./]
    --cp=<c>          Specific heat at const pressure [default: 3.5e8]
    --Lnorm=<L>       Luminosity in ergs/sec [default: 3.846e33]
    --last=<l>        Specify last <l> iterations for spectra calculations [default: 20]
    --do-not-write    Don't save data to files, just print to screen [default: False]
"""

from __future__ import print_function
from collections import OrderedDict
import time
import plot_spectrum

def main(out_dir, quantity, output, cp, Lnorm, last, do_not_write=False):

    start_time = time.time()

    if (out_dir[-1] != '/'):
        out_dir = out_dir + '/'

    save = False; savefile = ''; start = 0; end = 1000000000
    args = OrderedDict({'last_number_files':last, 'saveplot':save,
            'savefile':savefile, 'no_plots':True,
            'magnetic':False, 'list':False,
            'cp':cp, 'Lnorm':Lnorm,
            'quantity':quantity,
            'start_file':start, 'end_file':end})

    locations = ['upper', 'middle', 'lower']

    ##########################################
    # Prandtl 1 runs
    ##########################################
    start_Pr1_time = time.time()
    Pr = 1.0; _Pr = '1'; alt = True

    # key = directory name
    # value = [upper radial index, middle rad-ind, lower rad-ind, kappa, Ra]
    #           Ra = -1  --> use reference file to calculate it
    #           Ra /= -1 --> use this Rayleigh number
    dirs = OrderedDict({"nr3_2":[0,3,6,2e12,-1],         "nr3_4":[0,3,6,4e12,-1],
                        "nr3_6":[0,3,6,6e12,31608.1716], "nr3_8":[0,3,6,8e12,-1],
                        "nr3_12":[0,3,6,12e12,3947.0425]})

    for i,dir in enumerate(dirs):
        data_dir = "Prandtl-1/" + dir
        kappa = dirs[dir][-2]
        Ra = dirs[dir][-1]

        for j,loc in enumerate(locations):
            rad_index = dirs[dir][j]
            if (not do_not_write):
                write_to = out_dir + quantity+"_"+output+"_Pr"+_Pr+".dat"
            else:
                write_to = ''

            extras = OrderedDict({'data_dir':data_dir, 'rad_index':rad_index, 'write_to':write_to,
                                  'Pr':Pr, 'location':loc, 'kappa':kappa, 'Ra':Ra, 'alt':alt})

            kwargs = OrderedDict(args)           # get common calling args
            kwargs.update(extras)         # set extras
            print("working on {}".format(data_dir))
            plot_spectrum.main(**kwargs)  # do the call

    end_Pr1_time = time.time()

    ##########################################
    # Prandtl 2 runs
    ##########################################
    start_Pr2_time = time.time()
    Pr = 2.0; _Pr = '2'; alt = False

    dirs = OrderedDict({"kappa_2_nu_4":[  0,3,6,2e12, -1], "kappa_4_nu_8":[  0,3,6,4e12, -1],
                        "kappa_6_nu_12":[ 0,3,6,6e12, -1], "kappa_8_nu_16":[ 0,3,6,8e12, -1],
                        "kappa_12_nu_24":[0,3,6,12e12,-1], "kappa_16_nu_32":[0,1,2,16e12,-1],
                        "kappa_24_nu_48":[0,1,2,24e12,-1]})

    for i,dir in enumerate(dirs):
        data_dir = "Prandtl-2/" + dir
        kappa = dirs[dir][-2]
        Ra = dirs[dir][-1]

        for j,loc in enumerate(locations):
            rad_index = dirs[dir][j]
            if (not do_not_write):
                write_to = out_dir + quantity+"_"+output+"_Pr"+_Pr+".dat"
            else:
                write_to = ''

            extras = OrderedDict({'data_dir':data_dir, 'rad_index':rad_index, 'write_to':write_to,
                                  'Pr':Pr, 'location':loc, 'kappa':kappa, 'Ra':Ra, 'alt':alt})

            kwargs = OrderedDict(args)           # get common calling args
            kwargs.update(extras)         # set extras
            print("working on {}".format(data_dir))
            plot_spectrum.main(**kwargs)  # do the call

    end_Pr2_time = time.time()

    ##########################################
    # Prandtl 0.5 runs
    ##########################################
    start_Pr0_5_time = time.time()
    Pr = 0.5; _Pr = '0_5'; alt = False

    dirs = OrderedDict({"kappa_4_nu_2":[ 0,3,6,4e12, -1], "kappa_6_nu_3":[  0,3,6,6e12, -1],
                        "kappa_8_nu_4":[ 0,3,6,8e12, -1], "kappa_12_nu_6":[ 0,3,6,12e12,-1],
                        "kappa_16_nu_8":[0,3,6,16e12,-1], "kappa_24_nu_12":[0,3,6,24e12,-1]})

    for i,dir in enumerate(dirs):
        data_dir = "Prandtl-0.5/" + dir
        kappa = dirs[dir][-2]
        Ra = dirs[dir][-1]

        for j,loc in enumerate(locations):
            rad_index = dirs[dir][j]
            if (not do_not_write):
                write_to = out_dir + quantity+"_"+output+"_Pr"+_Pr+".dat"
            else:
                write_to = ''

            extras = OrderedDict({'data_dir':data_dir, 'rad_index':rad_index, 'write_to':write_to,
                                  'Pr':Pr, 'location':loc, 'kappa':kappa, 'Ra':Ra, 'alt':alt})

            kwargs = OrderedDict(args)           # get common calling args
            kwargs.update(extras)         # set extras
            print("working on {}".format(data_dir))
            plot_spectrum.main(**kwargs)  # do the call

    end_Pr0_5_time = time.time()

    ##########################################
    # Prandtl 4 runs
    ##########################################
    start_Pr4_time = time.time()
    Pr = 4.0; _Pr = '4'; alt = False

    dirs = OrderedDict({"kappa_2_nu_8":[  1,3,5,2e12, -1], "kappa_4_nu_16":[ 1,3,5,4e12, -1],
                        "kappa_6_nu_24":[ 1,3,5,6e12, -1], "kappa_8_nu_32":[ 1,3,5,8e12, -1],
                        "kappa_12_nu_48":[1,3,5,12e12,-1], "kappa_16_nu_64":[1,3,5,16e12,-1],
                        "kappa_24_nu_96":[1,3,5,24e12,-1]})

    for i,dir in enumerate(dirs):
        data_dir = "Prandtl-4/" + dir
        kappa = dirs[dir][-2]
        Ra = dirs[dir][-1]

        for j,loc in enumerate(locations):
            rad_index = dirs[dir][j]
            if (not do_not_write):
                write_to = out_dir + quantity+"_"+output+"_Pr"+_Pr+".dat"
            else:
                write_to = ''

            extras = OrderedDict({'data_dir':data_dir, 'rad_index':rad_index, 'write_to':write_to,
                                  'Pr':Pr, 'location':loc, 'kappa':kappa, 'Ra':Ra, 'alt':alt})

            kwargs = OrderedDict(args)           # get common calling args
            kwargs.update(extras)         # set extras
            print("working on {}".format(data_dir))
            plot_spectrum.main(**kwargs)  # do the call

    end_Pr4_time = time.time()

    ##########################################
    # Prandtl 0.25 runs
    ##########################################
    start_Pr0_25_time = time.time()
    Pr = 0.25; _Pr = '0_25'; alt = False

    dirs = OrderedDict({"kappa_8_nu_2":[  1,3,5,8e12, -1], "kappa_12_nu_3":[ 1,3,5,12e12,-1],
                        "kappa_16_nu_4":[ 1,3,5,16e12,-1], "kappa_24_nu_6":[ 1,3,5,24e12,-1],
                        "kappa_32_nu_8":[ 1,3,5,32e12,-1], "kappa_48_nu_12":[1,3,5,48e12,-1],
                        "kappa_64_nu_16":[1,3,5,64e12,-1]})

    for i,dir in enumerate(dirs):
        data_dir = "Prandtl-0.25/" + dir
        kappa = dirs[dir][-2]
        Ra = dirs[dir][-1]

        for j,loc in enumerate(locations):
            rad_index = dirs[dir][j]
            if (not do_not_write):
                write_to = out_dir + quantity+"_"+output+"_Pr"+_Pr+".dat"
            else:
                write_to = ''

            extras = OrderedDict({'data_dir':data_dir, 'rad_index':rad_index, 'write_to':write_to,
                                  'Pr':Pr, 'location':loc, 'kappa':kappa, 'Ra':Ra, 'alt':alt})

            kwargs = OrderedDict(args)           # get common calling args
            kwargs.update(extras)         # set extras
            print("working on {}".format(data_dir))
            plot_spectrum.main(**kwargs)  # do the call

    end_Pr0_25_time = time.time()

    end_time = time.time()

    elapsed_0_25 = end_Pr0_25_time - start_Pr0_25_time
    elapsed_0_5  = end_Pr0_5_time - start_Pr0_5_time
    elapsed_1    = end_Pr1_time - start_Pr1_time
    elapsed_2    = end_Pr2_time - start_Pr2_time
    elapsed_4    = end_Pr4_time - start_Pr4_time
    elapsed = end_time - start_time
    print("\n\n---Complete---\n")
    print("Timing Statistics:")
    print("\tPr = 0.25 took {:.2f} sec ({:.2f} min, {:.2f} hrs)".format(elapsed_0_25, elapsed_0_25/60., elapsed_0_25/3600.))
    print("\tPr = 0.5  took {:.2f} sec ({:.2f} min, {:.2f} hrs)".format(elapsed_0_5, elapsed_0_5/60., elapsed_0_5/3600.))
    print("\tPr = 1    took {:.2f} sec ({:.2f} min, {:.2f} hrs)".format(elapsed_1, elapsed_1/60., elapsed_1/3600.))
    print("\tPr = 2    took {:.2f} sec ({:.2f} min, {:.2f} hrs)".format(elapsed_2, elapsed_2/60., elapsed_2/3600.))
    print("\tPr = 4    took {:.2f} sec ({:.2f} min, {:.2f} hrs)".format(elapsed_4, elapsed_4/60., elapsed_4/3600.))
    print("\n\tTotal elapsed time {:.2f} sec ({:.2f} min, {:.2f} hrs)".format(elapsed, elapsed/60., elapsed/3600.))
    print()


if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    cp = float(args['--cp'])
    Lnorm = float(args['--Lnorm'])
    quantity = args['--quantity']
    output = args['--output']
    last = int(args['--last'])
    out_dir = args['--output-dir']

    main(out_dir, quantity, output, cp, Lnorm, last, do_not_write=args['--do-not-write'])

