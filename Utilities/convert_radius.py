"""
Module to "fix" Rayleigh radius. Rayleigh uses a Chebyshev
zeros grid, but includes the endpoints. It calculates the
roots then applies a "non-standard" transform as:

    r = (x - xmin)*scaling + Lbound

    with x in [-1,1], r in [Rmin,Rmax], xmin = 1st Chebyshev root

Invert this to get x:

    x = (r - Lbound)/scaling + xmin

then apply standard transform:

    r = x*(Rmax - Rmin)/2 + (Rmax + Rmin)/2

"""
from __future__ import print_function
import numpy as np
from NumericalToolsLocal import chebyshev

def Radius(radius):
    """rescale the radial grid in Rayleigh"""

    rmin = np.amin(radius); rmax = np.amax(radius)
    Nr = len(radius)

    # get 1st and last chebyshev zeros
    x = chebyshev.roots(Nr)
    xmin = x[0]; xmax = x[-1]

    # scaling and domain_delta
    Ubound = rmin; Lbound = rmax
    delta = Ubound - Lbound
    scaling = delta / (xmax - xmin)

    # calculate x in [-1,1] and reverse the array
    x = (radius - Lbound)/scaling + xmin
    x = x[::-1]

    # convert back to [Rmin,Rmax]
    rad = chebyshev.to_ab_space(x, rmin, rmax)

    return rad

