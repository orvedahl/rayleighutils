"""
Module to combine stored files generated using NumericalToolsLocal

Usage:
    combine_stored_files.py [options] <file1> <file2>

Options:
    --output=<o>    Save new dataset to file [default: combined_data.stored]
    --dsets=<d>     Comma separated list of datasets to merge [default: data,vals]
    --axes=<a>      Comma separated list of time axis for each datase [default: 0,1]
    --time=<t>      Name of the time dataset [default: time]
    --compress      Compress the resulting file [default: False]
"""
from __future__ import print_function
from NumericalToolsLocal.public import save_data, read_data
import sys
import numpy as np

def combine_files(datasets, axes, time, fname1, fname2, fname_new, compress=True):
    """
    file1: data(nx,ny,...,0:nt-1,...), more_data, even_more_data, ...

    file2: data(nx,ny,...,0:nt'-1,...), more_data, even_more_data, ...

    both files are assumed to hold the same data, just at different times.
    this routine will combine the time array and concatenate the data array
    along the given axis.

    datasets --- list of data names to be merged
    axes     --- list containing axis along which to merge each data set
    time     --- name of time component

    for example:

        combine_files(["data"], [1], "time", old_file1, old_file2, new_file)

    will combine the data set named "data" along axis=1 and the data set named
    "time" along axis 0. The results will be written to new_file.

    """
    # read the data
    print("\nReading data from file {}".format(fname1))
    try:
        data1, keys1 = read_data(fname1)
        print("\t...success")
    except:
        print("\t...FAILED\n")
        sys.exit()
    print("Reading data from file {}".format(fname2))
    try:
        data2, keys2 = read_data(fname2)
        print("\t...success")
    except:
        print("\t...FAILED\n")
        sys.exit()

    # make sure the data sets are the same
    if (keys1 != keys2):
        print("\n---ERROR: datasets are different")
        print("\nFile 1 contains:")
        for k in keys1:
            print("\t{}".format(k))
        print("\nFile 2 contains:")
        for k in keys2:
            print("\t{}".format(k))
        print()

    # make sure the passed data sets are in the files
    for dataset in datasets:
        if (dataset not in keys1):
            print("\n---ERROR: requested dataset not in file, data = {}".format(dataset))
            print()
            sys.exit()
    if (time not in keys1):
        print("\n---ERROR: requested time-key not in file, time = {}".format(dataset))
        print()
        sys.exit()

    # assumes fname1 is first in the time series
    time1 = data1[time]; time2 = data2[time] # extract time components
    tlast = time1[-1]                        # get last time from first dataset

    print("\nMerging time...")
    # figure out what kind of time overlap exists
    ind = np.where(time2 <= tlast)
    if (len(ind[0]) > 0):
        index = ind[0][-1]+1
        print("\t...adjusting for overlap")
        print("\t...skipped {} overlapping entries".format(len(time2[:index])))
    else:
        index = 0

    # merge the "new" time that does not overlap with time1
    data2[time] = np.concatenate((time1, time2[index:]))

    # make sure axes is a list the same length as datasets
    if (len(axes) == 1):
        axes = axes*len(datasets) # repeat the first element len(datasets) times

    # now merge other data sets along proper time axis
    print("\nMerging datasets...")
    for dset, axis in zip(datasets, axes):
        print("\tworking on: {}".format(dset))
        slc = [slice(None)]*len(np.shape(data2[dset])) # slice for entire dataset
        slc[axis] = slice(index,None)                  # adjust time axis to be [index:]
        data2[dset] = np.concatenate((data1[dset], data2[dset][slc]), axis=axis)

    # release some memory
    data1 = None

    # save new dataset
    print("\nStoring new results to file {}".format(fname_new))
    try:
        save_data(fname_new, compress=compress, **data2)
        print("\t...success\n")
    except:
        print("\t...FAILED\n")
        sys.exit()

if __name__ == "__main__":
    from docopt import docopt
    args = docopt(__doc__)

    output = args['--output']
    datasets = args['--dsets'].split(",")
    axes = args['--axes'].split(",")
    time = args['--time']
    compress = args['--compress']
    file1 = args['<file1>']
    file2 = args['<file2>']

    axes = [int(a) for a in axes]

    combine_files(datasets, axes, time, file1, file2, output, compress=compress)

