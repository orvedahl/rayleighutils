"""
Figure out what time instance is stored in each checkpoint file

Usage:
    find_time_of_checkpoint.py [options]

Options:
    --file=<l>      Only read time of single checkpoint file [default: quicksave_01]
    --all           Find time of every checkpoint file [default: False]
    --quicksaves    Only process the quicksave files [default: False]
    --standard      Only process the standard files [default: False]
    --data-dir=<d>  Specify location of data [default: Checkpoints]
    --Pm=<p>        Convert to magnetic diffusion time [default: 1]

"""

from __future__ import print_function
import os
import env
from ReadRayleigh.diagnostic_reading import Grid_Info

def main(filename, read_all, data_dir, quicksaves, standard, Prm, **kwargs):

    # get list of files & sort them
    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'

    # want to process multiple files
    if (read_all or standard or quicksaves):
        allfiles = os.listdir(data_dir)
        allfiles.sort()

        # only keep the files with "_grid_etc" in the name
        _files = []
        for f in allfiles:
            if ("_grid_etc" in f):
                _files.append(data_dir + f)

        files = []

        # remove the quicksaves
        if (standard):
            for f in _files:
                if ("quicksave" not in f):
                    files.append(f)

        # remove the standard ones
        if (quicksaves):
            for f in _files:
                if ("quicksave" in f):
                    files.append(f)

        # process all files
        if (not(standard) and not(quicksaves)):
            files = list(_files)

    # only process single file
    else:
        if ("_grid_etc" not in filename):
            filename = filename + "_grid_etc"
        files = [data_dir + filename]

    print("\nResults:")
    print("-----------------------------------------")
    print("Data directory = {}".format(data_dir))
    print("Reading all files = {}".format(read_all))
    print("-----------------------------------------")
    for f in files:
        itr_name = os.path.basename(f)
        split = itr_name.split("_")
        try:
            itr = int(split[0]) # standard checkpoint
        except:
            try:
                itr = -int(split[1]) # quicksave checkpoint
            except:
                print("===> could not determine iteration of file = {}".format(f))
                continue
        info = Grid_Info(f, itr)
        dt = info.tpar[0]*Prm
        old_dt = info.tpar[1]*Prm
        time = info.simtime*Prm
        print("\n{}\n\ttime = {} ({:.2e})\n\tdt = {}\n\told dt = {}".format(
              f, time, time, dt, old_dt))
    print()

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    filename = args['--file']
    read_all = args['--all']
    quicksaves = args['--quicksaves']
    standard = args['--standard']
    data_dir = args['--data-dir']
    Prm = float(args['--Pm'])

    main(filename, read_all, data_dir, quicksaves, standard, Prm)

