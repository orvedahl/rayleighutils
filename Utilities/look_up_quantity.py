"""
Module to search the lookup table for Rayleigh outputs.

R. Orvedahl 10-18-2017

Usage:
    look_up_quantity.py [options]

Options:
    --lut=<l>         Use specified look up table
    --list-shortcuts  List the available shortcuts [default: False]
    --names=<qs>      Return multiple indices, <qs> = comma separated list of names
    --inds=<is>       Return multiple quantities, <is> = comma separated list of indices
    --tex             Display indices as LaTeX equations in a plot (latex must be installed)
"""

from __future__ import print_function
import env
import importlib
from collections import OrderedDict
import lut as lookup_table
from lut_shortcuts import shortcuts

lut = lookup_table.lut
lut_tex = lookup_table.lut_tex

def change_lut(lut_file, verbose=False):
    """
    change what file loads the lut

    lut_file is assumed to be in the same location as the
    usual "lut" that is imported above for example:
    """
    global lut
    if (verbose):
        print("\nChanging lut...")
        print("   current version = {}".format(lut[-10]))

    lut_file = "Utilities." + lut_file
    lut = importlib.import_module(lut_file).lut
    lut_tex = importlib.import_module(lut_file).lut_tex
    if (verbose):
        print("       new version = {}".format(lut[-10]))
    return

def load_original_lut(verbose=False):
    """
    load the original look up table
    """
    change_lut("alt_lut", verbose=verbose)
    return

def quantity_in_lut(q, lut_in, not_found=5000):
    """
    check the lut for a specific quantity
    """
    vals = nonzero_lut(lut_in, not_found=not_found)

    if (type(q) == int):
        if (q in vals.keys()):
            in_lut = True
        else:
            in_lut = False
    else:
        if (ind in vals.values()):
            in_lut = True
        else:
            in_lut = False
    return in_lut

def nonzero_lut(lut_in, not_found=5000):
    """
    return available quantities in the given look up table

    not_found is the test value, the lut will equal not_found
    if the quantity does not exist in the given lut
    """
    vals = OrderedDict()
    for i in range(len(lut_in)):
        if (lut_in[i] != not_found):
            vals[i] = lut_lookup(i)

    # key = index, value = name
    return vals

def print_lut(lut_in, not_found=5000):
    """
    print the available entries in the give lut
    """
    vals = nonzero_lut(lut_in, not_found=not_found)
    print("\nAvailable quantities are:\n")
    for key in vals.keys():
        print("\t{} --> {:<15s}".format(key, vals[key]))
    print()

def shortcut_lookup(quantity, show=False, quiet_errors=False, alt=False):
    """
    use shortcuts to get the index instead of the lengthy exact name
    """
    quantity = quantity.lower()

    if (alt):
        # this is for backwards compatability, it loads the super old look up table
        load_original_lut(verbose=1)

    if (show):
        print("\nAvailable shortcuts are:\n")
        for key in shortcuts.keys():
            print("{:>15s} --> {}".format(key, shortcuts[key]))
        print()
        return None

    if (quantity not in shortcuts.keys()):
        if (not quiet_errors):
            print("\n---ERROR: shortcut quantity = {} not recognized\n".format(quantity))
        return None

    # extract full name
    q = shortcuts[quantity]

    # lookup corresponding index using the full name
    ind = lut_lookup(q, __check_shortcuts=False)

    return ind

def tex_name(index):
    """
    given the index, return the TeX description
    """
    try:
        index = int(index)
    except:
        print("\n---ERROR: tex_name expects an integer, index = {}".format(index))

    if (index in lut_tex.keys()):
        name = lut_tex[index]
        name = name.replace("'", "^\prime")
    else:
        name = None
    return name

def lut_lookup(quantity, __check_shortcuts=True):
    """
    search both shortcuts and the full look up table
    """

    # incoming quantity is an index, go get the name
    if (type(quantity) == int):
        if (quantity in lut.keys()):
            qname = lut[quantity]
        else:
            qname = None
        return qname

    else: # incoming quantity is a string, get the index

        quantity = quantity.lower()

        # check shortcuts first
        if (__check_shortcuts):
            ind = shortcut_lookup(quantity, quiet_errors=True, show=False)
            if (ind is not None):
                return ind

        for index in lut.keys():
            qname = lut[index].lower()
            if (quantity == qname):
                return index

        return None

##########################################################################
# the following routines are included for backwards compatability
##########################################################################
def find_available(lut_in, not_found=5000):
    vals = nonzero_lut(lut_in, not_found=not_found)
    return vals.keys()

def general_lookup(q=None, index=None, list=False, alt=False):
    if (alt):
        # this is for backwards compatability, it loads the super old look up table
        load_original_lut(verbose=1)

    if (q is None and index is None):
        print("\n---ERROR: must specify quantity or index to search lut\n")
        return None
    if (q is None):
        ind = lut_lookup(index) # index was given
    else:
        ind = lut_lookup(q)     # name was given
    return ind

def lookup_full_table(**kwargs):
    return general_lookup(**kwargs)

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    show = args['--list-shortcuts']

    q_names = args['--names']
    inds = args['--inds']
    display_tex = args['--tex']

    if ((q_names is None) and (inds is None) and (not(show))):
        print("\nYou need to tell me what you want...\n")

    if (args['--lut'] is not None):
        change_lut(args['--lut'], verbose=1)

    if (show):
        shortcut_lookup("", show=show)

    if (q_names is not None):
        q_names = q_names.split(",")

        print("\nGiven the name, finding the index:\n")
        for q in q_names:
            ind = lut_lookup(q)
            print("{:>15s} --> {}".format(q, ind))
        print()

    if (inds is not None):
        inds = [int(a) for a in inds.split(",")]

        print("\nGiven the index, finding the name:\n")
        for ind in inds:
            q = lut_lookup(ind)
            print("{:5d} --> {}".format(ind, q))
        print()

        if (display_tex):

            import subprocess
            try:
                s = subprocess.check_output(["latex", "--version"])
                latex_success = True
            except:
                print("\n---ERROR: latex does not seem to be installed, exiting...\n")
                latex_success = False

            if (latex_success):
                import matplotlib
                try:
                    matplotlib.rc('text', usetex=True)
                    matplotlib.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
                except:
                    print("\n---ERROR: failed to \"\usepackage{amsmath}\", some indices will not display properly\n")
                import matplotlib.pyplot as plt
                import numpy as np
                plt.clf()

                n_inds = len(inds)

                lim = 8
                ncols = n_inds // lim
                if (n_inds%lim != 0):
                    ncols += 1

                x = np.repeat((np.arange(ncols))/float(ncols+1), lim)
                y = (np.arange(lim)+1)/float(lim  +1)

                for i,ind in enumerate(inds):
                    ix = i
                    iy = i % lim
                    q = tex_name(ind)
                    string = r"{} = {}".format(ind, q)
                    plt.text(x[ix], 1.0 - y[iy], string, fontsize=16)
                plt.axis('off')
                plt.show()

