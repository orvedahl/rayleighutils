"""
Module for setting up an empty class
"""
from __future__ import print_function

class Empty():
    """
    Empty Structure. It can store anything by doing:
       ref = Empty(density=density, radius=radius)
     or
       ref = Empty(lut=lut)
    In the first case, ref has 2 attributes: density,
    and radius. The second case will only have lut.
    These are accessed in the usual way:
       density = ref.density
       radius  = ref.radius
     or
       lut = ref.lut
    """
    def __init__(self, **kwargs):
        # store each entry of kwargs into the class
        for item, value in kwargs.items():
            setattr(self, item, value)

