"""
Module to convert between different versions of the lut

Usage:
    convert_lut.py [options] <quantity_codes>...

    <quantity_codes> is a space or comma separated list of quantity codes to convert

    Examples:

        convert_lut.py --from-lut=old_lut 1 2  402     # only spaces, single/multiple spaces
        convert_lut.py --from-lut=old_lut 1,2,402      # only commas, no spaces
        convert_lut.py --from-lut=old_lut 1, 2,  402   # both commas & spaces
        convert_lut.py --from-lut=old_lut 1,2,402,     # passed trailing comma by accident

Options:
    --to-lut=<l>       Convert to this lut.py file [default: lut]
    --from-lut=<l>     Read contents from this lut.py file
    --verbose          Print verbose information [default: False]
    --all              Print information for every quantity in the old lut [default: False]
    --namelist         Print new quantities on single line separated by commas [default: False]
"""
from __future__ import print_function
import env
import os
from look_up_quantity import lut_lookup, change_lut
from collections import OrderedDict

def convert_lut(values, old_lut, new_lut, verbose, namelist, return_values=False, **kwargs):
    """
    values  --- list of quantity codes to convert
    old_lut --- specify the old lut to use
    new_lut --- what lut to use when doing the conversion
    """

    # these hold (quantity_code, quantity_name) pairs
    new_values = OrderedDict()

    # ensure we are using the proper old lut
    if (old_lut is not None):
        change_lut(old_lut, verbose=verbose)

    # read/store values using old lut
    for qcode in values:
        quantity = lut_lookup(qcode)
        if (quantity is not None): # only store if it is found in old lut
            new_values[qcode] = [quantity]

    # change the lut that look_up_quantity.py uses
    if (new_lut is not None):
        change_lut(new_lut, verbose=verbose)

    # perform lookup
    for qcode in new_values.keys():
        quantity = new_values[qcode][0]
        new_code = lut_lookup(quantity)
        if (new_code is None):
            if ("induction" in quantity):
                quantity = quantity.replace("induction", "induct")
                new_code = lut_lookup(quantity)
        new_values[qcode].append(new_code)

    print("\nResults:")
    print("-----------------------------------------")
    print("Old lut = {}".format(old_lut))
    print("New lut = {}".format(new_lut))
    print("-----------------------------------------")

    if (not return_values):
        print("\nOld Code   Name\t\t\t\tNew Code")
        print("-"*50)
        for code in new_values.keys():
            old_code = str(code)
            name = new_values[code][0]
            new_code = new_values[code][1]
            if (new_code is None):
                new_code = "variable name changed"
            print("{:<8s}   {:<25s}\t{}".format(old_code, name, new_code))
        print()

        if (namelist):
            print("-"*50)
            names = []; new_codes = []
            for code in new_values.keys():
                names.append(new_values[code][0])
                new_codes.append(new_values[code][1])
            codes = [str(n) for n in new_codes]
            print("Convenient Namelist Form:")
            print("\n\tQuantity names  = {}".format(", ".join(names)))
            print("\n\tQuantity values = {}".format(",".join(codes)))
            print()

    if (return_values):
        return new_values
    else:
        return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    old_lut = args['--from-lut']
    new_lut = args['--to-lut']
    verbose = args['--verbose']
    print_all = args['--all']
    namelist = args['--namelist']

    if (not print_all):
        quants = args['<quantity_codes>']

        if (len(quants) == 1):
            if ("," in quants[0]):
                if (quants[0][-1] == ","):           # if there is a comma at the very end
                    quants[0] = quants[0][:-1]       #    remove it
                quants = quants[0].split(",")
        else:
            quants = ",".join(quants)                # separate quantities by commas in
            quants = quants.replace(",,",",")        # remove possible double commas
            quants = quants.strip().replace(" ", "") # strip all white space
            if (quants[-1] == ","):                  # if there is a comma at the very end
                quants = quants[:-1]                 #    remove it

            quants = quants.split(",")               # make it a list of numbers

        values = [int(q) for q in quants]
    else:
        import numpy as np
        nmax = 5001
        values = [int(q) for q in np.arange(nmax)]

    convert_lut(values, old_lut, new_lut, verbose, namelist)

