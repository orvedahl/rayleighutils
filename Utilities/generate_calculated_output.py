"""
Run a few python routines to collect various analysis outputs

Usage:
    generate_calculated_output.py [options]

Options:
    --output=<o>        Save image as <o> [default: nondim_plots]
    --data-dir=<d>      Specifiy location of data [default: ./]
    --kappa=<k>         Give thermal diffusivity in cm**2/sec [default: 2e12]
    --Pr=<p>            Prandtl number, nu/kappa [default: 1]
    --cp=<c>            Specific heat at const pressure [default: 3.5e8]
    --Lnorm=<L>         Luminosity in ergs/sec [default: 3.846e33]
    --last-energy=<l>   Specify last <l> iterations for energy calculations [default: 28]
    --last-flux=<l>     Specify last <l> iterations for flux calculations [default: 10]
    --alt               Use alternate look up table [default: False]
"""

from __future__ import print_function

def main(output, last_energy, last_flux, kappa, data_dir, Pr, cp, Lnorm, alt):

    save = False; savefile = ''
    start = 0
    end = 1000000000

    #############################
    # get the energies
    #############################
    args = {'last_number_files':last_energy, 'kappa':kappa,
            'data_dir':data_dir, 'no_plots':True, 'start_file':start,
            'end_file':end, 'saveplot':save, 'savefile':savefile}
    # run the code:
    if (not alt):
        import plot_energy_trace
        plot_energy_trace.main(**args)
    else:
        print("\nWARNING: plot_energy_trace is not alt-compatable\n")

    print("-----------------------------------------")

    #############################
    # get the Rayleigh numbers
    #############################
    args = {'kappa':kappa, 'Pr':Pr, 'c_p':cp, 'save':save, 'output':savefile,
            'data_dir':data_dir, 'Luminosity':Lnorm}
    # run the code:
    import calculate_Ra
    calculate_Ra.main(**args)

    print("-----------------------------------------")

    #############################
    # get the w_BL & f_conv
    #############################
    args = {'last_number_files':last_flux, 'kappa':kappa, 'data_dir':data_dir,
            'no_plots':True, 'Lnorm':Lnorm, 'start_file':start, 'end_file':end,
            'saveplot':save, 'savefile':savefile, 'Pr':Pr, 'paper_data':alt}
    # run the code:
    import plot_energy_flux
    plot_energy_flux.main(**args)

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    output = args['--output']
    last_energy = int(args['--last-energy'])
    last_flux = int(args['--last-flux'])
    kappa = float(args['--kappa'])
    data_dir = args['--data-dir']
    Pr = float(args['--Pr'])
    cp = float(args['--cp'])
    Lnorm = float(args['--Lnorm'])
    alt = args['--alt']

    main(output, last_energy, last_flux, kappa, data_dir, Pr, cp, Lnorm, alt)

