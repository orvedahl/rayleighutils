"""
Extract information from a Rayleigh output file, i.e., the information
that is printed to the screen during the run and stored to a file.

Usage:
    parse_output_file.py [options] <output_file>

Options:

    --debug        Run in debug mode [default: False]
    --tstart=<t>   Start time at <t> [default: 0]
"""
from __future__ import print_function
import numpy as np

def parse(filename, **args):
    """parse Rayleigh output file"""

    # parse keyword args
    debug = args.pop('debug', False)
    tstart = float(args.pop('tstart', 0))

    # things to parse
    info = {"NCPU":None, "NPROW":None, "NRCOL":None,
            "N_R":None, "N_THETA":None, "ELL_MAX":None, "R_MIN":None, "R_MAX":None,
            "Num_Cheb_Domains":None,
            "Reference":None,
            "Ra":None, "Ek":None, "Pr":None, "Pm":None}

    itrs = []; dt = []
    found_ref = False
    restart = False

    mf = open(filename, 'r')
    for line in mf:

        # MPI data
        if ("NCPU" in line):
            info["NCPU"] = (line.split(":")[1]).strip()
        if ("NPROW" in line):
            info["NPROW"] = (line.split(":")[1]).strip()
        if ("NPCOL" in line):
            info["NPCOL"] = (line.split(":")[1]).strip()

        # Grid data
        if ("N_R" in line):
            info["N_R"] = (line.split(":")[1]).strip()
        if ("N_THETA" in line):
            info["N_THETA"] = (line.split(":")[1]).strip()
        if ("Ell_MAX" in line):
            info["ELL_MAX"] = (line.split(":")[1]).strip()
        if ("R_MIN" in line):
            info["R_MIN"] = (line.split(":")[1]).strip()
        if ("R_MAX" in line):
            info["R_MAX"] = (line.split(":")[1]).strip()
        if ("Chebyshev Domains" in line):
            info["Num_Cheb_Domains"] = (line.split(":")[1]).strip()

        # Reference state data
        if ("Reference type" in line):
            info["Reference"] = (line.split(":")[1]).strip()
            found_ref = True
        if (found_ref and "Boussinesq" in info["Reference"]):
            if ("Rayleigh Number" in line):
                info["Ra"] = (line.split(":")[1]).strip()
            if ("Ekman Number" in line):
                info["Ek"] = (line.split(":")[1]).strip()
            if ("Prandtl Number" in line and "Magnetic" not in line):
                info["Pr"] = (line.split(":")[1]).strip()
            if ("Magnetic Prandtl Number" in line):
                info["Pm"] = (line.split(":")[1]).strip()

        # restart time
        if ("Checkpoint time" in line):
            _l = line.split(":")
            _t = float(_l[1].strip())
            tstart = _t
            restart = True

        # time data
        if ("On iteration" in line):
            _l  = line.split(":")
            _i  = float(_l[1].split()[0])
            _dt = float(_l[2].strip())

            itrs.append(_i); dt.append(_dt)

    mf.close()

    itrs = np.array(itrs); dt = np.array(dt)
    time = np.zeros_like(dt)

    time[0] = tstart
    for i in range(len(time)-1):
        time[i+1] = time[i] + dt[i]

    # report
    print("\nParsed file = {}".format(filename))

    print("\nMPI ---")
    print("\tNCPU  : {}".format(info["NCPU"]))
    print("\tNPROW : {}".format(info["NPROW"]))
    print("\tNPCOL : {}".format(info["NPCOL"]))

    print("\nGrid ---")
    print("\tN_R              : {}".format(info["N_R"]))
    print("\tN_THETA          : {}".format(info["N_THETA"]))
    print("\tEll_MAX          : {}".format(info["ELL_MAX"]))
    print("\tR_MIN            : {}".format(info["R_MIN"]))
    print("\tR_MAX            : {}".format(info["R_MAX"]))
    print("\tNum Cheb Domains : {}".format(info["Num_Cheb_Domains"]))

    print("\nReference State ---")
    print("\tReference type   : {}".format(info["Reference"]))
    if (found_ref and "Boussinesq" in info["Reference"]):
        print("\tRayleigh number  : {}".format(info["Ra"]))
        print("\tEkman number     : {}".format(info["Ek"]))
        print("\tPrandtl number   : {}".format(info["Pr"]))
        print("\tMagnetic Prandtl : {}".format(info["Pm"]))

    print("\nTiming ---")
    if (restart):
        print("\n\tRESTART, used Checkpoint time for time[0]\n")
    print("\tfirst iteration  : {}".format(itrs[ 0]))
    print("\tlast iteration   : {}".format(itrs[-1]))
    print("\ttotal iterations : {}".format(itrs[-1]-itrs[0]+1))
    print("\tfirst time : {}".format(time[ 0]))
    print("\tlast time  : {}".format(time[-1]))
    print("\taverage dt : {}".format(np.mean(dt)))

    print()

if __name__ == "__main__":
    from docopt import docopt
    args = docopt(__doc__)

     # args has "--debug:False"
     # convert those to "debug:False"
    kwargs = {}
    for k in args.keys():
        key = k.replace("--", "")
        kwargs[key] = args[k]

    parse(args['<output_file>'], **kwargs)

