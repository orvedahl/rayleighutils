"""
Figure out what time instance is stored in each file

Usage:
    plot_entropy.py [options]

Options:
    --start=<s>     Starting file [default: 0]
    --end=<e>       Ending file [default: 100000000]
    --last=<l>      Use last <l> number of files (so files[-l:] is used) [default: 0]
    --data-dir=<d>  Specify location of data [default: ./]
    --Pm=<p>        Use to convert to magnetic time [default: 1]
    --write=<w>     Write results to file <w>

"""

from __future__ import print_function
import env
import os
from loop_progress import progress_bar
from time_utils import Timer
import numpy as np
from ReadRayleigh.diagnostic_reading import build_file_list, ShellSpectra, ShellSlice
from ReadRayleigh.diagnostic_reading import AzAverage, GlobalAverage, ShellAverage
from ReadRayleigh.diagnostic_reading import Meridional_Slice, Equatorial_Slice, Point_Probe
from collections import OrderedDict

def main(start_file, end_file, last_number_files, data_dir, Prm, write_results, **kwargs):

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    files = build_file_list(start_file, end_file, path=data_dir)

    # map directory name to the routine that opens those files
    file_opener = {}
    file_opener['Shell_Spectra'] = [ShellSpectra, "(0:lmax,0:mmax,0:nr-1,0:nq-1,0:nrec-1)"]
    file_opener['Shell_Slices'] = [ShellSlice, "(0:nphi-1,0:nth-1,0:nr-1,0:nq-1,0:nrec-1)"]
    file_opener['Shell_Avgs'] = [ShellAverage, "(0:nr-1,0:3,0:nq-1,0:nrec-1)"]
    file_opener['AZ_Avgs'] = [AzAverage, "(0:nth-1,0:nr-1,0:nq-1,0:nrec-1)"]
    file_opener['G_Avgs'] = [GlobalAverage, "(0:nrec-1,0:nq-1)"]
    file_opener['Meridional_Slice'] = [Meridional_Slice, "(0:nphi-1,0:nth-1,0:nr-1,0:nq-1,0:nrec-1)"]
    file_opener['Equatorial_Slice'] = [Equatorial_Slice, "(0:nphi-1,0:nr-1,0:nq-1,0:nrec-1)"]
    file_opener['Point_Probe'] = [Point_Probe, "(0:nphi-1,0:nth-1,0:nr-1,0:nq-1,0:nrec-1)"]

    file_type = None
    for key in file_opener.keys():
        if (key in data_dir):
            file_type = key
            break
    if (file_type is None):
        print("\nERROR: cannot determine file_type from directory name\n")
        return

    print("\nResults:")
    print("-----------------------------------------")
    print("Data directory = {}".format(data_dir))
    print("File opener = {}".format(file_type))
    print("-----------------------------------------")
    nfiles = len(files[-last_number_files:])
    print("\nReading {} files...".format(nfiles))
    if (write_results is None):
        for i,f in enumerate(files[-last_number_files:]):
            a = file_opener[file_type][0](filename=f, path='')
            a.time *= Prm
            nrec = len(a.time)
            ts = a.time[0]
            te = a.time[-1]
            print("\n{}\n\tnrec  = {}\n\tstart = {} ({:.2e})\n\tend   = {} ({:.2e})".format(
                  f, nrec, ts, ts, te, te))
            print("\n\t{} = {}".format(file_opener[file_type][1], np.shape(a.vals)))
        print()
    else:
        mf = open(write_results, 'w')
        mf.write("\nList file contents\n\n")
        mf.write("Data directory = {}\n".format(os.path.abspath(data_dir)))
        mf.write("File opener = {}\n".format(file_type))
        mf.write("Magnetic Prandtl = {}\n".format(Prm))
        mf.write("\nIteration\tNrec\tT start\t\tT_end\n")
        mf.write("-----------------------------------------------------------\n")
        print()
        T = Timer(); T.start()
        nf = len(files[-last_number_files:])
        progress_bar(0, nf, decimals=2)
        for i,f in enumerate(files[-last_number_files:]):
            progress_bar(i+1, nf, decimals=2)
            a = file_opener[file_type][0](filename=f, path='')
            a.time *= Prm
            nrec = len(a.time)
            ts = a.time[0]
            te = a.time[-1]
            itr = os.path.basename(f)
            mf.write("{}\t{}\t{}\t{}\n".format(itr,nrec, ts, te))
            #print("\t{} = {}".format(file_opener[file_type][1], np.shape(a.vals)))
        mf.close()
        T.stop(); dt = T.print_interval()
        print("\nComplete...")
        print("\nResults stored to {}\n".format(write_results))

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    data_dir = args['--data-dir']
    Prm = float(args['--Pm'])
    write = args['--write']

    main(start_file, end_file, last_number_files, data_dir, Prm, write)

