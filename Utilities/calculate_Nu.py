"""
Calculate the Nusselt number from Shell_Avg files

    Nu - 1 = vol_avg(convective_flux) / vol_avg(conductive_flux)

Usage:
    calculate_Nu.py [options]

Options:
    --start=<s>      Starting file [default: 0]
    --end=<e>        Ending file [default: 100000000]
    --last=<l>       Use last <l> number of files (so files[-l:] is used) [default: 0]
    --save           Save the image [default: False]
    --aspect=<a>     Set aspect ratio [default: 0.4]
    --output=<o>     Save image as <o> [default: calculate_Nu.png]
    --data-dir=<d>   Specify location of data [default: ./]
"""

from __future__ import print_function
import env
from ReadRayleigh.diagnostic_reading import ReferenceState, build_file_list, ShellAverage
from NumericalToolsLocal import integrate
from NumericalToolsLocal import differentiate
volume_avg = integrate.volume_avg
derivative = differentiate.derivative
from look_up_quantity import shortcut_lookup
import numpy as np

def main(start_file, end_file, last_number_files, save, output, data_dir,
         return_Nu=False, verbose=True, return_radial_funcs=False,
         data=None, ref_state=None, lut=None, zero_tol=1e-30, aspect_ratio=0.4,
         use_wrong_method=False, **kwargs):

    # integration method
    method = 'cheb-end'

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'

    # read Reference file
    if (ref_state is None):
        ref_state = ReferenceState(filename="reference", path=data_dir)

    radius = ref_state.radius
    router = radius[0]   # radius is backwards...
    rinner = radius[-1]
    H = router - rinner

    # read Shell_Avgs file
    if (data is None):
        files = build_file_list(start_file, end_file, path=data_dir+'Shell_Avgs')
        icount = 0.0
        for i,f in enumerate(files[-last_number_files:]):
            a = ShellAverage(f,path='')
            if (i == 0):
                data = np.zeros((a.nr,4,a.nq),dtype='float64')
                t_start = a.time[0]
            for j in range(a.niter):
                data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
                icount = icount+1.0
            t_end = a.time[-1]
        data = data/icount  # average is now stored in data
        delta_time = (t_end - t_start)

        if (verbose):
            print("\nFirst file: {}".format(files[-last_number_files:][0]))
            print("Last  file: {}".format(files[-last_number_files:][-1]))
            print("\nTotal number of files: {}".format(len(files[-last_number_files:])))
            print("\nFirst time: {} secs".format(t_start))
            print("Last  time: {} secs".format(t_end))
            print("\nAveraged over {} secs".format(delta_time))

    else:
        # ensure data array has correct dimensions and the lookup table was passed
        if (len(np.shape(data)) != 3):
            raise ValueError("\nwhen passing data array, it needs to be shape (nr,4,nq)\n")

        if (lut is None):
            raise ValueError("\nneed to pass lut in keywords, for example: lut=a.lut\n")

        # empty class to hold lut
        class empty_lut():
            def __init__(self, lut):
                self.lut = lut
        a = empty_lut(lut)

    if (use_wrong_method):
        print("\n\tWARNING: using 'wrong' method to get Nusselt number")
        print("\t\tcalculating Nu = F_conv / F_cond\n")
        # extract the appropriate fluxes
        i_cond_flux = a.lut[shortcut_lookup('cond_flux')] # Conductive energy flux
        i_conv_flux = a.lut[shortcut_lookup('conv_flux')] # Convective energy flux

        cond_flux = data[:,0,i_cond_flux]
        conv_flux = data[:,0,i_conv_flux]

        # volume average
        vol_cond_flux = volume_avg(cond_flux, radius, method=method, data_reversed=True)
        vol_conv_flux = volume_avg(conv_flux, radius, method=method, data_reversed=True)

        ind = np.where(abs(cond_flux) <= zero_tol)
        cond_flux[ind] = zero_tol

        # nusselt number as a function of depth
        Nusselt_of_r = conv_flux / cond_flux + 1

        if (abs(vol_cond_flux) <= zero_tol):
            vol_cond_flux = zero_tol

        # bulk Nusselt number = vol_avg(convective_flux) / vol_avg(conductive_flux) + 1
        Nusselt = vol_conv_flux / vol_cond_flux + 1

        if (return_Nu):
            if (return_radial_funcs):
                f_of_r = {}
                f_of_r['Nu'] = Nusselt_of_r
                f_of_r['F_cond'] = cond_flux
                f_of_r['F_conv'] = conv_flux

                f_of_r['keys'] = f_of_r.keys()

                return Nusselt, f_of_r
            else:
                return Nusselt

    # start with
    #       Nu = r_o/r_i * Q * D / (rho * c_p * kappa * delta_T)
    #        Q = -k * grad(T) = -rho * c_p * kappa * grad(T)
    # so
    #       Nu = 1/chi * (-rho * c_p * dT/dr) * D / (rho * c_p * kappa * delta_T)
    #          = -1/chi * dT/dr * D / delta_T
    # but
    #        D = nondimensionalization for distance
    #  delta_T = nondimensionalization for temperature
    # so
    #       Nu = -1/chi * dT_tilde/dr_tilde = -1/chi * (dT_0/dr + dT_1/dr)
    # where
    #      T_0 = some kind of background, in our case, the "reference" state
    #      T_1 = what is solved for in the temp. eqn and output in diagnostic files
    # enforcing/assuming
    #        0 = Lap(T_0)
    # leads to
    #     T_0(r) = r_o*r_i/(r_o - r_i)/r + r_i/(r_i - r_o)
    # and
    #   dT_0/dr = -chi/D * (r_o / r)**2, with dT_0/dr(r_o) = -chi/D
    # so we can write
    #       Nu = -1/chi * (-chi/D + dT_1/dr)
    # but
    #        D = 1, pretty much always
    # so
    #       Nu = 1 - 1/chi * dT_1/dr

    # get mean temperature and its gradient
    i_temp = a.lut[shortcut_lookup('s')]             # this is T_1
    T = data[:,0,i_temp]
    dTdr = derivative(T, radius, data_reversed=True, method=method, zeros=True) # gets dT_1/dr

    # evaluate temperature gradient at top of domain
    Nusselt = 1. - dTdr[0]/aspect_ratio
    Nusselt_of_r = 1. - dTdr/aspect_ratio

    if (return_Nu):
        if (return_radial_funcs):
            f_of_r = {}
            f_of_r['Nu'] = Nusselt_of_r

            f_of_r['keys'] = f_of_r.keys()

            return Nusselt, f_of_r
        else:
            return Nusselt

    print("\nInputs:")
    print("\n\tData Dir = {}".format(data_dir))
    print("\nNusselt Number = {}\n".format(Nusselt))

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(int(args['--start']), int(args['--end']), int(args['--last']),
             args['--save'], args['--output'], args['--data-dir'],
             aspect_ratio=float(args['--aspect']))

