"""
Find maximum index of output data.

   radius = [....]
   rad_inds = [....]

   find i such that radius[i] is maximum possible, it
   may not be i=0 or i=-1
"""
from __future__ import print_function
import numpy as np

def find_index(data, index='max'):
    """find max index of Rayleigh output data"""

    index = index.lower()
    num = len(data)
    iarg = np.argsort(data)

    if (index == 'max'):
        ind = iarg[-1]
    elif (index == 'min'):
        ind = iarg[0]
    elif (index == 'second-smallest'):
        ind = iarg[1]
    elif (index == 'second-largest'):
        ind = iarg[-2]
    else:
        raise ValueError("unrecognized index option, index = {}".format(index))

    return ind

