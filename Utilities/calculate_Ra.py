"""

    Plot the internal deposition of heat, Q(r),
    using the ReferenceState structure

    The Attributes of the ReferenceState Structure are:
    ----------------------------------
    self.n_r         : number of radial points
    self.radius      : radial coordinates
    self.density     : density
    self.dlnrho      : logarithmic derivative of density
    self.d2lnrho     : d_by_dr of dlnrho
    self.pressure    : pressure
    self.temperature : temperature
    self.dlnt        : logarithmic derivative of temperature
    self.dsdr        : entropy gradient (radial)
    self.entropy     : entropy
    self.gravity     : gravity
    --------------------------------------

Usage:
    calculate_Ra.py [options]

Options:
    --kappa=<k>      Thermal diffusivity in cm**2/sec [default: 2e12]
    --Pr=<p>         Prandtl number, nu/kappa [default: 1]
    --cp=<c>         Specific heat at constant pressure [default: 3.5e8]
    --Lnorm=<L>      Specify luminosity in ergs/sec [default: 3.846e33]
    --save           Save the image [default: False]
    --output=<o>     Save image as <o> [default: calculate_Ra.png]
    --data-dir=<d>   Specify location of data [default: ./]
"""

from __future__ import print_function
from python.diagnostic_reading import ReferenceState
import numpy as np
import env
from NumericalToolsLocal import integrate
my_integrator = integrate.integrate
volume_avg = integrate.volume_avg

def main(save, output, Luminosity, data_dir, kappa, Pr, c_p, return_Ra=False, **kwargs):

    # integration method
    method = 'cheb-end'

    if (data_dir[-1] != '/'):
        data_dir = data_dir + '/'
    ref = ReferenceState(filename="reference", path=data_dir)
    radius = ref.radius
    pressure = ref.pressure
    router = radius[0]   # radius is backwards...
    rinner = radius[-1]
    Pouter = pressure[0]
    Pinner = pressure[-1]
    H = router - rinner

    # equation 10 & 11 of Featherstone & Hindman 2016 ApJ 818
    # Q = alpha*(P(r) - P(router))
    # L = 4*pi*alpha* integral(  (P(r) - P(router))*r**2  )
    integrand = radius*radius*(pressure - Pouter)
    integrated = my_integrator(integrand, radius, method=method, data_reversed=True)
    alpha = Luminosity / 4. / np.pi / integrated
    Q = alpha*(pressure - Pouter)

    # equation 12 of Featherstone & Hindman 2016 ApJ 818,
    # note this should be an Indefinite integral
    Flux_Q = my_integrator(Q*radius*radius, radius, method=method, indefinite=True, data_reversed=True)/radius/radius

    # volume averages to calculate the Rayleigh number
    F_tilde    = volume_avg(Flux_Q, radius, method=method, data_reversed=True)
    grav_tilde = volume_avg(ref.gravity, radius, method=method, data_reversed=True)
    T_tilde    = volume_avg(ref.temperature, radius, method=method, data_reversed=True)
    rho_tilde  = volume_avg(ref.density, radius, method=method, data_reversed=True)

    nu = kappa*Pr
    Ra_F  = grav_tilde*F_tilde*H**4/c_p/rho_tilde/T_tilde/nu/kappa**2

    # point wise, evaluated at the top of the domain
    Ra_BL = ref.gravity[0]*Flux_Q[0]*H**4/c_p/ref.density[0]/ref.temperature[0]/nu/kappa**2

    if (return_Ra):
        return Ra_F, Ra_BL

    print("\nInputs:")
    print("\tkappa   = {}".format(kappa))
    print("\tPrandtl = {}".format(Pr))
    print("\tL_star  = {}".format(Luminosity))
    print("\tC_p     = {}".format(c_p))
    print("\nRayleigh Number (standard)   = {:.4e}".format(Ra_F))
    print("\nRayleigh Number (bdry layer) = {:.4e}\n".format(Ra_BL))

    if (False):
        # sanity check
        numerical = my_integrator(radius**2, radius, method=method, data_reversed=True)
        true = (router**3 - rinner**3)/3.
        print("\nsanity check: integrate r**2")
        print("\n\tnumerical : {}".format(numerical))
        print("\ttrue      : {}".format(true))
        print("\n\tabs error : {}".format(true-numerical))
        print("\trel error : {}".format((true-numerical)/true))
        print()

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    main(args['--save'], args['--output'], float(args['--Lnorm']),
         args['--data-dir'], float(args['--kappa']), float(args['--Pr']), float(args['--cp']))

