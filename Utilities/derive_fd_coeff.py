"""
Derive Finite Difference Equations Symbolically

R. Orvedahl (10-10-2016)

Usage:
  derive_fd_coeff.py [options]

Options:
  --help, -h         Display help message
  --debug, -d        Debugging flag [default: False]
  --order=<o>        What order difference equation to do [default: 2]
  
"""

import sys
import numpy as np
import sympy as sp
from sympy import Symbol, simplify, collect
from collections import OrderedDict

def derive(debug, order):
    """driver program for deriving equations"""

    dx30 = Symbol('dx30') # x(i+3) - x(i)
    dx20 = Symbol('dx20') # x(i+2) - x(i)
    dx10 = Symbol('dx10') # x(i+1) - x(i)
    dx01 = Symbol('dx01') # x(i-1) - x(i)
    dx02 = Symbol('dx02') # x(i-2) - x(i)
    dx03 = Symbol('dx03') # x(i-3) - x(i)
    fp3 = Symbol('fp3')   # f(i+3)
    fp2 = Symbol('fp2')   # f(i+2)
    fp1 = Symbol('fp1')   # f(i+1)
    f   = Symbol('f')     # f(i)
    fm1 = Symbol('fm1')   # f(i-1)
    fm2 = Symbol('fm2')   # f(i-2)
    fm3 = Symbol('fm3')   # f(i-3)

    if (order == 2):
        row1 = [dx10, 0.5*dx10**2]
        row2 = [dx01, 0.5*dx01**2]
        rhs  = [fp1-f, fm1-f]

        A = sp.Matrix((row1, row2))
        det = A.det()

        A[:,0] = rhs
        num = A.det()

    elif (order == 4):
        row1 = [dx02, dx02**2, dx02**3, dx02**4]
        row2 = [dx01, dx01**2, dx01**3, dx01**4]
        row3 = [dx10, dx10**2, dx10**3, dx10**4]
        row4 = [dx20, dx20**2, dx20**3, dx20**4]
        rhs  = [fm2-f, fm1-f, fp1-f, fp2-f]

        A = sp.Matrix((row1, row2, row3, row4))
        det = A.det()

        A[:,0] = rhs
        num = A.det()

    elif (order == 6):
        row1 = [dx03, dx03**2, dx03**3, dx03**4, dx03**5, dx03**6]
        row2 = [dx02, dx02**2, dx02**3, dx02**4, dx02**5, dx02**6]
        row3 = [dx01, dx01**2, dx01**3, dx01**4, dx01**5, dx01**6]
        row4 = [dx10, dx10**2, dx10**3, dx10**4, dx10**5, dx10**6]
        row5 = [dx20, dx20**2, dx20**3, dx20**4, dx20**5, dx20**6]
        row6 = [dx30, dx30**2, dx30**3, dx30**4, dx30**5, dx30**6]
        rhs  = [fm3-f, fm2-f, fm1-f, fp1-f, fp2-f, fp3-f]

        print('getting denominator...')
        A = sp.Matrix((row1, row2, row3, row4, row5, row6))
        det = A.det()

        print('getting numerator...')
        A[:,0] = rhs
        num = A.det()

    print('simplifying...')
    #result = simplify(num/det)
    #num = simplify(num)
    det = simplify(det)

    print("\nOrder = {}\n".format(order))
    print("Denominator:")
    print(det)

    coeff = collect(num, fm2, evaluate=False)
    print("\nfm2:")
    #print(coeff[fm2].subs([(dx01, -1.),(dx10, 1.), (dx20, 2.), (dx02, -2.)]))
    print(coeff[fm2])

    coeff = collect(num, fm1, evaluate=False)
    print("\nfm1:")
    #print(coeff[fm1].subs([(dx01, -1.),(dx10, 1.), (dx20, 2.), (dx02, -2.)]))
    print(coeff[fm1])

    coeff = collect(num, f, evaluate=False)
    print("\nf:")
    #print(coeff[f].subs([(dx01, -1.),(dx10, 1.), (dx20, 2.), (dx02, -2.)]))
    print(coeff[f])

    coeff = collect(num, fp1, evaluate=False)
    print("\nfp1:")
    #print(coeff[fp1].subs([(dx01, -1.),(dx10, 1.), (dx20, 2.), (dx02, -2.)]))
    print(coeff[fp1])

    coeff = collect(num, fp2, evaluate=False)
    print("\nfp2:")
    #print(coeff[fp2].subs([(dx01, -1.),(dx10, 1.), (dx20, 2.), (dx02, -2.)]))
    print(coeff[fp2])

if __name__ == "__main__":
    import docopt
    args = docopt.docopt(__doc__)

    debug = args['--debug']
    order  = int(args['--order'])

    derive(debug, order)

