"""
List files in a Rayleigh data directory

Usage:
    list_files.py [options]

Options:
    --data-dir=<d>       Specify location of data [default: ./]
    --start=<s>          Starting file [default: 0]
    --end=<e>            Ending file [default: 100000000]
    --last=<l>           Use last <l> number of files (so files[-l:] is used) [default: 0]
    --exclude-last=<l>   Do not include the last <l> files, so files[:-l] [default: 0]
    --sections=<s>       Print the files that give <s> collections of files [default: 1]

"""

from __future__ import print_function
import env
import numpy as np
from ReadRayleigh.diagnostic_reading import build_file_list

def main(data_dir, start_file, end_file, last_number_files, exclude_last, sections):

    if (exclude_last == 0):
        exclude_last = None
    else:
        exclude_last *= -1

    files = build_file_list(start_file, end_file, path=data_dir)

    total_found = len(files)
    files = files[-last_number_files:exclude_last]
    num_files = len(files)

    print("\nInputs:")
    print("\tdata directory  : {}".format(data_dir))
    print("\tstart iteration : {}".format(start_file))
    print("\tend iteration   : {}".format(end_file))
    print("\tlast number     : {}".format(last_number_files))
    print("\texclude last    : {}".format(exclude_last))

    print("\nResults:")
    print("\tfound {} total files".format(total_found))
    print("\tonly {} satisfy the given parameters".format(num_files))
    print("\tfirst file = {}".format(files[0]))
    print("\tlast file  = {}".format(files[-1]))

    if (sections > 1):
        print("\nSplitting file list into {}".format(sections))
        split_files = np.array_split(files, sections)
        for i in range(len(split_files)):
            print("\tsection {}".format(i+1))
            print("\t\tfirst file = {}".format(split_files[i][0]))
            print("\t\tlast  file = {}".format(split_files[i][-1]))
            print("\t\tnumber of files = {}".format(len(split_files[i])))
    print()

    return

if __name__ == "__main__":

    from docopt import docopt
    args = docopt(__doc__)

    data_dir = args['--data-dir']
    start_file = int(args['--start'])
    end_file   = int(args['--end'])
    last_number_files = int(args['--last'])
    exclude_last = int(args['--exclude-last'])
    sections = int(args['--sections'])

    main(data_dir, start_file, end_file, last_number_files, exclude_last, sections)

