
from __future__ import print_function
from time_utils import TimeAvg, CombineAverages, Average

import numpy as np

axis=0
method = 'int'
#method = 'mean'

def get_xN(a, b):
    x = np.arange(a,b)*1.0
    return x

x1 = get_xN(0,10)      # collection of data values
x2 = get_xN(1,7)
x3 = get_xN(1,17)
x4 = get_xN(22,37)
x5 = get_xN(17,49)

t1 = np.arange(len(x1))  # collection of corresponding times
t2 = np.arange(len(x2)) + t1[-1] + 1
t3 = np.arange(len(x3)) + t2[-1] + 1
t4 = np.arange(len(x4)) + t3[-1] + 1
t5 = np.arange(len(x5)) + t4[-1] + 1

class DataClass(): # class to mimic a data structure like AZ_Avgs
    def __init__(self, xi, ti):
        self.vals = xi
        self.time = ti
        self.niter = len(ti)

files = {1:DataClass(x1,t1), # list of "files" where key maps to the data class
         2:DataClass(x2,t2),
         3:DataClass(x3,t3),
         4:DataClass(x4,t4),
         5:DataClass(x5,t5)}

def file_opener(num, **kwargs): # return the proper data class by "opening" the file
    return files[num]

x_full = np.concatenate((x1, x2, x3, x4, x5)) # full data
t_full = np.concatenate((t1, t2, t3, t4, t5)) # full time

file_numbers = [1,2,3,4,5]
Tavg, Navg = TimeAvg(file_numbers, file_opener, axis=axis, data_attr="vals", method=method)

print("compute true average...")
true = Average(x_full, method=method, axis=axis, time=t_full)
true_l = len(t_full)

print("\nmethod = ", method)
print("TimeAvg", Tavg, Navg['nrec'])
print("TrueAvg", true, true_l)
print("\nsame length  = ",Navg['nrec']==true_l)
print("same average = ", Tavg == true)
print()

